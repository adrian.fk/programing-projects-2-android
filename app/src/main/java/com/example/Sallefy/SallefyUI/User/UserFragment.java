package com.example.Sallefy.SallefyUI.User;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacade;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacadeFactory;
import com.example.Sallefy.SallefyUI.SallefyAppActivity;
import com.example.Sallefy.SallefyUI.User.Controller.UserController;
import com.example.Sallefy.SallefyUI.User.Controller.UserMediator;
import com.example.Sallefy.SallefyUI.User.Model.DataUser;
import com.example.Sallefy.SallefyUI.User.View.UserView;
import com.example.Sallefy.SallefyUI.User.View.UserViewI;
import com.example.Sallefy.Utils.Constants;
import com.example.Sallefy.repositoryImpl.factoriesImpl.ImageApiRepositoryFactory;
import com.example.Sallefy.repositoryImpl.factoriesImpl.TrackApiRepositoryFactory;
import com.example.Sallefy.repositoryImpl.factoriesImpl.UserApiRepositoryFactory;

public class UserFragment extends Fragment implements UserMediator {
    private static final int REQUEST_IMG = 1;


    private UserViewI view;
    private UserController controller;
    private DataUser dataUser;


    public UserFragment() {}


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = new UserView(inflater, container);
        this.dataUser = new DataUser();

        Animation fadeInAnim = AnimationUtils.loadAnimation(this.view.getRootView().getContext(), R.anim.fade_in);
        this.view.getRootView().startAnimation(fadeInAnim);

        UserApiRepositoryFactory userApiRepositoryFactory = new UserApiRepositoryFactory();

        TrackApiRepositoryFactory trackApiRepositoryFactory = new TrackApiRepositoryFactory();

        ImageApiRepositoryFactory imageApiRepositoryFactory = new ImageApiRepositoryFactory();

        this.controller = new UserController(
                this,
                this.view,
                this.dataUser,
                userApiRepositoryFactory,
                trackApiRepositoryFactory,
                imageApiRepositoryFactory);

        Bundle specifications = getArguments();
        if (specifications != null) {
            this.dataUser.setUserLogin(
                    specifications.getString(
                            Constants.USER_SPECIFICATIONS.USER_NAME
                    )
            );
            this.controller.fetchUser(
                    this.dataUser.getUserLogin()
            );
        }

        return this.view.getRootView();
    }

    @Override
    public LifecycleOwner provideOwner() {
        return this;
    }

    @Override
    public void requestLogout() {
        SallefyAppActivity.logout();
    }

    @Override
    public void openSongOptions(String id) {
        Bundle b = new Bundle();
        b.putString(Constants.SONG_SPECIFICATIONS.SONG_ID, id);

        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_song_options, b);
    }

    @Override
    public void openPlaylistOptions(String id) {
        Bundle b = new Bundle();
        b.putString(Constants.PLAYLIST_SPECIFICATIONS.PLAYLIST_ID, id);

        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_playlist_options, b);
    }

    @Override
    public void openPlaylist(String id) {
        Bundle b = new Bundle();
        b.putString(Constants.PLAYLIST_SPECIFICATIONS.PLAYLIST_ID, id);

        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_to_playlist, b);
    }

    @Override
    public void openImgFile() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");

        startActivityForResult(intent, REQUEST_IMG);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case Activity.RESULT_CANCELED:
                this.view.displayNoImgAdded();
                break;

            case REQUEST_IMG:
                if(null != data) {
                    //Got image file reference from Android FileExplorer successfully
                    this.dataUser.setUserImgUri(data.getData());
                    this.view.displayUserImg(this.dataUser.getUserImgUri());
                    this.controller.postUserImg();
                }
                break;
        }
    }

    @Override
    public void playSong(long id) {
        MusicPlayerFacade musicPlayerFacade = MusicPlayerFacadeFactory.create();
        musicPlayerFacade.playSong(id);
    }

    @Override
    public void conclude() {
        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_home);
    }
}
