package com.example.Sallefy.SallefyUI.Playlist.View;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Playlist.Controller.ListenerInterfaces.AddPlaylistBtnClickListener;
import com.example.Sallefy.Utils.OnTextChangeListener;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddPlaylistView implements AddPlaylistViewI {
    private View rootView;
    private Bundle viewState;

    private CircleImageView playlistImage;
    private RelativeLayout imageContainer;

    private EditText playlistName;
    private EditText playlistDescription;

    private Button addPlaylistBtn;

    public AddPlaylistView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container) {
        this.rootView = inflater.inflate(R.layout.add_playlist_fragment, container, false);
        this.viewState = new Bundle();
        findViews();
        attachTextChangedListeners();
    }

    private void attachTextChangedListeners() {
        this.playlistName.addTextChangedListener(
                new OnTextChangeListener(
                        e -> onTextChanged()
                )
        );

        this.playlistDescription.addTextChangedListener(
                new OnTextChangeListener(
                        e -> onTextChanged()
                )
        );
    }

    private void onTextChanged() {
        if(getPlaylistName().length() != 0) {
            updateAddPlaylistBtnState(true);
        }
        else {
            updateAddPlaylistBtnState(false);
        }
        this.playlistName.setBackgroundResource(R.drawable.rounded_edittext);
    }

    private void findViews() {
        this.playlistName = getRootView().findViewById(R.id.add_playlist_name);
        this.playlistDescription = getRootView().findViewById(R.id.add_playlist_description);
        this.addPlaylistBtn = getRootView().findViewById(R.id.add_playlist_add_playlist_btn);
        this.imageContainer = getRootView().findViewById(R.id.add_playlist_change_image);
        this.playlistImage = getRootView().findViewById(R.id.add_playlist_image);
    }

    @Override
    public View getRootView() {
        return this.rootView;
    }

    @Override
    public Bundle getViewState() {
        return this.viewState;
    }

    @Override
    public void attachAddPlaylistBtnListener(AddPlaylistBtnClickListener listener) {
        this.addPlaylistBtn.setOnClickListener(v -> listener.clicked(getPlaylistName(), getPlaylistDescription()));
    }

    @Override
    public void updateAddPlaylistBtnState(boolean clickable) {
        if (clickable) {
            this.addPlaylistBtn.setBackgroundResource(R.drawable.rounded_button);
            this.addPlaylistBtn.setEnabled(true);
        }
        else {
            this.addPlaylistBtn.setBackgroundResource(R.drawable.rounded_button_untoggled);
            this.addPlaylistBtn.setEnabled(false);
        }
    }

    @Override
    public void toastMsg(String msg) {
        Toast.makeText(getRootView().getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void toastErrorMsg(String errMsg) {
        Toast.makeText(getRootView().getContext(), "ERROR: " + errMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getPlaylistName() {
        return this.playlistName.getText().toString();
    }

    @Override
    public String getPlaylistDescription() {
        return this.playlistDescription.getText().toString();
    }

    @Override
    public void displayPlaylistNameError() {
        this.playlistName.setBackgroundResource(R.drawable.rounded_edittext_red_border);
        Animation shake = AnimationUtils.loadAnimation(this.rootView.getContext(), R.anim.shake_edittext);
        this.playlistName.startAnimation(shake);
    }

    @Override
    public void displayPlaylistImg(Uri imgUri) {
        Glide.with(getRootView().getContext())
                .load(imgUri)
                .into(this.playlistImage);
    }

    @Override
    public void attachPlaylistImgListener(View.OnClickListener listener) {
        this.imageContainer.setOnClickListener(listener);
    }

    @Override
    public void displayNoImgAdded() {
        toastMsg("Was not able to add image successfully");
    }
}
