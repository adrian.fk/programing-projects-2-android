package com.example.Sallefy.SallefyUI.Home.Model;

import java.util.ArrayList;
import java.util.List;

public class PlaylistHomeContent {
    private List<PlaylistItem> items = new ArrayList<>();
    private int stackCounter;

    public PlaylistHomeContent(final int REQUIRED_ADD_CALLS) {
        this.stackCounter = REQUIRED_ADD_CALLS;
    }

    public PlaylistHomeContent(List<PlaylistItem> items) {

        this.items = items;
    }

    public PlaylistHomeContent() {}

    public void addItem(PlaylistItem item) {
        items.add(item);
    }

    public List<PlaylistItem> getItems() {
        return this.items;
    }

    public PlaylistItem createPlaylistItem(String id, String title, String uploader, String imgUrl) {
        return new PlaylistItem(
                id,
                title,
                uploader,
                imgUrl);
    }

    public boolean isStacked() {
        return this.stackCounter == 0;
    }

    public void registerSubStackFilled() {
        if(this.stackCounter > 0) {
            this.stackCounter--;
        }
    }

    public static class PlaylistItem {
        public final String id;
        public final String title;
        public final String uploader;
        public final String imgUrl;

        public PlaylistItem(String id, String title, String uploader, String imgUrl) {
            this.id = id;
            this.title = title;
            this.uploader = uploader;
            this.imgUrl = imgUrl;
        }
    }
}
