package com.example.Sallefy.SallefyUI.Home.Controller;

import com.example.Sallefy.Utils.FeaturedMediatorI;

public interface HomeMediator extends FeaturedMediatorI {

    void receiveUserCall(String userlogin);

    void receivePlaylistNavCall(String id);
}
