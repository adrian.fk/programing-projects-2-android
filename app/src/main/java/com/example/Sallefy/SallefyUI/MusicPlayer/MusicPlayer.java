package com.example.Sallefy.SallefyUI.MusicPlayer;


import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacade;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.MusicPlayerController;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.MusicPlayerMediator;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.TrackHandler;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.TrackHandlerFactory;
import com.example.Sallefy.SallefyUI.MusicPlayer.Model.Data;
import com.example.Sallefy.SallefyUI.MusicPlayer.Model.DataMusicPlayerFactory;
import com.example.Sallefy.SallefyUI.MusicPlayer.View.MusicPlayerView;
import com.example.Sallefy.SallefyUI.MusicPlayer.View.MusicPlayerViewI;
import com.example.Sallefy.repositories.factories.TrackRepositoryFactory;
import com.google.android.material.navigation.NavigationView;


public class MusicPlayer implements MusicPlayerFacade, MusicPlayerMediator {
    private static final String TAG = "AudioPlayerFacade";

    private LifecycleOwner owner;
    private NavController navController;
    private NavigationView navView;


    private MusicPlayerController controller;
    private MusicPlayerViewI view;
    private Data dataMusicPlayer;

    private TrackHandler trackHandler;


    public MusicPlayer() {
        this.dataMusicPlayer = DataMusicPlayerFactory.create();
        this.trackHandler = TrackHandlerFactory.create();
    }

    private void songPlayingChanged(String title, String uploader, String thumbnailUrl, String srcUrl) {
        this.controller.loadAudio(
                title,
                uploader,
                thumbnailUrl,
                srcUrl
        );
    }

    public void launch() {
        this.view.show();
    }

    @Override
    public void receiveProgress(int progress) {
        this.dataMusicPlayer.setProgress(progress);
        this.view.setProgressBarProgression(progress);
        this.view.setNextBtnClicked(false);
        this.view.setPrevBtnClicked(false);
    }

    @Override
    public void receiveDuration(int duration) {
        this.dataMusicPlayer.setDuration(duration);
        this.view.setProgressBarDuration(duration);
    }

    @Override
    public void receiveRepeatTrigger(boolean repeatTrigger) {
        this.dataMusicPlayer.setRepeat(repeatTrigger);
        this.view.setRepeatBtnState(repeatTrigger);
    }

    @Override
    public void receiveShuffleTrigger(boolean shuffleTrigger) {
        this.dataMusicPlayer.setShuffle(shuffleTrigger);
        this.view.setShuffleBtnState(shuffleTrigger);
    }

    public void receiveFullscreenTrigger() {
        NavOptions navOptions = new NavOptions.Builder()
                .setEnterAnim(R.anim.enter_from_bottom)
                .setExitAnim(R.anim.fade_out)
                .setPopEnterAnim(R.anim.fade_in)
                .setPopExitAnim(R.anim.exit_to_bottom)
                .build();

        getNavController().navigate(R.id.nav_fullscreen_player, null, navOptions);
    }

    @Override
    public void loadMusicPlayer(LifecycleOwner owner,
                                NavController navController,
                                NavigationView navView,
                                TrackRepositoryFactory trackRepositoryFactory) {
        this.owner = owner;
        this.navController = navController;
        this.navView = navView;
        this.view = new MusicPlayerView(navView.getRootView());
        this.controller = new MusicPlayerController(this.view, this, this.dataMusicPlayer);
        this.trackHandler.setup(owner, this.dataMusicPlayer, trackRepositoryFactory);

        this.dataMusicPlayer.currentlyPlaying.observe(
                owner,
                track -> songPlayingChanged(
                        track.getTitle(),
                        track.getUploader(),
                        track.getThumbnailUrl(),
                        track.getSrcUrl()
                )
        );
        this.dataMusicPlayer.playing.observe(owner, playing -> this.controller.processPlay(playing));
        this.dataMusicPlayer.provideMediaPlayer().setOnCompletionListener(
                player -> dataMusicPlayer.next()
        );
        launch();
    }

    @Override
    public void emptyQueue() {
        this.dataMusicPlayer.emptyQueue();
    }

    @Override
    public void enqueueSong(long id) {
        this.trackHandler.handle(
                this.trackHandler.createOperation(
                        TrackHandler.OPERATION_INTENTION_ENQUEUE,
                        id
                )
        );
    }

    @Override
    public void enqueueSong(long id, String title, String uploader, String thumbnail, String srcUrl) {
        if(srcUrl.length() > 1) {
            this.dataMusicPlayer.enqueue(
                    this.dataMusicPlayer.createTrack(
                            id,
                            title,
                            uploader,
                            thumbnail,
                            srcUrl
                    )
            );
        }
    }

    @Override
    public void playSong(long id) {
        this.trackHandler.handle(
                this.trackHandler.createOperation(
                        TrackHandler.OPERATION_INTENTION_PLAY,
                        id
                )
        );
    }

    @Override
    public void playSong(long id, String title, String uploader, String thumbnail, String srcUrl) {
        if(srcUrl.length() > 1) {
            this.dataMusicPlayer.setNewCurrentlyPlaying(
                    this.dataMusicPlayer.createTrack(
                            id,
                            title,
                            uploader,
                            thumbnail,
                            srcUrl
                    )
            );
        }
    }

    @Override
    public void play() {
        this.dataMusicPlayer.setPlaying(true);
    }

    @Override
    public void pause() {
        this.dataMusicPlayer.setPlaying(false);
    }

    @Override
    public void nextSong() {
        this.dataMusicPlayer.next();
    }

    @Override
    public void prevSong() {
        this.dataMusicPlayer.previous();
    }

    @Override
    public void setMiniPlayerVisibility(boolean visible) {
        if(visible) {
            this.view.show();
            this.controller.attendProgressBar();
        }
        else {
            this.view.hide();
        }
    }

    @Override
    public void setShuffle(boolean isShuffle) {
        receiveShuffleTrigger(isShuffle);
    }

    @Override
    public void setRepeat(boolean isRepeat) {
        receiveRepeatTrigger(isRepeat);
    }

    @Override
    public LifecycleOwner provideOwner() {
        return this.owner;
    }

    public NavController getNavController() {
        return navController;
    }

    public NavigationView getNavView() {
        return navView;
    }
}
