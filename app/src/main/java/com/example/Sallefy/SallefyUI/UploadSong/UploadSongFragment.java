package com.example.Sallefy.SallefyUI.UploadSong;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.Navigation;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.UploadSong.Controller.UploadSongController;
import com.example.Sallefy.SallefyUI.UploadSong.Controller.UploadSongMediator;
import com.example.Sallefy.SallefyUI.UploadSong.Model.DataUpload;
import com.example.Sallefy.SallefyUI.UploadSong.View.UploadSongView;
import com.example.Sallefy.Utils.Constants;
import com.example.Sallefy.repositoryImpl.factoriesImpl.ImageApiRepositoryFactory;
import com.example.Sallefy.repositoryImpl.factoriesImpl.TrackApiRepositoryFactory;


public class UploadSongFragment extends Fragment implements UploadSongMediator {
    private final int REQ_GET_SONG_URI = 1;
    private final int REQ_GET_IMG_URI = 2;

    private UploadSongView view;
    private UploadSongController controller;
    private DataUpload dataUpload;


    public UploadSongFragment() {
        this.dataUpload = new DataUpload();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        this.view = new UploadSongView(inflater, container);

        Animation fadeInAnim = AnimationUtils.loadAnimation(this.view.getRootView().getContext(), R.anim.fade_in);
        this.view.getRootView().startAnimation(fadeInAnim);

        TrackApiRepositoryFactory trackApiRepositoryFactory = new TrackApiRepositoryFactory();
        ImageApiRepositoryFactory imageRepositoryFactory = new ImageApiRepositoryFactory();

        this.controller = new UploadSongController(
                this,
                this.dataUpload,
                this.view,
                trackApiRepositoryFactory,
                imageRepositoryFactory
        );

        return this.view.getRootView();
    }

    @Override
    public void openFile() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("audio/mpeg");

        startActivityForResult(intent, REQ_GET_SONG_URI);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case Activity.RESULT_CANCELED:
                this.view.setChooseFileBtnState(
                        this.dataUpload.hasContent()
                );
                break;

            case REQ_GET_SONG_URI:
                if(null != data) {
                    this.dataUpload.setContentUri(data.getData());
                    this.view.setChooseFileBtnState(
                            this.dataUpload.hasContent()
                    );
                    this.controller.evaluateUploadRdy();
                }
                break;

            case REQ_GET_IMG_URI:
                if(null != data) {
                    //Got image file reference from Android FileExplorer successfully
                    this.dataUpload.setSongImgUri(data.getData());
                    this.view.displaySongImg(this.dataUpload.getSongImgUri());
                }
                break;
        }
    }

    @Override
    public void onSongUploaded(String trackUploadedID) {
        this.view.toastMessage(this.dataUpload.getTitle() + " uploaded successfully");

        Bundle b = new Bundle();
        b.putString(Constants.SONG_SPECIFICATIONS.SONG_ID, trackUploadedID);

        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_song_options, b);
    }

    @Override
    public void openImgFile() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");

        startActivityForResult(intent, REQ_GET_IMG_URI);
    }

    @Override
    public LifecycleOwner provideOwner() {
        return this;
    }
}