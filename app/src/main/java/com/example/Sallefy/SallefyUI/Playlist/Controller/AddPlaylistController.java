package com.example.Sallefy.SallefyUI.Playlist.Controller;

import com.example.Sallefy.SallefyUI.Playlist.Model.DataEditablePlaylist;
import com.example.Sallefy.SallefyUI.Playlist.View.AddPlaylistViewI;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.UserDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.ImageUploadDTO;
import com.example.Sallefy.repositories.Interfaces.ImageRepository;
import com.example.Sallefy.repositories.Interfaces.PlaylistRepository;
import com.example.Sallefy.repositories.Interfaces.UserRepository;
import com.example.Sallefy.repositories.factories.ImageRepositoryFactory;
import com.example.Sallefy.repositories.factories.PlaylistRepositoryFactory;
import com.example.Sallefy.repositories.factories.UserRepositoryFactory;
import com.example.Sallefy.repositoryImpl.repositoryImpl.SessionBearer;

public class AddPlaylistController {
    private AddPlaylistMediator mediator;
    private AddPlaylistViewI view;
    private DataEditablePlaylist dataPlaylist;

    private PlaylistRepository playlistRepository;

    private ImageRepositoryFactory imageRepositoryFactory;

    private UserDTO playlistOwnerObj;

    public AddPlaylistController(AddPlaylistMediator mediator,
                                 AddPlaylistViewI view,
                                 DataEditablePlaylist dataPlaylist,
                                 PlaylistRepositoryFactory playlistRepositoryFactory,
                                 UserRepositoryFactory userRepositoryFactory,
                                 ImageRepositoryFactory imageRepositoryFactory) {
        this.mediator = mediator;
        this.view = view;
        this.dataPlaylist = dataPlaylist;
        this.playlistRepository = playlistRepositoryFactory.create();
        UserRepository userRepository = userRepositoryFactory.createUserRepository();

        this.view.attachPlaylistImgListener(v -> playlistImgClicked());

        userRepository
                .getUserByName(SessionBearer.getUsernameIfThere())
                .observe(this.mediator.provideOwner(), this::processUserQuery);
        //We don't allow user to create playlist before updated user obj is retrieved
        this.view.updateAddPlaylistBtnState(false);

        this.imageRepositoryFactory = imageRepositoryFactory;
    }

    private void playlistImgClicked() {
        this.mediator.openImgFile();
    }

    private void processUserQuery(UserDTO userDTO) {
        if(userDTO.isStatus()) {
            this.playlistOwnerObj = userDTO;
            this.view.attachAddPlaylistBtnListener(this::addPlaylistBtnClicked);
        }
        else {
            this.view.toastErrorMsg("Internet connection failed");
        }
    }

    private void addPlaylistBtnClicked(String playlistName, String playlistDescription) {
        if (this.dataPlaylist.hasNewImg()) {
            this.view.updateAddPlaylistBtnState(false);
            ImageRepository imageRepository = this.imageRepositoryFactory.create();
            imageRepository
                    .uploadImage(this.dataPlaylist.getPlaylistImgUri())
                    .observe(this.mediator.provideOwner(), this::processImgUploadResponse);
        }
        else {
            addPlaylist(playlistName, playlistDescription, null);
        }
    }

    private void processImgUploadResponse(ImageUploadDTO imageUploadDTO) {
        if (imageUploadDTO.isStatus()) {
            addPlaylist(
                    this.view.getPlaylistName(),
                    this.view.getPlaylistDescription(),
                    imageUploadDTO.getImageUrl()
            );
        }
        else {
            this.view.displayNoImgAdded();
            addPlaylist(this.view.getPlaylistName(), this.view.getPlaylistDescription(), null);
        }
    }

    private void addPlaylist(String playlistName, String playlistDescription, String newImgUrl) {
        PlaylistDTO playlist = new PlaylistDTO();
        playlist.setName(playlistName);
        playlist.setDescription(playlistDescription);
        playlist.setOwnerObject(this.playlistOwnerObj);

        if (newImgUrl != null) {
            playlist.setThumbnail(newImgUrl);
            playlist.setCover(newImgUrl);
        }

        this.playlistRepository
                .createPlaylist(playlist)
                .observe(this.mediator.provideOwner(), this::processPlaylistCreationCallback);
    }

    private void processPlaylistCreationCallback(PlaylistDTO playlistDTO) {
        if (playlistDTO.isStatus()) {
            this.mediator.receiveMenuUpdateCall();
            this.view.toastMsg("Playlist: " + playlistDTO.getName() + " was created.");
            this.mediator.finishPlaylistCreation();
        }
        else {
            this.view.displayPlaylistNameError();
        }
    }
}
