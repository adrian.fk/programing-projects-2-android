package com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.Navigation;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacade;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacadeFactory;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.PlayerListingLogController;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.PlayerListingMediator;
import com.example.Sallefy.SallefyUI.MusicPlayer.Model.Data;
import com.example.Sallefy.SallefyUI.MusicPlayer.Model.DataMusicPlayerFactory;
import com.example.Sallefy.SallefyUI.MusicPlayer.View.PlayerListingLogView;
import com.example.Sallefy.Utils.Constants;

public class PlayerListingLogFragment extends Fragment implements PlayerListingMediator {
    private static PlayerListingLogFragment INSTANCE;

    private PlayerListingLogController controller;
    private Data musicPlayerData;
    private PlayerListingLogView view;

    private PlayerListingLogFragment() {}

    public static PlayerListingLogFragment getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PlayerListingLogFragment();
        }
        return INSTANCE;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        INSTANCE.musicPlayerData = DataMusicPlayerFactory.create();
        INSTANCE.musicPlayerData.currentlyPlaying.observe(this, this::processSongChanged);
    }

    private void processSongChanged(Data.Track track) {
        Animation fadeOutAnim = AnimationUtils.loadAnimation(this.view.getRootView().getContext(), R.anim.fade_out);
        this.view.getRootView().startAnimation(fadeOutAnim);

        this.controller.prepareLogContent(track);

        Animation fadeInAnim = AnimationUtils.loadAnimation(this.view.getRootView().getContext(), R.anim.fade_in);
        this.view.getRootView().startAnimation(fadeInAnim);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        INSTANCE.view = new PlayerListingLogView(inflater, container, getChildFragmentManager());

        this.controller = new PlayerListingLogController(INSTANCE, INSTANCE.view, INSTANCE.musicPlayerData);
        this.controller.prepareLogContent(INSTANCE.musicPlayerData.getCurrentlyPlaying());


        return INSTANCE.view.getRootView();
    }


    @Override
    public LifecycleOwner provideOwner() {
        return INSTANCE;
    }

    @Override
    public void openSongOptions(String id) {
        Bundle b = new Bundle();
        b.putString(Constants.SONG_SPECIFICATIONS.SONG_ID, id);

        Navigation
                .findNavController(INSTANCE.view.getRootView())
                .navigate(R.id.nav_song_options, b);
    }

    @Override
    public void playSong(String id) {
        MusicPlayerFacade musicPlayer = MusicPlayerFacadeFactory.create();
        musicPlayer.playSong(
                Long.parseLong(id)
        );
    }
}