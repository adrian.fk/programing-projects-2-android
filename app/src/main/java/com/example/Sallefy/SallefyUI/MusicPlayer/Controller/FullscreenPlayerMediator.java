package com.example.Sallefy.SallefyUI.MusicPlayer.Controller;

import com.example.Sallefy.Utils.FeaturedMediatorI;

public interface FullscreenPlayerMediator extends FeaturedMediatorI {
    void openSongOptions(String id);

    void openListings();
}
