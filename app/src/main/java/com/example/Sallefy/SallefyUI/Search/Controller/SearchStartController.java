package com.example.Sallefy.SallefyUI.Search.Controller;


import com.example.Sallefy.SallefyUI.Search.Model.DataSearch;

public class SearchStartController {
    private DataSearch dataSearch;


    public SearchStartController(DataSearch dataSearch) {
        this.dataSearch = dataSearch;
    }

    public void updateFilter(String filterName) {
        this.dataSearch.setSearchFilter(filterName);
    }
}
