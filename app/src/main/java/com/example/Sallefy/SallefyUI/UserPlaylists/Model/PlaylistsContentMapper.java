package com.example.Sallefy.SallefyUI.UserPlaylists.Model;

import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;

import java.util.Collection;

public class PlaylistsContentMapper {
    private PlaylistsContent content;

    public PlaylistsContentMapper(PlaylistsContent content) {
        this.content = content;
    }

    public void mapPlaylistDTOS(Collection<PlaylistDTO> playlistDTOS) {
        for (PlaylistDTO playlist : playlistDTOS) {
            this.content.addPlaylist(
                    this.content.createPlaylist(
                            String.valueOf(playlist.getId()),
                            playlist.getName(),
                            playlist.getThumbnail()
                    )
            );
        }
    }
}
