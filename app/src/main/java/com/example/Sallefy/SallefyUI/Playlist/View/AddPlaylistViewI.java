package com.example.Sallefy.SallefyUI.Playlist.View;

import android.net.Uri;
import android.view.View;

import com.example.Sallefy.SallefyUI.Playlist.Controller.ListenerInterfaces.AddPlaylistBtnClickListener;
import com.example.Sallefy.Utils.ViewInterface;

public interface AddPlaylistViewI extends ViewInterface {
    void attachAddPlaylistBtnListener(AddPlaylistBtnClickListener listener);

    void updateAddPlaylistBtnState(boolean clickable);

    void toastMsg(String msg);

    void toastErrorMsg(String errMsg);

    String getPlaylistName();

    String getPlaylistDescription();

    void displayPlaylistNameError();

    void displayPlaylistImg(Uri imgUri);

    void attachPlaylistImgListener(View.OnClickListener listener);

    void displayNoImgAdded();
}
