package com.example.Sallefy.SallefyUI.Home.Controller;

import com.example.Sallefy.SallefyUI.Home.Model.FollowersHomeContent;
import com.example.Sallefy.SallefyUI.Home.Model.PlaylistHomeContent;
import com.example.Sallefy.SallefyUI.Home.View.HomeView;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacade;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacadeFactory;
import com.example.Sallefy.SallefyUI.PlaylistMenu.PlaylistMenuFacade;
import com.example.Sallefy.SallefyUI.PlaylistMenu.PlaylistMenuFacadeFactory;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.UserDTO;
import com.example.Sallefy.repositories.Interfaces.PlaylistRepository;
import com.example.Sallefy.repositories.Interfaces.UserRepository;
import com.example.Sallefy.repositories.factories.PlaylistRepositoryFactory;
import com.example.Sallefy.repositories.factories.UserRepositoryFactory;
import com.example.Sallefy.repositoryImpl.repositoryImpl.SessionBearer;

import java.util.Collection;

public class HomeController {
    private static final int REQUIRED_PLAYLIST_API_CALLS = 2;

    private MusicPlayerFacade musicPlayerFacade;


    private HomeMediator mediator;
    private HomeView view;

    private UserRepository userRepository;
    private PlaylistRepository playlistRepository;

    private FollowersHomeContent followersContent;
    private PlaylistHomeContent playlistContent;

    public HomeController(HomeMediator mediator,
                          HomeView view,
                          UserRepositoryFactory userRepositoryFactory,
                          PlaylistRepositoryFactory playlistRepositoryFactory) {
        this.musicPlayerFacade = MusicPlayerFacadeFactory.create();

        this.mediator = mediator;
        this.view = view;

        this.userRepository = userRepositoryFactory.createUserRepository();
        this.playlistRepository = playlistRepositoryFactory.create();

        this.followersContent = new FollowersHomeContent();
        this.playlistContent = new PlaylistHomeContent(REQUIRED_PLAYLIST_API_CALLS);

        this.view.attachCurrentUserListener(v -> currentUserFrameClicked());
    }

    private void currentUserFrameClicked() {
        this.mediator.receiveUserCall(SessionBearer.getUsernameIfThere());
    }

    public void init() {
        //Fetches data about current user
        this.userRepository
                .getUserByName(SessionBearer.getUsernameIfThere())
                .observe(this.mediator.provideOwner(), this::loadUser);

        //Following
        this.userRepository
                .getCurrentUserFollowing()
                .observe(this.mediator.provideOwner(), this::processUserResponse);

        //Created playlists
        this.userRepository
                .getCurrentUserPlaylists()
                .observe(this.mediator.provideOwner(), this::loadPlaylists);

        //TODO replace with more appropriate playlist call
        //All currently created playlists, as the system does not have many playlists to suggest
        this.playlistRepository
                .getAllPlaylists()
                .observe(this.mediator.provideOwner(), this::loadPlaylists);

    }

    private void loadUser(UserDTO userDTO) {
        PlaylistMenuFacade playlistMenuFacade = PlaylistMenuFacadeFactory.create();
        playlistMenuFacade.setupUserHeader(
                userDTO.getLogin(),
                userDTO.getEmail(),
                userDTO.getImageUrl()
        );

        this.view.setUserImage(
                userDTO.getImageUrl()
        );
        this.view.setUsername(SessionBearer.getUsernameIfThere());
    }

    private void loadPlaylists(Collection<PlaylistDTO> playlistDTOS) {
        for(PlaylistDTO playlist : playlistDTOS) {
            this.playlistContent.addItem(
                    this.playlistContent.createPlaylistItem(
                            String.valueOf(playlist.getId()),
                            playlist.getName(),
                            playlist.getOwnerObject().getLogin(),
                            playlist.getThumbnail()
                    )
            );
        }
        this.playlistContent.registerSubStackFilled();
        evaluateViewUpdate();
    }

    private void processUserResponse(Collection<UserDTO> userDTOS) {
        for (UserDTO user : userDTOS) {
            this.followersContent.addItem(
                    this.followersContent.createFollower(
                            String.valueOf(user.getId()),
                            user.getLogin(),
                            user.getImageUrl()
                    )
            );
        }


        this.view.setFollowersContent(
                this.followersContent.getItems(),
                this::followerClicked,
                this::followerLongClicked
        );
    }

    private void evaluateViewUpdate() {
        if(this.playlistContent.isStacked()) {
            this.view.setPlaylistsContent(
                    this.playlistContent.getItems(),
                    this::playlistClicked,
                    this::playlistLongClicked
            );
        }
    }

    private void followerLongClicked(String idFollower) {

    }

    private void followerClicked(String userLogin) {
        this.mediator.receiveUserCall(userLogin);
    }

    private void playlistClicked(String idPlaylist) {
        this.mediator.receivePlaylistNavCall(
                idPlaylist
        );
    }

    private void playlistLongClicked(String  idFollower) {
        fetchPlaylist(Long.parseLong(idFollower));
    }

    private void fetchPlaylist(long id) {
        this.playlistRepository
                .getPlaylistById(id)
                .observe(this.mediator.provideOwner(), this::processPlaylistResponse);
    }

    private void processPlaylistResponse(PlaylistDTO playlistDTO) {
        this.view.toastMsg(playlistDTO.getName() + " enqueued");

        this.musicPlayerFacade.emptyQueue();

        for (TrackDTO track : playlistDTO.getTracks()) {
            this.musicPlayerFacade.enqueueSong(
                    track.getId(),
                    track.getName(),
                    track.getOwner().getLogin(),
                    track.getThumbnail(),
                    track.getUrl()
            );
        }
        this.musicPlayerFacade.nextSong();
    }
}
