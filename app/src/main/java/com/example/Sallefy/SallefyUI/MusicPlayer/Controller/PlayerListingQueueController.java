package com.example.Sallefy.SallefyUI.MusicPlayer.Controller;

import com.example.Sallefy.SallefyUI.MusicPlayer.Model.Data;
import com.example.Sallefy.SallefyUI.MusicPlayer.View.PlayerListingViewI;
import com.example.Sallefy.SallefyUI.Playlist.Model.PlaylistContent;

public class PlayerListingQueueController {
    private final PlayerListingMediator mediator;
    private final PlayerListingViewI view;

    private final Data dataMusicPlayer;

    public PlayerListingQueueController(PlayerListingMediator mediator, PlayerListingViewI view, Data dataMusicPlayer) {
        this.mediator = mediator;
        this.view = view;
        this.dataMusicPlayer = dataMusicPlayer;
        this.dataMusicPlayer.currentlyPlaying.observe(
                this.mediator.provideOwner(),
                this::processSongChanged
        );
    }

    private void processSongChanged(Data.Track track) {
        if (this.view.selectTrackIfExist(String.valueOf(track.getId()), track.getTitle())) {
            this.view.toggleTrackIfSelected();
        }
    }

    public void prepareQueueContent(Data.Track currentTrack) {
        final PlaylistContent queueContent = new PlaylistContent();

        if (currentTrack != null){
            queueContent.addItem(
                    queueContent.createSongItem(
                            String.valueOf(currentTrack.getId()),
                            currentTrack.getTitle(),
                            currentTrack.getUploader(),
                            currentTrack.getThumbnailUrl(),
                            currentTrack.getSrcUrl(),
                            currentTrack.isFavorite()
                    )
            );
        }

        for (Data.Track track : this.dataMusicPlayer.getQueue()) {
            queueContent.addItem(
                    queueContent.createSongItem(
                            String.valueOf(track.getId()),
                            track.getTitle(),
                            track.getUploader(),
                            track.getThumbnailUrl(),
                            track.getSrcUrl(),
                            track.isFavorite()
                    )
            );
        }
        this.view.displaySongs(
                queueContent.getItems(),
                playQueue -> {
                    this.mediator.playSong(playQueue.getItems().get(0).id);
                },
                this.mediator::openSongOptions
        );

        if (this.dataMusicPlayer.getCurrentlyPlaying() != null) {
            this.view.selectTrackIfExist(
                    String.valueOf(this.dataMusicPlayer.getCurrentlyPlaying().getId()),
                    this.dataMusicPlayer.getCurrentlyPlaying().getTitle()
            );
        }
    }
}
