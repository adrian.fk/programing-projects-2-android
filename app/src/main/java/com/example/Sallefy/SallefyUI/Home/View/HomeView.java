package com.example.Sallefy.SallefyUI.Home.View;

import android.view.View;

import com.example.Sallefy.SallefyUI.Home.Controller.ListenerInterfaces.FollowerClickListener;
import com.example.Sallefy.SallefyUI.Home.Controller.ListenerInterfaces.FollowerLongClickListener;
import com.example.Sallefy.SallefyUI.Home.Controller.ListenerInterfaces.PlaylistClickListener;
import com.example.Sallefy.SallefyUI.Home.Controller.ListenerInterfaces.PlaylistLongClickListener;
import com.example.Sallefy.SallefyUI.Home.Model.FollowersHomeContent;
import com.example.Sallefy.SallefyUI.Home.Model.PlaylistHomeContent;
import com.example.Sallefy.Utils.ViewInterface;

import java.util.List;

public interface HomeView extends ViewInterface{
    void setUsername(String username);

    void attachCurrentUserListener(View.OnClickListener listener);

    void setFollowersContent(List<FollowersHomeContent.Follower> items,
                             FollowerClickListener followerClickListener,
                             FollowerLongClickListener followerLongClickListener);

    void setPlaylistsContent(List<PlaylistHomeContent.PlaylistItem> items,
                             PlaylistClickListener playlistClickListener,
                             PlaylistLongClickListener playlistLongClickListener);

    void setUserImage(String imgUrl);

    void toastError(String errMsg);

    void toastMsg(String msg);
}
