package com.example.Sallefy.SallefyUI.MusicPlayer.Controller;


import android.os.Handler;

import com.example.Sallefy.Cast.CastProxy;
import com.example.Sallefy.Cast.CastProxyFactory;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacade;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacadeFactory;
import com.example.Sallefy.SallefyUI.MusicPlayer.Model.Data;
import com.example.Sallefy.SallefyUI.MusicPlayer.Model.PlaybackLocation;
import com.example.Sallefy.SallefyUI.MusicPlayer.View.FullscreenPlayerViewI;


public class FullscreenPlayerController {
    private final FullscreenPlayerMediator mediator;
    private final FullscreenPlayerViewI view;
    private final Data dataMusicPlayer;

    private final MusicPlayerFacade musicPlayerFacade;
    private final CastProxy castProxy = CastProxyFactory.create();
    private Runnable progressBarUpdater;
    private Handler handler;

    public FullscreenPlayerController(FullscreenPlayerMediator mediator, FullscreenPlayerViewI view, Data dataMusicPlayer) {
        this.mediator = mediator;
        this.musicPlayerFacade = MusicPlayerFacadeFactory.create();
        this.view = view;
        this.dataMusicPlayer = dataMusicPlayer;

        this.view.attachPlayButtonListener(v -> this.playBtnClicked());
        this.view.attachNextButtonListener(v -> this.nextBtnClicked());
        this.view.attachPrevButtonListener(v -> this.prevBtnClicked());
        this.view.attachRepeatButtonListener(v -> this.repeatBtnClicked());
        this.view.attachShuffleButtonListener(v -> this.shuffleBtnClicked());
        this.view.attachImageLongPressListener(v -> {
            this.imageLongPress();
            return true;
        });
        this.view.attachListingButtonListener(v -> this.onListingBtnClicked());
        setUpProgressBarUpdater();

        this.view.attachProgressBarChangeCallback(this::processProgressBarChanged);

        this.view.setPlayState(this.dataMusicPlayer.isPlaying());
        this.view.setRepeatBtnState(this.dataMusicPlayer.isRepeat());
        this.view.setShuffleBtnState(this.dataMusicPlayer.isShuffle());
    }

    private void onListingBtnClicked() {
        this.mediator.openListings();
    }

    private void processProgressBarChanged(int progress) {
        this.dataMusicPlayer.setProgress(progress);
        this.dataMusicPlayer.provideMediaPlayer().seekTo(
                dataMusicPlayer.getProgress()
        );
    }


    private void imageLongPress() {
        this.mediator.openSongOptions(String.valueOf(this.dataMusicPlayer.getCurrentlyPlaying().getId()));
    }

    private void setUpProgressBarUpdater(){
        this.handler = new Handler();
        this.progressBarUpdater = () -> {
            if(this.dataMusicPlayer.isPlaying()) {
                if (this.dataMusicPlayer.getPlaybackLocation() == PlaybackLocation.LOCAL) {
                    this.view.updateProgressBar(this.dataMusicPlayer.getProgress());
                    this.handler.post(this.progressBarUpdater);
                }
                else {
                    this.castProxy.addSeekBar(this.view.getSeekbar());
                }
            }
        };
    }



    private void nextBtnClicked(){
        this.view.setNextBtnClicked(true);
        this.musicPlayerFacade.nextSong();
    }

    private void prevBtnClicked() {
        this.view.setPrevBtnClicked(true);
        this.musicPlayerFacade.prevSong();
    }

    private void shuffleBtnClicked() {
        boolean isShuffle = !this.dataMusicPlayer.isShuffle();
        this.view.setShuffleBtnState(isShuffle);
        this.musicPlayerFacade.setShuffle(isShuffle);
    }

    private void repeatBtnClicked() {
        boolean isRepeat = !this.dataMusicPlayer.isRepeat();
        this.view.setRepeatBtnState(isRepeat);
        this.musicPlayerFacade.setRepeat(isRepeat);
    }


    private void playBtnClicked() {
        if(this.dataMusicPlayer.isPlaying()){
            this.musicPlayerFacade.pause();
        }else {
            this.musicPlayerFacade.play();
            this.attendProgressBar();
        }
        this.view.setPlayState(this.dataMusicPlayer.isPlaying());
    }

    public void attendProgressBar() {
        this.handler.post(this.progressBarUpdater);
        this.view.setDurationProgressBar(this.dataMusicPlayer.getDuration());
    }

}
