package com.example.Sallefy.SallefyUI.MusicPlayer.Controller;

import com.example.Sallefy.Utils.FeaturedMediatorI;

public interface PlayerListingMediator extends FeaturedMediatorI {
    void openSongOptions(String id);

    void playSong(String id);
}
