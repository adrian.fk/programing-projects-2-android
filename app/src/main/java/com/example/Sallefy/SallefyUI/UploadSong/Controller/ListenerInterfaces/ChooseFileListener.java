package com.example.Sallefy.SallefyUI.UploadSong.Controller.ListenerInterfaces;

public interface ChooseFileListener {
    void onChooseFile();
}
