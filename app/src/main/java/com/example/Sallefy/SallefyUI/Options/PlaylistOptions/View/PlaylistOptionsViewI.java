package com.example.Sallefy.SallefyUI.Options.PlaylistOptions.View;

import com.example.Sallefy.SallefyUI.Options.PlaylistOptions.Controller.ListenerInterfaces.DeletePlaylistBtnClickListener;
import com.example.Sallefy.SallefyUI.Options.PlaylistOptions.Controller.ListenerInterfaces.FollowPlaylistBtnClickListener;
import com.example.Sallefy.SallefyUI.Options.PlaylistOptions.Controller.ListenerInterfaces.OptionMenuClickListener;
import com.example.Sallefy.Utils.ViewInterface;

public interface PlaylistOptionsViewI extends ViewInterface {
    void updatePlaylistHeader(
            String playlistName,
            String playlistOwner,
            String playlistImgUrl
    );

    void displayFollowBtnState(boolean followed);

    void attachFollowPlaylistBtnListener(FollowPlaylistBtnClickListener listener);

    void attachRenamePlaylistBtnListener(OptionMenuClickListener listener);

    void attachDeleteBtnListener(DeletePlaylistBtnClickListener listener);

    void attachPlaylistOwnerBtnListener(OptionMenuClickListener listener);

    void attachEnqueuePlaylistBtnListener(OptionMenuClickListener listener);

    void displayPlaylistOptions(boolean visible);

    void displayRenameBtn(boolean visible);

    void displayDeleteBtn(boolean visible);

    void toastMsg(String msg);
}
