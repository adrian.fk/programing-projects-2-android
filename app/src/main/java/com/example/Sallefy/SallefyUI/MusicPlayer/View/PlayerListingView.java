package com.example.Sallefy.SallefyUI.MusicPlayer.View;

import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerTabStrip;
import androidx.viewpager.widget.ViewPager;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.MusicPlayer.View.Adapter.PlayerListingViewPagerAdapter;
import com.example.Sallefy.Utils.ViewInterface;

public class PlayerListingView implements ViewInterface {
    private final View rootView;
    private final FragmentManager pagerManager;

    private ViewPager viewPager;

    public PlayerListingView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             FragmentManager fragmentManager) {
        this.rootView = inflater.inflate(R.layout.fragment_player_listing, container, false);
        this.pagerManager = fragmentManager;
        findViews();
        setupTabTitleStrip();
        fillTabs();
    }

    private void setupTabTitleStrip() {
        final PagerTabStrip strip = getRootView().findViewById(R.id.player_listing_pager_header);
        strip.setDrawFullUnderline(false);
        strip.setTabIndicatorColor(Color.DKGRAY);
        strip.setBackgroundColor(getRootView().getResources().getColor(R.color.colorHeaderBackground));
        strip.setNonPrimaryAlpha(0.5f);
        strip.setTextSpacing(25);
        strip.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
    }

    private void fillTabs() {
        FragmentPagerAdapter adapterViewPager = new PlayerListingViewPagerAdapter(
                this.pagerManager, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
        );

        this.viewPager.setAdapter(adapterViewPager);
    }

    private void findViews() {
        this.viewPager = getRootView().findViewById(R.id.player_listing_pager);
    }

    @Override
    public View getRootView() {
        return this.rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }
}
