package com.example.Sallefy.SallefyUI.Options.SongOptions.View;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.Sallefy.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class SongOptionsView implements SongOptionsViewI{
    private View rootView;

    private LinearLayout mainSection;

    //CLICKABLE SECTIONS
    private LinearLayout playSection;
    private LinearLayout favSection;
    private LinearLayout enqueueSection;
    private LinearLayout userProfileSection;
    private LinearLayout addToPlaylistSection;
    private LinearLayout shareSection;
    private LinearLayout deleteSection;

    //IMAGES
    private CircleImageView songImage;
    private ImageView favButton;

    //TEXT
    private TextView songTitle;
    private TextView songUploader;


    public SongOptionsView(LayoutInflater inflater, ViewGroup container) {
        this.rootView = inflater.inflate(R.layout.fragment_song_options, container, false);
        findViews();
    }

    private void findViews() {
        this.mainSection = this.rootView.findViewById(R.id.options_song_container);

        this.playSection = this.rootView.findViewById(R.id.options_song_play);
        this.favSection = this.rootView.findViewById(R.id.options_song_favorite);
        this.enqueueSection = this.rootView.findViewById(R.id.options_song_queue);
        this.userProfileSection = this.rootView.findViewById(R.id.options_song_user);
        this.addToPlaylistSection = getRootView().findViewById(R.id.options_song_add_to_playlist);
        this.shareSection = this.rootView.findViewById(R.id.options_song_share);
        this.deleteSection = this.rootView.findViewById(R.id.options_song_delete);

        this.favButton = this.rootView.findViewById(R.id.options_song_favorite_img);
        this.songImage = this.rootView.findViewById(R.id.options_song_img);

        this.songTitle = this.rootView.findViewById(R.id.options_song_title);
        this.songUploader = this.rootView.findViewById(R.id.options_song_uploader);
    }

    @Override
    public View getRootView() {
        return this.rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }

    @Override
    public void attachPlayBtnListener(View.OnClickListener listener) {
        this.playSection.setOnClickListener(listener);
    }

    @Override
    public void attachFavBtnListener(View.OnClickListener listener) {
        this.favSection.setOnClickListener(listener);
    }

    @Override
    public void attachEnqueueBtnListener(View.OnClickListener listener) {
        this.enqueueSection.setOnClickListener(listener);
    }

    @Override
    public void attachUserProfileBtnListener(View.OnClickListener listener) {
        this.userProfileSection.setOnClickListener(listener);
    }

    @Override
    public void attachAddToPlaylistBtnListener(View.OnClickListener listener) {
        this.addToPlaylistSection.setOnClickListener(listener);
    }

    @Override
    public void attachShareBtnListener(View.OnClickListener listener) {
        this.shareSection.setOnClickListener(listener);
    }

    @Override
    public void attachDeleteBtnListener(View.OnClickListener listener) {
        this.deleteSection.setOnClickListener(listener);
    }

    @Override
    public void toastConnectionError() {
        toastMsg("An error occurred meanwhile contacting the server");
    }

    @Override
    public void toastSongDeleted() {
        toastMsg("Song successfully deleted");
    }

    @Override
    public void toastMsg(String msg) {
        Toast.makeText(
                this.getRootView().getContext(),
                msg,
                Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void updateFavBtnState(boolean isFavorite) {
        if(isFavorite) {
            this.favButton.setImageResource(R.drawable.favorite_button_filled_colored);
        }
        else {
            this.favButton.setImageResource(R.drawable.favorite_button);
        }
    }

    @Override
    public void updateView(boolean showView) {
        if(showView) {
            this.mainSection.setVisibility(View.VISIBLE);
        }
        else {
            this.mainSection.setVisibility(View.GONE);
        }
    }

    @Override
    public void updateView(boolean showView, boolean isCreatedByUser) {
        updateView(showView);
        if(isCreatedByUser) {
            this.deleteSection.setVisibility(View.VISIBLE);
        }
        else {
            this.deleteSection.setVisibility(View.GONE);
        }
    }

    @Override
    public void updateView(boolean showView,
                           String songTitle,
                           String songUploader,
                           String songImage,
                           boolean isCreatedByUser,
                           boolean isFavorite) {

        updateView(showView, isCreatedByUser);

        Glide.with(this.rootView.getContext())
                .asBitmap()
                .load(songImage)
                .into(this.songImage);

        this.songTitle.setText(songTitle);
        this.songUploader.setText(songUploader);
        updateFavBtnState(isFavorite);
    }

}
