package com.example.Sallefy.SallefyUI.Options.PlaylistOptions.Controller.ListenerInterfaces;

public interface DeletePlaylistBtnClickListener {
    void deletePlaylist();
}
