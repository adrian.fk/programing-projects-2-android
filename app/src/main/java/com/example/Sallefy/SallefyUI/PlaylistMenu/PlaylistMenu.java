package com.example.Sallefy.SallefyUI.PlaylistMenu;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.NavController;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.PlaylistMenu.Controller.PlaylistMenuMediator;
import com.example.Sallefy.SallefyUI.PlaylistMenu.Controller.PlaylistsMenuController;
import com.example.Sallefy.SallefyUI.PlaylistMenu.Model.DataPlaylistMenu;
import com.example.Sallefy.SallefyUI.PlaylistMenu.Model.MenuSelector;
import com.example.Sallefy.SallefyUI.PlaylistMenu.View.PlaylistMenuView;
import com.example.Sallefy.Utils.Constants;
import com.example.Sallefy.repositoryImpl.factoriesImpl.PlaylistApiRepositoryFactory;
import com.example.Sallefy.repositoryImpl.factoriesImpl.UserApiRepositoryFactory;
import com.google.android.material.navigation.NavigationView;

public class PlaylistMenu implements PlaylistMenuFacade, PlaylistMenuMediator {

    private LifecycleOwner owner;
    private NavController navController;

    private PlaylistsMenuController controller;
    private PlaylistMenuView view;
    private DataPlaylistMenu dataPlaylistMenu;
    private MenuSelector selector;

    public PlaylistMenu() {}

    @Override
    public void setup(LifecycleOwner owner, NavController navController, @NonNull NavigationView navView) {
        this.owner = owner;
        this.navController = navController;

        this.view = new PlaylistMenuView(navView);
        this.dataPlaylistMenu = new DataPlaylistMenu();
        this.selector = new MenuSelector();

        PlaylistApiRepositoryFactory playlistApiRepositoryFactory = new PlaylistApiRepositoryFactory();
        UserApiRepositoryFactory userApiRepositoryFactory = new UserApiRepositoryFactory();

        this.controller = new PlaylistsMenuController(
                this,
                this.view,
                this.dataPlaylistMenu,
                playlistApiRepositoryFactory,
                userApiRepositoryFactory
        );
    }

    @Override
    public void setupUserHeader(String username, String email, String userImgUrl) {
        if (userImgUrl == null) {
            userImgUrl = Constants.COSMETICS.DEFAULT_USR_IMG;
        }
        this.view.displayUserHeader(username, email, userImgUrl);
    }

    @Override
    public void updatePlaylistMenu() {
        this.controller.updateMenuContent();
    }

    @Override
    public boolean hasSelectedItem() {
        return selector.isSelected();
    }

    @Override
    public MenuSelector.SelectedItem retrieveSelectedItem() {
        return this.selector.popSelectedItem();
    }


    @Override
    public LifecycleOwner provideOwner() {
        return this.owner;
    }

    @Override
    public void receiveSelectedPlaylist(String idPlaylist, String playlistName) {
        this.selector.select(idPlaylist, playlistName);

        Bundle b = new Bundle();
        b.putString(Constants.PLAYLIST_SPECIFICATIONS.PLAYLIST_ID, idPlaylist);
        b.putString(Constants.PLAYLIST_SPECIFICATIONS.PLAYLIST_NAME, playlistName);
        this.navController.navigate(R.id.nav_to_playlist, b);
    }
}
