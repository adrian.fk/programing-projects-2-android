package com.example.Sallefy.SallefyUI.UploadSong.Controller;

import com.example.Sallefy.Utils.FeaturedMediatorI;

public interface UploadSongMediator extends FeaturedMediatorI {
    void openFile();

    void onSongUploaded(String trackUploadedID);

    void openImgFile();
}
