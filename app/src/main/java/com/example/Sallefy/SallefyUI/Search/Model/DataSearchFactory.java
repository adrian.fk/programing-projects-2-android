package com.example.Sallefy.SallefyUI.Search.Model;

public interface DataSearchFactory {
    DataSearch data = new DataSearch();

    static DataSearch create() {
        return data;
    }
}
