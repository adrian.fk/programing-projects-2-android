package com.example.Sallefy.SallefyUI.Home.Controller.ListenerInterfaces;

public interface FollowerClickListener {
    void onClicked(String userLogin);
}
