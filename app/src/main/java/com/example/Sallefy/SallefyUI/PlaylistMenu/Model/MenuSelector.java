package com.example.Sallefy.SallefyUI.PlaylistMenu.Model;

public class MenuSelector {
    private boolean isSelected;

    private SelectedItem selectedItem;

    public MenuSelector() {
        this.selectedItem = new SelectedItem();
    }

    public void select(String id, String name) {
        this.isSelected = true;
        this.selectedItem.setIdSelected(id);
        this.selectedItem.setNameSelected(name);
    }

    public SelectedItem popSelectedItem() {
        if(isSelected) {
            this.isSelected = false;
            return selectedItem;
        }
        return new SelectedItem();
    }

    public boolean isSelected() {
        return this.isSelected;
    }

    public static class SelectedItem {
        private String idSelected;
        private String nameSelected;

        public String getId() {
            return idSelected;
        }

        void setIdSelected(String idSelected) {
            this.idSelected = idSelected;
        }

        public String getName() {
            return nameSelected;
        }

        void setNameSelected(String nameSelected) {
            this.nameSelected = nameSelected;
        }
    }
}
