package com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;


import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacade;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacadeFactory;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.FullscreenPlayerController;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.FullscreenPlayerMediator;
import com.example.Sallefy.SallefyUI.MusicPlayer.Model.Data;
import com.example.Sallefy.SallefyUI.MusicPlayer.Model.DataMusicPlayerFactory;
import com.example.Sallefy.SallefyUI.MusicPlayer.View.FullscreenPlayerView;
import com.example.Sallefy.Utils.Constants;

public class FullscreenPlayerFragment extends Fragment implements FullscreenPlayerMediator {
    private FullscreenPlayerView view;
    private Data dataMusicPlayer;
    private FullscreenPlayerController controller;
    private MusicPlayerFacade musicPlayerFacade;


    public FullscreenPlayerFragment() {
    }


    @Override
    public void onStop() {
        super.onStop();
        musicPlayerFacade.setMiniPlayerVisibility(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = new FullscreenPlayerView(inflater, container);

        Animation fadeInAnim = AnimationUtils.loadAnimation(this.view.getRootView().getContext(), R.anim.fade_in);
        this.view.getRootView().startAnimation(fadeInAnim);

        this.dataMusicPlayer = DataMusicPlayerFactory.create();
        this.musicPlayerFacade = MusicPlayerFacadeFactory.create();
        this.controller = new FullscreenPlayerController(this, this.view, this.dataMusicPlayer);


        this.dataMusicPlayer.currentlyPlaying.observe(getViewLifecycleOwner(), track -> {
                    this.view.setSongImage(dataMusicPlayer.getCurrentlyPlaying().getThumbnailUrl());
                    this.view.setTextTitle(dataMusicPlayer.getCurrentlyPlaying().getTitle());
                    this.view.setTextUploader(dataMusicPlayer.getCurrentlyPlaying().getUploader());
                    this.view.setShuffleBtnState(dataMusicPlayer.isShuffle());
                    this.view.setRepeatBtnState(dataMusicPlayer.isRepeat());
                    this.view.setDurationProgressBar(this.dataMusicPlayer.getDuration());
                }
        );

        this.musicPlayerFacade.setMiniPlayerVisibility(false);
        processPlayingState(this.dataMusicPlayer.isPlaying());

        return this.view.getRootView();
    }

    private void processPlayingState(Boolean playing) {
        if(playing){
            this.controller.attendProgressBar();
        }
    }

    @Override
    public void openSongOptions(String id) {
        Bundle b = new Bundle();
        b.putString(Constants.SONG_SPECIFICATIONS.SONG_ID, id);

        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_song_options, b);
    }

    @Override
    public void openListings() {
        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_player_listing);
    }

    @Override
    public LifecycleOwner provideOwner() {
        return this;
    }
}
