package com.example.Sallefy.SallefyUI.Playlist.Controller;

import com.example.Sallefy.SallefyUI.Playlist.Model.DataEditablePlaylist;
import com.example.Sallefy.SallefyUI.Playlist.View.ModifyPlaylistViewI;
import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.ImageUploadDTO;
import com.example.Sallefy.repositories.Interfaces.ImageRepository;
import com.example.Sallefy.repositories.Interfaces.PlaylistRepository;
import com.example.Sallefy.repositories.factories.ImageRepositoryFactory;
import com.example.Sallefy.repositories.factories.PlaylistRepositoryFactory;

public class ModifyPlaylistController {
    private ModifyPlaylistMediator mediator;
    private ModifyPlaylistViewI view;
    private DataEditablePlaylist data;
    private PlaylistRepository playlistRepository;
    private ImageRepository imageRepository;

    public ModifyPlaylistController(ModifyPlaylistMediator mediator,
                                    DataEditablePlaylist dataPlaylist,
                                    ModifyPlaylistViewI view,
                                    ImageRepositoryFactory imageRepositoryFactory,
                                    PlaylistRepositoryFactory playlistRepositoryFactory) {
        this.mediator = mediator;
        this.data = dataPlaylist;
        this.view = view;
        this.imageRepository = imageRepositoryFactory.create();
        this.playlistRepository = playlistRepositoryFactory.create();


        this.view.attachConfirmBtnClickListener(v -> confirmBtnClicked());
        this.view.attachPlaylistImgListener(v -> playlistImgClicked());

        fetchPlaylist();
    }

    private void playlistImgClicked() {
        this.mediator.openImgFile();
    }

    private void fetchPlaylist() {
        this.playlistRepository
                .getPlaylistById(this.data.getPlaylistId())
                .observe(this.mediator.provideOwner(), this::processPlaylistResponse);
    }

    private void processPlaylistResponse(PlaylistDTO playlistDTO) {
        if(this.data.getPlaylistName() == null) {
            this.data.setPlaylistName(
                    playlistDTO.getName()
            );
            this.view.displayPlaylistTitle(
                    this.data.getPlaylistName()
            );
        }
        this.view.displayPlaylistImg(playlistDTO.getThumbnail());
        this.data.setPlaylistHolder(playlistDTO);
    }

    private void confirmBtnClicked() {
        if (!this.view.getNewTitle().equals("")) {
            this.data.getPlaylistHolder().setName(
                    this.view.getNewTitle()
            );
        }

        if (this.data.hasNewImg()) {
            this.imageRepository
                    .uploadImage(this.data.getPlaylistImgUri())
                    .observe(this.mediator.provideOwner(), this::processImgUploadResponse);
        }
        else {
            updatePlaylist();
        }
    }

    private void processImgUploadResponse(ImageUploadDTO imageUploadDTO) {
        if (imageUploadDTO.isStatus()) {
            this.data.getPlaylistHolder().setThumbnail(imageUploadDTO.getImageUrl());
        }
        else {
            this.view.toastMsg("Image could not be uploaded..");
        }

        updatePlaylist();
    }

    private void updatePlaylist() {
        this.playlistRepository
                .updatePlaylist(this.data.getPlaylistHolder())
                .observe(this.mediator.provideOwner(), this::processPlaylistUpdated);
    }

    private void processPlaylistUpdated(BaseDTO baseDTO) {
        if (baseDTO.isStatus()) {
            this.view.toastMsg(this.data.getPlaylistHolder().getName() + " modified successfully");
            this.mediator.conclude();
        }
    }
}
