package com.example.Sallefy.SallefyUI.MusicPlayer.Model;

import android.media.MediaPlayer;

import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;

import com.example.Sallefy.Cast.CastProxyImpl;

import java.util.ArrayList;
import java.util.Random;

public class Data implements CastProxyImpl {
    private static final String TAG = "MusicPlayer";

    private MediaPlayer mediaPlayer;


    public final MutableLiveData<Boolean> playing;
    public final MutableLiveData<Track> currentlyPlaying;


    private PlaybackLocation playbackLocation;
    private int progress;
    private int duration;
    private boolean repeat;
    private boolean shuffle;
    private Random shuffleNumGenerator;
    private ArrayList<Track> queue;
    private ArrayList<Track> log;

    public Data() {
        this.currentlyPlaying = new MutableLiveData<>();
        this.queue = new ArrayList<>();
        this.log = new ArrayList<>();
        this.mediaPlayer =  new MediaPlayer();
        this.playing = new MutableLiveData<>();
        this.shuffleNumGenerator = new Random();
        this.playbackLocation = PlaybackLocation.LOCAL;
    }

    @Override
    public PlaybackLocation getPlaybackLocation() {
        return playbackLocation;
    }

    private boolean trackExists(Track track) {
        return track != null;
    }

    public boolean mediaPlayerExists() {
        return this.mediaPlayer != null;
    }

    private void prependQueue(Track track) {
        if(trackExists(track)) {
            this.queue.add(0, track);
        }
    }

    public void enqueue(Track track) {
        this.queue.add(track);
    }

    public void emptyQueue() {
        this.queue.clear();
    }

    public void next() {
        if(this.repeat) {
            this.mediaPlayer.seekTo(0);
            this.mediaPlayer.start();
        }
        else {
            if(!this.shuffle) {
                Track track = dequeue();
                if (trackExists(track)) {
                    setNewCurrentlyPlaying(track);
                }
            }
            else {
                Track track = dequeueRdm();
                if (trackExists(track)) {
                    setNewCurrentlyPlaying(track);
                }
            }
        }
    }

    private Track dequeueRdm() {
        int rndIndex;
        if(this.queue.size() > 0) {
            rndIndex = this.shuffleNumGenerator.nextInt(this.queue.size());
        }
        else {
            rndIndex = 0;
        }

        return dequeue(rndIndex);
    }

    public void previous() {
        if(this.repeat) {
            this.mediaPlayer.seekTo(0);
        }
        else {
            Track track = getLastLogEntry();
            if (trackExists(track)) {
                prependQueue(getCurrentlyPlaying());
                setCurrentlyPlaying(track);
            }
            else {
                this.mediaPlayer.seekTo(0);
            }
        }
    }

    private Track dequeue(int pos) {
        Track out = null;
        if(this.queue.size() > 0 && this.queue.size() > pos && pos >= 0) {
            out = this.queue.get(pos);
            this.queue.remove(pos);
        }
        return out;
    }

    private Track dequeue() {
        Track out = null;
        if(this.queue.size() > 0) {
            out = this.queue.get(0);
            this.queue.remove(0);
        }
        return out;
    }

    public void setShuffle(boolean shuffle) {
        this.shuffle = shuffle;
    }

    public boolean isShuffle() {
        return shuffle;
    }

    private void logCurrent() {
        if(trackExists(this.currentlyPlaying.getValue())) {
            this.log.add(this.currentlyPlaying.getValue());
        }
    }

    @Nullable
    private Track getLastLogEntry() {
        Track out = null;
        if(this.log.size() > 0) {
            out = this.log.get(this.log.size() - 1);
            this.log.remove(this.log.size() - 1);
        }
        return out;
    }


    @Override
    public Track getCurrentlyPlaying() {
        return this.currentlyPlaying.getValue();
    }

    public void setCurrentlyPlaying(Track track) {
        if(trackExists(track)) {
            this.currentlyPlaying.setValue(track);
        }
    }

    public ArrayList<Track> getQueue() {
        return queue;
    }

    public ArrayList<Track> getLog() {
        return log;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }

    public void setNewCurrentlyPlaying(Track track) {
        if(trackExists(track)) {
            logCurrent();
            setCurrentlyPlaying(track);
        }
    }

    public void updatePlayState(Boolean playing) {
        if (playing) {
            this.mediaPlayer.start();
        } else {
            this.mediaPlayer.pause();
        }
    }

    @Override
    public int getDuration() {
        return this.duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public int getProgress() {
        return this.progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public boolean isPlaying() {
        if(this.playing.getValue() != null) return this.playing.getValue();
        return false;
    }

    @Override
    public void setPlaying(Boolean playing) {
        this.playing.setValue(playing);
    }

    @Override
    public void onApplicationConnected() {
        this.mediaPlayer.stop();
        this.playbackLocation = PlaybackLocation.REMOTE;
    }

    @Override
    public void onRemotePlayerStreamEnded() {
        next();
    }

    @Override
    public void onApplicationDisconnected(long pos) {
        this.playbackLocation = PlaybackLocation.LOCAL;
        if(getCurrentlyPlaying() != null) {
            setCurrentlyPlaying(
                    createTrack(
                            getCurrentlyPlaying().id,
                            getCurrentlyPlaying().title,
                            getCurrentlyPlaying().uploader,
                            getCurrentlyPlaying().thumbnailUrl,
                            getCurrentlyPlaying().srcUrl
                    )
            );
        }
    }

    public MediaPlayer provideMediaPlayer() {
        return this.mediaPlayer;
    }

    public Track createTrack(long id, String title, String uploader, String thumbnailUrl, String srcUrl) {
        return new Track(id, title, uploader, thumbnailUrl, srcUrl);
    }

    public Track createTrack(long id, String title, String uploader, String thumbnailUrl, String srcUrl, boolean isFavorite) {
        return new Track(id, title, uploader, thumbnailUrl, srcUrl, isFavorite);
    }

    public static class Track {
        private long id;
        private String title;
        private String uploader;
        private String thumbnailUrl;
        private String srcUrl;
        private boolean isFavorite;

        Track(long id, String title, String uploader, String thumbnailUrl, String srcUrl) {
            this.id = id;
            this.title = title;
            this.uploader = uploader;
            this.thumbnailUrl = thumbnailUrl;
            this.srcUrl = srcUrl;
        }

        Track(long id, String title, String uploader, String thumbnailUrl, String srcUrl, boolean isFavorite) {
            this.id = id;
            this.title = title;
            this.uploader = uploader;
            this.thumbnailUrl = thumbnailUrl;
            this.srcUrl = srcUrl;
            this.isFavorite = isFavorite;
        }

        public long getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getUploader() {
            return uploader;
        }

        public String getThumbnailUrl() {
            return thumbnailUrl;
        }

        public String getSrcUrl() {
            return srcUrl;
        }

        public boolean isFavorite() {
            return isFavorite;
        }
    }
}
