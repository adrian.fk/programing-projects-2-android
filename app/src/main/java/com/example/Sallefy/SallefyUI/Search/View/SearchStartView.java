package com.example.Sallefy.SallefyUI.Search.View;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Search.Controller.ListenerInterfaces.SearchStartBtnClickListener;
import com.example.Sallefy.SallefyUI.Search.Model.SearchConstants;
import com.example.Sallefy.Utils.ViewInterface;

import java.util.ArrayList;

public class SearchStartView implements ViewInterface {
    private static final String TAG = "SearchStartView";

    private View rootView;

    private SearchStartBtnClickListener searchStartBtnClickListener;

    private Button toggledBtn;

    private ArrayList<Button> buttons;

    private int[] buttonIds = {R.id.search_start_tracks, R.id.search_start_playlists, R.id.search_start_users};

    public SearchStartView(LayoutInflater inflater, ViewGroup container,
                           SearchStartBtnClickListener searchStartBtnClickListener,
                           String toggledSearchFilter) {
        this.rootView = inflater.inflate(R.layout.fragment_search_start, container, false);
        this.searchStartBtnClickListener = searchStartBtnClickListener;
        Log.d(TAG, "SearchView: inflated");
        this.buttons = new ArrayList<>();
        this.attachButtonListener(
                this::processBtnClick
        );
        initializeBtnStatus(toggledSearchFilter);
    }

    private void initializeBtnStatus(String toggledSearchFilter) {
        switch (toggledSearchFilter) {
            case SearchConstants.FILTERS.TRACKS:
                this.toggleBtn(this.rootView.findViewById(R.id.search_start_tracks));
                break;

            case SearchConstants.FILTERS.PLAYLISTS:
                this.toggleBtn(this.rootView.findViewById(R.id.search_start_playlists));
                break;

            case SearchConstants.FILTERS.USERS:
                this.toggleBtn(this.rootView.findViewById(R.id.search_start_users));
                break;
        }
    }

    @Override
    public View getRootView() {
        return this.rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }

    private void attachButtonListener(View.OnClickListener listener) {
        for(int btn = 0; btn < buttonIds.length; btn++) {
            this.buttons.add(this.rootView.findViewById(this.buttonIds[btn]));
            this.buttons.get(btn).setOnClickListener(listener);
        }
    }

    private void processBtnClick(View v) {
        if(this.toggledBtn != null) {
            if (v.getId() != this.toggledBtn.getId()) {
                this.toggleBtn(v);
            }
            else {
                this.untoggleBtn(v);
            }
        }
        else {
            this.toggleBtn(v);
        }
    }

    private void toggleBtn(View btn) {
        this.rootView.findViewById(btn.getId()).setBackgroundResource(R.drawable.rounded_button_toggled);
        if(this.toggledBtn != null) {
            this.rootView.findViewById(this.toggledBtn.getId()).setBackgroundResource(R.drawable.rounded_button_untoggled);
        }
        this.toggledBtn = this.rootView.findViewById(btn.getId());
        switch (btn.getId()) {
            case R.id.search_start_tracks:
                this.searchStartBtnClickListener.updateFilter(SearchConstants.FILTERS.TRACKS);
                break;

            case R.id.search_start_playlists:
                this.searchStartBtnClickListener.updateFilter(SearchConstants.FILTERS.PLAYLISTS);
                break;

            case R.id.search_start_users:
                this.searchStartBtnClickListener.updateFilter(SearchConstants.FILTERS.USERS);
                break;
        }
    }

    private void untoggleBtn(View btn) {
        this.rootView.findViewById(btn.getId()).setBackgroundResource(R.drawable.rounded_button_untoggled);
        this.toggledBtn = null;
        this.searchStartBtnClickListener.updateFilter(SearchConstants.FILTERS.NONE);
    }
}
