package com.example.Sallefy.SallefyUI.Songs.Controller.ListenerInterfaces;

public interface SongFavoriteBtnClick {
    void clicked(String trackId);
}
