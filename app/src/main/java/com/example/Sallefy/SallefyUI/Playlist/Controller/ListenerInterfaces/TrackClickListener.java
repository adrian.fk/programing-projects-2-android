package com.example.Sallefy.SallefyUI.Playlist.Controller.ListenerInterfaces;

import com.example.Sallefy.SallefyUI.Playlist.Model.PlaylistContent;

public interface TrackClickListener {
    void onClicked(PlaylistContent playlists);
}
