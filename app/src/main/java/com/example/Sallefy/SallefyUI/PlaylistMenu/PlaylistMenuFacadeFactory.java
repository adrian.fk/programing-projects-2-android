package com.example.Sallefy.SallefyUI.PlaylistMenu;

public interface PlaylistMenuFacadeFactory {
    PlaylistMenuFacade playlistMenuFacade = new PlaylistMenu();

    static PlaylistMenuFacade create() {
        return playlistMenuFacade;
    }
}
