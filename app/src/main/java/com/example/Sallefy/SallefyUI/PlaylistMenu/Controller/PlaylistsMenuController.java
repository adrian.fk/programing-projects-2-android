package com.example.Sallefy.SallefyUI.PlaylistMenu.Controller;

import androidx.lifecycle.MutableLiveData;

import com.example.Sallefy.SallefyUI.PlaylistMenu.Model.DataPlaylistMenu;
import com.example.Sallefy.SallefyUI.PlaylistMenu.Model.DataPlaylistMenuMapper;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;
import com.example.Sallefy.repositories.Interfaces.CurrentUserRepository;
import com.example.Sallefy.repositories.Interfaces.PlaylistRepository;
import com.example.Sallefy.SallefyUI.PlaylistMenu.View.PlaylistsMenuViewI;
import com.example.Sallefy.repositories.factories.PlaylistRepositoryFactory;
import com.example.Sallefy.repositories.factories.UserRepositoryFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class PlaylistsMenuController {
    private PlaylistMenuMediator mediator;
    private DataPlaylistMenu dataPlaylistMenu;
    private PlaylistsMenuViewI view;

    private PlaylistRepository playlistRepository;
    private CurrentUserRepository currentUserRepository;

    private DataPlaylistMenuMapper dataMapper;

    private MutableLiveData<Collection<PlaylistDTO>> currentUserFollowingPlaylistsCache;
    private MutableLiveData<List<PlaylistDTO>> currentUserPlaylistCache;


    public PlaylistsMenuController(PlaylistMenuMediator mediator,
                                   PlaylistsMenuViewI view,
                                   DataPlaylistMenu dataPlaylistMenu,
                                   PlaylistRepositoryFactory playlistRepositoryFactory,
                                   UserRepositoryFactory userRepositoryFactory) {
        this.mediator = mediator;
        this.dataPlaylistMenu = dataPlaylistMenu;
        this.view = view;
        this.playlistRepository = playlistRepositoryFactory.create();
        this.currentUserRepository = userRepositoryFactory.createCurrentUserRepository();


        this.view.attachMenuItemClickListener(v -> {
            processMenuItemClicked(v.getTitle().toString());
            return false;
        });
        updateMenuContent();
    }

    public void updateMenuContent() {
        this.dataPlaylistMenu = new DataPlaylistMenu();
        this.dataMapper = new DataPlaylistMenuMapper(this.dataPlaylistMenu);

        if (this.currentUserPlaylistCache == null) {
            this.currentUserPlaylistCache = this.playlistRepository
                    .getCurrentUserPlaylists();

            this.currentUserPlaylistCache
                    .observe(this.mediator.provideOwner(), this::processCurrentUserPlaylistsResponse);
        }
        else {
            this.playlistRepository
                    .getCurrentUserPlaylists();
        }

        if (this.currentUserFollowingPlaylistsCache == null) {
            this.currentUserFollowingPlaylistsCache = this.currentUserRepository
                    .getCurrentUserFollowingPlaylists();

            this.currentUserFollowingPlaylistsCache
                    .observe(this.mediator.provideOwner(), this::processCurrentUserFollowingPlaylistsResponse);
        }
        else {
            this.currentUserRepository
                    .getCurrentUserFollowingPlaylists();
        }

    }

    private void processCurrentUserFollowingPlaylistsResponse(Collection<PlaylistDTO> playlistDTOS) {
        processCurrentUserPlaylistsResponse(new ArrayList<>(playlistDTOS));
    }

    private void processCurrentUserPlaylistsResponse(List<PlaylistDTO> playlistDTOS) {
        for (PlaylistDTO playlist : playlistDTOS) {
            this.dataMapper.mapPlaylistDTO(playlist);
        }
        this.view.setContent(this.dataPlaylistMenu.getPlaylists());
    }

    private void processMenuItemClicked(String playlistTitle) {
        this.mediator.receiveSelectedPlaylist(
                this.dataPlaylistMenu.getPlaylistByName(playlistTitle).id,
                playlistTitle
        );
    }
}
