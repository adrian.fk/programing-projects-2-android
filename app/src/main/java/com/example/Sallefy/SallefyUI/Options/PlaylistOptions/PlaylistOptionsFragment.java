package com.example.Sallefy.SallefyUI.Options.PlaylistOptions;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacade;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacadeFactory;
import com.example.Sallefy.SallefyUI.Options.PlaylistOptions.Controller.PlaylistOptionsController;
import com.example.Sallefy.SallefyUI.Options.PlaylistOptions.Controller.PlaylistOptionsMediator;
import com.example.Sallefy.SallefyUI.Options.PlaylistOptions.Model.Data;
import com.example.Sallefy.SallefyUI.Options.PlaylistOptions.View.PlaylistOptionsView;
import com.example.Sallefy.SallefyUI.PlaylistMenu.PlaylistMenuFacade;
import com.example.Sallefy.SallefyUI.PlaylistMenu.PlaylistMenuFacadeFactory;
import com.example.Sallefy.Utils.Constants;
import com.example.Sallefy.repositoryImpl.factoriesImpl.PlaylistApiRepositoryFactory;

public class PlaylistOptionsFragment extends Fragment implements PlaylistOptionsMediator {
    private PlaylistOptionsController controller;
    private PlaylistOptionsView view;
    private Data playlistData;

    public PlaylistOptionsFragment() {
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = new PlaylistOptionsView(inflater, container);
        this.view.displayPlaylistOptions(false);

        Animation fadeInAnim = AnimationUtils.loadAnimation(this.view.getRootView().getContext(), R.anim.fade_in);
        this.view.getRootView().startAnimation(fadeInAnim);

        Bundle specifications = getArguments();
        if(specifications != null) {
            String playlistID = specifications.getString(Constants.PLAYLIST_SPECIFICATIONS.PLAYLIST_ID);
            if(null != playlistID) {
                this.playlistData = new Data(Long.parseLong(playlistID));

                PlaylistApiRepositoryFactory playlistApiRepositoryFactory
                        = new PlaylistApiRepositoryFactory();

                this.controller = new PlaylistOptionsController(
                        this,
                        this.view,
                        this.playlistData,
                        playlistApiRepositoryFactory
                );
                this.controller.init();
            }
        }

        return this.view.getRootView();
    }

    @Override
    public LifecycleOwner provideOwner() {
        return this;
    }

    @Override
    public void enqueueSong(long id, String title, String owner, String imgUrl, String srcUrl) {
        MusicPlayerFacade musicPlayerFacade = MusicPlayerFacadeFactory.create();
        musicPlayerFacade.enqueueSong(id, title, owner, imgUrl, srcUrl);
    }

    @Override
    public void navToUserProfile(String username) {
        Bundle b = new Bundle();
        b.putString(Constants.USER_SPECIFICATIONS.USER_NAME, username);

        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_user_profile, b);
    }

    @Override
    public void finishPlaylistOptions() {
        PlaylistMenuFacade playlistMenuFacade = PlaylistMenuFacadeFactory.create();
        playlistMenuFacade.updatePlaylistMenu();
        NavController navController = Navigation.findNavController(this.view.getRootView());
        navController.navigate(R.id.nav_home);
    }

    @Override
    public void editPlaylist(String id) {
        Bundle b = new Bundle();
        b.putString(Constants.PLAYLIST_SPECIFICATIONS.PLAYLIST_ID, id);
        b.putString(Constants.PLAYLIST_SPECIFICATIONS.PLAYLIST_NAME, this.playlistData.getCurrentPlaylist().getName());

        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_playlist_rename, b);
    }
}
