package com.example.Sallefy.SallefyUI.UserPlaylists.View;

import com.example.Sallefy.SallefyUI.UserPlaylists.Controller.ListenerInterfaces.PlaylistClickListener;
import com.example.Sallefy.Utils.ViewInterface;

import java.util.ArrayList;

public interface UserPlaylistsViewI<T> extends ViewInterface {
    void attachPlaylistClickListener(PlaylistClickListener listener);

    void attachPlaylistLongClickListener(PlaylistClickListener listener);

    void displayContent(ArrayList<T> playlistItems);

    void toastMsg(String msg);

    void toastError(String errMsg);
}
