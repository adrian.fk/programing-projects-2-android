package com.example.Sallefy.SallefyUI.MusicPlayer.View;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.ListenerInterfaces.ProgressBarUpdatedCallback;

import de.hdodenhof.circleimageview.CircleImageView;

public class FullscreenPlayerView implements FullscreenPlayerViewI{
    private View rootView;

    private CircleImageView songImage;
    private TextView title;
    private TextView uploader;
    private ImageButton playButton;
    private ImageButton prevButton;
    private ImageButton nextButton;
    private ImageButton repeatButton;
    private ImageButton shuffleButton;
    private ImageButton listingButton;
    private SeekBar progressBar;


    public FullscreenPlayerView(LayoutInflater inflater, ViewGroup container) {
        this.rootView = inflater.inflate(R.layout.fragment_fullscreen_player, container, false);
        this.songImage = getRootView().findViewById(R.id.fullscreen_player_img);
        this.title = getRootView().findViewById(R.id.fullscreen_player_title);
        this.uploader = getRootView().findViewById(R.id.fullscreen_player_uploader);
        this.playButton = getRootView().findViewById(R.id.fullscreen_player_play);
        this.prevButton = getRootView().findViewById(R.id.fullscreen_player_previous);
        this.nextButton = getRootView().findViewById(R.id.fullscreen_player_next);
        this.repeatButton = getRootView().findViewById(R.id.fullscreen_player_repeat);
        this.shuffleButton = getRootView().findViewById(R.id.fullscreen_player_shuffle);
        this.listingButton = getRootView().findViewById(R.id.fullscreen_player_listing_btn);
        this.progressBar = getRootView().findViewById(R.id.fullscreen_player_seekbar);
    }


    public void setSongImage(String imgUrl) {
        Glide.with(getRootView())
                .asBitmap()
                .load(imgUrl)
                .into(this.songImage);
    }


    @Override
    public View getRootView() {
        return this.rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }

    public void attachImageLongPressListener(View.OnLongClickListener listener){
        this.songImage.setOnLongClickListener(listener);
    }

    public void attachProgressBarListener(SeekBar.OnSeekBarChangeListener listener) {
        this.progressBar.setOnSeekBarChangeListener(listener);
    }

    public void attachPlayButtonListener(View.OnClickListener listener) {
        this.playButton.setOnClickListener(listener);
    }

    public void attachPrevButtonListener(View.OnClickListener listener) {
        this.prevButton.setOnClickListener(listener);
    }

    public void attachNextButtonListener(View.OnClickListener listener) {
        this.nextButton.setOnClickListener(listener);
    }

    public void attachRepeatButtonListener(View.OnClickListener listener) {
        this.repeatButton.setOnClickListener(listener);
    }

    public void attachShuffleButtonListener(View.OnClickListener listener) {
        this.shuffleButton.setOnClickListener(listener);
    }

    @Override
    public void attachListingButtonListener(View.OnClickListener listener) {
        this.listingButton.setOnClickListener(listener);
    }

    @Override
    public SeekBar getSeekbar() {
        return this.progressBar;
    }


    public void setTextTitle(String input) {
        this.title.setText(input);
    }

    public void setTextUploader(String input) {
        this.uploader.setText(input);
    }


    public void setNextBtnClicked(Boolean clicked) {
        if(clicked) {
            this.nextButton.setImageResource(R.drawable.ic_next_pressed);
        }
        else {
            this.nextButton.setImageResource(R.drawable.ic_next);
        }
    }

    public void setPrevBtnClicked(Boolean clicked) {
        if (clicked) {
            this.prevButton.setImageResource(R.drawable.ic_next_pressed);
        }
        else {
            this.prevButton.setImageResource(R.drawable.ic_next);
        }
    }


    public void setShuffleBtnState(Boolean state) {
        if (state) {
            this.shuffleButton.setImageResource(R.drawable.ic_shuffle_color);
        }
        else {
            this.shuffleButton.setImageResource(R.drawable.ic_shuffle);
        }
    }


    public void setRepeatBtnState(Boolean state) {
        if (state) {
            this.repeatButton.setImageResource(R.drawable.ic_repeat_color);
        }
        else {
            this.repeatButton.setImageResource(R.drawable.ic_repeat);
        }
    }

    public void setPlayState(Boolean isPlaying) {
        if(isPlaying) {
            this.playButton.setImageResource(R.drawable.ic_pause_button);
        }
        else {
            this.playButton.setImageResource(R.drawable.ic_play_button);
        }
    }

    public void attachProgressBarChangeCallback(ProgressBarUpdatedCallback listener) {
        this.progressBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {}

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { listener.callback(seekBar.getProgress()); }
        });
    }


    public void updateProgressBar(int progress) {
        this.progressBar.setProgress(progress);
    }

    public void setDurationProgressBar(int duration){
        this.progressBar.setMax(duration);
    }
}
