package com.example.Sallefy.SallefyUI.UploadSong.Controller;

import com.example.Sallefy.SallefyUI.UploadSong.Model.DataUpload;
import com.example.Sallefy.SallefyUI.UploadSong.View.UploadSongViewI;
import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.ImageUploadDTO;
import com.example.Sallefy.repositories.Interfaces.ImageRepository;
import com.example.Sallefy.repositories.Interfaces.TrackRepository;
import com.example.Sallefy.repositories.factories.ImageRepositoryFactory;
import com.example.Sallefy.repositories.factories.TrackRepositoryFactory;

public class UploadSongController {
    private UploadSongMediator mediator;
    private UploadSongViewI view;
    private DataUpload dataUpload;

    private TrackRepository trackRepository;
    private final ImageRepositoryFactory imageRepositoryFactory;


    public UploadSongController(UploadSongMediator mediator,
                                DataUpload dataUpload,
                                UploadSongViewI view,
                                TrackRepositoryFactory trackRepositoryFactory, ImageRepositoryFactory imageRepositoryFactory) {
        this.mediator = mediator;
        this.dataUpload = dataUpload;
        this.view = view;
        this.trackRepository = trackRepositoryFactory.create();
        this.imageRepositoryFactory = imageRepositoryFactory;

        this.view.attachChooseFileListener(this::chooseFileClicked);
        this.view.attachUploadBtnListener(this::uploadBtnClicked);
        this.view.attachTextChangedListener(e -> evaluateUploadRdy());
        this.view.attachSongImgListener(v -> songImgClicked());
    }

    private void songImgClicked() {
        this.mediator.openImgFile();
    }

    private void uploadBtnClicked() {
        this.view.setUploadBtnState(false);
        if (this.dataUpload.hasNewImg()) {
            ImageRepository imageRepository = this.imageRepositoryFactory.create();
            imageRepository
                    .uploadImage(this.dataUpload.getSongImgUri())
                    .observe(this.mediator.provideOwner(), this::processImgUpload);
        }
        else {
            uploadSong();
        }
    }

    private void processImgUpload(ImageUploadDTO imageUploadDTO) {
        if (imageUploadDTO.isStatus()) {
            //Successfully uploaded
            this.dataUpload.setUploadedSongUrl(imageUploadDTO.getImageUrl());
        }
        else {
            this.view.displayNoImgAdded();
        }
        uploadSong();
    }

    private void uploadSong() {
        this.dataUpload.setTitle(
                this.view.getSongName()
        );
        TrackDTO trackDTO = new TrackDTO(true, "E_NO_ERROR");
        trackDTO.setName(this.dataUpload.getTitle());
        trackDTO.setThumbnail(
                this.dataUpload.hasNewImg() ?
                        this.dataUpload.getUploadedSongUrl() : ""
        );
        this.trackRepository
                .createTrack(
                        trackDTO,
                        this.dataUpload.getContentUri()
                )
                .observe(this.mediator.provideOwner(), this::processSongUploaded);
    }

    private void processSongUploaded(TrackDTO trackDTO) {
        if (trackDTO.isStatus()) {
            this.mediator.onSongUploaded(String.valueOf(trackDTO.getId()));
        }
        this.view.setUploadBtnState(false);
    }

    private void chooseFileClicked() {
        this.mediator.openFile();
    }

    public void evaluateUploadRdy() {
        if(this.dataUpload.hasContent() && this.view.getSongName().length() > 0) {
            this.view.setUploadBtnState(true);
        }
        else {
            this.view.setUploadBtnState(false);
        }
    }


}
