package com.example.Sallefy.SallefyUI.PlaylistMenu.Model;

import java.util.ArrayList;
import java.util.HashMap;

public class DataPlaylistMenu {
    private ArrayList<Playlist> playlists;
    private HashMap<String, Integer> playlistIdToArrayIndexMap;
    private HashMap<String, Integer> playlistNameToArrayIndexMap;

    public DataPlaylistMenu() {
        this.playlists = new ArrayList<>();
        this.playlistIdToArrayIndexMap = new HashMap<>();
        this.playlistNameToArrayIndexMap = new HashMap<>();
    }

    public void addPlaylist(Playlist p) {
        if(!playlistIdToArrayIndexMap.containsKey(p.id)) {
            this.playlistIdToArrayIndexMap.put(p.id, this.playlists.size());
            this.playlistNameToArrayIndexMap.put(p.title, this.playlists.size());
            this.playlists.add(p);
        }
    }

    public ArrayList<Playlist> getPlaylists() {
        return playlists;
    }

    public Playlist getPlaylistById(String id) {
        return this.playlists.get(
                this.playlistIdToArrayIndexMap.get(id)
        );
    }

    public Playlist getPlaylistByName(String id) {
        return this.playlists.get(
                this.playlistNameToArrayIndexMap.get(id)
        );
    }

    public Playlist createPlaylist(
            String id,
            String title,
            String thumbnailUrl) {
        return new Playlist(id, title, thumbnailUrl);
    }

    static public class Playlist {
        public final String id;
        public final String title;
        public final String thumbnailUrl;

        public Playlist(String id, String title, String thumbnailUrl) {
            this.id = id;
            this.title = title;
            this.thumbnailUrl = thumbnailUrl;
        }
    }
}
