package com.example.Sallefy.SallefyUI.Search.Controller;


import com.example.Sallefy.Utils.FeaturedMediatorI;

public interface SearchMediator extends FeaturedMediatorI {
    void receivePlaylist(String id, String playlistName);

    void openSongOptions(String id);

    void openPlaylistOptions(String id);

    void openUserProfile(String id);
}
