package com.example.Sallefy.SallefyUI.UserPlaylists.View;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.UserPlaylists.Controller.ListenerInterfaces.PlaylistClickListener;

import java.util.ArrayList;

public class UserPlaylistsView implements UserPlaylistsViewI {
    private View rootView;
    private Bundle viewState;

    private PlaylistClickListener playlistClickListener;
    private PlaylistClickListener playlistLongClickListener;

    private RecyclerView playlistsView;


    public UserPlaylistsView(@NonNull LayoutInflater inflater, ViewGroup container) {
        this.rootView = inflater.inflate(R.layout.fragment_user_playlists, container, false);
        findViews();
    }

    private void findViews() {
        this.playlistsView = getRootView().findViewById(R.id.user_playlists_fragment_list);
    }

    @Override
    public View getRootView() {
        return this.rootView;
    }

    @Override
    public Bundle getViewState() {
        return this.viewState;
    }

    @Override
    public void attachPlaylistClickListener(PlaylistClickListener listener) {
        this.playlistClickListener = listener;
    }

    @Override
    public void attachPlaylistLongClickListener(PlaylistClickListener listener) {
        this.playlistLongClickListener = listener;
    }

    @Override
    public void displayContent(ArrayList playlistItems) {
        UserPlaylistsViewAdapter viewAdapter = new UserPlaylistsViewAdapter(
                this.rootView.getContext(),
                playlistItems,
                this.playlistClickListener,
                this.playlistLongClickListener
        );
        this.playlistsView.setAdapter(viewAdapter);
        this.playlistsView.setLayoutManager(new LinearLayoutManager(this.rootView.getContext()));
    }

    @Override
    public void toastMsg(String msg) {
        Toast.makeText(getRootView().getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void toastError(String errMsg) {
        Toast.makeText(getRootView().getContext(), "ERROR: " + errMsg, Toast.LENGTH_SHORT).show();
    }
}
