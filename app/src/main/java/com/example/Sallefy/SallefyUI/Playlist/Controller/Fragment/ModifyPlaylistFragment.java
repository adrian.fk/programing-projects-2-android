package com.example.Sallefy.SallefyUI.Playlist.Controller.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Playlist.Controller.ModifyPlaylistController;
import com.example.Sallefy.SallefyUI.Playlist.Controller.ModifyPlaylistMediator;
import com.example.Sallefy.SallefyUI.Playlist.Model.DataEditablePlaylist;
import com.example.Sallefy.SallefyUI.Playlist.View.ModifyPlaylistView;
import com.example.Sallefy.SallefyUI.PlaylistMenu.PlaylistMenuFacade;
import com.example.Sallefy.SallefyUI.PlaylistMenu.PlaylistMenuFacadeFactory;
import com.example.Sallefy.Utils.Constants;
import com.example.Sallefy.repositoryImpl.factoriesImpl.ImageApiRepositoryFactory;
import com.example.Sallefy.repositoryImpl.factoriesImpl.PlaylistApiRepositoryFactory;

import java.util.Objects;

public class ModifyPlaylistFragment extends Fragment implements ModifyPlaylistMediator {
    private final int REQ_GET_IMG_URI = 1;


    private ModifyPlaylistView view;
    private ModifyPlaylistController controller;
    private DataEditablePlaylist dataPlaylist;

    public ModifyPlaylistFragment() {
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = new ModifyPlaylistView(inflater, container);
        this.dataPlaylist = new DataEditablePlaylist();

        Animation fadeInAnim = AnimationUtils.loadAnimation(this.view.getRootView().getContext(), R.anim.fade_in);
        this.view.getRootView().startAnimation(fadeInAnim);

        Bundle specifications = getArguments();
        if (specifications != null) {
            this.dataPlaylist.setPlaylistId(
                    Long.parseLong(
                            Objects.requireNonNull(specifications.getString(Constants.PLAYLIST_SPECIFICATIONS.PLAYLIST_ID))
                    )
            );
            String playlistName = specifications.getString(Constants.PLAYLIST_SPECIFICATIONS.PLAYLIST_NAME);
            if (playlistName != null) {
                this.dataPlaylist.setPlaylistName(playlistName);
                this.view.displayPlaylistTitle(playlistName);
            }

            ImageApiRepositoryFactory imageApiRepositoryFactory = new ImageApiRepositoryFactory();
            PlaylistApiRepositoryFactory playlistApiRepositoryFactory = new PlaylistApiRepositoryFactory();

            this.controller = new ModifyPlaylistController(
                    this,
                    this.dataPlaylist,
                    this.view,
                    imageApiRepositoryFactory,
                    playlistApiRepositoryFactory
            );
        }

        return this.view.getRootView();
    }

    @Override
    public LifecycleOwner provideOwner() {
        return this;
    }

    @Override
    public void conclude() {
        PlaylistMenuFacade playlistMenuFacade = PlaylistMenuFacadeFactory.create();
        playlistMenuFacade.updatePlaylistMenu();

        NavController navController = Navigation.findNavController(this.view.getRootView());

        if (!navController.popBackStack()) {
            navController.navigate(R.id.nav_home);
        }
    }

    @Override
    public void openImgFile() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");

        startActivityForResult(intent, REQ_GET_IMG_URI);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case Activity.RESULT_CANCELED:
                this.view.displayNoImgAdded();
                break;

            case REQ_GET_IMG_URI:
                if(null != data) {
                    //Got image file reference from Android FileExplorer successfully
                    this.dataPlaylist.setPlaylistImgUri(data.getData());
                    this.view.displayPlaylistImg(this.dataPlaylist.getPlaylistImgUri());
                }
                break;
        }
    }
}
