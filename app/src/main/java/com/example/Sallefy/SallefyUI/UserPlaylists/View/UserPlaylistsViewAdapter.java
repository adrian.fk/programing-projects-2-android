package com.example.Sallefy.SallefyUI.UserPlaylists.View;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.UserPlaylists.Controller.ListenerInterfaces.PlaylistClickListener;
import com.example.Sallefy.SallefyUI.UserPlaylists.Model.PlaylistsContent;
import com.example.Sallefy.Utils.Constants;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserPlaylistsViewAdapter extends RecyclerView.Adapter<UserPlaylistsViewAdapter.UserPlaylistViewHolder> {
    private Context context;

    private PlaylistClickListener playlistClickListener;
    private PlaylistClickListener playlistLongClickListener;

    private ArrayList<String> playlistIds;
    private ArrayList<String> playlistTitles;
    private ArrayList<String> playlistsThumbnails;

    public UserPlaylistsViewAdapter(Context context,
                                    ArrayList<PlaylistsContent.Playlist> listItems,
                                    PlaylistClickListener playlistClickListener,
                                    PlaylistClickListener playlistLongClickListener) {
        this.context = context;
        this.playlistClickListener = playlistClickListener;
        this.playlistLongClickListener = playlistLongClickListener;
        this.playlistIds = new ArrayList<>();
        this.playlistTitles = new ArrayList<>();
        this.playlistsThumbnails = new ArrayList<>();

        for (PlaylistsContent.Playlist playlistListItem : listItems) {
            this.playlistIds.add(playlistListItem.id);
            this.playlistTitles.add(playlistListItem.title);
            this.playlistsThumbnails.add(playlistListItem.imgUrl);
        }
    }

    @NonNull
    @Override
    public UserPlaylistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View holderView = LayoutInflater.from(parent.getContext()).inflate(R.layout.playlist_listitem, parent, false);
        return new UserPlaylistViewHolder(holderView);
    }

    @Override
    public void onBindViewHolder(@NonNull UserPlaylistViewHolder holder, int position) {
        if (this.playlistsThumbnails.get(position) == null) {
            this.playlistsThumbnails.set(position, Constants.COSMETICS.DEFAULT_PLAYLIST_IMG);
        }

        Glide.with(this.context)
                .asBitmap()
                .load(this.playlistsThumbnails.get(position))
                .into(holder.image);
        holder.title.setText(this.playlistTitles.get(position));
        holder.parentLayout.setOnClickListener(v -> this.playlistClickListener.playlistClicked(this.playlistIds.get(position)));
        holder.parentLayout.setOnLongClickListener(
                v -> {
                    this.playlistLongClickListener.playlistClicked(this.playlistIds.get(position));
                    return true;
                });
    }

    @Override
    public int getItemCount() {
        return playlistIds.size();
    }

    static class UserPlaylistViewHolder extends RecyclerView.ViewHolder{
        private CircleImageView image;
        private TextView title;
        private RelativeLayout parentLayout;

        public UserPlaylistViewHolder(@NonNull View itemView) {
            super(itemView);
            this.image = itemView.findViewById(R.id.playlist_item_image);
            this.title = itemView.findViewById(R.id.playlist_item_title);
            this.parentLayout = itemView.findViewById(R.id.playlist_item);
        }
    }
}
