package com.example.Sallefy.SallefyUI.Songs.Controller.ListenerInterfaces;

public interface SongItemClickListener {
    void registerClick(String id, int position);
}
