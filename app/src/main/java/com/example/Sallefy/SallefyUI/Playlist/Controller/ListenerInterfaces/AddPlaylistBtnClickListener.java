package com.example.Sallefy.SallefyUI.Playlist.Controller.ListenerInterfaces;

public interface AddPlaylistBtnClickListener {
    void clicked(String playlistName, String playlistDescription);
}
