package com.example.Sallefy.SallefyUI.Search;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.Navigation;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Search.Controller.SearchController;
import com.example.Sallefy.SallefyUI.Search.Controller.SearchMediator;
import com.example.Sallefy.SallefyUI.Search.Model.DataSearch;
import com.example.Sallefy.SallefyUI.Search.Model.DataSearchFactory;
import com.example.Sallefy.SallefyUI.Search.View.SearchView;
import com.example.Sallefy.Utils.Constants;
import com.example.Sallefy.repositoryImpl.factoriesImpl.SearchApiRepositoryFactory;
import com.example.Sallefy.repositoryImpl.factoriesImpl.TrackApiRepositoryFactory;
import com.example.Sallefy.repositoryImpl.factoriesImpl.UserApiRepositoryFactory;


public class SearchFragment extends Fragment implements SearchMediator {

    private SearchController controller;
    private SearchView view;


    private DataSearch dataSearch;


    public SearchFragment() {}


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.dataSearch = DataSearchFactory.create();
        this.dataSearch.setState(DataSearch.STATE_SEARCHING);
        this.view = new SearchView(inflater, container);

        Animation fadeInAnim = AnimationUtils.loadAnimation(this.view.getRootView().getContext(), R.anim.fade_in);
        this.view.getRootView().startAnimation(fadeInAnim);

        SearchApiRepositoryFactory searchApiRepositoryFactory = new SearchApiRepositoryFactory();
        UserApiRepositoryFactory userApiRepositoryFactory = new UserApiRepositoryFactory();
        TrackApiRepositoryFactory trackApiRepositoryFactory = new TrackApiRepositoryFactory();
        this.controller = new SearchController(
                this,
                this.view,
                searchApiRepositoryFactory,
                trackApiRepositoryFactory,
                userApiRepositoryFactory
        );
        this.dataSearch.keyword().observe(getViewLifecycleOwner(), controller::search);

        return view.getRootView();
    }

    @Override
    public void onStop() {
        super.onStop();
        this.dataSearch.setState(DataSearch.STATE_EXIT);
    }

    @Override
    public LifecycleOwner provideOwner() {
        return this;
    }

    @Override
    public void receivePlaylist(String id, String playlistName) {
        Bundle b = new Bundle();
        b.putString(Constants.PLAYLIST_SPECIFICATIONS.PLAYLIST_ID, id);
        b.putString(Constants.PLAYLIST_SPECIFICATIONS.PLAYLIST_NAME, playlistName);

        Navigation
                .findNavController(requireActivity(), R.id.nav_host_fragment)
                .navigate(R.id.nav_to_playlist, b);

    }

    @Override
    public void openSongOptions(String id) {
        Bundle b = new Bundle();
        b.putString(Constants.SONG_SPECIFICATIONS.SONG_ID, id);

        Navigation
                .findNavController(requireActivity(), R.id.nav_host_fragment)
                .navigate(R.id.nav_song_options, b);
    }

    @Override
    public void openPlaylistOptions(String id) {
        Bundle b = new Bundle();
        b.putString(Constants.PLAYLIST_SPECIFICATIONS.PLAYLIST_ID, id);

        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_playlist_options, b);
    }

    @Override
    public void openUserProfile(String username) {
        Bundle b = new Bundle();
        b.putString(Constants.USER_SPECIFICATIONS.USER_NAME, username);

        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_user_profile, b);
    }
}
