package com.example.Sallefy.SallefyUI.Search.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchContent {

    private final List<SearchItem> items = new ArrayList<SearchItem>();
    private final Map<String, SearchItem> itemMap = new HashMap<String, SearchItem>();

    public void addItem(SearchItem item) {
        items.add(item);
        itemMap.put(item.id, item);
    }

    public SearchItem createSearchItem(String type,
                                       String id,
                                       String title,
                                       String uploader,
                                       String imgUrl,
                                       String srcUrl,
                                       boolean favorite) {
        return new SearchItem(type, id, title, uploader, imgUrl, srcUrl, favorite);
    }

    public List<SearchItem> getItems() {
        return items;
    }

    public Map<String, SearchItem> getItemMap() {
        return itemMap;
    }

    public SearchItem getItemById(String id) {
        return this.itemMap.get(id);
    }

    public String getItemId(SearchItem item) {
        return item.id;
    }

    public static class SearchItem {
        public final String type;
        public final String id;
        public final String title;
        public final String uploader;
        public final String imgUrl;
        public final String srcUrl;
        public final boolean favorite;

        public SearchItem(String type, String id, String title, String uploader, String imgUrl, String srcUrl, boolean favorite) {
            this.type = type;
            this.id = id;
            this.title = title;
            this.uploader = uploader;
            this.imgUrl = imgUrl;
            this.srcUrl = srcUrl;
            this.favorite = favorite;
        }

        @Override
        public String toString() {
            return title;
        }
    }
}
