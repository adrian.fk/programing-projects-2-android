package com.example.Sallefy.SallefyUI.MusicPlayer.Model;

public enum PlaybackLocation {
    REMOTE,
    LOCAL
}
