package com.example.Sallefy.SallefyUI.Playlist.Controller.Fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacade;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacadeFactory;
import com.example.Sallefy.SallefyUI.Playlist.Controller.PlaylistController;
import com.example.Sallefy.SallefyUI.Playlist.Controller.PlaylistMediator;
import com.example.Sallefy.SallefyUI.Playlist.Model.DataPlaylist;
import com.example.Sallefy.SallefyUI.Playlist.View.PlaylistView;
import com.example.Sallefy.SallefyUI.PlaylistMenu.PlaylistMenuFacade;
import com.example.Sallefy.SallefyUI.PlaylistMenu.PlaylistMenuFacadeFactory;
import com.example.Sallefy.Utils.Constants;
import com.example.Sallefy.repositoryImpl.factoriesImpl.PlaylistApiRepositoryFactory;


public class PlaylistFragment extends Fragment implements PlaylistMediator {
    private static final String TAG = "PlaylistFragment";

    private MusicPlayerFacade musicPlayer = MusicPlayerFacadeFactory.create();
    private PlaylistMenuFacade playlistMenu = PlaylistMenuFacadeFactory.create();

    private PlaylistView view;
    private PlaylistController controller;
    private DataPlaylist dataPlaylist;



    public PlaylistFragment() {
        // Required empty public constructor for NavController
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: Creating view");
        this.view = new PlaylistView(inflater, container);

        Animation fadeInAnim = AnimationUtils.loadAnimation(this.view.getRootView().getContext(), R.anim.fade_in);
        this.view.getRootView().startAnimation(fadeInAnim);

        this.dataPlaylist = new DataPlaylist();

        PlaylistApiRepositoryFactory playlistApiRepositoryFactory = new PlaylistApiRepositoryFactory();

        Log.d(TAG, "onCreateView: Creating controller");
        this.controller = new PlaylistController(
                this,
                this.view,
                playlistApiRepositoryFactory
        );

        Bundle playlistSpecifications = getArguments();
        if(playlistSpecifications != null) {
            String playlistId = playlistSpecifications.getString(Constants.PLAYLIST_SPECIFICATIONS.PLAYLIST_ID);
            String playlistName = playlistSpecifications.getString(Constants.PLAYLIST_SPECIFICATIONS.PLAYLIST_NAME);

            if(playlistId != null) {
                if(playlistName != null) {
                    this.dataPlaylist.setPlaylistName(playlistName);
                }
                this.dataPlaylist.setPlaylistId(
                        Long.parseLong(playlistId)
                );
                this.controller.init(this.dataPlaylist);
            }
        }
        else {
            if(this.playlistMenu.hasSelectedItem()) {
                   this.dataPlaylist.setPlaylistId(
                           Long.parseLong(this.playlistMenu.retrieveSelectedItem().getId())
                   );
                   this.controller.init(this.dataPlaylist);
            }
            else {
                if(this.dataPlaylist.getPlaylistId() != null) {
                    this.controller.init(this.dataPlaylist);
                }
                else {
                    navHome();
                }
            }
        }

        return this.view.getRootView();
    }

    private void navHome() {
        Navigation
                .findNavController(requireActivity(), R.id.nav_host_fragment)
                .navigate(R.id.nav_home);
    }


    @Override
    public LifecycleOwner provideOwner() {
        return this;
    }

    @Override
    public void openPlaylistOptions(String id) {
        Bundle b = new Bundle();
        b.putString(Constants.PLAYLIST_SPECIFICATIONS.PLAYLIST_ID, id);

        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_playlist_options, b);

    }

    @Override
    public void playSong(long id) {
        this.musicPlayer.playSong(id);
    }

    @Override
    public void enqueueSong(String id,  String title, String uploader, String thumbnail, String srcUrl) {
        this.musicPlayer.enqueueSong(
                Long.parseLong(id),
                title,
                uploader,
                thumbnail,
                srcUrl
        );
    }

    @Override
    public void openSongOptions(String id) {
        Bundle b = new Bundle();
        b.putString(Constants.SONG_SPECIFICATIONS.SONG_ID, id);

        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_song_options, b);
    }

    @Override
    public void navBack() {
        Navigation
                .findNavController(this.view.getRootView())
                .popBackStack();
    }
}
