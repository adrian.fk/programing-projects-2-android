package com.example.Sallefy.SallefyUI.Home.View.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Home.Controller.ListenerInterfaces.PlaylistClickListener;
import com.example.Sallefy.SallefyUI.Home.Controller.ListenerInterfaces.PlaylistLongClickListener;
import com.example.Sallefy.SallefyUI.Home.Model.PlaylistHomeContent;
import com.example.Sallefy.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class PlaylistHomeViewAdapter extends RecyclerView.Adapter<PlaylistHomeViewAdapter.PlaylistViewHolder>{
    private static final String TAG = "PlaylistViewAdapter";

    private PlaylistClickListener playlistClickListener;
    private PlaylistLongClickListener playlistLongClickListener;

    private ArrayList<String> idPlaylists;
    private ArrayList<String> playlistTitles;
    private ArrayList<String> uploaderNames;
    private ArrayList<String> playlistThumbnails;


    private Context context;

    public PlaylistHomeViewAdapter(Context context,
                                   List<PlaylistHomeContent.PlaylistItem> items,
                                   PlaylistClickListener playlistClickListener,
                                   PlaylistLongClickListener playlistLongClickListener) {
        this.idPlaylists = new ArrayList<>();
        this.playlistTitles = new ArrayList<>();
        this.uploaderNames = new ArrayList<>();
        this.playlistThumbnails = new ArrayList<>();
        this.playlistClickListener = playlistClickListener;
        this.playlistLongClickListener = playlistLongClickListener;
        this.context = context;

        for(PlaylistHomeContent.PlaylistItem item : items) {
            this.idPlaylists.add(item.id);
            this.playlistTitles.add(item.title);
            this.uploaderNames.add(item.uploader);
            this.playlistThumbnails.add(item.imgUrl);
        }
    }

    @NonNull
    @Override
    public PlaylistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.playlist_listitem, parent, false);
        PlaylistViewHolder holder = new PlaylistViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull PlaylistViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: called -  " + position);
        if(this.playlistThumbnails.get(position) == null
            || (this.playlistThumbnails.get(position) != null && this.playlistThumbnails.get(position).length() < 1)) {
            this.playlistThumbnails.set(position, Constants.COSMETICS.DEFAULT_PLAYLIST_IMG);
        }

        Glide.with(this.context)
                .asBitmap()
                .load(playlistThumbnails.get(position))
                .into(holder.image);
        holder.playlistTitle.setText(playlistTitles.get(position));
        holder.uploaderText.setText(uploaderNames.get(position));
        holder.parentLayout.setOnClickListener(
                item -> this.playlistClickListener.onClicked(this.idPlaylists.get(position))
        );
        holder.parentLayout.setOnLongClickListener(
                item -> {
                    this.playlistLongClickListener.onLongClick(this.idPlaylists.get(position));
                    return true;
                }
        );
    }

    @Override
    public int getItemCount() {
        return playlistTitles.size();
    }


    public class PlaylistViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView image;
        private TextView playlistTitle;
        private TextView uploaderText;
        private RelativeLayout parentLayout;

        public PlaylistViewHolder(@NonNull View itemView) {
            super(itemView);
            this.image = itemView.findViewById(R.id.playlist_item_image);
            this.playlistTitle = itemView.findViewById(R.id.playlist_item_title);
            this.uploaderText = itemView.findViewById(R.id.playlist_item_uploadertxt);
            this.parentLayout = itemView.findViewById(R.id.playlist_item);
        }
    }
}
