package com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade;

import com.example.Sallefy.SallefyUI.MusicPlayer.MusicPlayer;

public interface MusicPlayerFacadeFactory {
    MusicPlayer MUSIC_PLAYER = new MusicPlayer();

    static MusicPlayerFacade create() {
        return MUSIC_PLAYER;
    }
}
