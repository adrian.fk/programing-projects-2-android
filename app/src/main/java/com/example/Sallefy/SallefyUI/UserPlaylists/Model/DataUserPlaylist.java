package com.example.Sallefy.SallefyUI.UserPlaylists.Model;

import com.example.Sallefy.Utils.Constants;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;

public class DataUserPlaylist {
    private String state;
    private String songIdHolder;
    private PlaylistDTO playlist;
    private TrackDTO track;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSongId() {
        return songIdHolder;
    }

    public void setSongId(String songId) {
        this.songIdHolder = songId;
    }

    public boolean isStateBrowse() {
        return this.state.equals(Constants.PLAYLISTS_STATE.BROWSE);
    }

    public void setPlaylist(PlaylistDTO playlistDTO) {
        this.playlist = playlistDTO;
    }

    public PlaylistDTO getPlaylist() {
        return this.playlist;
    }

    public void setSongIdHolder(String songIdHolder) {
        this.songIdHolder = songIdHolder;
    }

    public TrackDTO getTrack() {
        return track;
    }

    public void setTrack(TrackDTO track) {
        this.track = track;
    }
}
