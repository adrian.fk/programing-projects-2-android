package com.example.Sallefy.SallefyUI.Search.View;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Search.Controller.ListenerInterfaces.FavoriteBtnClick;
import com.example.Sallefy.SallefyUI.Search.Controller.ListenerInterfaces.FollowUserBtnClick;
import com.example.Sallefy.SallefyUI.Search.Controller.ListenerInterfaces.SearchItemClickListener;
import com.example.Sallefy.SallefyUI.Search.Controller.ListenerInterfaces.SearchItemLongClickListener;
import com.example.Sallefy.SallefyUI.Search.Model.SearchContent;

import java.util.List;

public class SearchView implements SearchViewI {
    private static final String TAG = "SearchView";

    private View rootView;

    private RecyclerView searchTracksRes;

    private SearchViewAdapter searchAdapter;

    public SearchView(LayoutInflater inflater, ViewGroup container) {
        this.rootView = inflater.inflate(R.layout.fragment_search, container, false);
        Log.d(TAG, "SearchView: inflated");
        findViewsById();
    }

    private void findViewsById() {
        this.searchTracksRes = this.rootView.findViewById(R.id.search_fragment_list);
    }

    @Override
    public View getRootView() {
        return this.rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }

    @Override
    public void displaySearch(
            List<SearchContent.SearchItem> items,
            SearchItemClickListener searchItemClickListener,
            SearchItemLongClickListener searchItemLongClickListener,
            FavoriteBtnClick favoriteBtnClick,
            FollowUserBtnClick followUsrBtnClick) {
        Log.d(TAG, "displaySearch()");

        this.searchAdapter = new SearchViewAdapter(
                this.rootView.getContext(),
                items,
                searchItemClickListener,
                searchItemLongClickListener,
                favoriteBtnClick,
                followUsrBtnClick);
        this.searchTracksRes.setAdapter(searchAdapter);
        this.searchTracksRes.setLayoutManager(new LinearLayoutManager(this.rootView.getContext()));
    }

    @Override
    public void toastMsg(String msg) {
        Toast.makeText(this.rootView.getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void toastErrorMsg(String errMsg) {
        Toast.makeText(this.rootView.getContext(), "ERROR: " + errMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean selectTrackIfExist(String trackID, String trackName) {
        if (this.searchAdapter != null) {
            return this.searchAdapter.selectViewHolderIfPresent(trackID, trackName);
        }
        return false;
    }

    @Override
    public void toggleTrackIfSelected() {
        if (this.searchAdapter != null) {
            this.searchAdapter.toggleTrackIfSelected();
        }
    }
}
