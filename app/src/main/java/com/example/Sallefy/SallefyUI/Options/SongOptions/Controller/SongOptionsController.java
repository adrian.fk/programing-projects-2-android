package com.example.Sallefy.SallefyUI.Options.SongOptions.Controller;

import com.example.Sallefy.SallefyUI.Options.SongOptions.Model.DataSongOptions;
import com.example.Sallefy.SallefyUI.Options.SongOptions.View.SongOptionsViewI;
import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.UserIsLikingTrackDTO;
import com.example.Sallefy.repositories.Interfaces.TrackRepository;
import com.example.Sallefy.repositories.factories.TrackRepositoryFactory;

public class SongOptionsController {

    private SongOptionsMediator mediator;
    private SongOptionsViewI view;
    private DataSongOptions dataSongOptions;
    private TrackRepository trackRepository;



    public SongOptionsController(SongOptionsMediator mediator, SongOptionsViewI view, DataSongOptions dataSongOptions, TrackRepositoryFactory trackRepositoryFactory) {
        this.mediator = mediator;

        this.view = view;

        this.dataSongOptions = dataSongOptions;

        this.trackRepository = trackRepositoryFactory.create();

        attachListeners();
    }

    private void attachListeners() {
        this.view.attachPlayBtnListener(v -> playSong());
        this.view.attachFavBtnListener(v -> favoriteSong());
        this.view.attachEnqueueBtnListener(v -> enqueueSong());
        this.view.attachUserProfileBtnListener(v -> viewUserProfile());
        this.view.attachDeleteBtnListener(v -> deleteSong());
        this.view.attachShareBtnListener(v -> shareSong());
        this.view.attachAddToPlaylistBtnListener(v -> addToPlaylistClicked());
    }

    private void addToPlaylistClicked() {
        this.mediator.openAddSongToPlaylists();
    }

    private void shareSong() {
        this.mediator.receiveShareTrigger(this.dataSongOptions.getSongTitle(), this.dataSongOptions.getSongUrl());
    }

    private void deleteSong() {
        this.trackRepository
                .deleteTrack(this.dataSongOptions.getSongId())
                .observe(this.mediator.provideOwner(), this::processDeleteSongCallback);
    }

    private void processDeleteSongCallback(BaseDTO baseDTO) {
        if(baseDTO.isStatus()) {
            this.view.toastSongDeleted();
            this.mediator.conclude();
        }
        else {
            this.view.toastConnectionError();
        }
    }

    private void viewUserProfile() {
        this.mediator.receiveUploaderProfileTrigger();
    }

    private void playSong() {
        this.mediator.receivePlaySongTrigger();
    }

    private void favoriteSong() {
        this.view.updateFavBtnState(!this.dataSongOptions.isFavorite());
        this.trackRepository
                .likeTrackById(this.dataSongOptions.getSongId())
                .observe(this.mediator.provideOwner(), this::processSongLikeCallback);
    }

    private void processSongLikeCallback(UserIsLikingTrackDTO userIsLikingTrackDTO) {
        if(userIsLikingTrackDTO.isStatus()) {
            this.dataSongOptions.setFavorite(userIsLikingTrackDTO.isLiked());
            this.mediator.receiveFavoriteTrigger();
        }
        else {
            this.view.toastConnectionError();
        }
    }

    private void enqueueSong() {
        this.mediator.receiveEnqueueTrigger();
    }

    public void configureSongObj(long songId, String clientUserLogin) {
        this.dataSongOptions.setClientUserLogin(clientUserLogin);

        this.trackRepository
                .getTrackById(songId)
                .observe(this.mediator.provideOwner(), this::processSongQueryResponse);
    }

    private void processSongQueryResponse(TrackDTO trackDTO) {
        if(trackDTO.isStatus()) {
            this.dataSongOptions.setSongId(trackDTO.getId());
            this.dataSongOptions.setSongTitle(trackDTO.getName());
            this.dataSongOptions.setSongUploader(
                    trackDTO.getOwner().getId(),
                    trackDTO.getOwner().getLogin()
            );
            this.dataSongOptions.setSongImg(trackDTO.getThumbnail());
            this.dataSongOptions.setSongUrl(trackDTO.getUrl());
            this.dataSongOptions.setFavorite(trackDTO.isLiked());
            this.dataSongOptions.setCreatedByUser(this.dataSongOptions.isCreatedByUserClient());

            this.view.updateView(
                    true,
                    this.dataSongOptions.getSongTitle(),
                    this.dataSongOptions.getUploaderName(),
                    this.dataSongOptions.getSongImg(),
                    this.dataSongOptions.isCreatedByUser(),
                    this.dataSongOptions.isFavorite()
            );
        }
        else {
            this.view.toastConnectionError();
            this.mediator.navBack();
        }
    }
}
