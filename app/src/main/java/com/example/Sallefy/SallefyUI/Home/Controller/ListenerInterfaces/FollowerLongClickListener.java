package com.example.Sallefy.SallefyUI.Home.Controller.ListenerInterfaces;

public interface FollowerLongClickListener {
    void onLongClick(String username);
}
