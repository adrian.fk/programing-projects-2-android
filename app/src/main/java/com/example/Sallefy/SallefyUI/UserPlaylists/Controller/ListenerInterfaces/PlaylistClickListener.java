package com.example.Sallefy.SallefyUI.UserPlaylists.Controller.ListenerInterfaces;

public interface PlaylistClickListener {
    void playlistClicked(String idPlaylist);
}
