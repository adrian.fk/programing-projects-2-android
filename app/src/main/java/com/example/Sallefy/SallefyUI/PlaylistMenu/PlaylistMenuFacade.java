package com.example.Sallefy.SallefyUI.PlaylistMenu;

import android.view.View;

import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.NavController;

import com.example.Sallefy.SallefyUI.PlaylistMenu.Model.MenuSelector;
import com.google.android.material.navigation.NavigationView;

public interface PlaylistMenuFacade {
    void setup(LifecycleOwner owner, NavController navController, NavigationView navView);

    void setupUserHeader(String username, String email, String userImgUrl);

    void updatePlaylistMenu();

    boolean hasSelectedItem();

    MenuSelector.SelectedItem retrieveSelectedItem();
}
