package com.example.Sallefy.SallefyUI.UploadSong.Model;

import android.net.Uri;

public class DataUpload {
    private String title;
    private boolean hasContent;
    private Uri contentUri;

    private Uri songImgUri;
    private String uploadedSongUrl;


    public DataUpload() {
    }

    public Uri getContentUri() {
        return contentUri;
    }

    public void setContentUri(Uri contentUri) {
        this.hasContent = true;
        this.contentUri = contentUri;
    }

    public boolean hasContent() {
        return hasContent;
    }

    public boolean hasValidUri() {
        return this.contentUri != null;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Uri getSongImgUri() {
        return songImgUri;
    }

    public void setSongImgUri(Uri songImgUri) {
        this.songImgUri = songImgUri;
    }

    public boolean hasNewImg() {
        return this.songImgUri != null;
    }

    public String getUploadedSongUrl() {
        return uploadedSongUrl;
    }

    public void setUploadedSongUrl(String uploadedSongUrl) {
        this.uploadedSongUrl = uploadedSongUrl;
    }
}