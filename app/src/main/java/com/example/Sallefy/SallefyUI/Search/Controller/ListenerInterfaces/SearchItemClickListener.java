package com.example.Sallefy.SallefyUI.Search.Controller.ListenerInterfaces;

public interface SearchItemClickListener {
    void registerClick(String id, String name, String type, int position);
}
