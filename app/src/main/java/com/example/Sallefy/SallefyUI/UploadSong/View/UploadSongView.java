package com.example.Sallefy.SallefyUI.UploadSong.View;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.UploadSong.Controller.ListenerInterfaces.ChooseFileListener;
import com.example.Sallefy.SallefyUI.UploadSong.Controller.ListenerInterfaces.UploadButtonListener;
import com.example.Sallefy.Utils.OnTextChangeListener;
import com.example.Sallefy.Utils.SingleMethodListener;

import de.hdodenhof.circleimageview.CircleImageView;

public class UploadSongView implements UploadSongViewI {
    private View rootView;
    private Bundle viewState;

    private CircleImageView songImage;
    private RelativeLayout imageContainer;

    private EditText songName;
    private Button chooseFileBtn;
    private Button uploadBtn;


    public UploadSongView(@NonNull LayoutInflater inflater,
                          ViewGroup container) {
        this.rootView = inflater.inflate(R.layout.fragment_uploadsong, container, false);
        findViews();
    }

    private void findViews() {
        this.songName = getRootView().findViewById(R.id.upload_song_title);
        this.chooseFileBtn = getRootView().findViewById(R.id.upload_song_choose_file_btn);
        this.uploadBtn = getRootView().findViewById(R.id.upload_song_upload_btn);
        this.imageContainer = getRootView().findViewById(R.id.upload_song_change_image);
        this.songImage = getRootView().findViewById(R.id.upload_song_image);
    }

    @Override
    public View getRootView() {
        return this.rootView;
    }

    @Override
    public Bundle getViewState() {
        return this.viewState;
    }

    @Override
    public void attachUploadBtnListener(UploadButtonListener listener) {
        this.uploadBtn.setOnClickListener(v -> listener.onUpload());
    }

    @Override
    public void attachChooseFileListener(ChooseFileListener listener) {
        this.chooseFileBtn.setOnClickListener(v -> listener.onChooseFile());
    }

    @Override
    public void setUploadBtnState(boolean clickable) {
        if (clickable) {
            this.uploadBtn.setEnabled(true);
            this.uploadBtn.setBackgroundResource(R.drawable.rounded_button_toggled);
        }
        else {
            this.uploadBtn.setEnabled(false);
            this.uploadBtn.setBackgroundResource(R.drawable.rounded_button_untoggled);
        }
    }

    @Override
    public void setChooseFileBtnState(boolean triggered) {
        if (triggered) {
            this.chooseFileBtn.setBackgroundResource(R.drawable.rounded_button_untoggled);
            this.chooseFileBtn.setText(R.string.upload_song_choose_file_triggered);
        }
        else {
            this.chooseFileBtn.setBackgroundResource(R.drawable.rounded_button_toggled);
            this.chooseFileBtn.setText(R.string.upload_song_choose_file_untriggered);
        }
    }

    @Override
    public void toastMessage(String msg) {
        Toast.makeText(getRootView().getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void toastErrorMsg(String errMsg) {
        Toast.makeText(getRootView().getContext(), "ERROR: " + errMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getSongName() {
        return this.songName.getText().toString();
    }

    @Override
    public void attachTextChangedListener(SingleMethodListener listener) {
        this.songName.addTextChangedListener(
                new OnTextChangeListener(listener)
        );
    }


    @Override
    public void displaySongImg(Uri imgUri) {
        Glide.with(getRootView().getContext())
                .load(imgUri)
                .into(this.songImage);
    }

    @Override
    public void attachSongImgListener(View.OnClickListener listener) {
        this.imageContainer.setOnClickListener(listener);
    }

    @Override
    public void displayNoImgAdded() {
        toastMsg("Was not able to add image successfully");
    }

    @Override
    public void toastMsg(String msg) {
        Toast.makeText(getRootView().getContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
