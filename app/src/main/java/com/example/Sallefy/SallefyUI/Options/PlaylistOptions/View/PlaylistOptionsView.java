package com.example.Sallefy.SallefyUI.Options.PlaylistOptions.View;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Options.PlaylistOptions.Controller.ListenerInterfaces.DeletePlaylistBtnClickListener;
import com.example.Sallefy.SallefyUI.Options.PlaylistOptions.Controller.ListenerInterfaces.FollowPlaylistBtnClickListener;
import com.example.Sallefy.SallefyUI.Options.PlaylistOptions.Controller.ListenerInterfaces.OptionMenuClickListener;

import de.hdodenhof.circleimageview.CircleImageView;

public class PlaylistOptionsView implements PlaylistOptionsViewI {

    private View rootView;
    private Bundle viewState;

    private LinearLayout optionsContainer;

    private CircleImageView playlistImg;
    private TextView playlistName;
    private TextView playlistOwner;

    private ImageView playlistFollowBtnImg;
    private LinearLayout playlistFollowBtn;
    private LinearLayout playlistRenameBtn;
    private LinearLayout playlistQueueBtn;
    private LinearLayout playlistUploaderBtn;
    private LinearLayout playlistDeleteBtn;


    public PlaylistOptionsView(LayoutInflater inflater, ViewGroup container) {
        this.rootView = inflater.inflate(R.layout.fragment_playlist_options, container, false);
        this.viewState = new Bundle();

        findViews();
    }

    private void findViews() {
        this.optionsContainer = getRootView().findViewById(R.id.options_playlist_container);

        this.playlistImg = getRootView().findViewById(R.id.options_playlist_img);
        this.playlistName = getRootView().findViewById(R.id.options_playlist_title);
        this.playlistOwner = getRootView().findViewById(R.id.options_playlist_uploader);
        this.playlistFollowBtnImg = getRootView().findViewById(R.id.options_playlist_favorite_img);
        this.playlistFollowBtn = getRootView().findViewById(R.id.options_playlist_favorite);
        this.playlistRenameBtn = getRootView().findViewById(R.id.options_playlist_rename);
        this.playlistQueueBtn = getRootView().findViewById(R.id.options_playlist_queue);
        this.playlistUploaderBtn = getRootView().findViewById(R.id.options_playlist_user);
        this.playlistDeleteBtn = getRootView().findViewById(R.id.options_playlist_delete);
    }

    @Override
    public View getRootView() {
        return this.rootView;
    }

    @Override
    public Bundle getViewState() {
        return this.viewState;
    }

    @Override
    public void updatePlaylistHeader(String playlistName, String playlistOwner, String playlistImgUrl) {
        this.playlistName.setText(playlistName);
        this.playlistOwner.setText(playlistOwner);
        Glide.with(getRootView().getContext())
                .asBitmap()
                .load(playlistImgUrl)
                .into(this.playlistImg);
    }

    @Override
    public void displayFollowBtnState(boolean followed) {
        if (followed) {
            this.playlistFollowBtnImg.setImageResource(R.drawable.check_icon);
            this.playlistFollowBtn.setAlpha(0.6f);
        }
        else {
            this.playlistFollowBtnImg.setImageResource(R.drawable.follow_icon);
            this.playlistFollowBtn.setAlpha(1f);
        }
    }

    @Override
    public void attachFollowPlaylistBtnListener(FollowPlaylistBtnClickListener listener) {
        this.playlistFollowBtn.setOnClickListener(v -> listener.clicked());
    }

    @Override
    public void attachRenamePlaylistBtnListener(OptionMenuClickListener listener) {
        this.playlistRenameBtn.setOnClickListener(v -> listener.clicked());
    }

    @Override
    public void attachDeleteBtnListener(DeletePlaylistBtnClickListener listener) {
        this.playlistDeleteBtn.setOnClickListener(v -> listener.deletePlaylist());
    }

    @Override
    public void attachPlaylistOwnerBtnListener(OptionMenuClickListener listener) {
        this.playlistUploaderBtn.setOnClickListener(v -> listener.clicked());
    }

    @Override
    public void attachEnqueuePlaylistBtnListener(OptionMenuClickListener listener) {
        this.playlistQueueBtn.setOnClickListener(v -> listener.clicked());
    }

    @Override
    public void displayPlaylistOptions(boolean visible) {
        if(visible) {
            this.optionsContainer.setVisibility(View.VISIBLE);
        }
        else {
            this.optionsContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void displayRenameBtn(boolean visible) {
        if (visible) {
            this.playlistRenameBtn.setVisibility(View.VISIBLE);
        }
        else {
            this.playlistRenameBtn.setVisibility(View.GONE);
        }
    }

    @Override
    public void displayDeleteBtn(boolean visible) {
        if (visible) {
            this.playlistDeleteBtn.setVisibility(View.VISIBLE);
        }
        else {
            this.playlistDeleteBtn.setVisibility(View.GONE);
        }
    }

    @Override
    public void toastMsg(String msg) {
        Toast toast = Toast.makeText(
                this.rootView.getContext(),
                msg,
                Toast.LENGTH_SHORT
        );
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
