package com.example.Sallefy.SallefyUI.MusicPlayer.View;

import com.example.Sallefy.SallefyUI.Playlist.Controller.ListenerInterfaces.TrackClickListener;
import com.example.Sallefy.SallefyUI.Playlist.Controller.ListenerInterfaces.TrackLongClickListener;
import com.example.Sallefy.SallefyUI.Playlist.Model.PlaylistContent;
import com.example.Sallefy.Utils.ViewInterface;

import java.util.List;

public interface PlayerListingViewI extends ViewInterface {
    void displaySongs(
            List<PlaylistContent.SongItem> songItems,
            TrackClickListener trackClickListener,
            TrackLongClickListener trackLongClickListener
    );

    boolean selectTrackIfExist(String trackID, String trackName);

    void toggleTrackIfSelected();
}
