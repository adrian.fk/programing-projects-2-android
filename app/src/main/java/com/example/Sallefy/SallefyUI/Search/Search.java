    package com.example.Sallefy.SallefyUI.Search;

import android.os.Bundle;

import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.NavController;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Search.Model.DataSearch;
import com.example.Sallefy.SallefyUI.Search.Model.DataSearchFactory;
import com.example.Sallefy.SallefyUI.Search.View.SearchBarView;
import com.example.Sallefy.Utils.AbstractFeaturedMediator;
import com.example.Sallefy.Utils.OnTextChangeListener;
import com.google.android.material.navigation.NavigationView;

import java.util.Objects;

    public class Search extends AbstractFeaturedMediator {

    private SearchBarView searchBarView;

    private DataSearch searchData = DataSearchFactory.create();



    public Search(LifecycleOwner owner, NavController navController, NavigationView navView) {
        super(owner, navController, navView);
        this.searchBarView = new SearchBarView(navView.getRootView(),
                new OnTextChangeListener(l -> processKeywordChanged()),
                l -> initSearch()
        );
        //TODO delete before deployment: The following line only targets PC emulators
        this.searchData.setState(DataSearch.STATE_SEARCH_READY);
        this.searchData.state().observe(owner, this::processStateExecution);
    }

    private void processStateExecution(String state) {
        switch (state) {
            case DataSearch.STATE_EXIT:
                this.searchBarView.setSearchKeyword("");
                break;
        }
    }

    private void initSearch() {
        this.searchData.setState(DataSearch.STATE_SEARCH_READY);
        getNavController().navigate(R.id.nav_search_start_fragment);
    }


    private void processKeywordChanged() {
        this.searchData.setKeyword(this.searchBarView.getSearchKeyword());
        switch(searchData.getState()) {
            case DataSearch.STATE_SEARCH_READY:
                getNavController().navigate(R.id.nav_search_fragment);
                break;

            case DataSearch.STATE_EXIT:
                searchData.setState(DataSearch.STATE_SEARCH_READY);
                break;
        }

    }
}
