package com.example.Sallefy.SallefyUI.MusicPlayer.View;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.ListenerInterfaces.ProgressBarUpdatedCallback;

public class MusicPlayerView implements MusicPlayerViewI {
    private View rootView;
    private Bundle viewState;

    private LinearLayout currentTrack;

    private TextView title;
    private TextView uploader;

    private ImageView thumbnail;

    private ImageButton playButton;
    private ImageButton prevButton;
    private ImageButton nextButton;
    private ImageButton repeatButton;
    private ImageButton shuffleButton;
    private SeekBar progressBar;


    public MusicPlayerView(View v) {
        this.rootView = v.findViewById(R.id.audioPlayer);
        this.viewState = new Bundle();
        findViewElements();

        //Hide player at creation
        this.hide();
    }

    private void findViewElements() {
        this.currentTrack = getRootView().findViewById(R.id.audioplayer_current_track);

        this.title = getRootView().findViewById(R.id.audioplayer_title);
        this.uploader = getRootView().findViewById(R.id.audioplayer_artist);
        this.thumbnail = getRootView().findViewById(R.id.audioplayer_image);

        this.playButton = getRootView().findViewById(R.id.audioplayer_play);
        this.prevButton = getRootView().findViewById(R.id.audioplayer_back);
        this.nextButton = getRootView().findViewById(R.id.audioplayer_next);
        this.repeatButton = getRootView().findViewById(R.id.audioplayer_repeat);
        this.shuffleButton = getRootView().findViewById(R.id.audioplayer_shuffle);
        this.progressBar = getRootView().findViewById(R.id.audioplayer_seekbar);
    }

    @Override
    public View getRootView() {
        return this.rootView;
    }

    @Override
    public Bundle getViewState() {
        return this.viewState;
    }

    @Override
    public void attachFullscreenTriggerListener(View.OnClickListener listener) {
        this.currentTrack.setOnClickListener(listener);
    }

    @Override
    public void attachPlayButtonListener(View.OnClickListener listener) {
        this.playButton.setOnClickListener(listener);
    }

    @Override
    public void attachPrevButtonListener(View.OnClickListener listener) {
        this.prevButton.setOnClickListener(listener);
    }

    @Override
    public void attachNextButtonListener(View.OnClickListener listener) {
        this.nextButton.setOnClickListener(listener);
    }

    @Override
    public void attachRepeatButtonListener(View.OnClickListener listener) {
        this.repeatButton.setOnClickListener(listener);
    }

    @Override
    public void attachShuffleButtonListener(View.OnClickListener listener) {
        this.shuffleButton.setOnClickListener(listener);
    }

    @Override
    public void setPlayBtnState(Boolean state) {
        if(state == STATE_PLAY_BUTTON_PLAY) {
            this.playButton.setImageResource(R.drawable.ic_play_button);
        }
        else {
            this.playButton.setImageResource(R.drawable.ic_pause_button);
        }
    }

    @Override
    public void setShuffleBtnState(Boolean state) {
        if (state) {
            this.shuffleButton.setImageResource(R.drawable.ic_shuffle_color);
        }
        else {
            this.shuffleButton.setImageResource(R.drawable.ic_shuffle);
        }
    }

    @Override
    public void setRepeatBtnState(Boolean state) {
        if (state) {
            this.repeatButton.setImageResource(R.drawable.ic_repeat_color);
        }
        else {
            this.repeatButton.setImageResource(R.drawable.ic_repeat);
        }
    }

    @Override
    public void attachProgressBarChangeCallback(ProgressBarUpdatedCallback listener) {
        this.progressBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {}

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { listener.callback(seekBar.getProgress()); }
        });
    }

    @Override
    public void setProgressBarDuration(int duration) {
        this.progressBar.setMax(duration);
    }

    @Override
    public void setProgressBarProgression(int progress) {
        this.progressBar.setProgress(progress);
    }

    @Override
    public void setTextTitle(String input) {
        this.title.setText(input);
    }

    @Override
    public void setTextUploader(String input) {
        this.uploader.setText(input);
    }

    @Override
    public void setThumbnail(String urlSrc) {
        Glide.with(this.rootView)
                .asBitmap()
                .load(urlSrc)
                .into(this.thumbnail);
    }

    @Override
    public void setContent(String title, String uploader, String imgUrl) {
        setTextTitle(title);
        setTextUploader(uploader);
        setThumbnail(imgUrl);
    }

    @Override
    public void setNextBtnClicked(Boolean clicked) {
        if(clicked) {
            this.nextButton.setImageResource(R.drawable.ic_next_pressed);
        }
        else {
            this.nextButton.setImageResource(R.drawable.ic_next);
        }
    }

    @Override
    public void setPrevBtnClicked(Boolean clicked) {
        if (clicked) {
            this.prevButton.setImageResource(R.drawable.ic_next_pressed);
        }
        else {
            this.prevButton.setImageResource(R.drawable.ic_next);
        }
    }

    @Override
    public SeekBar getSeekBar() {
        return this.progressBar;
    }

    @Override
    public void show() {
        this.rootView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hide() {
        this.rootView.setVisibility(View.GONE);
    }
}
