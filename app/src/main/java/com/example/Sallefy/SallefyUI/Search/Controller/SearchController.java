package com.example.Sallefy.SallefyUI.Search.Controller;


import androidx.lifecycle.MutableLiveData;

import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacade;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacadeFactory;
import com.example.Sallefy.SallefyUI.MusicPlayer.Model.Data;
import com.example.Sallefy.SallefyUI.MusicPlayer.Model.DataMusicPlayerFactory;
import com.example.Sallefy.SallefyUI.Search.Model.DataSearch;
import com.example.Sallefy.SallefyUI.Search.Model.SearchConstants;
import com.example.Sallefy.SallefyUI.Search.Model.SearchContent;
import com.example.Sallefy.SallefyUI.Search.Model.DataSearchFactory;
import com.example.Sallefy.SallefyUI.Search.View.SearchViewI;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.UserDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.SearchDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.UserIsFollowingDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.UserIsLikingTrackDTO;
import com.example.Sallefy.repositories.Interfaces.SearchGateway;
import com.example.Sallefy.repositories.Interfaces.TrackRepository;
import com.example.Sallefy.repositories.Interfaces.UserRepository;
import com.example.Sallefy.repositories.factories.SearchGatewayFactory;
import com.example.Sallefy.repositories.factories.TrackRepositoryFactory;
import com.example.Sallefy.repositories.factories.UserRepositoryFactory;

import java.util.List;


public class SearchController {
    private SearchMediator mediator;
    private SearchGateway searchGateway;
    private TrackRepository trackRepository;
    private UserRepository userRepository;
    private SearchViewI view;
    private DataSearch dataSearch = DataSearchFactory.create();
    private MusicPlayerFacade musicPlayerFacade = MusicPlayerFacadeFactory.create();

    private MutableLiveData<SearchDTO> searchResponse;

    public SearchController(SearchMediator mediator,
                            SearchViewI view,
                            SearchGatewayFactory searchGatewayFactory,
                            TrackRepositoryFactory trackRepositoryFactory,
                            UserRepositoryFactory userRepositoryFactory) {
        this.mediator = mediator;
        this.view = view;
        this.searchGateway = searchGatewayFactory.create();
        this.trackRepository = trackRepositoryFactory.create();
        this.userRepository = userRepositoryFactory.createUserRepository();

        Data dataMusicPlayer = DataMusicPlayerFactory.create();
        dataMusicPlayer.currentlyPlaying.observe(this.mediator.provideOwner(), this::processSongChanged);
    }

    private void processSongChanged(Data.Track track) {
        if (this.view.selectTrackIfExist(String.valueOf(track.getId()), track.getTitle())) {
            this.view.toggleTrackIfSelected();
        }
    }

    public void search(String keyword) {
        if (searchResponse == null) {
            this.searchResponse = this.searchGateway.globalSearchByKeyword(keyword);

            this.searchResponse.observe(this.mediator.provideOwner(), this::processSearch);
        }
        else {
            this.searchGateway.globalSearchByKeyword(keyword);
        }
    }

    private void processSearch(SearchDTO searchDTO) {
        SearchContent searchContent = new SearchContent();
        if(this.dataSearch.getSearchFilter().equals(SearchConstants.FILTERS.TRACKS)
                || this.dataSearch.getSearchFilter().equals(SearchConstants.FILTERS.NONE)) {
            for (TrackDTO track : searchDTO.getTracks()) {
                searchContent.addItem(
                        searchContent.createSearchItem(
                                TYPE.TRACK,
                                String.valueOf(track.getId()),
                                track.getName(),
                                track.getOwner().getLogin(),
                                track.getThumbnail(),
                                track.getUrl(),
                                track.isLiked()
                        )
                );
            }
        }
        if(this.dataSearch.getSearchFilter().equals(SearchConstants.FILTERS.PLAYLISTS)
                || this.dataSearch.getSearchFilter().equals(SearchConstants.FILTERS.NONE)) {
            for (PlaylistDTO playlist : searchDTO.getPlaylists()) {
                searchContent.addItem(
                        searchContent.createSearchItem(
                                TYPE.PLAYLIST,
                                String.valueOf(playlist.getId()),
                                playlist.getName(),
                                playlist.getOwnerObject().getLogin(),
                                playlist.getThumbnail(),
                                "",
                                playlist.isFollowedByUser()
                        )
                );
            }
        }
        if(this.dataSearch.getSearchFilter().equals(SearchConstants.FILTERS.USERS)
                || this.dataSearch.getSearchFilter().equals(SearchConstants.FILTERS.NONE)) {
            for (UserDTO user : searchDTO.getUsers()) {
                searchContent.addItem(
                        searchContent.createSearchItem(
                                TYPE.USER,
                                String.valueOf(user.getId()),
                                user.getLogin(),
                                "User",
                                user.getImageUrl(),
                                "",
                                user.isFollowed()
                        )
                );
            }
        }
        this.view.displaySearch(
                searchContent.getItems(),
                (id, title, type, pos) -> {
                    switch (type) {
                        case TYPE.TRACK:
                            trackClicked(searchDTO.getTracks(), id, pos);
                            break;

                        case TYPE.PLAYLIST:
                            playlistClicked(id, title);
                            break;

                        case TYPE.USER:
                            userClicked(title);
                            break;
                    }
                },
                (id, type) -> {
                  switch (type) {
                      case TYPE.TRACK:
                          trackLongClicked(id);
                          break;

                      case TYPE.PLAYLIST:
                          playlistLongClicked(id);
                          break;

                      case TYPE.USER:

                          break;
                  }
                  return true;
                },
                this::favSong,
                this::followUsrBtnClick
        );

        Data dataMusicPlayer = DataMusicPlayerFactory.create();
        if (dataMusicPlayer.getCurrentlyPlaying() != null) {
            this.view.selectTrackIfExist(
                    String.valueOf(dataMusicPlayer.getCurrentlyPlaying().getId()),
                    dataMusicPlayer.getCurrentlyPlaying().getTitle()
            );
        }
    }

    private void playlistLongClicked(String playlistID) {
        this.mediator.openPlaylistOptions(playlistID);
    }

    private void followUsrBtnClick(String username) {
        this.userRepository
                .followUserByUsername(username)
                .observe(this.mediator.provideOwner(), this::processFollowUserCallback);
    }

    private void processFollowUserCallback(UserIsFollowingDTO userIsFollowingDTO) {
        if(userIsFollowingDTO.isFollowed()) {
            this.view.toastMsg(userIsFollowingDTO.getUserLogin() + " followed!");
        }
        else {
            this.view.toastMsg(userIsFollowingDTO.getUserLogin() + " unfollowed.");
        }
    }

    private void trackLongClicked(String id) {
        this.mediator.openSongOptions(id);
    }

    private void favSong(String id) {
        this.trackRepository
                .likeTrackById(Long.parseLong(id))
                .observe(this.mediator.provideOwner(), this::processLike);
    }

    private void processLike(UserIsLikingTrackDTO userIsLikingTrackDTO) {
        if(!userIsLikingTrackDTO.isStatus()) {
            search(dataSearch.getKeyword());
        }
    }

    private void userClicked(String username) {
        this.mediator.openUserProfile(username);
    }

    private void playlistClicked(String id, String playlistName) {
        this.mediator.receivePlaylist(id, playlistName);
    }

    private void trackClicked(List<TrackDTO> tracks, String trackId, int trackNum) {
        int i = 0;

        //Play song
        this.musicPlayerFacade.playSong(Long.parseLong(trackId));

        this.musicPlayerFacade.emptyQueue();
        //Enqueue following songs
        for(TrackDTO track : tracks) {
            if(i++ > trackNum) {
                musicPlayerFacade.enqueueSong(
                        track.getId(),
                        track.getName(),
                        track.getOwner().getLogin(),
                        track.getThumbnail(),
                        track.getUrl()
                );
            }
        }
    }

    interface TYPE {
        String TRACK = "TRACK";
        String PLAYLIST = "PLAYLIST";
        String USER = "USER";
    }
}
