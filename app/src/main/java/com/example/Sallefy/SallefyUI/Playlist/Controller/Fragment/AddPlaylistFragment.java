package com.example.Sallefy.SallefyUI.Playlist.Controller.Fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Playlist.Controller.AddPlaylistController;
import com.example.Sallefy.SallefyUI.Playlist.Controller.AddPlaylistMediator;
import com.example.Sallefy.SallefyUI.Playlist.Model.DataEditablePlaylist;
import com.example.Sallefy.SallefyUI.Playlist.View.AddPlaylistView;
import com.example.Sallefy.SallefyUI.PlaylistMenu.PlaylistMenuFacade;
import com.example.Sallefy.SallefyUI.PlaylistMenu.PlaylistMenuFacadeFactory;
import com.example.Sallefy.repositoryImpl.factoriesImpl.ImageApiRepositoryFactory;
import com.example.Sallefy.repositoryImpl.factoriesImpl.PlaylistApiRepositoryFactory;
import com.example.Sallefy.repositoryImpl.factoriesImpl.UserApiRepositoryFactory;

public class AddPlaylistFragment extends Fragment implements AddPlaylistMediator {
    private AddPlaylistView view;
    private AddPlaylistController controller;

    private DataEditablePlaylist dataPlaylist;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        this.view = new AddPlaylistView(inflater, container);

        Animation fadeInAnim = AnimationUtils.loadAnimation(this.view.getRootView().getContext(), R.anim.fade_in);
        this.view.getRootView().startAnimation(fadeInAnim);

        this.dataPlaylist = new DataEditablePlaylist();

        PlaylistApiRepositoryFactory playlistApiRepositoryFactory = new PlaylistApiRepositoryFactory();
        UserApiRepositoryFactory userApiRepositoryFactory = new UserApiRepositoryFactory();
        ImageApiRepositoryFactory imageApiRepositoryFactory = new ImageApiRepositoryFactory();

        this.controller = new AddPlaylistController(
                this,
                this.view,
                this.dataPlaylist,
                playlistApiRepositoryFactory,
                userApiRepositoryFactory,
                imageApiRepositoryFactory
        );

        return this.view.getRootView();
    }

    @Override
    public LifecycleOwner provideOwner() {
        return this;
    }

    @Override
    public void receiveMenuUpdateCall() {
        PlaylistMenuFacade playlistMenuFacade = PlaylistMenuFacadeFactory.create();
        playlistMenuFacade.updatePlaylistMenu();
    }

    @Override
    public void finishPlaylistCreation() {
        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_home);
    }

    @Override
    public void openImgFile() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");

        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case Activity.RESULT_CANCELED:
                this.view.displayNoImgAdded();
                break;

            case 1:
                if(null != data) {
                    //Got image file reference from Android FileExplorer successfully
                    this.dataPlaylist.setPlaylistImgUri(data.getData());
                    this.view.displayPlaylistImg(this.dataPlaylist.getPlaylistImgUri());
                }
                break;
        }
    }
}
