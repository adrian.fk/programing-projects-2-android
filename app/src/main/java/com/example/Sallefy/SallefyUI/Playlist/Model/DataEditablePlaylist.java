package com.example.Sallefy.SallefyUI.Playlist.Model;

import android.net.Uri;

import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;

public class DataEditablePlaylist extends DataPlaylist {
    private PlaylistDTO playlistHolder;
    private Uri playlistImgUri;
    private boolean newImg;

    public Uri getPlaylistImgUri() {
        return playlistImgUri;
    }

    public void setPlaylistImgUri(Uri playlistImgUri) {
        this.newImg = true;
        this.playlistImgUri = playlistImgUri;
    }

    public boolean hasNewImg() {
        return newImg;
    }

    public PlaylistDTO getPlaylistHolder() {
        return playlistHolder;
    }

    public void setPlaylistHolder(PlaylistDTO playlistHolder) {
        this.playlistHolder = playlistHolder;
    }
}
