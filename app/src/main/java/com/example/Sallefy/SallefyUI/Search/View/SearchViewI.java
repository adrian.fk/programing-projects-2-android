package com.example.Sallefy.SallefyUI.Search.View;

import com.example.Sallefy.SallefyUI.Search.Controller.ListenerInterfaces.FavoriteBtnClick;
import com.example.Sallefy.SallefyUI.Search.Controller.ListenerInterfaces.FollowUserBtnClick;
import com.example.Sallefy.SallefyUI.Search.Controller.ListenerInterfaces.SearchItemClickListener;
import com.example.Sallefy.SallefyUI.Search.Controller.ListenerInterfaces.SearchItemLongClickListener;
import com.example.Sallefy.SallefyUI.Search.Model.SearchContent;
import com.example.Sallefy.Utils.ViewInterface;

import java.util.List;

public interface SearchViewI extends ViewInterface {
    void displaySearch(
            List<SearchContent.SearchItem> items,
            SearchItemClickListener searchItemClickListener,
            SearchItemLongClickListener searchItemLongClickListener,
            FavoriteBtnClick favoriteBtnClick,
            FollowUserBtnClick followUserBtnClick);

    void toastMsg(String msg);

    void toastErrorMsg(String errMsg);

    boolean selectTrackIfExist(String trackID, String trackName);

    void toggleTrackIfSelected();
}
