package com.example.Sallefy.SallefyUI;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.Sallefy.R;

/**
 * A simple {@link Fragment} subclass to hold a dump layout for when the program is evaluating
 * whether the user is already logged in or not and to serve as a "bounce back" for when the user is
 * attempting to trigger the backstack too far.
 */
public class Loading extends Fragment {
    private static boolean loaded;


    public Loading() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_loading, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (loaded) {
            Navigation
                    .findNavController(requireActivity(), R.id.nav_host_fragment)
                    .navigate(R.id.nav_home);
        }
        loaded = true;
    }
}
