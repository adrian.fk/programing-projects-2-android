package com.example.Sallefy.SallefyUI.Options.PlaylistOptions.Controller;

import com.example.Sallefy.Utils.FeaturedMediatorI;

public interface PlaylistOptionsMediator extends FeaturedMediatorI {
    void enqueueSong(long id, String title, String owner, String imgUrl, String srcUrl);

    void navToUserProfile(String username);

    void finishPlaylistOptions();

    void editPlaylist(String id);
}
