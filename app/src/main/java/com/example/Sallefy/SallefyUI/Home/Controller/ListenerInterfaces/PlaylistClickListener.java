package com.example.Sallefy.SallefyUI.Home.Controller.ListenerInterfaces;

public interface PlaylistClickListener {
    void onClicked(String id);
}
