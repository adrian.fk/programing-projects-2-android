package com.example.Sallefy.SallefyUI.UserPlaylists.Controller;

import com.example.Sallefy.SallefyUI.UserPlaylists.Model.DataUserPlaylist;
import com.example.Sallefy.SallefyUI.UserPlaylists.Model.PlaylistsContent;
import com.example.Sallefy.SallefyUI.UserPlaylists.Model.PlaylistsContentMapper;
import com.example.Sallefy.SallefyUI.UserPlaylists.View.UserPlaylistsViewI;
import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;
import com.example.Sallefy.repositories.Interfaces.CurrentUserRepository;
import com.example.Sallefy.repositories.Interfaces.PlaylistRepository;
import com.example.Sallefy.repositories.Interfaces.TrackRepository;
import com.example.Sallefy.repositories.factories.PlaylistRepositoryFactory;
import com.example.Sallefy.repositories.factories.TrackRepositoryFactory;
import com.example.Sallefy.repositories.factories.UserRepositoryFactory;

import java.util.ArrayList;
import java.util.Collection;

public class UserPlaylistsController {

    private UserPlaylistsMediator mediator;
    private UserPlaylistsViewI<PlaylistsContent.Playlist> view;
    private TrackRepository trackRepository;
    private PlaylistRepository playlistRepository;
    private CurrentUserRepository currentUserRepository;
    private PlaylistsContent content;
    private DataUserPlaylist dataUserPlaylist;

    public UserPlaylistsController(UserPlaylistsMediator mediator,
                                   UserPlaylistsViewI view,
                                   DataUserPlaylist dataUserPlaylist,
                                   TrackRepositoryFactory trackRepositoryFactory,
                                   PlaylistRepositoryFactory playlistRepositoryFactory,
                                   UserRepositoryFactory userRepositoryFactory) {
        this.mediator = mediator;
        this.view = view;
        this.dataUserPlaylist = dataUserPlaylist;
        this.trackRepository = trackRepositoryFactory.create();
        this.playlistRepository = playlistRepositoryFactory.create();
        this.currentUserRepository = userRepositoryFactory.createCurrentUserRepository();
        this.content = new PlaylistsContent();
    }

    public void initiateAddSongMode() {
        this.view.attachPlaylistClickListener(this::addSongToPlaylistClicked);
        this.view.attachPlaylistLongClickListener(this::addSongToPlaylistLongClicked);
    }

    public void initiateBrowseMode() {
        this.view.attachPlaylistClickListener(this::browsePlaylistClicked);
        this.view.attachPlaylistLongClickListener(this::browsePlaylistLongClicked);
    }

    private void browsePlaylistLongClicked(String idPlaylist) {
        this.mediator.openPlaylistOptions(idPlaylist);
    }

    private void browsePlaylistClicked(String idPlaylist) {
        this.mediator.openPlaylist(idPlaylist);
    }

    private void addSongToPlaylistLongClicked(String idPlaylist) {

    }

    private void addSongToPlaylistClicked(String idPlaylist) {
        this.playlistRepository
                .getPlaylistById(Long.parseLong(idPlaylist))
                .observe(this.mediator.provideOwner(), this::processPlaylistResponse);
    }

    private void processPlaylistResponse(PlaylistDTO playlistDTO) {
        this.dataUserPlaylist.setPlaylist(playlistDTO);
        fetchTrack();
    }

    private void fetchTrack() {
        this.trackRepository
                .getTrackById(Long.parseLong(this.dataUserPlaylist.getSongId()))
                .observe(this.mediator.provideOwner(), this::processTrackResponse);
    }

    private void processTrackResponse(TrackDTO trackDTO) {
        this.dataUserPlaylist.setTrack(trackDTO);
        updatePlaylist();
    }

    private void updatePlaylist() {
        ArrayList<TrackDTO> newTrackList = this.dataUserPlaylist.getPlaylist().getTracks();
        newTrackList.add(this.dataUserPlaylist.getTrack());
        this.dataUserPlaylist.getPlaylist().setTracks(newTrackList);

        this.playlistRepository
                .updatePlaylist(this.dataUserPlaylist.getPlaylist())
                .observe(this.mediator.provideOwner(), this::processPlaylistUpdatedResponse);
    }

    private void processPlaylistUpdatedResponse(BaseDTO baseDTO) {
        if (baseDTO.isStatus()) {
            this.view.toastMsg("Song added successfully");
            this.mediator.openPlaylist(String.valueOf(this.dataUserPlaylist.getPlaylist().getId()));
        }
        else {
            this.view.toastError("Song could not be added");
        }
    }

    public void fetchUserPlaylists() {
        this.currentUserRepository
                .getCurrentUserPlaylists()
                .observe(this.mediator.provideOwner(), this::processUserPlaylistsResponse);
    }

    private void processUserPlaylistsResponse(Collection<PlaylistDTO> playlistDTOS) {
        PlaylistsContentMapper playlistsContentMapper = new PlaylistsContentMapper(this.content);
        playlistsContentMapper.mapPlaylistDTOS(playlistDTOS);

        this.view.displayContent(
                this.content.getPlaylists()
        );
    }
}
