package com.example.Sallefy.SallefyUI.Home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.Navigation;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Home.Controller.HomeController;
import com.example.Sallefy.SallefyUI.Home.Controller.HomeMediator;
import com.example.Sallefy.SallefyUI.Home.View.HomeView;
import com.example.Sallefy.SallefyUI.Home.View.HomeViewImpl;
import com.example.Sallefy.Utils.Constants;
import com.example.Sallefy.repositoryImpl.factoriesImpl.PlaylistApiRepositoryFactory;
import com.example.Sallefy.repositoryImpl.factoriesImpl.UserApiRepositoryFactory;

public class HomeFragment extends Fragment implements HomeMediator {

    private HomeView view;
    private HomeController controller;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        this.view = new HomeViewImpl(inflater, container);

        Animation fadeInAnim = AnimationUtils.loadAnimation(this.view.getRootView().getContext(), R.anim.fade_in);
        this.view.getRootView().startAnimation(fadeInAnim);

        UserApiRepositoryFactory userApiRepositoryFactory = new UserApiRepositoryFactory();
        PlaylistApiRepositoryFactory playlistApiRepositoryFactory = new PlaylistApiRepositoryFactory();

        this.controller = new HomeController(this, this.view, userApiRepositoryFactory, playlistApiRepositoryFactory);
        this.controller.init();

        return this.view.getRootView();
    }

    @Override
    public LifecycleOwner provideOwner() {
        return this;
    }

    @Override
    public void receiveUserCall(String userLogin) {
        Bundle b = new Bundle();
        b.putString(Constants.USER_SPECIFICATIONS.USER_NAME, userLogin);

        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_user_profile, b);
    }

    @Override
    public void receivePlaylistNavCall(String  id) {
        Bundle b = new Bundle();
        b.putString(Constants.PLAYLIST_SPECIFICATIONS.PLAYLIST_ID, id);

        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_to_playlist, b);
    }


}