package com.example.Sallefy.SallefyUI.Playlist.View.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Playlist.Controller.ListenerInterfaces.TrackClickListener;
import com.example.Sallefy.SallefyUI.Playlist.Controller.ListenerInterfaces.TrackLongClickListener;
import com.example.Sallefy.SallefyUI.Playlist.Model.PlaylistContent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class PlaylistViewAdapter extends RecyclerView.Adapter<PlaylistViewAdapter.PlaylistViewHolder>{
    private static final String TAG = "PlaylistViewAdapter";
    private static final String SELECTED_POSITION_DISABLED = "-11";

    private TrackClickListener trackClickListener;
    private TrackLongClickListener trackLongClickListener;

    private final Map<String, PlaylistViewHolder> viewHolderCacheMap = new HashMap<>();
    private PlaylistViewHolder selectedViewHolder;
    private String selectedViewHolderID = SELECTED_POSITION_DISABLED;
    private String selectedViewHolderName;

    private int spawnCounter;

    private ArrayList<String> songIds;
    private ArrayList<String> songTitles;
    private ArrayList<String> uploaderNames;
    private ArrayList<String> songThumbnails;
    private ArrayList<String> songSrcUrls;
    private ArrayList<Boolean> favorites;


    private Context context;

    public PlaylistViewAdapter( Context context,
                                List<PlaylistContent.SongItem> items,
                                TrackClickListener trackClickListener,
                                TrackLongClickListener trackLongClickListener) {
        this.songIds = new ArrayList<>();
        this.songTitles = new ArrayList<>();
        this.uploaderNames = new ArrayList<>();
        this.songThumbnails = new ArrayList<>();
        this.songSrcUrls = new ArrayList<>();
        this.favorites = new ArrayList<>();
        this.trackClickListener = trackClickListener;
        this.trackLongClickListener = trackLongClickListener;
        this.context = context;

        for(PlaylistContent.SongItem item : items) {
            this.songIds.add(item.id);
            this.songTitles.add(item.title);
            this.uploaderNames.add(item.uploader);
            this.songThumbnails.add(item.imgUrl);
            this.songSrcUrls.add(item.srcUrl);
            this.favorites.add(item.favorite);
        }
    }

    private void toggleTrackIfIsSelected(String id) {
        if (id.equals(this.selectedViewHolderID)) {
            toggleViewHolderIfSelected();
        }
    }

    public void toggleViewHolderIfSelected() {
        untoggleAllViewHolders();
        if (this.selectedViewHolder != null) {
            if (this.selectedViewHolderName.equals(this.selectedViewHolder.songText.getText().toString().substring(0, 4))) {
                toggleViewHolder(this.selectedViewHolder, R.color.colorTextAccent);
            }
        }
        else {
            if (this.viewHolderCacheMap.get(this.selectedViewHolderID) != null) {
                this.selectedViewHolder = this.viewHolderCacheMap.get(this.selectedViewHolderID);
                if (this.selectedViewHolder != null &&
                        this.selectedViewHolderName.equals(this.selectedViewHolder.songText.getText().toString().substring(0, 4))) {
                    toggleViewHolder(this.selectedViewHolder, R.color.colorTextAccent);
                }
            }
        }
    }

    private void toggleViewHolder(PlaylistViewHolder vh, int color) {
        vh.songText.setTextColor(this.context.getColor(color));
    }

    private void untoggleAllViewHolders() {
        for (PlaylistViewHolder holder : viewHolderCacheMap.values()) {
            toggleViewHolder(holder, R.color.colorWhite);
        }
    }

    public boolean selectViewHolderIfPresent(String trackID, String name) {
        this.selectedViewHolderID = trackID;
        this.selectedViewHolderName = name.substring(0,4);
        if (this.viewHolderCacheMap.containsKey(trackID)) {
            this.selectedViewHolder = this.viewHolderCacheMap.get(trackID);
            return true;
        }
        return false;
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull PlaylistViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        toggleViewHolder(holder, R.color.colorWhite);
        this.viewHolderCacheMap.remove(holder.id);
    }

    @Override
    public int getItemViewType(int position) {
        this.spawnCounter = position;
        return super.getItemViewType(position);
    }

    @NonNull
    @Override
    public PlaylistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.song_item, parent, false);
        PlaylistViewHolder holder = new PlaylistViewHolder(v, this.songIds.get(spawnCounter));
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull PlaylistViewHolder holder, int position) {
        this.viewHolderCacheMap.put(this.songIds.get(position), holder);

        Glide.with(this.context)
                .asBitmap()
                .load(songThumbnails.get(position))
                .into(holder.image);
        holder.songText.setText(songTitles.get(position));
        holder.uploaderText.setText(uploaderNames.get(position));
        holder.parentLayout.setOnClickListener(
                item -> {
                    this.selectedViewHolderID = this.songIds.get(position);

                    PlaylistContent playlistContent = new PlaylistContent();

                    List<String> songIds = this.songIds.subList(position, this.songIds.size());
                    List<String> songTitles = this.songTitles.subList(position, this.songTitles.size());
                    List<String> uploaderNames = this.uploaderNames.subList(position, this.uploaderNames.size());
                    List<String> songThumbnails = this.songThumbnails.subList(position, this.songThumbnails.size());
                    List<String> songSrcUrls = this.songSrcUrls.subList(position, this.songSrcUrls.size());
                    List<Boolean> favorites = this.favorites.subList(position, this.favorites.size());

                    for (int i = 0; i < songIds.size(); i++) {
                        playlistContent.addItem(
                                playlistContent.createSongItem(
                                        songIds.get(i),
                                        songTitles.get(i),
                                        uploaderNames.get(i),
                                        songThumbnails.get(i),
                                        songSrcUrls.get(i),
                                        favorites.get(i)
                                )
                        );
                    }
                    this.trackClickListener.onClicked(playlistContent);
                }
        );
        holder.parentLayout.setOnLongClickListener(
                item -> {
                    this.trackLongClickListener.onLongClick(this.songIds.get(position));
                    return true;
                }
        );
        toggleTrackIfIsSelected(this.songIds.get(position));
    }

    @Override
    public int getItemCount() {
        return songTitles.size();
    }


    static class PlaylistViewHolder extends RecyclerView.ViewHolder {

        public final String id;

        private CircleImageView image;
        private TextView songText;
        private TextView uploaderText;
        private RelativeLayout parentLayout;

        PlaylistViewHolder(@NonNull View itemView, String id) {
            super(itemView);
            this.image = itemView.findViewById(R.id.playlist_item_image);
            this.songText = itemView.findViewById(R.id.playlist_item_title);
            this.uploaderText = itemView.findViewById(R.id.playlist_item_uploadertxt);
            this.parentLayout = itemView.findViewById(R.id.playlist_item);
            this.id = id;
        }
    }
}
