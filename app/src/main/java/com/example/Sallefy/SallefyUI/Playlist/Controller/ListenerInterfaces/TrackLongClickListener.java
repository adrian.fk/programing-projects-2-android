package com.example.Sallefy.SallefyUI.Playlist.Controller.ListenerInterfaces;

public interface TrackLongClickListener {
    void onLongClick(String id);
}
