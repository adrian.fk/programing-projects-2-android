package com.example.Sallefy.SallefyUI.Home.Model;

import java.util.ArrayList;
import java.util.HashMap;

public class FollowersHomeContent {

    private ArrayList<Follower> items;
    private HashMap<String, Integer> nameToIndexMap;

    public FollowersHomeContent() {
        this.items = new ArrayList<>();
        this.nameToIndexMap = new HashMap<>();
    }

    public void addItem(Follower follower) {
        this.nameToIndexMap.put(follower.userLogin, items.size());
        this.items.add(follower);
    }

    public Follower getItemByName(String name) {
        return items.get(
                this.nameToIndexMap.get(name)
        );
    }

    public ArrayList<Follower> getItems() {
        return items;
    }

    public Follower createFollower(String id, String userLogin, String imgUrl) {
        return new Follower(id, imgUrl, userLogin);
    }

    public static class Follower {
        public final String id;
        public final String imgUrl;
        public final String userLogin;

        private Follower(String id, String imgUrl, String userLogin) {
            this.id = id;
            this.imgUrl = imgUrl;
            this.userLogin = userLogin;
        }
    }
}
