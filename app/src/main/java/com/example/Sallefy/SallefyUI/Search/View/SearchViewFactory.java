package com.example.Sallefy.SallefyUI.Search.View;

import android.view.LayoutInflater;
import android.view.ViewGroup;

public class SearchViewFactory implements SearchViewFactoryI {
    private LayoutInflater inflater;
    private ViewGroup container;

    public SearchViewFactory(LayoutInflater inflater, ViewGroup container) {
        this.inflater = inflater;
        this.container = container;
    }

    @Override
    public SearchViewI create() {
        return new SearchView(inflater, container);
    }
}
