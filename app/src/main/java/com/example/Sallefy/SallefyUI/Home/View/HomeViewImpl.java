package com.example.Sallefy.SallefyUI.Home.View;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Home.Controller.ListenerInterfaces.FollowerClickListener;
import com.example.Sallefy.SallefyUI.Home.Controller.ListenerInterfaces.FollowerLongClickListener;
import com.example.Sallefy.SallefyUI.Home.Controller.ListenerInterfaces.PlaylistClickListener;
import com.example.Sallefy.SallefyUI.Home.Controller.ListenerInterfaces.PlaylistLongClickListener;
import com.example.Sallefy.SallefyUI.Home.Model.FollowersHomeContent;
import com.example.Sallefy.SallefyUI.Home.Model.PlaylistHomeContent;
import com.example.Sallefy.SallefyUI.Home.View.Adapter.FollowersHomeViewAdapter;
import com.example.Sallefy.SallefyUI.Home.View.Adapter.PlaylistHomeViewAdapter;
import com.example.Sallefy.Utils.Constants;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeViewImpl implements HomeView {
    private static final String TAG = "HomeViewImpl";

    private View rootView;

    private Bundle viewState;


    private RecyclerView followersPanel;
    private RecyclerView playlistPanel;

    private TextView username;
    private CircleImageView userImage;

    public HomeViewImpl(LayoutInflater inflater, ViewGroup container) {
        this.rootView = inflater.inflate(R.layout.fragment_home, container, false);

        findViews();
    }

    private void findViews() {
        this.username = getRootView().findViewById(R.id.home_username);
        this.userImage = getRootView().findViewById(R.id.home_user_image);

        this.followersPanel = getRootView().findViewById(R.id.home_followers_list);
        this.playlistPanel = getRootView().findViewById(R.id.home_fragment_playlist_list);
    }

    public void setUsername(String username) {
        this.username.setText(username);
        this.username.setVisibility(View.VISIBLE);
    }

    @Override
    public void attachCurrentUserListener(View.OnClickListener listener) {
        this.userImage.setOnClickListener(listener);
        this.username.setOnClickListener(listener);
    }

    public void setUserImage(String imgUrl) {
        if(imgUrl != null && imgUrl.length() > 0) {
            Glide.with(getRootView().getContext())
                    .asBitmap()
                    .load(imgUrl)
                    .into(this.userImage);
        }
        else {
            Glide.with(getRootView().getContext())
                    .asBitmap()
                    .load(Constants.COSMETICS.DEFAULT_USR_IMG)
                    .into(this.userImage);
        }
        this.userImage.setVisibility(View.VISIBLE);
    }

    @Override
    public void setFollowersContent(List<FollowersHomeContent.Follower> items,
                                    FollowerClickListener followerClickListener,
                                    FollowerLongClickListener followerLongClickListener) {
        FollowersHomeViewAdapter followersViewAdapter = new FollowersHomeViewAdapter(
                this.rootView.getContext(),
                items,
                followerClickListener,
                followerLongClickListener
        );

        this.followersPanel.setAdapter(followersViewAdapter);
        this.followersPanel.setLayoutManager(
                new LinearLayoutManager(
                        this.rootView.getContext(),
                        LinearLayoutManager.HORIZONTAL,
                        false
                )
        );
    }

    @Override
    public void setPlaylistsContent(List<PlaylistHomeContent.PlaylistItem> items,
                                    PlaylistClickListener playlistClickListener,
                                    PlaylistLongClickListener playlistLongClickListener) {
        PlaylistHomeViewAdapter playlistHomeViewAdapter = new PlaylistHomeViewAdapter(
                this.rootView.getContext(),
                items,
                playlistClickListener,
                playlistLongClickListener
        );

        this.playlistPanel.setAdapter(playlistHomeViewAdapter);
        this.playlistPanel.setLayoutManager(new LinearLayoutManager(this.rootView.getContext()));
    }


    @Override
    public void toastError(String errMsg) {
        Toast.makeText(this.rootView.getContext(), "ERROR: " + errMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void toastMsg(String msg) {
        Toast.makeText(getRootView().getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public View getRootView() {
        return this.rootView;
    }

    @Override
    public Bundle getViewState() {
        return this.viewState;
    }
}
