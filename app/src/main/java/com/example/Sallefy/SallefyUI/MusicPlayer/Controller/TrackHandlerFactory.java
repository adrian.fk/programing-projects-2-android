package com.example.Sallefy.SallefyUI.MusicPlayer.Controller;

public interface TrackHandlerFactory {
    TrackHandler instance = new TrackHandler();

    static TrackHandler create() {
        return instance;
    }
}
