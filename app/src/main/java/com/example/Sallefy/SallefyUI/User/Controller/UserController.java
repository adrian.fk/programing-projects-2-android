package com.example.Sallefy.SallefyUI.User.Controller;

import android.os.Handler;

import com.example.Sallefy.SallefyUI.Songs.Model.SongsContent;
import com.example.Sallefy.SallefyUI.User.Model.DataUser;
import com.example.Sallefy.SallefyUI.User.View.UserViewI;
import com.example.Sallefy.SallefyUI.UserPlaylists.Model.PlaylistsContent;
import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.UserDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.ImageUploadDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.UserIsLikingTrackDTO;
import com.example.Sallefy.repositories.Interfaces.ImageRepository;
import com.example.Sallefy.repositories.Interfaces.TrackRepository;
import com.example.Sallefy.repositories.Interfaces.UserRepository;
import com.example.Sallefy.repositories.factories.ImageRepositoryFactory;
import com.example.Sallefy.repositories.factories.TrackRepositoryFactory;
import com.example.Sallefy.repositories.factories.UserRepositoryFactory;
import com.example.Sallefy.repositoryImpl.repositoryImpl.SessionBearer;

import java.util.Collection;

public class UserController {
    private UserMediator mediator;
    private UserViewI view;
    private DataUser data;

    private final UserRepository userRepository;
    private final TrackRepository trackRepository;
    private final ImageRepositoryFactory imageRepositoryFactory;


    private SongsContent songsContents;
    private PlaylistsContent playlistContents;

    public UserController(UserMediator mediator,
                          UserViewI view,
                          DataUser data,
                          UserRepositoryFactory userRepositoryFactory,
                          TrackRepositoryFactory trackRepositoryFactory, ImageRepositoryFactory imageRepositoryFactory) {
        this.mediator = mediator;
        this.view = view;
        this.data = data;
        this.userRepository = userRepositoryFactory.createUserRepository();
        this.trackRepository = trackRepositoryFactory.create();
        this.imageRepositoryFactory = imageRepositoryFactory;

        this.playlistContents = new PlaylistsContent();
        this.songsContents = new SongsContent();

        this.view.attachPlaylistClickedListener(this::playlistClicked);
        this.view.attachPlaylistLongClickedListener(this::playlistLongClicked);
        this.view.attachSongClickedListener(this::songClicked);
        this.view.attachSongLongClickedListener(this::songLongClicked);
        this.view.attachSongFavBtnClickedListener(this::songFavBtnClicked);
    }

    private void onLogoutBtnClicked() {
        this.mediator.requestLogout();
    }

    private void songFavBtnClicked(String idSong) {
        this.trackRepository
                .likeTrackById(Long.parseLong(idSong))
                .observe(this.mediator.provideOwner(), this::processLike);
    }

    private void processLike(UserIsLikingTrackDTO userIsLikingTrackDTO) {
        if (userIsLikingTrackDTO.isStatus()) {
            if (userIsLikingTrackDTO.isLiked()) {
                this.view.toastMsg("Song added to favorite songs");
            }
            else {
                this.view.toastMsg("Song removed from favorite songs");
            }
        }
    }

    private void songClicked(String songId, int listPos) {
        this.mediator.playSong(
                Long.parseLong(songId)
        );
    }

    private void songLongClicked(String idSong) {
        this.mediator.openSongOptions(idSong);
    }

    private void playlistLongClicked(String idPlaylist) {
        this.mediator.openPlaylistOptions(idPlaylist);
    }

    private void playlistClicked(String idPlaylist) {
        this.mediator.openPlaylist(idPlaylist);
    }

    public void fetchUser(String username) {
        this.userRepository
                .getUserByName(username)
                .observe(this.mediator.provideOwner(), this::processUserResponse);
    }

    private void processUserResponse(UserDTO userDTO) {
        if (userDTO.isStatus()) {
            this.data.setUserHolder(userDTO);
            this.data.setFollowers(String.valueOf(userDTO.getFollowers()));
            this.data.setFollowing(String.valueOf(userDTO.getFollowing()));
            this.view.setUser(
                    userDTO.getLogin(),
                    userDTO.getImageUrl(),
                    String.valueOf(userDTO.getFollowers()),
                    String.valueOf(userDTO.getFollowing())
            );
            this.view.setHeaderVisibility(true);

            this.userRepository
                    .getTracksByUsername(this.data.getUserLogin())
                    .observe(this.mediator.provideOwner(), this::processTracks);


            this.userRepository
                    .getPlaylistsByUserName(this.data.getUserLogin())
                    .observe(this.mediator.provideOwner(), this::processUserPlaylists);
        }
    }

    private void processTracks(Collection<TrackDTO> trackDTOS) {
        for (TrackDTO track : trackDTOS) {
            this.songsContents.addItem(
                    this.songsContents.createSongItem(
                            String.valueOf(track.getId()),
                            track.getName(),
                            track.getOwner().getLogin(),
                            track.getThumbnail(),
                            track.getUrl(),
                            track.isLiked()
                    )
            );
        }
        this.view.displaySongsContent(this.songsContents.getItems());
        this.view.setContentVisibility(true);
    }

    private void processUserPlaylists(Collection<PlaylistDTO> playlistDTOS) {
        for (PlaylistDTO playlist : playlistDTOS) {
            this.playlistContents.addPlaylist(
                    this.playlistContents.createPlaylist(
                            String.valueOf(playlist.getId()),
                            playlist.getName(),
                            playlist.getThumbnail()
                    )
            );
        }
        this.view.displayPlaylistsContent(this.playlistContents.getPlaylists());
        this.view.setContentVisibility(true);
        evaluateUser();
    }

    private void evaluateUser() {
        boolean isUser = SessionBearer.getUsernameIfThere().equals(this.data.getUserLogin());

        if (isUser) {
            this.view.showUserConfiguration(isUser);
            this.view.attachUserImgClickListener(v -> onUserImgClicked());
            this.view.attachLogoutBtnClickListener(v -> onLogoutBtnClicked());
        }
    }

    private void onUserImgClicked() {
        this.mediator.openImgFile();
    }

    public void postUserImg() {
        Handler handler = new Handler();
        handler.post(
                () -> {
                    ImageRepository imageRepository = this.imageRepositoryFactory.create();
                    imageRepository
                            .uploadImage(this.data.getUserImgUri())
                            .observe(this.mediator.provideOwner(), this::processUserImgUploaded);
                }
        );
    }

    private void processUserImgUploaded(ImageUploadDTO imageUploadDTO) {
        if (imageUploadDTO.isStatus()) {
            this.data.getUserHolder().setImageUrl(imageUploadDTO.getImageUrl());
            this.data.getUserHolder().setLastModifiedBy(null);

            this.userRepository
                    .saveAccount(this.data.getUserHolder())
                    .observe(this.mediator.provideOwner(), this::onUserUpdatedResponse);
            //this.view.displayUserImg(imageUploadDTO.getImageUrl());
        }
        else {
            this.view.displayNoImgAdded();
        }
    }

    private void onUserUpdatedResponse(BaseDTO baseDTO) {
        if (baseDTO.isStatus()) {
            this.view.toastMsg("Image successfully updated");
            this.mediator.conclude();
        }
        else {
            this.view.displayNoImgAdded();
        }
    }
}
