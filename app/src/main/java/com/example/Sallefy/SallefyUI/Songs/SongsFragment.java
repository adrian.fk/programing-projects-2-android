package com.example.Sallefy.SallefyUI.Songs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.Navigation;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Songs.Controller.SongsController;
import com.example.Sallefy.SallefyUI.Songs.Controller.SongsMediator;
import com.example.Sallefy.SallefyUI.Songs.View.SongsView;
import com.example.Sallefy.Utils.Constants;
import com.example.Sallefy.repositoryImpl.factoriesImpl.TrackApiRepositoryFactory;
import com.example.Sallefy.repositoryImpl.factoriesImpl.UserApiRepositoryFactory;


public class SongsFragment extends Fragment implements SongsMediator {
    private SongsController controller;
    private SongsView view;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        this.view = new SongsView(inflater, container);

        Animation fadeInAnim = AnimationUtils.loadAnimation(this.view.getRootView().getContext(), R.anim.fade_in);
        this.view.getRootView().startAnimation(fadeInAnim);

        TrackApiRepositoryFactory trackApiRepositoryFactory = new TrackApiRepositoryFactory();
        UserApiRepositoryFactory userApiRepositoryFactory = new UserApiRepositoryFactory();
        this.controller = new SongsController(
                this,
                this.view,
                trackApiRepositoryFactory,
                userApiRepositoryFactory
        );


        this.controller.acquireSongs();

        return this.view.getRootView();
    }

    @Override
    public LifecycleOwner provideOwner() {
        return this;
    }

    @Override
    public void openSongOptions(String id) {
        Bundle b = new Bundle();
        b.putString(Constants.SONG_SPECIFICATIONS.SONG_ID, id);

        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_song_options, b);
    }
}