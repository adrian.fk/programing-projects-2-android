package com.example.Sallefy.SallefyUI.UserPlaylists.Controller;

import com.example.Sallefy.Utils.FeaturedMediatorI;

public interface UserPlaylistsMediator extends FeaturedMediatorI {
    void concludeUserPlaylists();

    void openPlaylist(String id);

    void openPlaylistOptions(String id);
}
