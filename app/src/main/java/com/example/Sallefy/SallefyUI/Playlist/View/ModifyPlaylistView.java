package com.example.Sallefy.SallefyUI.Playlist.View;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.example.Sallefy.R;
import com.example.Sallefy.Utils.OnTextChangeListener;
import com.google.android.material.snackbar.Snackbar;

import de.hdodenhof.circleimageview.CircleImageView;

public class ModifyPlaylistView implements ModifyPlaylistViewI {
    private View rootView;

    private CircleImageView playlistImage;
    private RelativeLayout imageContainer;

    private TextView txtTitle;
    private EditText newTitle;
    private Button btnConfirm;


    public ModifyPlaylistView(@NonNull LayoutInflater inflater, ViewGroup container) {
        this.rootView = inflater.inflate(R.layout.fragment_modify_playlist, container, false);
        findViews();

        setConfirmBtnState(false);

        this.newTitle.addTextChangedListener(
                new OnTextChangeListener(
                        e -> newTitleChanged()
                )
        );
    }

    private void newTitleChanged() {
        evaluateConfirmBtnState();
    }

    private void evaluateConfirmBtnState() {
        if (getNewTitle().length() > 0) {
            setConfirmBtnState(true);
        }
        else {
            setConfirmBtnState(false);
        }
    }

    private void findViews() {
        this.txtTitle = getRootView().findViewById(R.id.modify_playlist_title);
        this.newTitle = getRootView().findViewById(R.id.modify_playlist_title_new);
        this.btnConfirm = getRootView().findViewById(R.id.modify_playlist_confirm_btn);
        this.imageContainer = getRootView().findViewById(R.id.modify_playlist_change_image);
        this.playlistImage = getRootView().findViewById(R.id.modify_playlist_image);
    }

    @Override
    public void attachConfirmBtnClickListener(View.OnClickListener listener) {
        this.btnConfirm.setOnClickListener(listener);
    }

    @Override
    public void displayPlaylistTitle(String title) {
        this.txtTitle.setText(title);
    }

    @Override
    public void displayPlaylistImg(String imgUrl) {
        Glide.with(getRootView().getContext())
                .asBitmap()
                .load(imgUrl)
                .into(this.playlistImage);
    }

    @Override
    public void displayPlaylistImg(Uri imgUri) {
        Glide.with(getRootView().getContext())
                .load(imgUri)
                .into(this.playlistImage);
    }

    @Override
    public void attachPlaylistImgListener(View.OnClickListener listener) {
        this.imageContainer.setOnClickListener(listener);
    }

    @Override
    public void displayNoImgAdded() {
        toastMsg("Was not able to add image successfully");
    }

    @Override
    public void setConfirmBtnState(boolean clickable) {
        if (clickable) {
            this.btnConfirm.setEnabled(true);
            this.btnConfirm.setBackgroundResource(R.drawable.rounded_button_toggled);
        }
        else {
            this.btnConfirm.setEnabled(false);
            this.btnConfirm.setBackgroundResource(R.drawable.rounded_button_untoggled);
        }
    }

    @Override
    public String getNewTitle() {
        return this.newTitle.getText().toString();
    }

    @Override
    public void toastMsg(String msg) {
        Toast.makeText(getRootView().getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public View getRootView() {
        return this.rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }
}
