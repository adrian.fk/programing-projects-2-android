package com.example.Sallefy.SallefyUI.Options.PlaylistOptions.Controller;

import com.example.Sallefy.SallefyUI.Options.PlaylistOptions.Model.Data;
import com.example.Sallefy.SallefyUI.Options.PlaylistOptions.View.PlaylistOptionsViewI;
import com.example.Sallefy.SallefyUI.PlaylistMenu.PlaylistMenuFacade;
import com.example.Sallefy.SallefyUI.PlaylistMenu.PlaylistMenuFacadeFactory;
import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.UserIsFollowingDTO;
import com.example.Sallefy.repositories.Interfaces.PlaylistRepository;
import com.example.Sallefy.repositories.factories.PlaylistRepositoryFactory;
import com.example.Sallefy.repositoryImpl.repositoryImpl.SessionBearer;

public class PlaylistOptionsController {
    private PlaylistOptionsMediator mediator;
    private PlaylistOptionsViewI view;
    private Data data;

    private PlaylistRepository playlistRepository;

    public PlaylistOptionsController(PlaylistOptionsMediator mediator,
                                     PlaylistOptionsViewI view,
                                     Data data,
                                     PlaylistRepositoryFactory playlistRepositoryFactory) {
        this.mediator = mediator;
        this.view = view;
        this.data = data;
        this.playlistRepository = playlistRepositoryFactory.create();

        this.view.attachFollowPlaylistBtnListener(this::followBtnClicked);
        this.view.attachRenamePlaylistBtnListener(this::renamePlaylistClicked);
        this.view.attachDeleteBtnListener(this::deleteBtnClicked);
        this.view.attachEnqueuePlaylistBtnListener(this::onEnqueueBtnClicked);
        this.view.attachPlaylistOwnerBtnListener(this::onOwnerBtnClicked);
    }

    private void onOwnerBtnClicked() {
        this.mediator.navToUserProfile(
                this.data.getCurrentPlaylist().getOwnerObject().getLogin()
        );
    }

    private void onEnqueueBtnClicked() {
        for (TrackDTO track : this.data.getCurrentPlaylist().getTracks()) {
            this.mediator.enqueueSong(
                    track.getId(),
                    track.getName(),
                    track.getOwner().getLogin(),
                    track.getThumbnail(),
                    track.getUrl()
            );
        }
        this.view.toastMsg("Playlist " + this.data.getCurrentPlaylist().getName() + " enqueued");
    }

    private void renamePlaylistClicked() {
        this.mediator.editPlaylist(String.valueOf(this.data.getPlaylistID()));
    }

    private void deleteBtnClicked() {
        this.playlistRepository
                .deletePlaylist(
                        this.data.getCurrentPlaylist()
                )
                .observe(this.mediator.provideOwner(), this::processDeleteResponse);
    }

    private void processDeleteResponse(BaseDTO baseDTO) {
        if (baseDTO.isStatus()) {
            this.mediator.finishPlaylistOptions();
            this.view.toastMsg("Playlist successfully deleted");
        }
    }

    private void followBtnClicked() {
        this.playlistRepository
                .followPlayListById(this.data.getPlaylistID())
                .observe(this.mediator.provideOwner(), this::processPlaylistFollowedResponse);
    }

    private void processPlaylistFollowedResponse(UserIsFollowingDTO userIsFollowingDTO) {
        PlaylistMenuFacade playlistMenuFacade = PlaylistMenuFacadeFactory.create();
        playlistMenuFacade.updatePlaylistMenu();

        this.data.setFollowed(userIsFollowingDTO.isFollowed());
        this.view.displayFollowBtnState(userIsFollowingDTO.isFollowed());
    }

    public void init() {
        this.playlistRepository
                .getPlaylistById(this.data.getPlaylistID())
                .observe(this.mediator.provideOwner(), this::processPlaylistResponse);
    }

    private void processPlaylistResponse(PlaylistDTO playlistDTO) {
        boolean isUsersPlaylist = playlistDTO
                                            .getOwnerObject()
                                            .getLogin()
                                            .equals(SessionBearer.getUsernameIfThere());

        this.data.setCurrentPlaylist(playlistDTO);
        this.view.updatePlaylistHeader(
                playlistDTO.getName(),
                playlistDTO.getOwnerObject().getLogin(),
                playlistDTO.getCover()
        );
        this.view.displayFollowBtnState(playlistDTO.isFollowedByUser());
        this.view.displayRenameBtn(isUsersPlaylist);
        this.view.displayDeleteBtn(isUsersPlaylist);
        this.view.displayPlaylistOptions(true);
    }
}
