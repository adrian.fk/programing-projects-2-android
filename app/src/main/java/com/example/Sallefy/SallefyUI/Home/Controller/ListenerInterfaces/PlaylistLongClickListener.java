package com.example.Sallefy.SallefyUI.Home.Controller.ListenerInterfaces;

public interface PlaylistLongClickListener {
    void onLongClick(String id);
}
