package com.example.Sallefy.SallefyUI.Songs.Controller.ListenerInterfaces;

public interface SongItemLongClickListener {
    void onLongClick(String id);
}
