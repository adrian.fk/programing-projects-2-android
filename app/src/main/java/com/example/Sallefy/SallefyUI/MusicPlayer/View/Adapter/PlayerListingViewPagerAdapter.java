package com.example.Sallefy.SallefyUI.MusicPlayer.View.Adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Fragment.PlayerListingLogFragment;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Fragment.PlayerListingQueueFragment;

public class PlayerListingViewPagerAdapter extends FragmentPagerAdapter {

    private static final int NUM_PAGES = 2;

    private static final String TITLE_PAGE1 = "Queue";
    private static final String TITLE_PAGE2 = "Log";

    private static final int INDEX_PAGE1 = 0;
    private static final int INDEX_PAGE2 = 1;

    /**
     * @param fm supply a support Fragment Manager
     * @param behavior constants start with BEHAVIOUR
     */
    public PlayerListingViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (position == INDEX_PAGE1) {
            return PlayerListingQueueFragment.getInstance();
        }
        else {
            return PlayerListingLogFragment.getInstance();
        }
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (position == INDEX_PAGE1) return TITLE_PAGE1;
        if (position == INDEX_PAGE2) return TITLE_PAGE2;
        return super.getPageTitle(position);
    }
}
