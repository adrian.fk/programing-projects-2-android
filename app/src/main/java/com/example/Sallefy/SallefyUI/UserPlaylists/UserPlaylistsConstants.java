package com.example.Sallefy.SallefyUI.UserPlaylists;

public interface UserPlaylistsConstants {
    interface PLAYLISTS_STATE {
        String STATE_KEY = "STATE_KEY";
        String ADD_SONG_TO_PLAYLIST = "ADD_SONG_TO_PLAYLIST";
        String BROWSE = "BROWSE";
    }
}
