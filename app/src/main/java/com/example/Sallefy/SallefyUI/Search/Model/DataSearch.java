package com.example.Sallefy.SallefyUI.Search.Model;


import android.os.Bundle;

import androidx.lifecycle.MutableLiveData;

public class DataSearch {
    private static final String TAG = "SearchModel";

    public static final String STATE_SEARCH_READY = "floating";
    public static final String STATE_EXIT = "exit";
    public static final String STATE_SEARCHING = "search";




    private MutableLiveData<String> searchKeyword;
    private MutableLiveData<String> state;

    private String searchFilter;


    public DataSearch() {
        this.searchKeyword = new MutableLiveData<>();
        this.state = new MutableLiveData<>();
        this.setSearchFilter(SearchConstants.FILTERS.NONE);
    }


    public String getSearchFilter() {
        return searchFilter;
    }

    public void setSearchFilter(String searchFilter) {
        this.searchFilter = searchFilter;
    }

    public String getKeyword() {
        return this.searchKeyword.getValue();
    }

    public MutableLiveData<String> keyword() {
        return this.searchKeyword;
    }

    public void setKeyword(String searchKeyword) {
        this.searchKeyword.setValue(searchKeyword);
    }

    public MutableLiveData<String> state() {
        return this.state;
    }

    public String getState() {
        return this.state.getValue();
    }

    public void setState(String state) {
        this.state.setValue(state);
    }

}
