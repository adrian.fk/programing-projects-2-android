package com.example.Sallefy.SallefyUI.Search.View;

public interface SearchViewFactoryI {
    SearchViewI create();
}
