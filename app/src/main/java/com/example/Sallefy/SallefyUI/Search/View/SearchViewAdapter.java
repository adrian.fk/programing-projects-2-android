package com.example.Sallefy.SallefyUI.Search.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Search.Controller.ListenerInterfaces.FavoriteBtnClick;
import com.example.Sallefy.SallefyUI.Search.Controller.ListenerInterfaces.FollowUserBtnClick;
import com.example.Sallefy.SallefyUI.Search.Controller.ListenerInterfaces.SearchItemClickListener;
import com.example.Sallefy.SallefyUI.Search.Controller.ListenerInterfaces.SearchItemLongClickListener;
import com.example.Sallefy.SallefyUI.Search.Model.SearchContent;
import com.example.Sallefy.Utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class SearchViewAdapter extends RecyclerView.Adapter<SearchViewAdapter.SearchViewHolder> {
    private static final String TAG = "PlaylistViewAdapter";

    private SearchItemClickListener clickListener;
    private SearchItemLongClickListener longClickListener;
    private FavoriteBtnClick favoriteBtnClick;
    private FollowUserBtnClick followUserBtnClick;

    /**
     * Map: ViewHolderCacheMap
     * String: track ID
     * SearchViewHolder: pointer to View holder cached for that track ID
     */
    private final Map<String, SearchViewHolder> viewHolderCacheMap = new HashMap<>();
    private SearchViewHolder selectedViewHolder;
    private String selectedViewHolderID;
    private String selectedViewHolderName;

    private ArrayList<String> types;
    private ArrayList<Boolean> favorites;

    private ArrayList<String> searchIds;
    private ArrayList<String> searchTitles;
    private ArrayList<String> appendedText;
    private ArrayList<String> searchThumbnails;

    private Context context;

    private int spawnCounter;

    public SearchViewAdapter(Context context,
                             List<SearchContent.SearchItem> items,
                             SearchItemClickListener clickListener,
                             SearchItemLongClickListener longClickListener,
                             FavoriteBtnClick favoriteBtnClick,
                             FollowUserBtnClick followUserBtnClick) {
        this.clickListener = clickListener;
        this.longClickListener = longClickListener;
        this.favoriteBtnClick = favoriteBtnClick;
        this.followUserBtnClick = followUserBtnClick;
        this.types = new ArrayList<>();
        this.searchIds = new ArrayList<>();
        this.searchTitles = new ArrayList<>();
        this.appendedText = new ArrayList<>();
        this.searchThumbnails = new ArrayList<>();
        this.favorites = new ArrayList<>();
        this.context = context;

        for(SearchContent.SearchItem item : items) {
            this.types.add(item.type);
            this.searchIds.add(item.id);
            this.searchTitles.add(item.title);
            this.appendedText.add(item.uploader);
            this.searchThumbnails.add(item.imgUrl);
            this.favorites.add(item.favorite);
        }
    }

    @Override
    public int getItemViewType(int position) {
        this.spawnCounter = position;
        return super.getItemViewType(position);
    }

    @NonNull
    @Override
    public SearchViewAdapter.SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View holderView;
        switch (types.get(this.spawnCounter)) {
            default:
            case Constants.TYPES.TRACK:
                holderView = LayoutInflater.from(parent.getContext()).inflate(R.layout.song_item, parent, false);
                break;

            case Constants.TYPES.PLAYLIST:
                holderView = LayoutInflater.from(parent.getContext()).inflate(R.layout.playlist_listitem, parent, false);
                break;

            case Constants.TYPES.USER:
                holderView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_listitem, parent, false);
                break;
        }
        SearchViewHolder holder = new SearchViewHolder(holderView, this.searchIds.get(spawnCounter));
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: called -  " + position);
        if(types.get(position).equals(Constants.TYPES.TRACK)) {
            this.viewHolderCacheMap.put(this.searchIds.get(position), holder);

            holder.favorite.setVisibility(View.VISIBLE);
            if(this.favorites.get(position)) {
                holder.favorite.setImageResource(R.drawable.favorite_button_filled);
            }
            holder.favorite.setOnClickListener(
                    item -> {
                        this.favorites.set(position, !this.favorites.get(position));
                        favoriteBtnClick.clicked(this.searchIds.get(position));
                        if(this.favorites.get(position)) {
                            holder.favorite.setImageResource(R.drawable.favorite_button_filled);
                        }
                        else {
                            holder.favorite.setImageResource(R.drawable.favorite_button);
                        }
                    }

            );
        }
        if(types.get(position).equals(Constants.TYPES.USER)) {
            if (holder.followUser != null) {
                holder.followUser.setVisibility(View.VISIBLE);
                if (favorites.get(position)) {
                    holder.followUser.setBackgroundResource(R.drawable.check_icon);
                }
                else {
                    holder.followUser.setBackgroundResource(R.drawable.follow_icon);
                }

                holder.followUser.setOnClickListener(
                        item -> this.followUserBtnClick.clicked(this.searchTitles.get(position))
                );
            }
        }

        if (types.get(position).equals(Constants.TYPES.PLAYLIST)
                && searchThumbnails.get(position) == null) {
            searchThumbnails.set(position, Constants.COSMETICS.DEFAULT_PLAYLIST_IMG);
        }

        Glide.with(this.context)
                .asBitmap()
                .load(searchThumbnails.get(position))
                .into(holder.image);
        holder.searchTitle.setText(searchTitles.get(position));
        holder.appendedText.setText(appendedText.get(position));
        holder.parentLayout.setOnClickListener(
                item ->
                        this.clickListener.registerClick(
                                this.searchIds.get(position),
                                this.searchTitles.get(position),
                                this.types.get(position),
                                position
                        )
        );
        holder.parentLayout.setOnLongClickListener(
                item ->
                        this.longClickListener.onLongClick(
                                this.searchIds.get(position),
                                this.types.get(position)
                        )
        );
        toggleTrackIfIsSelected(this.searchIds.get(position));
    }

    private void toggleTrackIfIsSelected(String id) {
        if (id.equals(this.selectedViewHolderID)) {
            toggleTrackIfSelected();
        }
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull SearchViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        toggleViewHolder(holder, R.color.colorWhite);
        this.viewHolderCacheMap.remove(holder.id);
    }

    public void toggleTrackIfSelected() {
        untoggleAllViewHolders();
        if (this.selectedViewHolder != null) {
            if (this.selectedViewHolderName.equals(this.selectedViewHolder.searchTitle.getText().toString().substring(0, 4))) {
                toggleViewHolder(this.selectedViewHolder, R.color.colorTextAccent);
            }
        }
        else {
            if (this.viewHolderCacheMap.get(this.selectedViewHolderID) != null) {
                this.selectedViewHolder = this.viewHolderCacheMap.get(this.selectedViewHolderID);
                if (this.selectedViewHolder != null &&
                        this.selectedViewHolderName.equals(this.selectedViewHolder.searchTitle.getText().toString().substring(0, 4))) {
                    toggleViewHolder(this.selectedViewHolder, R.color.colorTextAccent);
                }
            }
        }
    }

    private void toggleViewHolder(SearchViewHolder vh, int color) {
        vh.searchTitle.setTextColor(this.context.getColor(color));
    }

    private void untoggleAllViewHolders() {
        for (SearchViewHolder holder : viewHolderCacheMap.values()) {
            toggleViewHolder(holder, R.color.colorWhite);
        }
    }

    public boolean selectViewHolderIfPresent(String trackID, String name) {
        this.selectedViewHolderID = trackID;
        this.selectedViewHolderName = name.substring(0, 4);
        if (this.viewHolderCacheMap.containsKey(trackID)) {
            this.selectedViewHolder = this.viewHolderCacheMap.get(trackID);
            return true;
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return searchTitles.size();
    }


    static class SearchViewHolder extends RecyclerView.ViewHolder {

        public final String id;

        private CircleImageView image;
        private TextView searchTitle;
        private TextView appendedText;
        private ImageButton favorite;
        private Button followUser;
        private RelativeLayout parentLayout;

        SearchViewHolder(@NonNull View itemView, String id) {
            super(itemView);
            this.id = id;
            this.image = itemView.findViewById(R.id.playlist_item_image);
            this.searchTitle = itemView.findViewById(R.id.playlist_item_title);
            this.appendedText = itemView.findViewById(R.id.playlist_item_uploadertxt);
            this.favorite = itemView.findViewById(R.id.search_tracks_favorite);
            this.followUser = itemView.findViewById(R.id.search_user_follow_button);
            this.parentLayout = itemView.findViewById(R.id.playlist_item);
        }
    }
}
