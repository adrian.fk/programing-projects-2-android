package com.example.Sallefy.SallefyUI.Playlist.Controller;

import com.example.Sallefy.Utils.FeaturedMediatorI;

public interface ModifyPlaylistMediator extends FeaturedMediatorI {
    void conclude();

    void openImgFile();
}
