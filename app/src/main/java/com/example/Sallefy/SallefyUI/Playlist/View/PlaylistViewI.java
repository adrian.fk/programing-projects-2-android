package com.example.Sallefy.SallefyUI.Playlist.View;

import android.view.View;

import com.example.Sallefy.SallefyUI.Playlist.Controller.ListenerInterfaces.TrackClickListener;
import com.example.Sallefy.SallefyUI.Playlist.Controller.ListenerInterfaces.TrackLongClickListener;
import com.example.Sallefy.SallefyUI.Playlist.Model.PlaylistContent;
import com.example.Sallefy.Utils.ViewInterface;

import java.util.List;

public interface PlaylistViewI extends ViewInterface {
    void setContent(
            List<PlaylistContent.SongItem> songItems,
            TrackClickListener trackClickListener,
            TrackLongClickListener trackLongClickListener
    );

    void setTitle(String title);

    void attachModifyButtonListener(View.OnClickListener listener);

    void attachFollowBtnListener(View.OnClickListener listener);

    void toastGeneralError();

    void toastConnectionError();

    void setFollowBtnState(boolean isFollowed);

    void setFollowBtnVisibility(boolean visible);

    boolean selectTrackIfExist(String trackID, String trackName);

    void toggleTrackIfSelected();
}
