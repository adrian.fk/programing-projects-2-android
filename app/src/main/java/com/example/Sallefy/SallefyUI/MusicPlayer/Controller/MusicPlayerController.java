package com.example.Sallefy.SallefyUI.MusicPlayer.Controller;

import android.os.Handler;

import com.example.Sallefy.Cast.CastProxy;
import com.example.Sallefy.Cast.CastProxyFactory;
import com.example.Sallefy.SallefyUI.MusicPlayer.Model.PlaybackLocation;
import com.example.Sallefy.SallefyUI.MusicPlayer.Model.Data;
import com.example.Sallefy.SallefyUI.MusicPlayer.View.MusicPlayerViewI;

import java.io.IOException;

public class MusicPlayerController {
    private static final String TAG = "MusicPlayerController";

    private MusicPlayerMediator playerMediator;

    private Data dataMusicPlayer;

    private MusicPlayerViewI view;

    private Handler handler;
    private Runnable progressUpdater;

    private CastProxy castProxy = CastProxyFactory.create();



    public MusicPlayerController(MusicPlayerViewI view, MusicPlayerMediator playerMediator, Data dataMusicPlayer) {
        this.playerMediator = playerMediator;
        this.view = view;
        this.dataMusicPlayer = dataMusicPlayer;
        this.handler = new Handler();

        this.view.attachPlayButtonListener(v -> this.playBtnClicked());
        this.view.attachNextButtonListener(v -> this.nextBtnClicked());
        this.view.attachPrevButtonListener(v -> this.prevBtnClicked());
        this.view.attachRepeatButtonListener(v -> this.repeatBtnClicked());
        this.view.attachShuffleButtonListener(v -> this.shuffleBtnClicked());
        this.view.attachFullscreenTriggerListener(v -> this.playerMediator.receiveFullscreenTrigger());


        this.dataMusicPlayer.provideMediaPlayer().setOnPreparedListener(
                player -> {
                    this.playerMediator.receiveDuration(player.getDuration());
                    this.playerMediator.receiveProgress(0);

                    if(this.dataMusicPlayer.getPlaybackLocation() == PlaybackLocation.LOCAL){
                        this.progressUpdater = () -> {
                            if (dataMusicPlayer.mediaPlayerExists()) {
                                playerMediator.receiveProgress(player.getCurrentPosition());
                            }
                            if(dataMusicPlayer.getPlaybackLocation() == PlaybackLocation.LOCAL) {
                                this.handler.post(progressUpdater);
                            }
                            else {
                                this.castProxy.addSeekBar(this.view.getSeekBar());
                            }
                        };
                        this.handler.post(progressUpdater);
                    }
                    else {
                        streamAudio();
                    }
                    this.dataMusicPlayer.setPlaying(true);
                });

        this.view.attachProgressBarChangeCallback((progress) -> {
            this.dataMusicPlayer.setProgress(progress);
            this.dataMusicPlayer.provideMediaPlayer().seekTo(
                    dataMusicPlayer.getProgress()
            );
        });


    }

    public void attendProgressBar() {
        if (this.progressUpdater != null) {
            this.handler.post(this.progressUpdater);
        }
    }


    public void loadAudio(String title, String uploader, String thumbnailUrl, String srcUrl) {
        if(dataMusicPlayer.mediaPlayerExists()) {
            if(dataMusicPlayer.isPlaying()) {
                this.dataMusicPlayer.updatePlayState(false);
            }
            dataMusicPlayer.provideMediaPlayer().reset();
        }
        try {
            dataMusicPlayer.provideMediaPlayer().setDataSource(srcUrl);
            dataMusicPlayer.provideMediaPlayer().prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.view.setContent(title, uploader, thumbnailUrl);
    }

    private void streamAudio() {
        this.castProxy.addSeekBar(this.view.getSeekBar());
        this.dataMusicPlayer.provideMediaPlayer().stop();
        this.castProxy.castMedia(
                this.dataMusicPlayer.getCurrentlyPlaying().getTitle(),
                this.dataMusicPlayer.getCurrentlyPlaying().getSrcUrl(),
                this.dataMusicPlayer.getCurrentlyPlaying().getThumbnailUrl(),
                this.dataMusicPlayer.getProgress(),
                this.dataMusicPlayer.getDuration(),
                true
        );
    }

    private void shuffleBtnClicked() {
        this.playerMediator.receiveShuffleTrigger(!this.dataMusicPlayer.isShuffle());
    }

    private void repeatBtnClicked() {
        this.playerMediator.receiveRepeatTrigger(!this.dataMusicPlayer.isRepeat());
    }

    private void prevBtnClicked() {
        this.view.setPrevBtnClicked(true);
        this.dataMusicPlayer.previous();
    }

    private void nextBtnClicked() {
        this.view.setNextBtnClicked(true);
        this.playerMediator.receiveRepeatTrigger(false);
        this.dataMusicPlayer.next();
    }

    private void playBtnClicked() {
        if(this.dataMusicPlayer.provideMediaPlayer() != null
                && this.dataMusicPlayer.provideMediaPlayer().getDuration() != 0) {
            this.dataMusicPlayer.setPlaying(
                    !this.dataMusicPlayer.isPlaying()
            );
        }
    }

    public void processPlay(boolean playTrigger) {
        if(playTrigger) {
            startPlaying();
        }
        else {
            stopPlaying();
        }
    }

    private void startPlaying() {
        this.view.setPlayBtnState(this.view.STATE_PLAY_BUTTON_PAUSE);
        if (this.dataMusicPlayer.getPlaybackLocation() == PlaybackLocation.REMOTE) {
            this.castProxy.togglePlayback(true);
        }
        else {
            this.dataMusicPlayer.updatePlayState(true);
        }
    }

    private void stopPlaying() {
        if(this.dataMusicPlayer.getPlaybackLocation() == PlaybackLocation.LOCAL) {
            dataMusicPlayer.provideMediaPlayer().seekTo(
                    dataMusicPlayer.provideMediaPlayer().getCurrentPosition()
            );
            this.dataMusicPlayer.updatePlayState(false);
        }
        else {
            this.castProxy.togglePlayback(false);
        }
        this.view.setPlayBtnState(this.view.STATE_PLAY_BUTTON_PLAY);
    }
}
