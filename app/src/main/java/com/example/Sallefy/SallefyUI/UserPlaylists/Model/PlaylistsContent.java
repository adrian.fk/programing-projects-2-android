package com.example.Sallefy.SallefyUI.UserPlaylists.Model;

import java.util.ArrayList;

public class PlaylistsContent {
    private ArrayList<Playlist> playlists = new ArrayList<>();

    public void addPlaylist(Playlist playlist) {
        playlists.add(playlist);
    }

    public Playlist createPlaylist(String id, String title, String imgUrl) {
        return new Playlist(id, title, imgUrl);
    }

    public ArrayList<Playlist> getPlaylists() {
        return this.playlists;
    }

    public static class Playlist {
        public final String id;
        public final String title;
        public final String imgUrl;

        private Playlist(String id, String title, String imgUrl) {
            this.id = id;
            this.title = title;
            this.imgUrl = imgUrl;
        }
    }
}
