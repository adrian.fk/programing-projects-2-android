package com.example.Sallefy.SallefyUI.User.Model;

import com.example.Sallefy.Utils.AbstractViewModel;


public final class SongsContent extends AbstractViewModel<SongsContent.Song> {
    public Song createSong(long id,
                           String title,
                           String owner,
                           String imgUrl,
                           String srcUrl,
                           boolean favorite) {

        return new Song(id, title, owner, imgUrl, srcUrl, favorite);
    }

    @Override
    public void addItem(Song songItem) {
        this.items.add(songItem);
    }


    public static class Song {
        public final long id;
        public final String title;
        public final String owner;
        public final String imgUrl;
        public final String srcUrl;
        public final boolean favorite;

        Song(long id, String title, String owner, String imgUrl, String srcUrl, boolean favorite) {
            this.id = id;
            this.title = title;
            this.owner = owner;
            this.imgUrl = imgUrl;
            this.srcUrl = srcUrl;
            this.favorite = favorite;
        }
    }
}
