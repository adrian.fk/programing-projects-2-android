package com.example.Sallefy.SallefyUI.Search.Controller.ListenerInterfaces;

public interface SearchStartBtnClickListener {
    void updateFilter(String filterName);
}
