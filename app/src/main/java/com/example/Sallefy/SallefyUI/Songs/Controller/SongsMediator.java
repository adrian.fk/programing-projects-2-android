package com.example.Sallefy.SallefyUI.Songs.Controller;

import com.example.Sallefy.Utils.FeaturedMediatorI;


public interface SongsMediator extends FeaturedMediatorI {

    void openSongOptions(String id);
}
