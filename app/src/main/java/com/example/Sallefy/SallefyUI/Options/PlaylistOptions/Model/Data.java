package com.example.Sallefy.SallefyUI.Options.PlaylistOptions.Model;

import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;

public class Data {
    private Long playlistID;

    private PlaylistDTO currentPlaylist;

    private boolean isFollowed;


    public Data() {
    }

    public Data(long playlistID) {
        this.playlistID = playlistID;
    }

    public Long getPlaylistID() {
        return playlistID;
    }

    public void setPlaylistID(long playlistID) {
        this.playlistID = playlistID;
    }

    public PlaylistDTO getCurrentPlaylist() {
        return currentPlaylist;
    }

    public void setCurrentPlaylist(PlaylistDTO currentPlaylist) {
        this.currentPlaylist = currentPlaylist;
    }

    public boolean isFollowed() {
        return isFollowed;
    }

    public void setFollowed(boolean followed) {
        isFollowed = followed;
    }
}
