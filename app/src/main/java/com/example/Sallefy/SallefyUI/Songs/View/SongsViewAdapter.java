package com.example.Sallefy.SallefyUI.Songs.View;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Songs.Controller.ListenerInterfaces.SongFavoriteBtnClick;
import com.example.Sallefy.SallefyUI.Songs.Controller.ListenerInterfaces.SongItemClickListener;
import com.example.Sallefy.SallefyUI.Songs.Controller.ListenerInterfaces.SongItemLongClickListener;
import com.example.Sallefy.SallefyUI.Songs.Model.SongsContent;
import com.example.Sallefy.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class SongsViewAdapter extends RecyclerView.Adapter<SongsViewAdapter.SongsViewHolder> {
    private static final String TAG = "PlaylistViewAdapter";

    private SongItemLongClickListener longClickListener;
    private SongItemClickListener clickListener;
    private SongFavoriteBtnClick favoriteBtnClick;

    private ArrayList<String> types;
    private ArrayList<Boolean> favorites;

    private ArrayList<String> songIds;
    private ArrayList<String> songTitles;
    private ArrayList<String> appendedText;
    private ArrayList<String> songThumbnails;

    private Context context;

    public SongsViewAdapter(Context context,
                            List<SongsContent.SongItem> items,
                            SongItemClickListener clickListener,
                            SongItemLongClickListener longClickListener,
                            SongFavoriteBtnClick favoriteBtnClick) {
        this.clickListener = clickListener;
        this.longClickListener = longClickListener;
        this.favoriteBtnClick = favoriteBtnClick;
        this.types = new ArrayList<>();
        this.songIds = new ArrayList<>();
        this.songTitles = new ArrayList<>();
        this.appendedText = new ArrayList<>();
        this.songThumbnails = new ArrayList<>();
        this.favorites = new ArrayList<>();
        this.context = context;

        for(SongsContent.SongItem item : items) {
            this.types.add(item.type);
            this.songIds.add(item.id);
            this.songTitles.add(item.title);
            this.appendedText.add(item.uploader);
            this.songThumbnails.add(item.imgUrl);
            this.favorites.add(item.favorite);
        }
    }

    @NonNull
    @Override
    public SongsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.song_item, parent, false);
        SongsViewHolder holder = new SongsViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull SongsViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: called -  " + position);
        if(types.get(position).equals(Constants.TYPES.TRACK)) {
            holder.favorite.setVisibility(View.VISIBLE);
            if(this.favorites.get(position)) {
                holder.favorite.setImageResource(R.drawable.favorite_button_filled);
            }
            holder.favorite.setOnClickListener(
                    item -> {
                        this.favorites.set(position, !this.favorites.get(position));
                        favoriteBtnClick.clicked(this.songIds.get(position));
                        if(this.favorites.get(position)) {
                            holder.favorite.setImageResource(R.drawable.favorite_button_filled);
                        }
                        else {
                            holder.favorite.setImageResource(R.drawable.favorite_button);
                        }

                    }

            );
        }

        Glide.with(this.context)
                .asBitmap()
                .load(songThumbnails.get(position))
                .into(holder.image);
        holder.searchTitle.setText(songTitles.get(position));
        holder.appendedText.setText(appendedText.get(position));
        holder.parentLayout.setOnClickListener(
                item ->
                        this.clickListener.registerClick(
                                this.songIds.get(position),
                                position
                        )
        );
        holder.parentLayout.setOnLongClickListener(
                item -> {
                    this.longClickListener.onLongClick(
                            songIds.get(position)
                    );
                    return true;
                }
        );
    }

    @Override
    public int getItemCount() {
        return songTitles.size();
    }


    public class SongsViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView image;
        private TextView searchTitle;
        private TextView appendedText;
        private ImageButton favorite;
        private RelativeLayout parentLayout;

        public SongsViewHolder(@NonNull View itemView) {
            super(itemView);
            this.image = itemView.findViewById(R.id.playlist_item_image);
            this.searchTitle = itemView.findViewById(R.id.playlist_item_title);
            this.appendedText = itemView.findViewById(R.id.playlist_item_uploadertxt);
            this.favorite = itemView.findViewById(R.id.search_tracks_favorite);
            this.parentLayout = itemView.findViewById(R.id.playlist_item);
        }
    }
}
