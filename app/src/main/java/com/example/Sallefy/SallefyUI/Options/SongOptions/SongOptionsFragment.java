package com.example.Sallefy.SallefyUI.Options.SongOptions;




import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacade;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacadeFactory;
import com.example.Sallefy.SallefyUI.Options.SongOptions.Controller.SongOptionsController;
import com.example.Sallefy.SallefyUI.Options.SongOptions.Controller.SongOptionsMediator;
import com.example.Sallefy.SallefyUI.Options.SongOptions.Model.DataSongOptions;
import com.example.Sallefy.SallefyUI.Options.SongOptions.View.SongOptionsView;
import com.example.Sallefy.Utils.Constants;
import com.example.Sallefy.repositoryImpl.factoriesImpl.TrackApiRepositoryFactory;


import com.example.Sallefy.repositoryImpl.repositoryImpl.SessionBearer;


public class SongOptionsFragment extends Fragment implements SongOptionsMediator {
    private static final String TAG = "SongOptionsFragment";
    private static final String baseTrackDeepLink = "https://sallefy.com/tracks/";

    private MusicPlayerFacade musicPlayer = MusicPlayerFacadeFactory.create();


    private SongOptionsView view;
    private SongOptionsController controller;
    private DataSongOptions dataSongOptions;


    public SongOptionsFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = new SongOptionsView(inflater, container);

        Animation fadeInAnim = AnimationUtils.loadAnimation(this.view.getRootView().getContext(), R.anim.fade_in);
        this.view.getRootView().startAnimation(fadeInAnim);

        Bundle specifications = getArguments();
        if (specifications != null) {
            String songId = specifications.getString(Constants.SONG_SPECIFICATIONS.SONG_ID);
            String userLogin = SessionBearer.getUsernameIfThere();


            if (songId != null && userLogin != null) {
                TrackApiRepositoryFactory trackApiRepositoryFactory = new TrackApiRepositoryFactory();

                this.dataSongOptions = new DataSongOptions();

                this.controller = new SongOptionsController(this, this.view, this.dataSongOptions, trackApiRepositoryFactory);

                this.controller.configureSongObj(Long.parseLong(songId), userLogin);
            } else {
                Log.d(TAG, "onCreateView: Not sufficient information - either userLogin or songId is missing");
                navBack();
            }
        } else {
            Log.d(TAG, "onCreateView: songId not passed in");
            navBack();
        }
        return this.view.getRootView();
    }

    public void navBack() {
        Navigation
                .findNavController(this.view.getRootView())
                .popBackStack();
    }


    @Override
    public LifecycleOwner provideOwner() {
        return this;
    }

    @Override
    public void receivePlaySongTrigger() {
        this.musicPlayer.playSong(this.dataSongOptions.getSongId());
    }

    @Override
    public void receiveFavoriteTrigger() {

    }

    @Override
    public void receiveEnqueueTrigger() {
        this.musicPlayer.enqueueSong(
                this.dataSongOptions.getSongId(),
                this.dataSongOptions.getSongTitle(),
                this.dataSongOptions.getUploaderName(),
                this.dataSongOptions.getSongImg(),
                this.dataSongOptions.getSongUrl()
        );
        this.view.toastMsg(this.dataSongOptions.getSongTitle() + " enqueued");
    }

    @Override
    public void receiveUploaderProfileTrigger() {
        Bundle b = new Bundle();
        b.putString(Constants.USER_SPECIFICATIONS.USER_NAME, this.dataSongOptions.getUploaderName());

        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_user_profile, b);
    }

    @Override
    public void openAddSongToPlaylists() {
        Bundle b = new Bundle();
        b.putString(
                Constants.PLAYLISTS_STATE.STATE_KEY,
                Constants.PLAYLISTS_STATE.ADD_SONG_TO_PLAYLIST
        );
        b.putString(
                Constants.SONG_SPECIFICATIONS.SONG_ID,
                String.valueOf(this.dataSongOptions.getSongId())
        );

        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_my_playlists, b);
    }

    @Override
    public void conclude() {
        NavController navController = Navigation.findNavController(this.view.getRootView());
        if (!navController.popBackStack())
                navController.navigate(R.id.nav_home);
    }

    @Override
    public void receiveShareTrigger(String title, String url) {
        Intent sendIntent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString("url", url);


        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Hey listen to " + title + " on Sallefy!\n" + url);
        sendIntent.setType("text/plain");

        Intent shareIntent = Intent.createChooser(sendIntent, "Sharing " + title);
        startActivity(shareIntent);
    }

    private String generateDeepLinkUri(long id) {
        return baseTrackDeepLink + id;
    }
}
