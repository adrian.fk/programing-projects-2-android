package com.example.Sallefy.SallefyUI.UserPlaylists;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.UserPlaylists.Controller.UserPlaylistsController;
import com.example.Sallefy.SallefyUI.UserPlaylists.Controller.UserPlaylistsMediator;
import com.example.Sallefy.SallefyUI.UserPlaylists.Model.DataUserPlaylist;
import com.example.Sallefy.SallefyUI.UserPlaylists.View.UserPlaylistsView;
import com.example.Sallefy.Utils.Constants;
import com.example.Sallefy.repositoryImpl.factoriesImpl.PlaylistApiRepositoryFactory;
import com.example.Sallefy.repositoryImpl.factoriesImpl.TrackApiRepositoryFactory;
import com.example.Sallefy.repositoryImpl.factoriesImpl.UserApiRepositoryFactory;

public class UserPlaylistsFragment extends Fragment implements UserPlaylistsMediator {
    private UserPlaylistsController controller;
    private UserPlaylistsView view;
    private DataUserPlaylist dataUserPlaylist;

    private Bundle specifications;

    public UserPlaylistsFragment() {
        this.dataUserPlaylist = new DataUserPlaylist();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.specifications = getArguments();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = new UserPlaylistsView(inflater, container);

        Animation fadeInAnim = AnimationUtils.loadAnimation(this.view.getRootView().getContext(), R.anim.fade_in);
        this.view.getRootView().startAnimation(fadeInAnim);

        initiate();

        return this.view.getRootView();
    }

    private void initiate() {
        TrackApiRepositoryFactory trackApiRepositoryFactory = new TrackApiRepositoryFactory();
        PlaylistApiRepositoryFactory playlistApiRepositoryFactory = new PlaylistApiRepositoryFactory();
        UserApiRepositoryFactory userApiRepositoryFactory = new UserApiRepositoryFactory();

        this.controller = new UserPlaylistsController(
                this,
                this.view,
                this.dataUserPlaylist,
                trackApiRepositoryFactory,
                playlistApiRepositoryFactory,
                userApiRepositoryFactory
        );
        if(this.specifications != null) {
            this.dataUserPlaylist.setState(
                    this.specifications.getString(Constants.PLAYLISTS_STATE.STATE_KEY)
            );
            this.dataUserPlaylist.setSongId(
                    this.specifications.getString(Constants.SONG_SPECIFICATIONS.SONG_ID)
            );
        }
        else {
            this.dataUserPlaylist.setState(Constants.PLAYLISTS_STATE.BROWSE);
        }
        evaluateControllerInitiation();
        this.controller.fetchUserPlaylists();
    }

    private void evaluateControllerInitiation() {
        switch (this.dataUserPlaylist.getState()) {
            case Constants.PLAYLISTS_STATE.BROWSE:
                this.controller.initiateBrowseMode();
                break;

            case Constants.PLAYLISTS_STATE.ADD_SONG_TO_PLAYLIST:
                this.controller.initiateAddSongMode();
                break;
        }
    }

    @Override
    public void concludeUserPlaylists() {
        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_home);
    }

    @Override
    public void openPlaylist(String id) {
        Bundle b = new Bundle();
        b.putString(Constants.PLAYLIST_SPECIFICATIONS.PLAYLIST_ID, id);

        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_to_playlist, b);
    }

    @Override
    public void openPlaylistOptions(String id) {
        Bundle b = new Bundle();
        b.putString(Constants.PLAYLIST_SPECIFICATIONS.PLAYLIST_ID, id);

        Navigation
                .findNavController(this.view.getRootView())
                .navigate(R.id.nav_playlist_options, b);
    }

    @Override
    public LifecycleOwner provideOwner() {
        return this;
    }
}
