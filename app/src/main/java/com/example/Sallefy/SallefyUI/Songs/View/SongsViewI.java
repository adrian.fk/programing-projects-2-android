package com.example.Sallefy.SallefyUI.Songs.View;

import com.example.Sallefy.SallefyUI.Songs.Controller.ListenerInterfaces.SongFavoriteBtnClick;
import com.example.Sallefy.SallefyUI.Songs.Controller.ListenerInterfaces.SongItemClickListener;
import com.example.Sallefy.SallefyUI.Songs.Controller.ListenerInterfaces.SongItemLongClickListener;
import com.example.Sallefy.SallefyUI.Songs.Model.SongsContent;
import com.example.Sallefy.Utils.ViewInterface;

import java.util.List;

public interface SongsViewI extends ViewInterface {
    void displaySongs(
            List<SongsContent.SongItem> songs,
            SongItemClickListener songItemClickListener,
            SongItemLongClickListener songItemLongClickListener,
            SongFavoriteBtnClick songFavoriteBtnClick);
}
