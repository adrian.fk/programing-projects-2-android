package com.example.Sallefy.SallefyUI.Search;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.Sallefy.SallefyUI.Search.Controller.SearchStartController;
import com.example.Sallefy.SallefyUI.Search.Model.DataSearch;
import com.example.Sallefy.SallefyUI.Search.Model.DataSearchFactory;
import com.example.Sallefy.SallefyUI.Search.View.SearchStartView;


public class SearchStartFragment extends Fragment {
    private SearchStartController controller;
    private SearchStartView view;
    private DataSearch dataSearch;

    public SearchStartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.dataSearch = DataSearchFactory.create();
        this.controller = new SearchStartController(this.dataSearch);

        this.view = new SearchStartView(inflater,
                container,
                controller::updateFilter,
                this.dataSearch.getSearchFilter()
        );


        return this.view.getRootView();
    }
}
