package com.example.Sallefy.SallefyUI.Search.Controller.ListenerInterfaces;

public interface SearchItemLongClickListener {
    boolean onLongClick(String id, String type);
}
