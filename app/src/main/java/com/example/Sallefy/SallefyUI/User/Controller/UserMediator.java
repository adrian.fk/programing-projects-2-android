package com.example.Sallefy.SallefyUI.User.Controller;

import com.example.Sallefy.Utils.FeaturedMediatorI;

public interface UserMediator extends FeaturedMediatorI {
    void requestLogout();

    void openSongOptions(String id);

    void openPlaylistOptions(String id);

    void openPlaylist(String id);

    void openImgFile();

    void playSong(long id);

    void conclude();
}
