package com.example.Sallefy.SallefyUI.Playlist.View;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Playlist.Controller.ListenerInterfaces.TrackClickListener;
import com.example.Sallefy.SallefyUI.Playlist.Controller.ListenerInterfaces.TrackLongClickListener;
import com.example.Sallefy.SallefyUI.Playlist.Model.PlaylistContent;
import com.example.Sallefy.SallefyUI.Playlist.View.Adapter.PlaylistViewAdapter;

import java.util.List;

public class PlaylistView implements PlaylistViewI {
    private static final String TAG = "PlaylistView";

    private PlaylistViewAdapter adapter;

    private Button modifyButton;

    private Button followButton;

    private View rootView;

    private TextView playlistTitle;

    private RecyclerView songsList;


    public PlaylistView(LayoutInflater inflater, ViewGroup container) {
        this.rootView = inflater.inflate(R.layout.fragment_playlist, container, false);
        Log.d(TAG, "PlaylistView: View inflated");
        findViewsById();
        Log.d(TAG, "PlaylistView: View elements found");
    }

    @Override
    public void setContent(List<PlaylistContent.SongItem> songItems,
                           TrackClickListener trackClickListener,
                           TrackLongClickListener trackLongClickListener) {
        if(songItems.size() == 0) {
            this.rootView.findViewById(R.id.playlist_empty_txt).setVisibility(View.VISIBLE);
        }
        else {
            this.rootView.findViewById(R.id.playlist_empty_txt).setVisibility(View.GONE);

            Log.d(TAG, "initRecyclerView: initRecyclerView()");
            this.adapter = new PlaylistViewAdapter(
                    this.rootView.getContext(),
                    songItems,
                    trackClickListener,
                    trackLongClickListener
            );
            this.songsList.setAdapter(this.adapter);
            this.songsList.setLayoutManager(new LinearLayoutManager(this.rootView.getContext()));
        }
    }

    private void findViewsById() {
        this.playlistTitle = this.rootView.findViewById(R.id.playlist_title);
        this.songsList = this.rootView.findViewById(R.id.playlist_list);
        this.modifyButton = this.rootView.findViewById(R.id.nav_playlist_rename);
        this.followButton = this.rootView.findViewById(R.id.playlist_follow_button);
    }

    @Override
    public void setTitle(String s) {
        playlistTitle.setText(s);
    }

    @Override
    public void attachModifyButtonListener(View.OnClickListener listener) {
        modifyButton.setOnClickListener(listener);
    }

    @Override
    public void attachFollowBtnListener(View.OnClickListener listener) {
        this.followButton.setOnClickListener(listener);
    }

    @Override
    public void toastGeneralError() {
        Toast toast = Toast.makeText(
                this.rootView.getContext(),
                "Oops! Something went wrong.. ",
                Toast.LENGTH_SHORT
        );
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    @Override
    public void toastConnectionError() {
        Toast toast = Toast.makeText(
                this.rootView.getContext(),
                "Error occurred when contacting server",
                Toast.LENGTH_SHORT
        );
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    @Override
    public void setFollowBtnState(boolean isFollowed) {
        if(isFollowed) {
            this.followButton.setBackgroundResource(R.drawable.rounded_button_untoggled);
            this.followButton.setText(R.string.playlist_unfollowbtn);
        }
        else {
            this.followButton.setBackgroundResource(R.drawable.rounded_button_toggled);
            this.followButton.setText(R.string.playlist_followbtn);
        }
    }

    @Override
    public void setFollowBtnVisibility(boolean visible) {
        if(visible) {
            this.followButton.setVisibility(View.VISIBLE);
        }
        else {
            this.followButton.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean selectTrackIfExist(String trackID, String trackName) {
        if (this.adapter != null) {
            return this.adapter.selectViewHolderIfPresent(trackID, trackName);
        }
        return false;
    }

    @Override
    public void toggleTrackIfSelected() {
        if (this.adapter != null) {
            this.adapter.toggleViewHolderIfSelected();
        }
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        Bundle b = new Bundle();
        b.putString("playlistTitle", this.playlistTitle.getText().toString());
        return b;
    }
}
