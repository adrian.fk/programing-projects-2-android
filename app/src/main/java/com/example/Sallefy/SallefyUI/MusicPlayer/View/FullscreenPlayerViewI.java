package com.example.Sallefy.SallefyUI.MusicPlayer.View;

import android.view.View;
import android.widget.SeekBar;

import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.ListenerInterfaces.ProgressBarUpdatedCallback;
import com.example.Sallefy.Utils.ViewInterface;

public interface FullscreenPlayerViewI extends ViewInterface {

    void attachImageLongPressListener(View.OnLongClickListener listener);

    void attachProgressBarListener(SeekBar.OnSeekBarChangeListener listener);

    void attachPlayButtonListener(View.OnClickListener listener);

    void attachPrevButtonListener(View.OnClickListener listener);

    void attachNextButtonListener(View.OnClickListener listener);

    void attachRepeatButtonListener(View.OnClickListener listener);

    void attachShuffleButtonListener(View.OnClickListener listener);

    void attachListingButtonListener(View.OnClickListener listener);

    SeekBar getSeekbar();

    void setTextTitle(String input);

    void setTextUploader(String input);

    void setNextBtnClicked(Boolean clicked);

    void setPrevBtnClicked(Boolean clicked);

    void setSongImage(String imgUrl);

    void setShuffleBtnState(Boolean state);

    void setRepeatBtnState(Boolean state);

    void setPlayState(Boolean isPlaying);

    void attachProgressBarChangeCallback(ProgressBarUpdatedCallback listener);

    void updateProgressBar(int progress);

    void setDurationProgressBar(int duration);
}
