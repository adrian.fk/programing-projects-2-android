package com.example.Sallefy.SallefyUI.Playlist.View;

import android.net.Uri;
import android.view.View;

import com.example.Sallefy.Utils.ViewInterface;

public interface ModifyPlaylistViewI extends ViewInterface {
    void attachConfirmBtnClickListener(View.OnClickListener listener);

    void displayPlaylistTitle(String title);

    void displayPlaylistImg(String imgUrl);

    void displayPlaylistImg(Uri imgUri);

    void attachPlaylistImgListener(View.OnClickListener listener);

    void displayNoImgAdded();

    void setConfirmBtnState(boolean clickable);

    String getNewTitle();

    void toastMsg(String msg);
}
