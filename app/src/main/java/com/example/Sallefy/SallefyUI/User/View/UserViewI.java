package com.example.Sallefy.SallefyUI.User.View;

import android.net.Uri;
import android.view.View;

import com.example.Sallefy.SallefyUI.Songs.Controller.ListenerInterfaces.SongFavoriteBtnClick;
import com.example.Sallefy.SallefyUI.Songs.Controller.ListenerInterfaces.SongItemClickListener;
import com.example.Sallefy.SallefyUI.Songs.Controller.ListenerInterfaces.SongItemLongClickListener;
import com.example.Sallefy.SallefyUI.Songs.Model.SongsContent;
import com.example.Sallefy.SallefyUI.UserPlaylists.Controller.ListenerInterfaces.PlaylistClickListener;
import com.example.Sallefy.SallefyUI.UserPlaylists.Model.PlaylistsContent;
import com.example.Sallefy.Utils.ViewInterface;

import java.util.ArrayList;
import java.util.List;

public interface UserViewI extends ViewInterface {
    void attachPlaylistClickedListener(PlaylistClickListener listener);

    void attachPlaylistLongClickedListener(PlaylistClickListener listener);

    void attachSongClickedListener(SongItemClickListener listener);

    void attachSongLongClickedListener(SongItemLongClickListener listener);

    void attachSongFavBtnClickedListener(SongFavoriteBtnClick listener);

    void attachLogoutBtnClickListener(View.OnClickListener listener);

    void attachUserImgClickListener(View.OnClickListener listener);

    void showUserConfiguration(boolean isUser);

    void displaySongsContent(List<SongsContent.SongItem> listItems);

    void displayPlaylistsContent(ArrayList<PlaylistsContent.Playlist> listItems);

    void setUser(String username, String imgUrl, String followers, String following);

    void displayUserImg(Uri userImgUri);

    void displayUserImg(String userImgUrl);

    void displayNoImgAdded();

    void toastMsg(String msg);

    void setHeaderVisibility(boolean visible);

    void setContentVisibility(boolean visible);
}
