package com.example.Sallefy.SallefyUI.Home.View.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Home.Controller.ListenerInterfaces.FollowerClickListener;
import com.example.Sallefy.SallefyUI.Home.Controller.ListenerInterfaces.FollowerLongClickListener;
import com.example.Sallefy.SallefyUI.Home.Model.FollowersHomeContent;
import com.example.Sallefy.Utils.Constants;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class FollowersHomeViewAdapter extends RecyclerView.Adapter<FollowersHomeViewAdapter.FollowersViewHolder>{
    private static final String TAG = "FollowersHomeViewAdapte";

    private FollowerClickListener followerClickListener;
    private FollowerLongClickListener followerLongClickListener;

    private ArrayList<String> userIds;
    private ArrayList<String> userLogins;
    private ArrayList<String> imgUrls;


    private Context context;

    public FollowersHomeViewAdapter(Context context,
                                    List<FollowersHomeContent.Follower> items,
                                    FollowerClickListener followerClickListener,
                                    FollowerLongClickListener followerLongClickListener) {
        this.userIds = new ArrayList<>();
        this.userLogins = new ArrayList<>();
        this.imgUrls = new ArrayList<>();
        this.followerClickListener = followerClickListener;
        this.followerLongClickListener = followerLongClickListener;
        this.context = context;

        for(FollowersHomeContent.Follower item : items) {
            this.userIds.add(item.id);
            this.userLogins.add(item.userLogin);
            this.imgUrls.add(item.imgUrl);
        }
    }

    @NonNull
    @Override
    public FollowersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.followers_home_listitem, parent, false);
        return new FollowersViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull FollowersViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: called -  " + position);
        if (this.imgUrls.get(position) != null && this.imgUrls.get(position).length() > 0) {
            Glide.with(this.context)
                    .asBitmap()
                    .load(imgUrls.get(position))
                    .into(holder.image);
        }
        else {
            Glide.with(this.context)
                    .asBitmap()
                    .load(Constants.COSMETICS.DEFAULT_USR_IMG)
                    .into(holder.image);
        }
        holder.userLogin.setText(userLogins.get(position));
        holder.parentLayout.setOnClickListener(
                item -> this.followerClickListener.onClicked(this.userLogins.get(position))
        );
        holder.parentLayout.setOnLongClickListener(
                item -> {
                    this.followerLongClickListener.onLongClick(this.userLogins.get(position));
                    return true;
                }
        );
    }

    @Override
    public int getItemCount() {
        return userLogins.size();
    }


    static class FollowersViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView image;
        private TextView userLogin;
        private LinearLayout parentLayout;

        FollowersViewHolder(@NonNull View itemView) {
            super(itemView);
            this.image = itemView.findViewById(R.id.follower_card_image);
            this.userLogin = itemView.findViewById(R.id.follower_card_userlogin);
            this.parentLayout = itemView.findViewById(R.id.home_followers_card);
        }
    }
}
