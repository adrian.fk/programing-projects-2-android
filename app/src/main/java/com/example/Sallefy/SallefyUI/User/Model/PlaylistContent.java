package com.example.Sallefy.SallefyUI.User.Model;

import com.example.Sallefy.Utils.AbstractViewModel;

public class PlaylistContent extends AbstractViewModel<PlaylistContent.Playlist> {

    @Override
    public void addItem(Playlist item) {
        this.items.add(item);
    }

    public Playlist createPlaylist(String id, String title, String imgUrl) {
        return new Playlist(id, title, imgUrl);
    }

    public static class Playlist {
        public final String id;
        public final String title;
        public final String imgUrl;

        Playlist(String id, String title, String imgUrl) {
            this.id = id;
            this.title = title;
            this.imgUrl = imgUrl;
        }
    }
}
