package com.example.Sallefy.SallefyUI.MusicPlayer.Controller;

import com.example.Sallefy.Utils.FeaturedMediatorI;

public interface MusicPlayerMediator extends FeaturedMediatorI {


    void receiveProgress(int progress);

    void receiveDuration(int duration);

    void receiveShuffleTrigger(boolean shuffleTrigger);

    void receiveRepeatTrigger(boolean repeatTrigger);

    void receiveFullscreenTrigger();
}
