package com.example.Sallefy.SallefyUI.Search.View;

import android.os.Bundle;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Search.Controller.ListenerInterfaces.SearchBarListenerI;
import com.example.Sallefy.Utils.ViewInterface;

public class SearchBarView implements ViewInterface {
    private static final String TAG = "SearchBarView";
    private final View rootView;

    private EditText searchKeyword;

    public SearchBarView(View rootView, TextWatcher textWatcher, SearchBarListenerI listener) {
        this.rootView = rootView;
        Log.d(TAG, "SearchBarView inflated");
        findViewsById();
        this.searchKeyword.addTextChangedListener(textWatcher);
        this.searchKeyword.setOnClickListener(listener);
    }

    private void findViewsById() {
        this.searchKeyword = this.rootView.findViewById(R.id.search_keyword);
    }

    public String getSearchKeyword() {
        return this.searchKeyword.getText().toString();
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword.setText(searchKeyword);
    }

    public void clearKeywordFocus() {
        this.searchKeyword.clearFocus();
    }

    public boolean keywordExist() {
        return searchKeyword.getText().toString().length() > 0;
    }

    @Override
    public View getRootView() {
        return this.rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }
}
