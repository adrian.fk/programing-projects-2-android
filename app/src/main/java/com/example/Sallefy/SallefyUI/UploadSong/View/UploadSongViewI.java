package com.example.Sallefy.SallefyUI.UploadSong.View;

import android.net.Uri;
import android.view.View;

import com.example.Sallefy.SallefyUI.UploadSong.Controller.ListenerInterfaces.ChooseFileListener;
import com.example.Sallefy.SallefyUI.UploadSong.Controller.ListenerInterfaces.UploadButtonListener;
import com.example.Sallefy.Utils.SingleMethodListener;
import com.example.Sallefy.Utils.ViewInterface;

public interface UploadSongViewI extends ViewInterface {
    void attachUploadBtnListener(UploadButtonListener listener);

    void attachChooseFileListener(ChooseFileListener listener);

    void setUploadBtnState(boolean clickable);

    void setChooseFileBtnState(boolean triggered);

    void toastMessage(String msg);

    void toastErrorMsg(String errMsg);

    String getSongName();

    void attachTextChangedListener(SingleMethodListener listener);

    void displaySongImg(Uri imgUri);

    void attachSongImgListener(View.OnClickListener listener);

    void displayNoImgAdded();

    void toastMsg(String msg);
}
