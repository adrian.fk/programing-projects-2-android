package com.example.Sallefy.SallefyUI.PlaylistMenu.Model;

import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;

public class DataPlaylistMenuMapper {
    private DataPlaylistMenu dataPlaylistMenu;

    public DataPlaylistMenuMapper(DataPlaylistMenu dataPlaylistMenu) {
        this.dataPlaylistMenu = dataPlaylistMenu;
    }

    public void mapPlaylistDTO(PlaylistDTO playlistDTO) {
        this.dataPlaylistMenu.addPlaylist(
                dataPlaylistMenu.createPlaylist(
                        String.valueOf(playlistDTO.getId()),
                        playlistDTO.getName(),
                        playlistDTO.getThumbnail()
                )
        );
    }
}
