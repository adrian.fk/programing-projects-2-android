package com.example.Sallefy.SallefyUI.User.View;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Songs.Controller.ListenerInterfaces.SongFavoriteBtnClick;
import com.example.Sallefy.SallefyUI.Songs.Controller.ListenerInterfaces.SongItemClickListener;
import com.example.Sallefy.SallefyUI.Songs.Controller.ListenerInterfaces.SongItemLongClickListener;
import com.example.Sallefy.SallefyUI.Songs.View.SongsViewAdapter;
import com.example.Sallefy.SallefyUI.UserPlaylists.Controller.ListenerInterfaces.PlaylistClickListener;
import com.example.Sallefy.SallefyUI.UserPlaylists.Model.PlaylistsContent;
import com.example.Sallefy.SallefyUI.UserPlaylists.View.UserPlaylistsViewAdapter;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserView implements UserViewI{
    private View rootView;

    private SongItemClickListener songClickListener;
    private SongItemLongClickListener songLongClickListener;
    private SongFavoriteBtnClick songFavoriteClickListener;

    private PlaylistClickListener playlistClickListener;
    private PlaylistClickListener playlistLongClickListener;

    private LinearLayout userHeader;
    private LinearLayout contentSection;

    private CircleImageView userImage;
    private TextView txtUsername;
    private TextView txtUserFollowers;
    private TextView txtUserFollowing;

    private Button logoutButton;

    private RecyclerView userSongs;
    private RecyclerView userPlaylists;

    public UserView(@NonNull LayoutInflater inflater, ViewGroup container) {
        this.rootView = inflater.inflate(R.layout.fragment_user, container, false);

        findViews();

        setHeaderVisibility(false);
        setContentVisibility(false);
    }

    private void findViews() {
        this.userHeader = getRootView().findViewById(R.id.user_header);
        this.contentSection = getRootView().findViewById(R.id.userprofile_lists_layout);

        this.userImage = getRootView().findViewById(R.id.userprofile_user_image);
        this.txtUsername = getRootView().findViewById(R.id.userprofile_username);
        this.txtUserFollowers = getRootView().findViewById(R.id.userprofile_num_followers);
        this.txtUserFollowing = getRootView().findViewById(R.id.userprofile_num_following);

        this.userSongs = getRootView().findViewById(R.id.songs_list);
        this.userPlaylists = getRootView().findViewById(R.id.playlist_list);

        this.logoutButton = getRootView().findViewById(R.id.userprofile_logout_button);
    }

    @Override
    public void attachPlaylistClickedListener(PlaylistClickListener listener) {
        this.playlistClickListener = listener;
    }

    @Override
    public void attachPlaylistLongClickedListener(PlaylistClickListener listener) {
        this.playlistLongClickListener = listener;
    }

    @Override
    public void attachSongClickedListener(SongItemClickListener listener) {
        this.songClickListener = listener;
    }

    @Override
    public void attachSongLongClickedListener(SongItemLongClickListener listener) {
        this.songLongClickListener = listener;
    }

    @Override
    public void attachSongFavBtnClickedListener(SongFavoriteBtnClick listener) {
        this.songFavoriteClickListener = listener;
    }

    @Override
    public void attachLogoutBtnClickListener(View.OnClickListener listener) {
        this.logoutButton.setOnClickListener(listener);
    }

    @Override
    public void attachUserImgClickListener(View.OnClickListener listener) {
        this.userImage.setOnClickListener(listener);
    }

    @Override
    public void showUserConfiguration(boolean isUser) {
        if (isUser) {
            this.logoutButton.setVisibility(View.VISIBLE);
            this.userImage.setForeground(getRootView().getContext().getDrawable(R.drawable.add_photo_icon));
        }
        else {
            this.logoutButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void displaySongsContent(List<com.example.Sallefy.SallefyUI.Songs.Model.SongsContent.SongItem> listItems) {
        SongsViewAdapter songsViewAdapter
                = new SongsViewAdapter(
                        getRootView().getContext(),
                        listItems,
                        this.songClickListener,
                        this.songLongClickListener,
                        this.songFavoriteClickListener
                );
        this.userSongs.setAdapter(songsViewAdapter);
        this.userSongs.setLayoutManager(new LinearLayoutManager(getRootView().getContext()));
    }

    @Override
    public void displayPlaylistsContent(ArrayList<PlaylistsContent.Playlist> listItems) {
        UserPlaylistsViewAdapter viewAdapter
                = new UserPlaylistsViewAdapter(
                this.rootView.getContext(),
                listItems,
                this.playlistClickListener,
                this.playlistLongClickListener
        );
        this.userPlaylists.setAdapter(viewAdapter);
        this.userPlaylists.setLayoutManager(new LinearLayoutManager(this.rootView.getContext()));
    }

    @Override
    public void setUser(String username, String imgUrl, String followers, String following) {
        Glide.with(this.rootView.getContext())
                .asBitmap()
                .load(imgUrl)
                .into(this.userImage);

        this.txtUsername.setText(username);
        this.txtUserFollowers.setText(followers);
        this.txtUserFollowing.setText(following);
    }

    @Override
    public void displayUserImg(Uri userImgUri) {
        Glide.with(getRootView().getContext())
                .asBitmap()
                .load(userImgUri)
                .into(this.userImage);
    }

    @Override
    public void displayUserImg(String userImgUrl) {
        Glide.with(getRootView().getContext())
                .asBitmap()
                .load(userImgUrl)
                .into(this.userImage);
    }

    @Override
    public void displayNoImgAdded() {

    }

    @Override
    public void toastMsg(String msg) {
        Toast.makeText(getRootView().getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setHeaderVisibility(boolean visible) {
        if (visible) {
            this.userHeader.setVisibility(View.VISIBLE);
        }
        else {
            this.userHeader.setVisibility(View.GONE);
        }
    }

    @Override
    public void setContentVisibility(boolean visible) {
        if (visible) {
            this.contentSection.setVisibility(View.VISIBLE);
        }
        else {
            this.contentSection.setVisibility(View.GONE);
        }
    }

    @Override
    public View getRootView() {
        return this.rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }
}
