package com.example.Sallefy.SallefyUI.Playlist.Controller;

import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacade;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacadeFactory;
import com.example.Sallefy.SallefyUI.MusicPlayer.Model.Data;
import com.example.Sallefy.SallefyUI.MusicPlayer.Model.DataMusicPlayerFactory;
import com.example.Sallefy.SallefyUI.PlaylistMenu.PlaylistMenuFacade;
import com.example.Sallefy.SallefyUI.PlaylistMenu.PlaylistMenuFacadeFactory;
import com.example.Sallefy.SallefyUI.Playlist.Model.DataPlaylist;
import com.example.Sallefy.SallefyUI.Playlist.Model.PlaylistContent;
import com.example.Sallefy.SallefyUI.Playlist.View.PlaylistViewI;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.UserIsFollowingDTO;
import com.example.Sallefy.repositories.Interfaces.PlaylistRepository;
import com.example.Sallefy.repositories.factories.PlaylistRepositoryFactory;
import com.example.Sallefy.repositoryImpl.repositoryImpl.SessionBearer;

import java.util.List;

public class PlaylistController {
    private PlaylistMediator mediator;
    private PlaylistViewI view;

    private DataPlaylist dataPlaylist;

    private PlaylistRepository playlistRepository;

    private PlaylistContent playlistContent;


    public PlaylistController(PlaylistMediator mediator,
                              PlaylistViewI view,
                              PlaylistRepositoryFactory playlistRepositoryFactory) {
        this.mediator = mediator;
        this.view = view;
        this.playlistContent = new PlaylistContent();
        this.view.attachModifyButtonListener(v -> this.settingsButtonClicked());
        this.view.attachFollowBtnListener(v -> this.followBtnClick());
        this.playlistRepository = playlistRepositoryFactory.create();
        this.view.setFollowBtnVisibility(false);

        Data dataMusicPlayer = DataMusicPlayerFactory.create();
        dataMusicPlayer.currentlyPlaying.observe(this.mediator.provideOwner(), this::processSongChanged);
    }

    private void processSongChanged(Data.Track track) {
        if (this.view.selectTrackIfExist(String.valueOf(track.getId()), track.getTitle())) {
            this.view.toggleTrackIfSelected();
        }
    }

    private void followBtnClick() {
        this.playlistRepository
                .followPlayListById(this.dataPlaylist.getPlaylistId())
                .observe(this.mediator.provideOwner(), this::processFollowResponse);
        this.dataPlaylist.setFollowed(!this.dataPlaylist.isFollowed());
        //this.view.setFollowBtnState(this.dataPlaylist.isFollowed());
    }

    private void processFollowResponse(UserIsFollowingDTO userIsFollowingDTO) {
        this.dataPlaylist.setFollowed(userIsFollowingDTO.isFollowed());
        this.view.setFollowBtnState(this.dataPlaylist.isFollowed());
        PlaylistMenuFacade playlistMenuFacade = PlaylistMenuFacadeFactory.create();
        playlistMenuFacade.updatePlaylistMenu();
    }

    public void init(DataPlaylist dataPlaylist) {
        this.dataPlaylist = dataPlaylist;

        this.playlistRepository
                .getPlaylistById(this.dataPlaylist.getPlaylistId())
                .observe(this.mediator.provideOwner(), this::processPlaylistResponse);
    }

    private void processPlaylistResponse(PlaylistDTO playlistDTO) {
        if(playlistDTO.isStatus()) {
            this.dataPlaylist.setFollowed(playlistDTO.isFollowedByUser());
            if(playlistDTO.getOwnerObject().getLogin().equals(SessionBearer.getUsernameIfThere())) {
                this.view.setFollowBtnVisibility(false);
            }
            else {
                this.view.setFollowBtnState(
                        this.dataPlaylist.isFollowed()
                );
                this.view.setFollowBtnVisibility(true);
            }

            parseSongs(playlistDTO.getTracks());
            this.dataPlaylist.setPlaylistName(playlistDTO.getName());
            this.view.setTitle(this.dataPlaylist.getPlaylistName());
            this.view.setContent(
                    this.playlistContent.getItems(),
                    this::onSongClicked,
                    this::processSongLongClicked
            );

            Data dataMusicPlayer = DataMusicPlayerFactory.create();
            if (dataMusicPlayer.getCurrentlyPlaying() != null) {
                this.view.selectTrackIfExist(
                        String.valueOf(dataMusicPlayer.getCurrentlyPlaying().getId()),
                        dataMusicPlayer.getCurrentlyPlaying().getTitle()
                );
            }
        }
        else{
            this.mediator.navBack();
        }
    }

    private void processSongLongClicked(String id) {
        this.mediator.openSongOptions(id);
    }

    private void onSongClicked(PlaylistContent playlists) {
        this.mediator.playSong(
                Long.parseLong(playlists.popFirst().id)
        );

        MusicPlayerFacade musicPlayerFacade = MusicPlayerFacadeFactory.create();
        musicPlayerFacade.emptyQueue();

        for (PlaylistContent.SongItem songItem : playlists.getItems()) {
            this.mediator.enqueueSong(
                    songItem.id,
                    songItem.title,
                    songItem.uploader,
                    songItem.imgUrl,
                    songItem.srcUrl
            );
        }
    }

    private void settingsButtonClicked() {
        this.mediator.openPlaylistOptions(
                String.valueOf(this.dataPlaylist.getPlaylistId())
        );
    }

    private void parseSongs(List<TrackDTO> songs) {
        for (TrackDTO track : songs) {
            this.playlistContent.addItem(
                    this.playlistContent.createSongItem(
                            String.valueOf(track.getId()),
                            track.getName(),
                            track.getOwner().getLogin(),
                            track.getThumbnail(),
                            track.getUrl(),
                            track.isLiked()
                    )
            );
        }
    }
}
