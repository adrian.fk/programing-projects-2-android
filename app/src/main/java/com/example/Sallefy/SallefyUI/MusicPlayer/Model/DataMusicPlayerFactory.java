package com.example.Sallefy.SallefyUI.MusicPlayer.Model;

public interface DataMusicPlayerFactory {
    Data DATA_MUSIC_PLAYER = new Data();

    static Data create() {
        return DATA_MUSIC_PLAYER;
    }
}
