package com.example.Sallefy.SallefyUI.MusicPlayer.View;

import android.view.View;
import android.widget.SeekBar;

import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.ListenerInterfaces.ProgressBarUpdatedCallback;
import com.example.Sallefy.Utils.ViewInterface;

public interface MusicPlayerViewI extends ViewInterface {
    Boolean STATE_PLAY_BUTTON_PLAY = true;
    Boolean STATE_PLAY_BUTTON_PAUSE = false;


    void attachFullscreenTriggerListener(View.OnClickListener listener);

    void attachPlayButtonListener(View.OnClickListener listener);

    void attachPrevButtonListener(View.OnClickListener listener);

    void attachNextButtonListener(View.OnClickListener listener);

    void attachRepeatButtonListener(View.OnClickListener listener);

    void attachShuffleButtonListener(View.OnClickListener listener);

    void setPlayBtnState(Boolean state);

    void setShuffleBtnState(Boolean state);

    void setRepeatBtnState(Boolean state);

    void attachProgressBarChangeCallback(ProgressBarUpdatedCallback callback);

    void setProgressBarDuration(int duration);

    void setProgressBarProgression(int progress);

    void setTextTitle(String input);

    void setTextUploader(String input);

    void setThumbnail(String urlSrc);

    void setContent(String title, String uploader, String imgUrl);

    void setNextBtnClicked(Boolean clicked);

    void setPrevBtnClicked(Boolean clicked);

    SeekBar getSeekBar();

    void show();

    void hide();
}
