package com.example.Sallefy.SallefyUI.Playlist.Model;

import java.util.ArrayList;
import java.util.List;

public class PlaylistContent {
    private List<SongItem> items = new ArrayList<>();

    public void addItem(SongItem item) {
        items.add(item);
    }

    public List<SongItem> getItems() {
        return this.items;
    }

    public SongItem popFirst() {
        SongItem item = this.items.get(0);
        this.items.remove(0);
        return item;
    }

    public SongItem createSongItem(String id,
                                   String title,
                                   String uploader,
                                   String imgUrl,
                                   String srcUrl,
                                   boolean favorized) {
        return new SongItem(
                id,
                title,
                uploader,
                imgUrl,
                srcUrl,
                favorized);
    }


    public class SongItem {
        public final String id;
        public final String title;
        public final String uploader;
        public final String imgUrl;
        public final String srcUrl;
        public final boolean favorite;

        SongItem(String id, String title, String uploader, String imgUrl, String srcUrl, boolean favorite) {
            this.id = id;
            this.title = title;
            this.uploader = uploader;
            this.imgUrl = imgUrl;
            this.srcUrl = srcUrl;
            this.favorite = favorite;
        }
    }
}
