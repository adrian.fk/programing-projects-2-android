package com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade;

import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.NavController;

import com.example.Sallefy.repositories.factories.TrackRepositoryFactory;
import com.google.android.material.navigation.NavigationView;

public interface MusicPlayerFacade {
    void loadMusicPlayer(LifecycleOwner owner,
                         NavController navController,
                         NavigationView navigationView,
                         TrackRepositoryFactory trackRepositoryFactory);

    void emptyQueue();

    void enqueueSong(long id);

    void enqueueSong(long id, String title, String uploader, String thumbnail, String srcUrl);

    void playSong(long id);

    void playSong(long id, String title, String uploader, String thumbnail, String srcUrl);

    void play();

    void pause();

    void nextSong();

    void prevSong();

    void setMiniPlayerVisibility(boolean visible);

    void setShuffle(boolean isShuffle);

    void setRepeat(boolean isRepeat);
}
