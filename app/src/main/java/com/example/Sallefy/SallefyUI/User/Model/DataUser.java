package com.example.Sallefy.SallefyUI.User.Model;

import android.net.Uri;

import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.UserDTO;

public class DataUser {
    private String userLogin;
    private String following;
    private String followers;

    private Uri userImgUriHolder;

    private UserDTO userHolder;

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getFollowing() {
        return following;
    }

    public void setFollowing(String following) {
        this.following = following;
    }

    public String getFollowers() {
        return followers;
    }

    public void setFollowers(String followers) {
        this.followers = followers;
    }

    public Uri getUserImgUri() {
        return userImgUriHolder;
    }

    public void setUserImgUri(Uri userImgUri) {
        this.userImgUriHolder = userImgUri;
    }

    public UserDTO getUserHolder() {
        return userHolder;
    }

    public void setUserHolder(UserDTO userHolder) {
        this.userHolder = userHolder;
    }
}
