package com.example.Sallefy.SallefyUI.PlaylistMenu.View;


import android.view.View;

import com.example.Sallefy.SallefyUI.PlaylistMenu.Controller.ListenerInterfaces.MenuItemListenerI;
import com.example.Sallefy.SallefyUI.PlaylistMenu.Model.DataPlaylistMenu;
import com.example.Sallefy.Utils.ViewInterface;

import java.util.Collection;
import java.util.List;

public interface PlaylistsMenuViewI {
    void displayUserHeader(String username, String email, String userImgUrl);

    void setContent(List<DataPlaylistMenu.Playlist> playlists);

    void attachMenuItemClickListener(MenuItemListenerI listener);

    View getHeaderView();
}
