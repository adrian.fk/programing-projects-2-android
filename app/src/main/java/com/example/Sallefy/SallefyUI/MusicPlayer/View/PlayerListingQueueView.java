package com.example.Sallefy.SallefyUI.MusicPlayer.View;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Playlist.Controller.ListenerInterfaces.TrackClickListener;
import com.example.Sallefy.SallefyUI.Playlist.Controller.ListenerInterfaces.TrackLongClickListener;
import com.example.Sallefy.SallefyUI.Playlist.Model.PlaylistContent;
import com.example.Sallefy.SallefyUI.Playlist.View.Adapter.PlaylistViewAdapter;

import java.util.List;

public class PlayerListingQueueView implements PlayerListingViewI {
    private final View rootView;
    private RecyclerView queueList;

    private PlaylistViewAdapter songsAdapter;

    public PlayerListingQueueView(@NonNull LayoutInflater inflater,
                                  ViewGroup container,
                                  FragmentManager fragmentManager) {
        this.rootView = inflater.inflate(R.layout.fragment_player_listing_list, container, false);
        findViews();
    }

    private void findViews() {
        this.queueList = getRootView().findViewById(R.id.playlist_list);
    }

    @Override
    public void displaySongs(List<PlaylistContent.SongItem> songItems,
                             TrackClickListener trackClickListener,
                             TrackLongClickListener trackLongClickListener) {
        this.songsAdapter = new PlaylistViewAdapter(
                this.rootView.getContext(),
                songItems,
                trackClickListener,
                trackLongClickListener
        );
        this.queueList.setAdapter(this.songsAdapter);
        this.queueList.setLayoutManager(new LinearLayoutManager(this.rootView.getContext()));
    }

    @Override
    public boolean selectTrackIfExist(String trackID, String trackName) {
        if (this.songsAdapter != null) {
            return this.songsAdapter.selectViewHolderIfPresent(trackID, trackName);
        }
        return false;
    }

    @Override
    public void toggleTrackIfSelected() {
        if (this.songsAdapter != null) {
            this.songsAdapter.toggleViewHolderIfSelected();
        }
    }

    @Override
    public View getRootView() {
        return this.rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }
}
