package com.example.Sallefy.SallefyUI.Songs.View;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.Songs.Controller.ListenerInterfaces.SongFavoriteBtnClick;
import com.example.Sallefy.SallefyUI.Songs.Controller.ListenerInterfaces.SongItemClickListener;
import com.example.Sallefy.SallefyUI.Songs.Controller.ListenerInterfaces.SongItemLongClickListener;
import com.example.Sallefy.SallefyUI.Songs.Model.SongsContent;

import java.util.List;

public class SongsView implements SongsViewI {
    private View rootView;

    private RecyclerView songsRecyclerView;

    public SongsView(@NonNull LayoutInflater inflater,
                     ViewGroup container) {
        this.rootView = inflater.inflate(R.layout.fragment_favsongs, container, false);
        this.songsRecyclerView = this.rootView.findViewById(R.id.songs_list);
    }

    @Override
    public View getRootView() {
        return this.rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }

    @Override
    public void displaySongs(List<SongsContent.SongItem> songs,
                             SongItemClickListener songItemClickListener,
                             SongItemLongClickListener songItemLongClickListener,
                             SongFavoriteBtnClick songFavoriteBtnClick) {
        SongsViewAdapter songsAdapter = new SongsViewAdapter(
                this.rootView.getContext(),
                songs,
                songItemClickListener,
                songItemLongClickListener,
                songFavoriteBtnClick
        );
        this.songsRecyclerView.setAdapter(songsAdapter);
        this.songsRecyclerView.setLayoutManager(new LinearLayoutManager(this.rootView.getContext()));
    }
}
