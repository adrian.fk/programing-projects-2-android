package com.example.Sallefy.SallefyUI.Options.SongOptions.Model;

public class DataSongOptions {
    //CLIENT USER
    private String clientUserLogin;

    //SONG
    private long songId;

    private String songTitle;

    private Uploader songUploader;

    private String songImg;

    private String songUrl;

    private boolean isFavorite;
    private boolean createdByUser;


    public String getClientUserLogin() {
        return clientUserLogin;
    }

    public void setClientUserLogin(String clientUserLogin) {
        this.clientUserLogin = clientUserLogin;
    }

    public long getSongId() {
        return songId;
    }

    public void setSongId(long songId) {
        this.songId = songId;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public void setSongTitle(String songTitle) {
        this.songTitle = songTitle;
    }

    public Uploader getSongUploader() {
        return songUploader;
    }

    public String getUploaderName() {
        return this.songUploader.name;
    }

    public long getUploaderId() {
        return this.songUploader.id;
    }

    public void setSongUploader(long uploaderId, String uploaderName) {
        this.songUploader = new Uploader(uploaderId, uploaderName);
    }

    public String getSongImg() {
        return songImg;
    }

    public void setSongImg(String songImg) {
        this.songImg = songImg;
    }

    public boolean isCreatedByUserClient() {
        return this.clientUserLogin.equals(this.songUploader.name);
    }

    public boolean isCreatedByUser() {
        return createdByUser;
    }

    public void setCreatedByUser(boolean createdByUser) {
        this.createdByUser = createdByUser;
    }

    public String getSongUrl() {
        return songUrl;
    }

    public void setSongUrl(String songUrl) {
        this.songUrl = songUrl;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    static class Uploader {
        public final long id;
        public final String name;

        Uploader(long id, String name) {
            this.id = id;
            this.name = name;
        }
    }
}
