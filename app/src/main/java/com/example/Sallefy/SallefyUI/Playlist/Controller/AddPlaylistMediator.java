package com.example.Sallefy.SallefyUI.Playlist.Controller;

import com.example.Sallefy.Utils.FeaturedMediatorI;

public interface AddPlaylistMediator extends FeaturedMediatorI {
    void receiveMenuUpdateCall();

    void finishPlaylistCreation();

    void openImgFile();
}
