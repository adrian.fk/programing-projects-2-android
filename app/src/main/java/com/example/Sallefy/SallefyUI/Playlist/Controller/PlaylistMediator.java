package com.example.Sallefy.SallefyUI.Playlist.Controller;

import com.example.Sallefy.Utils.FeaturedMediatorI;

public interface PlaylistMediator extends FeaturedMediatorI {
    void openPlaylistOptions(String id);

    void playSong(long id);

    void enqueueSong(String id,  String title, String uploader, String thumbnail, String srcUrl);

    void openSongOptions(String id);

    void navBack();
}
