package com.example.Sallefy.SallefyUI.MusicPlayer.Controller.ListenerInterfaces;

public interface ProgressBarUpdatedCallback {
    void callback(int progress);
}
