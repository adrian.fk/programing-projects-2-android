package com.example.Sallefy.SallefyUI.Options.SongOptions.Controller;

import com.example.Sallefy.Utils.FeaturedMediatorI;

public interface SongOptionsMediator extends FeaturedMediatorI {
    void receivePlaySongTrigger();

    void receiveFavoriteTrigger();

    void receiveEnqueueTrigger();

    void receiveUploaderProfileTrigger();

    void openAddSongToPlaylists();

    void conclude();

    void receiveShareTrigger(String title, String url);

    void navBack();
}
