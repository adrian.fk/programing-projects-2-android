package com.example.Sallefy.SallefyUI.Options.SongOptions.View;

import android.view.View;

import com.example.Sallefy.Utils.ViewInterface;

public interface SongOptionsViewI extends ViewInterface {
    void attachPlayBtnListener(View.OnClickListener listener);
    void attachFavBtnListener(View.OnClickListener listener);
    void attachEnqueueBtnListener(View.OnClickListener listener);
    void attachUserProfileBtnListener(View.OnClickListener listener);
    void attachAddToPlaylistBtnListener(View.OnClickListener listener);
    void attachShareBtnListener(View.OnClickListener listener);
    void attachDeleteBtnListener(View.OnClickListener listener);

    void toastConnectionError();

    void toastSongDeleted();

    void toastMsg(String msg);

    void updateFavBtnState(boolean isFavorite);

    void updateView(boolean showView);
    void updateView(boolean showView, boolean isCreatedByUser);
    void updateView(boolean showView,
                    String songTitle,
                    String songUploader,
                    String songImage,
                    boolean isCreatedByUser,
                    boolean isFavorite);
}
