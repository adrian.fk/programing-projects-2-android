package com.example.Sallefy.SallefyUI.Search.Model;

public class SearchConstants {
    public interface FILTERS {
        String NONE = "NONE";
        String TRACKS = "TRACKS";
        String PLAYLISTS = "PLAYLISTS";
        String USERS = "USERS";
    }
}
