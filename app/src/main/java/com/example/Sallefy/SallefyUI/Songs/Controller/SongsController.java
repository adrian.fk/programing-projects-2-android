package com.example.Sallefy.SallefyUI.Songs.Controller;


import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacade;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacadeFactory;
import com.example.Sallefy.SallefyUI.Songs.Model.SongsContent;
import com.example.Sallefy.SallefyUI.Songs.View.SongsViewI;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.UserIsLikingTrackDTO;
import com.example.Sallefy.repositories.Interfaces.CurrentUserRepository;
import com.example.Sallefy.repositories.Interfaces.TrackRepository;
import com.example.Sallefy.repositories.factories.TrackRepositoryFactory;
import com.example.Sallefy.repositories.factories.UserRepositoryFactory;

import java.util.Collection;

public class SongsController {

    private final MusicPlayerFacade musicPlayerFacade = MusicPlayerFacadeFactory.create();

    private SongsMediator mediator;
    private SongsViewI view;

    private SongsContent songsContent;

    private TrackRepository trackRepository;
    private CurrentUserRepository currentUserRepository;

    public SongsController(SongsMediator mediator,
                           SongsViewI view,
                           TrackRepositoryFactory trackRepositoryFactory,
                           UserRepositoryFactory userRepositoryFactory) {
        this.mediator = mediator;
        this.view = view;
        this.trackRepository = trackRepositoryFactory.create();
        this.currentUserRepository = userRepositoryFactory.createCurrentUserRepository();
        this.songsContent = new SongsContent();
    }

    public void acquireSongs() {
        this.currentUserRepository
                .getCurrentUserLikedTracks()
                .observe(this.mediator.provideOwner(), this::processTrackList);
    }

    private void processTrackList(Collection<TrackDTO> tracks) {
        for(TrackDTO track : tracks) {
            if(track.isLiked()) {
                this.songsContent.addItem(
                        this.songsContent.createSongItem(
                                String.valueOf(track.getId()),
                                track.getName(),
                                track.getOwner().getLogin(),
                                track.getThumbnail(),
                                track.getUrl(),
                                track.isLiked()
                        )
                );
            }
        }

        this.view.displaySongs(
                this.songsContent.getItems(),
                (id, position) -> songClicked(tracks, id, position),
                this::songLongClicked,
                this::favBtnClicked
        );
    }

    private void songLongClicked(String id) {
        this.mediator.openSongOptions(id);
    }

    private void favBtnClicked(String trackId) {
        this.trackRepository
                .likeTrackById(Long.parseLong(trackId))
                .observe(this.mediator.provideOwner(), this::processLike);
    }

    private void processLike(UserIsLikingTrackDTO userIsLikingTrackDTO) {
        if(!userIsLikingTrackDTO.isStatus()) {
            acquireSongs();
        }
    }

    private void songClicked(Collection<TrackDTO> tracks, String id, int position) {
        int i = 0;
        this.musicPlayerFacade.playSong(Long.parseLong(id));
        this.musicPlayerFacade.emptyQueue();
        for(TrackDTO track : tracks) {
            if(i++ > position) {
                this.musicPlayerFacade.enqueueSong(
                        track.getId(),
                        track.getName(),
                        track.getOwner().getLogin(),
                        track.getThumbnail(),
                        track.getUrl()
                );
            }
        }
    }
}
