package com.example.Sallefy.SallefyUI.Search.Controller.ListenerInterfaces;

public interface FavoriteBtnClick {
    void clicked(String trackId);
}
