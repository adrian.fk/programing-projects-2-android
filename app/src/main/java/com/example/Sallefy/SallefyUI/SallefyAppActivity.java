package com.example.Sallefy.SallefyUI;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.MutableLiveData;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;


import com.cloudinary.android.MediaManager;



import com.example.Sallefy.Cast.CastProxyFactory;
import com.example.Sallefy.Cast.CastProxy;
import com.example.Sallefy.Login.LoginActivity;
import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacade;
import com.example.Sallefy.SallefyUI.MusicPlayer.Controller.Facade.MusicPlayerFacadeFactory;
import com.example.Sallefy.SallefyUI.PlaylistMenu.PlaylistMenuFacade;
import com.example.Sallefy.SallefyUI.PlaylistMenu.PlaylistMenuFacadeFactory;
import com.example.Sallefy.SallefyUI.Search.Search;
import com.example.Sallefy.Utils.Constants;
import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.example.Sallefy.repositories.Interfaces.UserTokenRepository;
import com.example.Sallefy.repositoryImpl.factoriesImpl.TrackApiRepositoryFactory;
import com.example.Sallefy.repositoryImpl.factoriesImpl.UserApiRepositoryFactory;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastState;
import com.google.android.gms.cast.framework.CastStateListener;
import com.google.android.gms.cast.framework.IntroductoryOverlay;
import com.google.android.material.navigation.NavigationView;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class SallefyAppActivity extends AppCompatActivity {

    public interface PreferencesConstants {
        String SHARED_PREFERENCES_FILE_KEY = "sallefySharedPrefs";
        String USER_LOGIN = "userLogin";
        String USER_PASSWORD = "userPassword";
        String USER_LOGIN_DEFAULT_VALUE = "";
        String USER_LOGIN_EMPTY = "";
    }

    private static final MutableLiveData<Boolean> logoutCommand = new MutableLiveData<>();

    private CastProxy castProxy;
    private CastContext castContext;
    private CastStateListener castStateListener;
    private IntroductoryOverlay introductoryOverlay;

    private static MediaManager mediaManager;

    private AppBarConfiguration mAppBarConfiguration;

    private MenuItem mediaRouteMenuItem;

    private User user;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        this.mediaRouteMenuItem = CastButtonFactory.setUpMediaRouteButton(
                getApplicationContext(),
                menu,
                R.id.media_route_menu_item
        );
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.toolbar).setVisibility(View.GONE);

        logoutCommand.observe(this, this::executeLogoutCommand);

        this.castProxy = CastProxyFactory.create();
        this.castContext = CastContext.getSharedInstance(this);

        this.castStateListener = newState -> {
            if (newState != CastState.NO_DEVICES_AVAILABLE) {
                showIntroductoryOverlay();
            }
        };

        this.castProxy.init(this);


        this.user = new User();


        Bundle session = getIntent().getExtras();
        if(session != null) {
            String userLogin = session.getString(Constants.LOGIN_CONSTANTS.USER_LOGIN);
            String userPassword = session.getString(Constants.LOGIN_CONSTANTS.USER_PASSWORD);


            saveUserLogin(userLogin, userPassword);
            logoutCommand.setValue(false);
            initiate();
        }
        else {
            attemptLoadUser();
        }
    }

    private void executeLogoutCommand(Boolean logout) {
        if (logout) {
            saveUserLogin(PreferencesConstants.USER_LOGIN_EMPTY, "");
            MusicPlayerFacade musicPlayerFacade = MusicPlayerFacadeFactory.create();
            musicPlayerFacade.pause();
            launchAuthentication();
        }
    }

    public static void logout() {
        logoutCommand.setValue(true);
    }

    private void launchAuthentication() {
        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(i);
        finish();
    }

    private void attemptLoadUser() {
        SharedPreferences sharedPreferences = getSharedPreferences(PreferencesConstants.SHARED_PREFERENCES_FILE_KEY, MODE_PRIVATE);
        this.user.login = sharedPreferences.getString(PreferencesConstants.USER_LOGIN, PreferencesConstants.USER_LOGIN_DEFAULT_VALUE);
        this.user.password = sharedPreferences.getString(PreferencesConstants.USER_PASSWORD, PreferencesConstants.USER_LOGIN_DEFAULT_VALUE);
        if(!this.user.login.equals(PreferencesConstants.USER_LOGIN_DEFAULT_VALUE)) {
            requestLogin();
        }
        else {
            launchAuthentication();
        }
    }

    private void requestLogin() {
        UserApiRepositoryFactory userApiRepositoryFactory = new UserApiRepositoryFactory();
        UserTokenRepository userTokenRepository = userApiRepositoryFactory.createUserRepository();
        userTokenRepository
                .login(this.user.login, this.user.password)
                .observe(this, this::processUserLoginAttempt);
    }

    private void processUserLoginAttempt(BaseDTO baseDTO) {
        if(baseDTO.isStatus()) {
            initiate();
        }
        else {
            launchAuthentication();
        }
    }

    private void initiate() {
        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_songs, R.id.nav_upload,
                R.id.nav_add_playlist, R.id.nav_to_playlist, R.id.nav_search_start_fragment,
                R.id.nav_my_playlists)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);

        TrackApiRepositoryFactory trackRepositoryFactory = new TrackApiRepositoryFactory();
        MusicPlayerFacade musicPlayerFacade = MusicPlayerFacadeFactory.create();
        musicPlayerFacade.loadMusicPlayer(
                this,
                navController,
                navigationView,
                trackRepositoryFactory
        );

        PlaylistMenuFacade playlistMenuFacade = PlaylistMenuFacadeFactory.create();
        playlistMenuFacade.setup(
                this,
                navController,
                navigationView
        );
        Search search = new Search(this, navController, navigationView);

        //Prerequisite done
        toolbar.setVisibility(View.VISIBLE);
        navController.navigate(R.id.nav_home);

        //Following instruction "Logs out" the saved user
        //saveUserLogin(PreferencesConstants.USER_LOGIN_EMPTY, "");


        if (mediaManager == null) {
            Map<String, String> config = new HashMap<>();
            config.put("cloud_name", "la-salle-sallefy");

            MediaManager.init(this, config);
            mediaManager = MediaManager.get();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    private void saveUserLogin(String userLogin, String userPassword) {
        SharedPreferences preferences = this.getSharedPreferences(PreferencesConstants.SHARED_PREFERENCES_FILE_KEY, MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = preferences.edit();
        preferencesEditor.putString(PreferencesConstants.USER_LOGIN, userLogin);
        preferencesEditor.putString(PreferencesConstants.USER_PASSWORD, userPassword);
        preferencesEditor.apply();
    }

    private void showIntroductoryOverlay() {
        if (introductoryOverlay != null) {
            introductoryOverlay.remove();
        }
        if ((mediaRouteMenuItem != null) && mediaRouteMenuItem.isVisible()) {
            new Handler().post(() -> {
                introductoryOverlay = new IntroductoryOverlay.Builder(
                        SallefyAppActivity.this, mediaRouteMenuItem)
                        .setTitleText("Introducing Cast")
                        .setSingleTime()
                        .setOnOverlayDismissedListener(
                                () -> introductoryOverlay = null)
                        .build();
                introductoryOverlay.show();
            });
        }
    }

    @Override
    protected void onPause() {
        this.castContext.removeCastStateListener(this.castStateListener);
        super.onPause();
    }

    @Override
    protected void onResume() {
        this.castContext.addCastStateListener(this.castStateListener);
        super.onResume();
    }

    private static class User {
        String login;
        String password;
    }
}