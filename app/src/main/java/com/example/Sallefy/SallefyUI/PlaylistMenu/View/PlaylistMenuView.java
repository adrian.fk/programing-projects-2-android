package com.example.Sallefy.SallefyUI.PlaylistMenu.View;

import android.content.Context;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.Menu;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.PlaylistMenu.Controller.ListenerInterfaces.MenuItemListenerI;
import com.example.Sallefy.SallefyUI.PlaylistMenu.Model.DataPlaylistMenu;
import com.google.android.material.navigation.NavigationView;

import java.util.List;

public class PlaylistMenuView implements PlaylistsMenuViewI {
    private View headerView;
    private SubMenu playlistMenu;

    private MenuItemListenerI listener;

    private TextView txtUsername;
    private TextView txtEmail;
    private ImageView imgUser;

    public PlaylistMenuView(NavigationView navView) {

        this.headerView = navView.getHeaderView(0);
        Context context = navView.getContext();
        String rawPlaylistTitle = context.getText(R.string.menu_playlists_header).toString();
        SpannableString playlistTitle = new SpannableString(rawPlaylistTitle);
        playlistTitle.setSpan(new TextAppearanceSpan(
                        context,
                        R.style.TextAppearance44),
                0,
                playlistTitle.length(),
                0
        );
        this.playlistMenu = navView.getMenu().addSubMenu(playlistTitle);
        findViews();
    }

    private void findViews() {
        this.txtUsername = getHeaderView().findViewById(R.id.nav_drawer_user_txt);
        this.txtEmail = getHeaderView().findViewById(R.id.nav_drawer_user_mail);
        this.imgUser = getHeaderView().findViewById(R.id.nav_drawer_user_img);
    }

    @Override
    public void displayUserHeader(String username, String email, String userImgUrl) {
        this.txtUsername.setText(username);
        this.txtEmail.setText(email);
        Glide.with(getHeaderView().getContext())
                .asBitmap()
                .load(userImgUrl)
                .into(this.imgUser);
    }

    @Override
    public void setContent(List<DataPlaylistMenu.Playlist> playlists) {
        this.playlistMenu.clear();
        int i = 0;
        for(DataPlaylistMenu.Playlist playlist : playlists) {
            this.playlistMenu
                    .add(R.id.playlist_group, R.id.nav_to_playlist, i, playlist.title)
                    .setOnMenuItemClickListener(this.listener);
            i++;
        }
    }

    @Override
    public void attachMenuItemClickListener(MenuItemListenerI listener) {
        this.listener = listener;
    }

    @Override
    public View getHeaderView() {
        return this.headerView;
    }
}
