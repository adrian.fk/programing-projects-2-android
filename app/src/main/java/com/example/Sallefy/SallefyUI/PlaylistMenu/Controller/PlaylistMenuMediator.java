package com.example.Sallefy.SallefyUI.PlaylistMenu.Controller;

import com.example.Sallefy.Utils.FeaturedMediatorI;

public interface PlaylistMenuMediator extends FeaturedMediatorI {

    void receiveSelectedPlaylist(String idPlaylist, String playlistName);
}
