package com.example.Sallefy.SallefyUI.Search.Controller.ListenerInterfaces;

public interface FollowUserBtnClick {
    void clicked(String username);
}
