package com.example.Sallefy.SallefyUI.MusicPlayer.Controller;

import android.util.Log;

import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;

import com.example.Sallefy.SallefyUI.MusicPlayer.Model.Data;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Outgoing.CoordsDTO;
import com.example.Sallefy.repositories.Interfaces.TrackRepository;
import com.example.Sallefy.repositories.factories.TrackRepositoryFactory;

public class  TrackHandler {
    private static final String TAG = "TrackHandler";

    public static final String OPERATION_INTENTION_PLAY = "PLAY";
    public static final String OPERATION_INTENTION_ENQUEUE = "ENQUEUE";

    @Nullable
    private LifecycleOwner owner;

    private Data dataMusicPlayer;
    private TrackRepository trackRepository;

    private MutableLiveData<TrackDTO> trackCache;


    public void setup(LifecycleOwner owner,
                      Data dataMusicPlayer,
                      TrackRepositoryFactory trackRepositoryFactory) {
        this.owner = owner;
        this.dataMusicPlayer = dataMusicPlayer;
        this.trackRepository = trackRepositoryFactory.create();
    }

    public void handle(Operation operation) {
        if (ownerExists()) {
            if (this.trackCache == null) {
                this.trackCache = this.trackRepository.getTrackById(operation.id);
                this.trackCache.observe(
                        this.owner,
                        trackDTO ->
                            processOperation(
                                    operation,
                                    trackDTO.getId(),
                                    trackDTO.getName(),
                                    trackDTO.getOwner().getLogin(),
                                    trackDTO.getThumbnail(),
                                    trackDTO.getUrl(),
                                    trackDTO.isLiked()
                            )
                        );
            }
            else {
                this.trackRepository
                        .getTrackById(operation.id);
            }
        }
        else {
            Log.d(TAG, "execute(): No owner found..\n"+ TAG +" not setup." +
                    "\nConfigure by launching the setup method, before attempting to use this object..");
        }
    }

    private void processOperation(Operation operation,
                                  long id,
                                  String title,
                                  String uploader,
                                  String thumbnailUrl,
                                  String srcUrl,
                                  boolean isFavorite) {
            switch (operation.intention) {
            case OPERATION_INTENTION_PLAY:
                this.dataMusicPlayer.setNewCurrentlyPlaying(
                        this.dataMusicPlayer.createTrack(
                                id, title, uploader, thumbnailUrl, srcUrl, isFavorite
                        )
                );
                this.trackRepository.playTrack(id, new CoordsDTO(1,1));
                break;

            case OPERATION_INTENTION_ENQUEUE:
                this.dataMusicPlayer.enqueue(
                        this.dataMusicPlayer.createTrack(
                                id, title, uploader, thumbnailUrl, srcUrl, isFavorite
                        )
                );
                break;
        }
    }

    private boolean ownerExists() {
        return this.owner != null;
    }

    public Operation createOperation(String intention, long id) {
        return new Operation(intention, id);
    }

    private static class Operation {
        final String intention;
        final long id;

        Operation(String intention, long id) {
            this.intention = intention;
            this.id = id;
        }
    }
}
