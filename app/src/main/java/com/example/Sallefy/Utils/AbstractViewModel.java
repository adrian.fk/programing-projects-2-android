package com.example.Sallefy.Utils;

import java.util.ArrayList;

public abstract class AbstractViewModel<T> {
    protected ArrayList<T> items;

    public AbstractViewModel() {
        this.items = new ArrayList<>();
    }

    public ArrayList<T> getItems() {
        return items;
    }

    public abstract void addItem(T item);
}
