package com.example.Sallefy.Utils;

public interface ExceptionalCallback {
    void call(Throwable e);
}
