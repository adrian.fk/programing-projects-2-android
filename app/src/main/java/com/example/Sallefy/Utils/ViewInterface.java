package com.example.Sallefy.Utils;

import android.os.Bundle;
import android.view.View;

public interface ViewInterface {
    View getRootView();

    Bundle getViewState();
}
