package com.example.Sallefy.Utils;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.IOException;
import java.util.function.Consumer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitCallbackAdapter<S> extends AbstractCallbackAdapter<S> implements Callback<S> {
    private static final String TAG = "RetrofitCallbackAdapter";

    public RetrofitCallbackAdapter(@NonNull Consumer<S> onSuccess,
                                   @Nullable Consumer<Throwable> onFail,
                                   @Nullable ExceptionalCallback onException) {
        super(onSuccess, onFail, onException);
    }

    @Override
    public void onResponse(@NonNull Call<S> call, @NonNull Response<S> response) {
        int code = response.code();
        if (!response.isSuccessful()) {
            Log.d(TAG, "ERROR: " + code);
            try {
                this.onFail.accept(new Throwable(
                        response.errorBody().string()
                ));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }
        this.onSuccess.accept(response.body());
    }

    @Override
    public void onFailure(@NonNull Call<S> call, @NonNull Throwable t) {
        this.onException.call(t);
    }
}
