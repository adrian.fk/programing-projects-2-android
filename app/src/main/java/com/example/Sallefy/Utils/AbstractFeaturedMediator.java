package com.example.Sallefy.Utils;

import androidx.lifecycle.LifecycleOwner;
import androidx.navigation.NavController;

import com.google.android.material.navigation.NavigationView;

public abstract class AbstractFeaturedMediator {
    private LifecycleOwner owner;
    private NavController navController;
    private NavigationView navView;

    public AbstractFeaturedMediator(LifecycleOwner owner, NavController navController, NavigationView navView) {
        this.owner = owner;
        this.navController = navController;
        this.navView = navView;
    }

    public LifecycleOwner getOwner() {
        return owner;
    }

    public void setOwner(LifecycleOwner owner) {
        this.owner = owner;
    }

    public NavController getNavController() {
        return navController;
    }

    public void setNavController(NavController navController) {
        this.navController = navController;
    }

    public NavigationView getNavView() {
        return navView;
    }

    public void setNavView(NavigationView navView) {
        this.navView = navView;
    }
}
