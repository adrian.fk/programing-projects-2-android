package com.example.Sallefy.Utils;

import android.text.Editable;

public interface SingleMethodListener {
    void executeCallback(Editable s);
}
