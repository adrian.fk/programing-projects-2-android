package com.example.Sallefy.Utils;

@FunctionalInterface
public interface RejectConsumerCallback<T> {
    void reject(T data);
}
