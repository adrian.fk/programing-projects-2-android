package com.example.Sallefy.Utils;

@FunctionalInterface
public interface RejectCallback {
    void reject();
}
