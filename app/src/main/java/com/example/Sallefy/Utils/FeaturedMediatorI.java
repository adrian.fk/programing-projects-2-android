package com.example.Sallefy.Utils;

import androidx.lifecycle.LifecycleOwner;

public interface FeaturedMediatorI {
    LifecycleOwner provideOwner();
}
