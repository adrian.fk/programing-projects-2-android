package com.example.Sallefy.Utils;

import com.example.Sallefy.SallefyUI.UserPlaylists.UserPlaylistsConstants;

public class Constants implements UserPlaylistsConstants {
    public static final String URL = "url";

    public interface NETWORK {
        String BASE_URL = "http://" + "sallefy.eu-west-3.elasticbeanstalk.com/api/";
    }

    public interface TYPES {
        String TRACK = "TRACK";
        String USER = "USER";
        String PLAYLIST = "PLAYLIST";
    }

    public interface LOGIN_STATE {
        String back = "back";
        String login = "login";
        String register = "register";
        String registerSuccess = "registerSuccess";
        String forgotPassword = "forgotPassword";
        String authenticated = "authenticated";

    }

    public interface LOGIN_CONSTANTS {
        String USER_LOGIN = "USERLOGIN";
        String USER_PASSWORD = "USERPASSWORD";
        String TOKEN = "TOKEN";
    }

    public interface SONG_SPECIFICATIONS {
        String SONG_ID = "SONG_ID";
        String SONG_OWNER_LOGIN = "SONG_OWNER_LOGIN";
    }

    public interface PLAYLIST_SPECIFICATIONS {
        String PLAYLIST_ID = "PLAYLIST_ID";
        String PLAYLIST_NAME = "PLAYLIST_NAME";
    }

    public interface USER_SPECIFICATIONS {
        String USER_NAME = "USER_NAME";
    }

    public interface COSMETICS {
        String DEFAULT_USR_IMG = "https://lh3.googleusercontent.com/proxy/-hOLspYQdjXqFOOn3Rom2ibNlEN0__TBeI5XMGleUhMuaksGGReWWEpDSUDeuWTuFVAEqdLU9i6XE6ukg83g4llP7nDbCYQ";
        String DEFAULT_USR_IMG_OLD = "https://www.autoria.no/wp-content/uploads/2018/09/placeholder-user.jpg";
        String DEFAULT_PLAYLIST_IMG = "https://images.macrumors.com/t/8k-7BpnxpJjF0uXF-JQmDnaejPY=/800x0/article-new/2018/05/apple-music-note-800x420.jpg";
    }
}
