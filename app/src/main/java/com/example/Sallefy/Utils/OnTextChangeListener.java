package com.example.Sallefy.Utils;

import android.text.Editable;
import android.text.TextWatcher;


public class OnTextChangeListener implements TextWatcher {
    private SingleMethodListener listener;

    public OnTextChangeListener(SingleMethodListener listener) {
        this.listener = listener;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {}

    @Override
    public void afterTextChanged(Editable s) {
        listener.executeCallback(s);
    }
}
