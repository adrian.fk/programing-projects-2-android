package com.example.Sallefy.Utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.function.Consumer;

public class AbstractCallbackAdapter<S> {
    protected final Consumer<S> onSuccess;
    protected final Consumer<Throwable> onFail;
    protected ExceptionalCallback onException;

    public AbstractCallbackAdapter(@NonNull Consumer<S> onSuccess,
                                   @Nullable Consumer<Throwable> onFail,
                                   @Nullable ExceptionalCallback onException) {
        this.onSuccess = onSuccess;
        this.onFail = null != onFail ? onFail : (t) -> {};
        this.onException = null != onException ? onException : (t) -> {};
    }
}
