package com.example.Sallefy.repositories.Interfaces;

import androidx.lifecycle.MutableLiveData;

import com.example.Sallefy.repositories.DataTransferObjects.Incoming.SearchDTO;

public interface SearchGateway {
    MutableLiveData<SearchDTO> globalSearchByKeyword(String keyword);
}
