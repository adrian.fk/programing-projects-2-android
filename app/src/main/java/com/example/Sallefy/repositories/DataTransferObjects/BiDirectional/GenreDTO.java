package com.example.Sallefy.repositories.DataTransferObjects.BiDirectional;

import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.google.gson.annotations.SerializedName;

/**
 * Bi directional data transfer object for CRUD operations on
 * genre performed primarily through GenreRepository
 */
public class GenreDTO extends BaseDTO {
    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    public GenreDTO(boolean status, String error) {
        super(status, error);
    }

    public GenreDTO(boolean status, String error, String id, String name) {
        super(status, error);
        this.id = id;
        this.name = name;
    }

    public GenreDTO(String id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
