package com.example.Sallefy.repositories.Interfaces;

public interface IsAuthenticatedGateway {
    boolean isSessionOpen();
}
