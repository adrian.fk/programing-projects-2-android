package com.example.Sallefy.repositories.DataTransferObjects;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

public class BaseDTO {
    private transient boolean status;
    private transient String error;
    private Date timestampFetched;

    public BaseDTO(boolean status, String error) {
        this.status = status;
        this.error = error;
    }

    /**
     * No error constructor available for subclasses to provide
     * a simpler interface to clients when the dto is filled successfuly
     */
    public BaseDTO() {
        this.status = true;
        this.error = "E_NO_ERROR";
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Date getTimestampFetched() {
        return timestampFetched;
    }

    public void stamp() {
        LocalTime localTime = LocalTime.now();
        Instant now = localTime
                .atDate(LocalDate.now())
                .atZone(ZoneId.systemDefault()).toInstant();

        this.timestampFetched = Date.from(now);
    }
}
