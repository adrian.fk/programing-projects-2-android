package com.example.Sallefy.repositories.factories;

import com.example.Sallefy.repositories.Interfaces.IsAuthenticatedGateway;
import com.example.Sallefy.repositories.Interfaces.SessionAuthenticationGateway;
import com.example.Sallefy.repositories.Interfaces.SessionAuthenticationRepository;
import com.example.Sallefy.repositories.Interfaces.UserLoginGateway;

public abstract class AuthenticationRepositoryFactory {
    public abstract SessionAuthenticationRepository createRepository();

    public abstract IsAuthenticatedGateway createIsAuthenticateGateway();

    public abstract SessionAuthenticationGateway createAuthenticationGateway();

    public abstract UserLoginGateway createUserLoginGateway();
}
