package com.example.Sallefy.repositories.factories;

import com.example.Sallefy.repositories.Interfaces.CurrentUserRepository;
import com.example.Sallefy.repositories.Interfaces.BaseUserRepository;
import com.example.Sallefy.repositories.Interfaces.UserRepository;

public abstract class UserRepositoryFactory {
    public abstract UserRepository createUserRepository();
    public abstract BaseUserRepository createBaseUserRepository();
    public abstract CurrentUserRepository createCurrentUserRepository();
}
