package com.example.Sallefy.repositories.factories;

import com.example.Sallefy.repositories.Interfaces.SearchGateway;

public abstract class SearchGatewayFactory {
    public abstract SearchGateway create();
}
