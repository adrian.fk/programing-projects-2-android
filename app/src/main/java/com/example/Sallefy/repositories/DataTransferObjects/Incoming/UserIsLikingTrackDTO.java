package com.example.Sallefy.repositories.DataTransferObjects.Incoming;

import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;

public class UserIsLikingTrackDTO extends BaseDTO {
    private boolean liked;

    public UserIsLikingTrackDTO(boolean status, String error) {
        super(status, error);
        this.liked = false;
    }

    public UserIsLikingTrackDTO(boolean liked) {
        this.liked = liked;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }
}
