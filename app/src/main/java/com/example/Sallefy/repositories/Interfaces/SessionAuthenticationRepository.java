package com.example.Sallefy.repositories.Interfaces;


import androidx.lifecycle.MutableLiveData;

public interface SessionAuthenticationRepository extends
        SessionAuthenticationGateway,
        IsAuthenticatedGateway,
        UserLoginGateway {
    MutableLiveData<Boolean> authenticate(String username, String password, boolean rememberMe);

    boolean isSessionOpen();
}
