package com.example.Sallefy.repositories.Interfaces;


import androidx.lifecycle.MutableLiveData;

import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.GenreDTO;

import java.util.Collection;
import java.util.Optional;

public interface GenreRepository {
    MutableLiveData<BaseDTO> createGenre(GenreDTO newGenre);

    MutableLiveData<BaseDTO> updateGenre(GenreDTO genreToUpdate);

    MutableLiveData<BaseDTO> deleteGenre(long id);

    MutableLiveData<Collection<GenreDTO>> getAllGenres();

    MutableLiveData<GenreDTO> getGenreById(long id);
}
