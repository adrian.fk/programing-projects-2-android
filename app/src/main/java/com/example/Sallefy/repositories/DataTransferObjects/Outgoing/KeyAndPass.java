package com.example.Sallefy.repositories.DataTransferObjects.Outgoing;

public class KeyAndPass {
    private String key;
    private String newPassword;

    public KeyAndPass(String key, String newPassword) {
        this.key = key;
        this.newPassword = newPassword;
    }

    // Getter Methods

    public String getKey() {
        return key;
    }

    public String getNewPassword() {
        return newPassword;
    }

    // Setter Methods

    public void setKey(String key) {
        this.key = key;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
