package com.example.Sallefy.repositories.Interfaces;

import android.net.Uri;

import androidx.lifecycle.MutableLiveData;

import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.UserIsLikingTrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Outgoing.CoordsDTO;

import java.util.Collection;

public interface TrackRepository {
    MutableLiveData<Collection<TrackDTO>> getTrackByGenreId(long genreId);
    public MutableLiveData<Collection<TrackDTO>> getAllTracks(boolean sortByLiked,
                                                              boolean sortByPlayed,
                                                              boolean sortByRecent,
                                                              int maxNumTracks);
    MutableLiveData<TrackDTO> createTrack(TrackDTO track, Uri path);
    MutableLiveData<BaseDTO> updateTrack(TrackDTO track);
    MutableLiveData<TrackDTO> getTrackById(long id);
    MutableLiveData<BaseDTO> deleteTrack(long id);
    MutableLiveData<BaseDTO> playTrack(long trackId, CoordsDTO coords);
    MutableLiveData<UserIsLikingTrackDTO> likeTrackById(long trackId);
}
