package com.example.Sallefy.repositories.DataTransferObjects.Incoming;

import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;

public class UserIsFollowingDTO extends BaseDTO {
    private String userLogin;
    private boolean followed;

    public UserIsFollowingDTO(boolean status, String error, boolean followed) {
        super(status, error);
        this.followed = followed;
    }

    public UserIsFollowingDTO(boolean followed) {
        this.followed = followed;
    }

    public boolean isFollowed() {
        return followed;
    }

    public void setFollowed(boolean followed) {
        this.followed = followed;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }
}
