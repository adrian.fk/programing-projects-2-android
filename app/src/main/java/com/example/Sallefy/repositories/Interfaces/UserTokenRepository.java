package com.example.Sallefy.repositories.Interfaces;

import androidx.lifecycle.MutableLiveData;

import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;

public interface UserTokenRepository {
    MutableLiveData<BaseDTO> login(String username, String password);
}
