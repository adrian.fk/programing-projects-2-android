package com.example.Sallefy.repositories.DataTransferObjects.Incoming;

import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;

public class ImageUploadDTO extends BaseDTO {
    private String imageUrl;

    public ImageUploadDTO() {
    }

    public ImageUploadDTO(boolean status, String error) {
        super(status, error);
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
