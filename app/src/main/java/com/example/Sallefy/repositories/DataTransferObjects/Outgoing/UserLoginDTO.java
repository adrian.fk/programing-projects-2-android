package com.example.Sallefy.repositories.DataTransferObjects.Outgoing;

public class UserLoginDTO {
    private String password;
    private boolean rememberMe;
    private String username;

    public UserLoginDTO(String username, String password, boolean rememberMe) {
        this.password = password;
        this.rememberMe = rememberMe;
        this.username = username;
    }

    // Getter Methods

    public String getPassword() {
        return password;
    }

    public boolean getRememberMe() {
        return rememberMe;
    }

    public String getUsername() {
        return username;
    }

    // Setter Methods

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
