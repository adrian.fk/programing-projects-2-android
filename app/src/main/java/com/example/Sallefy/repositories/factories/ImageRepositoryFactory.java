package com.example.Sallefy.repositories.factories;

import com.example.Sallefy.repositories.Interfaces.ImageRepository;

public abstract class ImageRepositoryFactory {
    public abstract ImageRepository create();
}
