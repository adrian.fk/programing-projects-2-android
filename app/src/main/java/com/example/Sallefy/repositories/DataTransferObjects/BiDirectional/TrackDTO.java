package com.example.Sallefy.repositories.DataTransferObjects.BiDirectional;

import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;

/**
 *  * Bi directional data transfer object for CRUD operations on
 *  * a track (song) performed primarily through TrackRepository and UserRepository
 */
public class TrackDTO extends BaseDTO {


    @SerializedName("liked")
    private boolean liked;

    @SerializedName("color")
    private String color;

    @SerializedName("likes")
    private int likes;

    @SerializedName("duration")
    private float duration;

    @SerializedName("genres")
    private ArrayList<GenreDTO> genres = new ArrayList<>();

    @SerializedName("id")
    private Long id;

    @SerializedName("name")
    private String name;

    @SerializedName("owner")
    private UserDTO owner;

    @SerializedName("released")
    private String released;

    @SerializedName("thumbnail")
    private String thumbnail;

    @SerializedName("url")
    private String url;

    public TrackDTO(boolean status, String error) {
        super(status, error);
    }

    public TrackDTO(boolean status,
                    String error,
                    String color,
                    float duration,
                    ArrayList<GenreDTO> genres,
                    String name,
                    UserDTO owner,
                    String released,
                    String thumbnail,
                    String url,
                    boolean liked,
                    int likes) {
        super(status, error);
        this.color = color;
        this.duration = duration;
        this.genres = genres;
        this.id = null;
        this.name = name;
        this.owner = owner;
        this.released = released;
        this.thumbnail = thumbnail;
        this.url = url;
        this.liked = liked;
        this.likes = likes;
    }

    public TrackDTO(String color,
                    float duration,
                    ArrayList<GenreDTO> genres,
                    String name,
                    UserDTO owner,
                    String released,
                    String thumbnail,
                    String url,
                    boolean liked,
                    int likes) {
        this.color = color;
        this.duration = duration;
        this.genres = genres;
        this.id = null;
        this.name = name;
        this.owner = owner;
        this.released = released;
        this.thumbnail = thumbnail;
        this.url = url;
        this.liked = liked;
        this.likes = likes;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public ArrayList<GenreDTO> getGenres() {
        return genres;
    }

    public void setGenres(ArrayList<GenreDTO> genres) {
        this.genres = genres;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserDTO getOwner() {
        return owner;
    }

    public void setOwner(UserDTO owner) {
        this.owner = owner;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String generatePublicId() {
        StringBuilder publicIdBuilder = new StringBuilder();
        Date date = new Date();

        if(null == this.getOwner() || null == this.getOwner().getLogin()) {
            publicIdBuilder
                    .append(this.getId());
        }
        else {
            publicIdBuilder
                    .append(this.getOwner().getLogin())
                    .append("/");
            if(null == this.getName()) {
                publicIdBuilder
                        .append(this.getId());
            }
        }

        return publicIdBuilder.toString();
    }
}
