package com.example.Sallefy.repositories.DataTransferObjects.Incoming;

import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.UserDTO;

import java.util.List;

public class SearchDTO extends BaseDTO {
    private List<PlaylistDTO> playlists;
    private List<TrackDTO> tracks;
    private List<UserDTO> users;

    public SearchDTO(List<PlaylistDTO> playlists,
                     List<TrackDTO> tracks,
                     List<UserDTO> users) {
        super();
        this.playlists = playlists;
        this.tracks = tracks;
        this.users = users;
    }


    public List<PlaylistDTO> getPlaylists() {
        return playlists;
    }

    public void setPlaylists(List<PlaylistDTO> playlists) {
        this.playlists = playlists;
    }

    public List<TrackDTO> getTracks() {
        return tracks;
    }

    public void setTracks(List<TrackDTO> tracks) {
        this.tracks = tracks;
    }

    public List<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(List<UserDTO> users) {
        this.users = users;
    }
}
