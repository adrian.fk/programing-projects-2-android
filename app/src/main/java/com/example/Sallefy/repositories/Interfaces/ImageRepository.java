package com.example.Sallefy.repositories.Interfaces;

import android.net.Uri;

import androidx.lifecycle.MutableLiveData;

import com.example.Sallefy.repositories.DataTransferObjects.Incoming.ImageUploadDTO;

/**
 *  This repository handles the different uploads of images to Cloudinary
 */

public interface ImageRepository {
    /**
     * @param imageUri corresponding image URI targeting local file
     * @return String containing the URL that belongs to the uploaded image.
     */
    MutableLiveData<ImageUploadDTO> uploadImage(Uri imageUri);
}
