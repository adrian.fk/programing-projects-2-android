package com.example.Sallefy.repositories.factories;

import com.example.Sallefy.repositories.Interfaces.TrackRepository;

public abstract class TrackRepositoryFactory {

    public abstract TrackRepository create();
}
