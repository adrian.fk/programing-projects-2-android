package com.example.Sallefy.repositories.Interfaces;

import androidx.lifecycle.MutableLiveData;

import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.UserIsFollowingDTO;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Generic interface for retrieving and updating PlaylistDTOs on the server / database side
 */
public interface PlaylistRepository {
    /**
     * Function defines interface for retrieving all playlists registered at the repository
     * endpoint
     *
     * @return Observable mutable live data (MLD) which will be updated with real data the
     * upon completion of fetch. MLD may immediately contain cached data. This data may not be
     * synchronized with the server and therefore wait for the post-fetch change if you only
     * want the most reliable data
     */
    MutableLiveData<Collection<PlaylistDTO>> getAllPlaylists();

    /**
     * Function defines interface for registering a new playlist at the repository endpoint. Do
     * not define id on playlist dto, this is allocated on the repository side.
     *
     * @return Observable mutable live data (MLD) containing the response BaseDTO for this
     * operation in case the client wishes to trigger a special operation upon completion. The MLD
     * object value is never posted if operation was successful.
     */
    MutableLiveData<PlaylistDTO> createPlaylist(PlaylistDTO playlist);

    /**
     * Function defines interface for updating a playlist at the repository endpoint. Playlist is
     * targeted by setting id on PlaylistDTO
     *
     * @return Observable mutable live data (MLD) containing the response BaseDTO for this
     * operation in case the client wishes to trigger a special operation upon completion. The MLD
     * object value is never posted if operation was successful.
     */
    MutableLiveData<BaseDTO> updatePlaylist(PlaylistDTO playlist);

    /**
     * Function defines interface for deleting a playlist at the repository endpoint. Playlist is
     * targeted by setting id on PlaylistDTO
     *
     * @return Observable mutable live data (MLD) containing the response BaseDTO for this
     * operation in case the client wishes to trigger a special operation upon completion. The MLD
     * object value is never posted if operation was successful.
     */
    MutableLiveData<BaseDTO> deletePlaylist(PlaylistDTO playlist);

    /**
     * Function defines interface for retrieving a specific playlist by id.
     *
     * @return Observable mutable live data (MLD) which will be updated with real data the
     * upon completion of fetch. MLD may immediately contain cached data. This data may not be
     * synchronized with the server and therefore wait for the post-fetch change if you only
     * want the most reliable data
     */
    MutableLiveData<PlaylistDTO> getPlaylistById(long id);

    /**
     * Function defines interface for retrieving a specific playlist by id.
     *
     * @return Observable mutable live data (MLD) which will be updated with real data the
     * upon completion of fetch. MLD may immediately contain cached data. This data may not be
     * synchronized with the server and therefore wait for the post-fetch change if you only
     * want the most reliable data
     */
    MutableLiveData<UserIsFollowingDTO> isUserFollowingPlaylist(long playlistId);

    /**
     * Function defines interface to follow a specific playlist by id.
     *
     * @return Observable mutable live data (MLD) containing the response BaseDTO for this
     * operation in case the client wishes to trigger a special operation upon completion. The MLD
     * object value is never posted if operation was successful.
     */
    MutableLiveData<UserIsFollowingDTO> followPlayListById(long playlistId);

    public MutableLiveData<List<PlaylistDTO>> getCurrentUserPlaylists();
}
