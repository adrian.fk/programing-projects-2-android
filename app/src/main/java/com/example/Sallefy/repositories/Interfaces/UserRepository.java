package com.example.Sallefy.repositories.Interfaces;

public interface UserRepository extends CurrentUserRepository, BaseUserRepository, UserTokenRepository {
}