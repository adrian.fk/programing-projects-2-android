package com.example.Sallefy.repositories.DataTransferObjects.BiDirectional;


import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 *  * Bi directional data transfer object for CRUD operations on
 *  * playlist performed primarily through PlaylistRepository and UserRepository
 */
public class PlaylistDTO extends BaseDTO {
    @SerializedName("cover")
    private String cover;

    @SerializedName("description")
    private String description;

    @SerializedName("id")
    private Long id;

    @SerializedName("name")
    private String name;

    @SerializedName("owner")
    private UserDTO OwnerObject;

    @SerializedName("publicAccessible")
    private boolean publicAccessible;

    @SerializedName("thumbnail")
    private String thumbnail;

    @SerializedName("tracks")
    private ArrayList<TrackDTO> tracks = new ArrayList<>();

    @SerializedName("followed")
    private boolean followedByUser;

    public PlaylistDTO() {
        this.id = null;
    }

    public PlaylistDTO(long id, String name, String description, String cover, String thumbnail, boolean publicAccessible, UserDTO owner, ArrayList<TrackDTO> tracks) {
        super(true, "No Error");
        this.id = id;
        this.name = name;
        this.description = description;
        this.cover = cover;
        this.thumbnail = thumbnail;
        this.publicAccessible = publicAccessible;
        this.OwnerObject = owner;
        this.tracks = tracks;
    }

    public PlaylistDTO(boolean status, String error) {
        super(status, error);
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserDTO getOwnerObject() {
        return OwnerObject;
    }

    public void setOwnerObject(UserDTO ownerObject) {
        OwnerObject = ownerObject;
    }

    public boolean isPublicAccessible() {
        return publicAccessible;
    }

    public void setPublicAccessible(boolean publicAccessible) {
        this.publicAccessible = publicAccessible;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public ArrayList<TrackDTO> getTracks() {
        return tracks;
    }

    public void setTracks(ArrayList<TrackDTO> tracks) {
        this.tracks = tracks;
    }

    public boolean isFollowedByUser() {
        return followedByUser;
    }

    public void setFollowedByUser(boolean followedByUser) {
        this.followedByUser = followedByUser;
    }
}
