package com.example.Sallefy.repositories.DataTransferObjects.Incoming;

import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginDTO extends BaseDTO {
    @SerializedName("id_token")
    @Expose
    private String idToken;

    public LoginDTO(String idToken) {
        this.idToken = idToken;
    }

    @Override
    public String toString() {
        return "LoginUserToken{" +
                "idToken='" + idToken + '\'' +
                '}';
    }

    public String getIdToken() {
        return idToken;
    }
}
