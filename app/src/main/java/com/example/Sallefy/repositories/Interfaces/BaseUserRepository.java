package com.example.Sallefy.repositories.Interfaces;

import androidx.lifecycle.MutableLiveData;

import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.UserDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.UserIsFollowingDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Outgoing.UserRegister;

import java.util.ArrayList;
import java.util.Collection;

public interface BaseUserRepository {
    MutableLiveData<BaseDTO> saveAccount(UserDTO user);
    MutableLiveData<Collection<UserDTO>> getAllUsers();
    MutableLiveData<BaseDTO> createUser(UserRegister user);
    MutableLiveData<UserDTO> updateUser(UserDTO user);
    MutableLiveData<BaseDTO> deleteUser(String username);
    MutableLiveData<Collection<String>> getAuthorizationTypes();
    MutableLiveData<UserDTO> getUserByName(String username);
    MutableLiveData<UserIsFollowingDTO> followUserByUsername(String username);
    MutableLiveData<Collection<PlaylistDTO>> getPlaylistsByUserName(String username);
    MutableLiveData<Collection<TrackDTO>> getTracksByUsername(String username);
}
