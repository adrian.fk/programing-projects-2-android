package com.example.Sallefy.repositories.Interfaces;

import androidx.lifecycle.MutableLiveData;

import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.UserDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.UserIsFollowingDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.UserIsLikingTrackDTO;

import java.util.Collection;

public interface CurrentUserRepository {
    /**
     * Get operation for the current user's followers
     *
     * For more see {@link MutableLiveData}
     *
     * @return A collection view of UserDTOs representing users that follow the current user.
     * Collection could be empty
     */
    MutableLiveData<Collection<UserDTO>> getCurrentUserFollowers();

    /**
     * Get operation to retrieve the user's following the current user
     *
     * For more see {@link MutableLiveData}
     *
     * @return Collection view of UserDTOs representing the user's following the app user.
     * Collection could be empty.
     */
    MutableLiveData<Collection<UserDTO>> getCurrentUserFollowing();

    /**
     * Get operation to retrieve the playlists associated with the current user
     *
     * For more see {@link MutableLiveData}
     *
     * @return Collection view of PlaylistDTOs representing the playlists saved by this user
     * Collection could be empty.
     */
    MutableLiveData<Collection<PlaylistDTO>> getCurrentUserPlaylists();

    /**
     * Get operation to retrieve only those playlists followed by the current user
     *
     * For more see {@link MutableLiveData}
     *
     * @return Collection view of PlaylistDTOs representing the playlists this user follows
     * Collection could be empty.
     */
    MutableLiveData<Collection<PlaylistDTO>> getCurrentUserFollowingPlaylists();

    /**
     * Get operation to retrieve a specific playlist from the repository based on its Id. Operation
     * should fail if playlist being retrieved is not visible to the current user (i.e. they are not
     * the owner and the playlist is not public).
     *
     * For more see {@link MutableLiveData}
     *
     * @param playlistId The unique identifier of a playlist
     * @return Optionally a PlaylistDTO.
     */
    MutableLiveData<PlaylistDTO> getCurrentUserPlaylistById(long playlistId);

    /**
     * Get operation to retrieve all the tracks created by the current user
     *
     * For more see {@link MutableLiveData}
     *
     * @return Collection view of TrackDTOs representing the tracks created by this user.
     * Collection could be empty.
     */
    MutableLiveData<Collection<TrackDTO>> getCurrentUserTracks();

    /**
     * Get operation to retrieve all the tracks liked by the current user
     *
     * For more see {@link MutableLiveData}
     *
     * @return Collection view of TrackDTOs representing the tracks liked by this user.
     * Collection could be empty.
     */
    MutableLiveData<Collection<TrackDTO>> getCurrentUserLikedTracks();

    /**
     * Get operation to retrieve a specific track from the repository based on its Id.
     *
     * For more see {@link MutableLiveData}
     *
     * @param id The unique identifier of a track
     *
     * @return Optionally a TrackDTO.
     */
    MutableLiveData<TrackDTO> getCurrentUserTrackById(long id);

    /**
     * Get operation to retrieve object representing whether the user is following a specific user
     * targeted by the other user's username.
     *
     * For more see {@link MutableLiveData}
     *
     * @param username Unique username representing user to check against
     *
     * @return Proprietary object representing whether the user is liking a track.
     */
    MutableLiveData<UserIsFollowingDTO> getCurrentUserIsFollowingUserByName(String username);
}
