package com.example.Sallefy.repositories.Interfaces;

import androidx.lifecycle.MutableLiveData;

public interface SessionAuthenticationGateway {
    MutableLiveData<Boolean> authenticate(String username, String password, boolean rememberMe);
}
