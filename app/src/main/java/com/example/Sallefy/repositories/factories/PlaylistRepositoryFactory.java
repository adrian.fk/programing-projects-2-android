package com.example.Sallefy.repositories.factories;

import com.example.Sallefy.repositories.Interfaces.PlaylistRepository;

public abstract class PlaylistRepositoryFactory {
    public abstract PlaylistRepository create();
}
