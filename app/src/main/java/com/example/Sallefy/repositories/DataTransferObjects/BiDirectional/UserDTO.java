package com.example.Sallefy.repositories.DataTransferObjects.BiDirectional;

import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 *  * Bi directional data transfer object for CRUD operations on
 *  * playlist performed through the various User properties. User
 *  DTOs are also composite members of other objects such as tracks
 *  and playlists for which they are owners.
 */
public class UserDTO extends BaseDTO {
    @SerializedName("activated")
    private boolean activated; //not implemented on server side

    @SerializedName("authorities")
    private  ArrayList<String> authorities = new ArrayList<>();

    @SerializedName("createdBy")
    private String createdBy;

    @SerializedName("createdDate")
    private String createdDate;

    @SerializedName("email")
    private String email;

    @SerializedName("firstName")
    private String firstName;

    @SerializedName("followers")
    private int followers;

    @SerializedName("following")
    private int following;

    @SerializedName("followed")
    private boolean followed;

    @SerializedName("id")
    private long id;

    @SerializedName("imageUrl")
    private String imageUrl;

    @SerializedName("langKey")
    private String langKey;

    @SerializedName("lastModifiedBy")
    private String lastModifiedBy;

    @SerializedName("lastModifiedDate")
    private String lastModifiedDate;

    @SerializedName("lastName")
    private String lastName;

    @SerializedName("login")
    private String login;

    @SerializedName("playlists")
    private long playlists;

    @SerializedName("tracks")
    private long tracks;


    public UserDTO() { }

    public UserDTO(boolean status, String error) {
        super(status, error);
    }

    /**
     * Constructor do
     * @param activated
     * @param email
     * @param imageUrl
     * @param login
     */
    public UserDTO(boolean activated, String email, String imageUrl, String login) {
        super();
        this.activated = activated;
        this.email = email;
        this.imageUrl = imageUrl;
        this.login = login;
    }

    public UserDTO(ArrayList<String> authorities,
                   String createdBy,
                   String createdDate,
                   String email,
                   String firstName,
                   int followers,
                   int following,
                   long id,
                   String imageUrl,
                   String langKey,
                   String lastModifiedBy,
                   String lastModifiedDate,
                   String lastName,
                   String login,
                   long playlists,
                   long tracks) {
        super();
        this.authorities = authorities;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.email = email;
        this.firstName = firstName;
        this.followers = followers;
        this.following = following;
        this.id = id;
        this.imageUrl = imageUrl;
        this.langKey = langKey;
        this.lastModifiedBy = lastModifiedBy;
        this.lastModifiedDate = lastModifiedDate;
        this.lastName = lastName;
        this.login = login;
        this.playlists = playlists;
        this.tracks = tracks;
    }
    // Getter Methods

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public ArrayList<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(ArrayList<String> authorities) {
        this.authorities = authorities;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getFollowers() {
        return followers;
    }

    public void setFollowers(int followers) {
        this.followers = followers;
    }

    public int getFollowing() {
        return following;
    }

    public void setFollowing(int following) {
        this.following = following;
    }

    public boolean isFollowed() {
        return followed;
    }

    public void setFollowed(boolean followed) {
        this.followed = followed;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLangKey() {
        return langKey;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public long getPlaylists() {
        return playlists;
    }

    public void setPlaylists(long playlists) {
        this.playlists = playlists;
    }

    public long getTracks() {
        return tracks;
    }

    public void setTracks(long tracks) {
        this.tracks = tracks;
    }
}