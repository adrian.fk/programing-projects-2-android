package com.example.Sallefy.repositoryImpl.repositoryImpl;

import android.os.Handler;

import androidx.lifecycle.MutableLiveData;

import com.example.Sallefy.Utils.RetrofitCallbackAdapter;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.SearchDTO;
import com.example.Sallefy.repositories.Interfaces.SearchGateway;
import com.example.Sallefy.repositoryImpl.Services.factories.ServiceFactory;
import com.example.Sallefy.repositoryImpl.Services.interfaces.SearchService;

import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import retrofit2.Call;

public class SearchApiRepository extends SessionObjectRepository implements SearchGateway {
    private static final Map<String, SearchDTO> searchCacheMap = new ConcurrentHashMap<>();

    private MutableLiveData<SearchDTO> searchDTO;
    private SearchService searchService;

    public SearchApiRepository(ServiceFactory<SearchService> searchServiceFactory) {
        this.searchService = searchServiceFactory.create();
        this.searchDTO = new MutableLiveData<>();
    }

    @Override
    public MutableLiveData<SearchDTO> globalSearchByKeyword(String keyword) {
        Handler handler = new Handler();
        if (searchCacheMap.containsKey(keyword)) {
            SearchDTO cachedSearchDTO = searchCacheMap.get(keyword);

            Date validTimeIntervalStartpoint = Timestamper.getValidTime(Timestamper.MINUTE, -1);

            if (cachedSearchDTO != null &&
                    Timestamper.isValidTimeInterval(validTimeIntervalStartpoint, cachedSearchDTO.getTimestampFetched())) {

                handler.post(
                        () -> searchDTO.postValue(
                                cachedSearchDTO
                        )
                );
            }
            else {
                if (searchCacheMap.keySet().size() > 0) {
                    handler.post(() -> cleanUpCache(validTimeIntervalStartpoint));
                }
                fetchSearchData(keyword);
            }
        }
        else {
            fetchSearchData(keyword);
        }

        return this.searchDTO;
    }

    private void cleanUpCache(Date validTimeIntervalStartpoint) {
        for (String key : searchCacheMap.keySet()) {
            if (!Timestamper.isValidTimeInterval(
                    validTimeIntervalStartpoint,
                    Objects.requireNonNull(searchCacheMap.get(key)).getTimestampFetched())) {

                searchCacheMap.remove(key);
            }
        }
    }

    private void fetchSearchData(String keyword) {
        Call<SearchDTO> call = this.searchService.search(keyword, this.getSession().getTokenWithBearer());

        call.enqueue(new RetrofitCallbackAdapter<>(
                        response -> {
                            response.stamp();
                            searchCacheMap.put(keyword, response);
                            searchDTO.postValue(response);
                        },
                        null,
                        null
                )
        );
    }
}
