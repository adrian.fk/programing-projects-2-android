package com.example.Sallefy.repositoryImpl.repositoryImpl;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

public class Timestamper {
    static final String MONTH = "MONTH";
    static final String HOUR = "HOUR";
    static final String DAY = "DAY";
    static final String YEAR = "YEAR";
    static final String MINUTE = "MINUTE";

    static boolean isValidTimeInterval(Date validRangeTimestap, Date targetTimestamp) {
        return targetTimestamp.after(validRangeTimestap);
    }

    static Date getValidTime(String configKey, int offsetAmount) {
        Date now = Date.from(Instant.now());
        Calendar c = Calendar.getInstance();
        c.setTime(now);

        switch (configKey) {
            case MINUTE:
                c.add(Calendar.MINUTE, offsetAmount);
                break;

            case HOUR:
                c.add(Calendar.HOUR, offsetAmount);
                break;

            case MONTH:
                c.add(Calendar.MONTH, offsetAmount);
                break;

            case DAY:
                c.add(Calendar.DAY_OF_MONTH, offsetAmount);
                break;

            case YEAR:
                c.add(Calendar.YEAR, offsetAmount);
                break;
        }

        return c.getTime();
    }
}
