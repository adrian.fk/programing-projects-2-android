package com.example.Sallefy.repositoryImpl.repositoryImpl;

import androidx.lifecycle.MutableLiveData;

import com.example.Sallefy.Utils.RetrofitCallbackAdapter;
import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.UserDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.LoginDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.UserIsFollowingDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Outgoing.UserLoginDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Outgoing.UserRegister;
import com.example.Sallefy.repositories.Interfaces.UserRepository;
import com.example.Sallefy.repositoryImpl.Services.factories.ServiceFactory;
import com.example.Sallefy.repositoryImpl.Services.interfaces.CurrentUserService;
import com.example.Sallefy.repositoryImpl.Services.interfaces.UserService;
import com.example.Sallefy.repositoryImpl.Services.interfaces.UserTokenService;

import java.util.Collection;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class UserApiRepository extends SessionObjectRepository implements UserRepository {
    private CurrentUserService currentUserService;
    private ServiceFactory<CurrentUserService> currentUserServiceFactory;
    private UserService userService;
    private ServiceFactory<UserService> userServiceFactory;
    private UserTokenService userTokenService;
    private ServiceFactory<UserTokenService> userTokenServiceFactory;

    private final MutableLiveData<Collection<PlaylistDTO>> playlistsCache = new MutableLiveData<>();


    public UserApiRepository(
                             ServiceFactory<CurrentUserService> currentUserServiceFactory,
                             ServiceFactory<UserService> userServiceFactory,
                             ServiceFactory<UserTokenService> userTokenServiceFactory) {

        this.currentUserServiceFactory = currentUserServiceFactory;
        this.userServiceFactory = userServiceFactory;
        this.userTokenServiceFactory = userTokenServiceFactory;
    }

    private CurrentUserService getCurrentUserService() {
        if(null == this.currentUserService) {
            this.currentUserService = this.currentUserServiceFactory.create();
        }

        return this.currentUserService;
    }

    private UserService getUserService() {
        if(null == this.userService) {
            this.userService = this.userServiceFactory.create();
        }

        return this.userService;
    }

    private UserTokenService getUserTokenService() {
        if(null == this.userTokenService) {
            this.userTokenService = this.userTokenServiceFactory.create();
        }

        return this.userTokenService;
    }

    @Override
    public MutableLiveData<Collection<UserDTO>> getCurrentUserFollowers() {
        Call<Collection<UserDTO>> call =
                this.getCurrentUserService().getUserFollowers(this.getSession().getToken());

        final MutableLiveData<Collection<UserDTO>> followers = new MutableLiveData<>();

        call.enqueue(new RetrofitCallbackAdapter<>(
                        followers::postValue,
                        null,
                        null
                )
        );

        return followers;
    }

    @Override
    public MutableLiveData<Collection<UserDTO>> getCurrentUserFollowing() {
        Call<Collection<UserDTO>> call =
                this.getCurrentUserService().getUserFollowing(this.getSession().getToken());

        final MutableLiveData<Collection<UserDTO>> following = new MutableLiveData<>();

        call.enqueue(new RetrofitCallbackAdapter<>(
                        following::postValue,
                        null,
                        null
                )
        );

        return following;
    }

    @Override
    public MutableLiveData<Collection<PlaylistDTO>> getCurrentUserPlaylists() {
        Call<Collection<PlaylistDTO>> call =
                this.getCurrentUserService().getUserOwnedPlaylists(this.getSession().getToken());

        final MutableLiveData<Collection<PlaylistDTO>> playlists = new MutableLiveData<>();

        call.enqueue(new RetrofitCallbackAdapter<>(
                        playlists::postValue,
                        null,
                        null
                )
        );

        return playlists;
    }

    @Override
    public MutableLiveData<Collection<PlaylistDTO>> getCurrentUserFollowingPlaylists() {
        Call<Collection<PlaylistDTO>> call =
                this.getCurrentUserService().getUserFollowingPlaylists(this.getSession().getToken());

        call.enqueue(new RetrofitCallbackAdapter<>(
                        playlistsCache::postValue,
                        null,
                        null
                )
        );

        return playlistsCache;
    }

    @Override
    public MutableLiveData<PlaylistDTO> getCurrentUserPlaylistById(long playlistId) {
        Call<PlaylistDTO> call =
                this.getCurrentUserService().getUserPlaylistById(this.getSession().getToken(), playlistId);

        final MutableLiveData<PlaylistDTO> playlist = new MutableLiveData<>();

        call.enqueue(new RetrofitCallbackAdapter<>(
                        playlist::postValue,
                        (t) -> playlist.postValue(new PlaylistDTO(false, t.getMessage())),
                        (t) -> playlist.postValue(new PlaylistDTO(false, t.getMessage()))
                )
        );

        return playlist;
    }

    @Override
    public MutableLiveData<Collection<TrackDTO>> getCurrentUserTracks() {
        Call<Collection<TrackDTO>> call =
                this.getCurrentUserService().getUserOwnTracks(this.getSession().getToken());

        final MutableLiveData<Collection<TrackDTO>> tracks = new MutableLiveData<>();

        call.enqueue(new RetrofitCallbackAdapter<>(
                tracks::postValue,
                null,
                null
                )
        );

        return tracks;
    }

    @Override
    public MutableLiveData<Collection<TrackDTO>> getCurrentUserLikedTracks() {
        Call<Collection<TrackDTO>> call =
                this.getCurrentUserService().getUserLikedTracks(this.getSession().getToken());

        final MutableLiveData<Collection<TrackDTO>> tracks = new MutableLiveData<>();

        call.enqueue(new RetrofitCallbackAdapter<>(
                        tracks::postValue,
                        null,
                        null
                )
        );

        return tracks;
    }

    @Override
    public MutableLiveData<TrackDTO> getCurrentUserTrackById(long id) {
        Call<TrackDTO> call =
                this.getCurrentUserService().getUserTrackById(this.getSession().getToken(), id);

        final MutableLiveData<TrackDTO> track = new MutableLiveData<>();

        call.enqueue(new RetrofitCallbackAdapter<>(
                        track::postValue,
                        (t) -> track.postValue(new TrackDTO(false, t.getMessage())),
                        (t) -> track.postValue(new TrackDTO(false, t.getMessage()))
                )
        );

        return track;
    }

    @Override
    public MutableLiveData<UserIsFollowingDTO> getCurrentUserIsFollowingUserByName(String username) {
        Call<UserIsFollowingDTO> call =
                this.getCurrentUserService().getUserIsFollowingUserByUsername(
                        this.getSession().getToken(),
                        username
                );

        final MutableLiveData<UserIsFollowingDTO> response = new MutableLiveData<>();

        call.enqueue(new RetrofitCallbackAdapter<>(
                        response::postValue,
                        (t) -> response.postValue(
                                new UserIsFollowingDTO(false, t.getMessage(), false)
                        ),
                        (t) -> response.postValue(
                                new UserIsFollowingDTO(false, t.getMessage(), false)
                        )
                )
        );

        return response;
    }

    @Override
    public MutableLiveData<BaseDTO> saveAccount(UserDTO user) {
        Call<UserDTO> call =
                this.getUserService().updateWithUserDTO(this.getSession().getToken(), user);

        final MutableLiveData<BaseDTO> response = new MutableLiveData<>();

        call.enqueue(new RetrofitCallbackAdapter<>(
                (userResponse) -> response.postValue(
                        new BaseDTO(true,
                                    "E_NO_ERROR: created User " + userResponse.getLogin()
                        )
                ),
                (t) -> response.postValue(new BaseDTO(false, t.getMessage())),
                (t) -> response.postValue(new BaseDTO(false, t.getMessage()))
        ));

        return response;
    }

    @Override
    public MutableLiveData<Collection<UserDTO>> getAllUsers() {
        Call<Collection<UserDTO>> call =
                this.getUserService().getAllUserDTOs(this.getSession().getToken());

        final MutableLiveData<Collection<UserDTO>> allUsers = new MutableLiveData<>();

        call.enqueue(new RetrofitCallbackAdapter<>(
                        allUsers::postValue,
                        null,
                        null
                )
        );

        return allUsers;
    }

    @Override
    public MutableLiveData<BaseDTO> createUser(UserRegister user) {
        Call<ResponseBody> call =
                this.getUserService().registerUser(this.getSession().getToken(), user);

        final MutableLiveData<BaseDTO> response = new MutableLiveData<>();
        call.enqueue(new RetrofitCallbackAdapter<>(
                (responseBody -> response.postValue(new BaseDTO(true, "E_NO_ERROR"))),
                (t) -> response.postValue(new BaseDTO(false, "E_REGISTER_FAILURE")),
                (t) -> response.postValue(new BaseDTO(false, "E_NO_CONNECTION"))
                )
        );

        return response;
    }

    @Override
    public MutableLiveData<UserDTO> updateUser(UserDTO user) {
        Call<UserDTO> call =
                this.getUserService().updateWithUserDTO(this.getSession().getToken(), user);

        final MutableLiveData<UserDTO> userCache = new MutableLiveData<>();
        call.enqueue(new RetrofitCallbackAdapter<>(
                    (incomingUser) -> {
                        incomingUser.setStatus(true);
                        incomingUser.setError("E_NO_ERROR");
                        userCache.postValue(incomingUser);
                    },
                    (t) -> userCache.postValue(new UserDTO(false, t.getMessage())),
                    (t) -> userCache.postValue(new UserDTO(false, t.getMessage()))
                )
        );

        return userCache;
    }

    @Override
    public MutableLiveData<BaseDTO> deleteUser(String username) {
        Call<ResponseBody> call =
                this.getUserService().deleteUser(this.getSession().getToken(), username);

        final MutableLiveData<BaseDTO> response = new MutableLiveData<>();
        call.enqueue(new RetrofitCallbackAdapter<>(
                        (responseBody -> response.postValue(new BaseDTO(true, "E_NO_ERROR"))),
                        (t) -> response.postValue(new BaseDTO(false, t.getMessage())),
                        (t) -> response.postValue(new BaseDTO(false, t.getMessage()))
                )
        );

        return response;
    }

    @Override
    public MutableLiveData<Collection<String>> getAuthorizationTypes() {
        Call<Collection<String>> call =
                this.getUserService().getAuthorities(this.getSession().getToken());

        final MutableLiveData<Collection<String>> response = new MutableLiveData<>();
        call.enqueue(new RetrofitCallbackAdapter<>(
                        response::postValue,
                        null,
                null
                )
        );

        return response;
    }

    @Override
    public MutableLiveData<UserDTO> getUserByName(String username) {
        Call<UserDTO> call =
                this.getUserService().getUserDTOByLogin(this.getSession().getToken(), username);

        final MutableLiveData<UserDTO> response = new MutableLiveData<>();
        call.enqueue(new RetrofitCallbackAdapter<>(
                        (responseBody -> {
                            responseBody.setStatus(true);
                            responseBody.setError("E_NO_ERROR");
                            response.postValue(responseBody);
                        }),
                        (t) -> response.postValue(new UserDTO(false, t.getMessage())),
                        (t) -> response.postValue(new UserDTO(false, t.getMessage()))
                )
        );

        return response;
    }

    @Override
    public MutableLiveData<UserIsFollowingDTO> followUserByUsername(String username) {
        Call<UserIsFollowingDTO> call =
                this.getCurrentUserService().followUserByUsername(this.getSession().getToken(), username);

        final MutableLiveData<UserIsFollowingDTO> response = new MutableLiveData<>();

        call.enqueue(
                new RetrofitCallbackAdapter<>(
                        responseDTO -> {
                            responseDTO.setUserLogin(username);
                            response.postValue(responseDTO);
                        },
                        null,
                        null
                )
        );

        return response;
    }

    @Override
    public MutableLiveData<Collection<PlaylistDTO>> getPlaylistsByUserName(String username) {
        Call<Collection<PlaylistDTO>> call =
                this.getUserService().getPlaylistsByLogin(this.getSession().getToken(), username);

        final MutableLiveData<Collection<PlaylistDTO>> response = new MutableLiveData<>();

        call.enqueue(new RetrofitCallbackAdapter<>(
                        response::postValue,
                        null,
                null
                )
        );

        return response;
    }

    @Override
    public MutableLiveData<Collection<TrackDTO>> getTracksByUsername(String username) {
        Call<Collection<TrackDTO>> call =
                this.getUserService().getTracksByLogin(this.getSession().getToken(), username);

        final MutableLiveData<Collection<TrackDTO>> response = new MutableLiveData<>();

        call.enqueue(new RetrofitCallbackAdapter<>(
                        response::postValue,
                        null,
                        null
                )
        );

        return response;
    }

    @Override
    public MutableLiveData<BaseDTO> login(String username, String password) {
        final MutableLiveData<BaseDTO> response = new MutableLiveData<>();

        Call<LoginDTO> call = getUserTokenService().loginUser(
                new UserLoginDTO(username, password, true)
        );

        call.enqueue(new RetrofitCallbackAdapter<>(
                (res) -> {
                    SessionBearer.createSession(res.getIdToken(), username);
                    response.postValue(new BaseDTO());
                },
                (res) -> response.postValue(
                        new BaseDTO(
                                false,
                                "E_INVALID_ARGS"
                        )
                ),
                (res) -> response.postValue(
                        new BaseDTO(
                                false,
                                "E_NO_CONNECTION"
                        )
                )
        ));

        return response;
    }
}
