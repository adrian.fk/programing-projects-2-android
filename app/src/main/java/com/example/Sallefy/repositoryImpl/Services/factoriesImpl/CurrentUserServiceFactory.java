package com.example.Sallefy.repositoryImpl.Services.factoriesImpl;

import com.example.Sallefy.repositoryImpl.Services.factories.RetrofitServiceFactory;
import com.example.Sallefy.repositoryImpl.Services.interfaces.CurrentUserService;

public class CurrentUserServiceFactory extends RetrofitServiceFactory<CurrentUserService> {
    @Override
    public CurrentUserService create() {
        return retrofit.create(CurrentUserService.class);
    }
}
