package com.example.Sallefy.repositoryImpl.factoriesImpl;

import com.example.Sallefy.repositories.Interfaces.TrackRepository;
import com.example.Sallefy.repositories.factories.TrackRepositoryFactory;
import com.example.Sallefy.repositoryImpl.repositoryImpl.TrackApiRepository;
import com.example.Sallefy.repositoryImpl.Services.factoriesImpl.TrackServiceFactory;

public class TrackApiRepositoryFactory extends TrackRepositoryFactory {
    @Override
    public TrackRepository create() {
        return new TrackApiRepository(new TrackServiceFactory());
    }
}
