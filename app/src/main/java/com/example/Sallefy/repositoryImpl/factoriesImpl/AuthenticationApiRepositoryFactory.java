package com.example.Sallefy.repositoryImpl.factoriesImpl;

import com.example.Sallefy.repositories.Interfaces.IsAuthenticatedGateway;
import com.example.Sallefy.repositories.Interfaces.SessionAuthenticationGateway;
import com.example.Sallefy.repositories.Interfaces.SessionAuthenticationRepository;
import com.example.Sallefy.repositories.Interfaces.UserLoginGateway;
import com.example.Sallefy.repositories.factories.AuthenticationRepositoryFactory;
import com.example.Sallefy.repositoryImpl.repositoryImpl.authentication.AuthenticationRepository;
import com.example.Sallefy.repositoryImpl.Services.factoriesImpl.UserTokenServiceFactory;

public class AuthenticationApiRepositoryFactory extends AuthenticationRepositoryFactory {
    @Override
    public SessionAuthenticationRepository createRepository() {
        return new AuthenticationRepository(new UserTokenServiceFactory());
    }

    @Override
    public IsAuthenticatedGateway createIsAuthenticateGateway() {
        return this.createRepository();
    }

    @Override
    public SessionAuthenticationGateway createAuthenticationGateway() {
        return this.createRepository();

    }

    @Override
    public UserLoginGateway createUserLoginGateway() {
        return this.createRepository();
    }
}
