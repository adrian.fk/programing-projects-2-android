package com.example.Sallefy.repositoryImpl.repositoryImpl.authentication;

import androidx.lifecycle.MutableLiveData;

import com.example.Sallefy.Login.Model.LoginUserToken;
import com.example.Sallefy.Login.Model.UserLogin;
import com.example.Sallefy.Utils.RetrofitCallbackAdapter;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.LoginDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Outgoing.UserLoginDTO;
import com.example.Sallefy.repositories.Interfaces.SessionAuthenticationRepository;
import com.example.Sallefy.repositoryImpl.repositoryImpl.SessionBearer;
import com.example.Sallefy.repositoryImpl.repositoryImpl.SessionObjectRepository;
import com.example.Sallefy.repositoryImpl.Services.factories.ServiceFactory;
import com.example.Sallefy.repositoryImpl.Services.interfaces.UserTokenService;

import retrofit2.Call;

public class AuthenticationRepository extends SessionObjectRepository implements SessionAuthenticationRepository {
    private UserTokenService tokenService = null;
    ServiceFactory<UserTokenService> serviceFactory;

    public AuthenticationRepository(ServiceFactory<UserTokenService> tokenServiceFactory) {
        serviceFactory = tokenServiceFactory;
    }
    private UserTokenService getTokenService() {
        if (null == this.tokenService) {
            this.tokenService = serviceFactory.create();
        }

        return this.tokenService;
    }

    @Override
    public MutableLiveData<Boolean> authenticate(String username, String password, boolean rememberMe) {
        Call<LoginDTO> call =
                this.getTokenService().loginUser(new UserLoginDTO(username, password, rememberMe));

        MutableLiveData<Boolean> loginSuccess = new MutableLiveData<>();

        call.enqueue(new RetrofitCallbackAdapter<>(
                (res) -> {
                    SessionBearer.createSession(res.getIdToken(), username);
                    loginSuccess.postValue(true);
                },
                (res) -> loginSuccess.postValue(false),
                (res) -> loginSuccess.postValue(false)
        ));
        return loginSuccess;
    }

    @Override
    public boolean isSessionOpen() {
        return null != this.getSession();
    }

    @Override
    public String getUserLogin() {
        return this.getSession().getUserLogin();
    }
}
