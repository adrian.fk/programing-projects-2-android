package com.example.Sallefy.repositoryImpl.factoriesImpl;

import com.example.Sallefy.repositories.Interfaces.SearchGateway;
import com.example.Sallefy.repositories.factories.SearchGatewayFactory;
import com.example.Sallefy.repositoryImpl.repositoryImpl.SearchApiRepository;
import com.example.Sallefy.repositoryImpl.Services.factoriesImpl.SearchServiceFactory;

public class SearchApiRepositoryFactory extends SearchGatewayFactory {
    @Override
    public SearchGateway create() {
        return new SearchApiRepository(new SearchServiceFactory());
    }
}
