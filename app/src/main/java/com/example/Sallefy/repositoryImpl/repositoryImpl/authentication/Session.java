package com.example.Sallefy.repositoryImpl.repositoryImpl.authentication;

public class Session {
    private String token;
    private String userLogin;

    Session() {}

    public Session(String token, String userLogin) {
        this.token = token;
        this.userLogin = userLogin;
    }

    public void configureSession(String token, String userID) {
        this.token = token;
        this.userLogin = userID;
    }

    public String getTokenWithBearer() {
        return "Bearer " + this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public boolean isConfigured() {
        return this.token != null && this.userLogin != null;
    }

    public String getToken() {
        return "Bearer " + this.token;
    }
}
