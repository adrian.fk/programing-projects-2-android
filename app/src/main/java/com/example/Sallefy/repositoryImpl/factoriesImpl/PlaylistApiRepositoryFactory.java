package com.example.Sallefy.repositoryImpl.factoriesImpl;

import com.example.Sallefy.repositories.Interfaces.PlaylistRepository;
import com.example.Sallefy.repositories.factories.PlaylistRepositoryFactory;
import com.example.Sallefy.repositoryImpl.repositoryImpl.PlaylistApiRepository;
import com.example.Sallefy.repositoryImpl.Services.factoriesImpl.PlaylistServiceFactory;

public class PlaylistApiRepositoryFactory extends PlaylistRepositoryFactory {
    @Override
    public PlaylistRepository create() {
        return new PlaylistApiRepository(new PlaylistServiceFactory());
    }
}
