package com.example.Sallefy.repositoryImpl.Services.interfaces;

import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.UserDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Outgoing.UserRegister;

import java.util.Collection;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UserService {
    @GET("users")
    Call<Collection<UserDTO>> getAllUserDTOs(@Header("Authorization") String token);

    @POST("register")
    Call<ResponseBody> registerUser(@Header("Authorization") String token, @Body UserRegister managedUserVM);

    @PUT("users")
    Call<UserDTO> updateWithUserDTO(@Header("Authorization") String token, @Body UserDTO user);

    @GET("users/authorities")
    Call<Collection<String>> getAuthorities(@Header("Authorization") String token);

    @GET("users/{login}")
    Call<UserDTO> getUserDTOByLogin(@Header("Authorization") String token, @Path("login") String login);

    @DELETE("users/{login}")
    Call<ResponseBody> deleteUser(@Header("Authorization") String token, @Path("login") String login);

    @GET("users/{login}/playlists")
    Call<Collection<PlaylistDTO>> getPlaylistsByLogin(@Header("Authorization") String token, @Path("login") String login);

    @GET("users/{login}/tracks")
    Call<Collection<TrackDTO>> getTracksByLogin(@Header("Authorization") String token, @Path("login") String login);
}
