package com.example.Sallefy.repositoryImpl.Services.interfaces;

import com.example.Sallefy.repositories.DataTransferObjects.Incoming.SearchDTO;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface SearchService {
    @GET("search")
    Call<SearchDTO> search(@Query("keyword") String keyword, @Header("Authorization") String token);
}
