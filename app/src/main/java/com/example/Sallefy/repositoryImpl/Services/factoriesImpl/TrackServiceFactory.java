package com.example.Sallefy.repositoryImpl.Services.factoriesImpl;

import com.example.Sallefy.repositoryImpl.Services.factories.RetrofitServiceFactory;
import com.example.Sallefy.repositoryImpl.Services.interfaces.TrackService;

public class TrackServiceFactory extends RetrofitServiceFactory<TrackService> {
    @Override
    public TrackService create() {
        return retrofit.create(TrackService.class);
    }
}
