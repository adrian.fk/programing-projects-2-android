package com.example.Sallefy.repositoryImpl.Services.factoriesImpl;

import com.example.Sallefy.repositoryImpl.Services.factories.RetrofitServiceFactory;
import com.example.Sallefy.repositoryImpl.Services.interfaces.SearchService;

public class SearchServiceFactory extends RetrofitServiceFactory<SearchService> {
    public SearchService create() {
        return retrofit.create(SearchService.class);
    }
}
