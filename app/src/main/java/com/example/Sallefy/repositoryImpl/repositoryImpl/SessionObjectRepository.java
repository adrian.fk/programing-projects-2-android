package com.example.Sallefy.repositoryImpl.repositoryImpl;

import com.example.Sallefy.repositoryImpl.repositoryImpl.authentication.Session;

public abstract class SessionObjectRepository {
    private Session session;

    protected Session getSession() {
        if(null == session) {
            try {
                session = SessionBearer.getSession();
            } catch (SessionNotOpenException e) {
                return null;
            }
        }

        return session;
    }
}
