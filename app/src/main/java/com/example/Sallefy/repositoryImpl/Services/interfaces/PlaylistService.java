package com.example.Sallefy.repositoryImpl.Services.interfaces;

import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.UserIsFollowingDTO;

import java.util.Collection;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface PlaylistService {
    @GET("playlists")
    Call<Collection<PlaylistDTO>> getAllPlaylists(@Header("Authorization") String token);

    @GET("playlists/{id}")
    Call<PlaylistDTO> getPlaylistById(@Header("Authorization") String token, @Path("id") long playlistId);

    @POST("playlists")
    Call<PlaylistDTO> createPlaylist(@Header("Authorization") String token, @Body PlaylistDTO newPlaylist);

    @PUT("playlists")
    Call<PlaylistDTO> updatePlaylist(@Header("Authorization") String token, @Body PlaylistDTO playlist);

    @DELETE("playlists/{id}")
    Call<ResponseBody> deletePlaylist(@Header("Authorization") String token, @Path("id") long playlistId);

    @GET("playlists/{id}/follow")
    Call<UserIsFollowingDTO> isUserFollowing(@Header("Authorization") String token, @Path("id") long playlistId);

    @PUT("playlists/{id}/follow")
    Call<UserIsFollowingDTO> followPlaylist(@Header("Authorization") String token, @Path("id") long playlistId);

    @GET("me/playlists")
    Call<List<PlaylistDTO>> getCurrentUserPlaylists(@Header("Authorization") String token);

    @GET("me/playlists/following")
    Call<List<PlaylistDTO>> getFollowingPlaylists(@Header("Authorization") String token);

    @GET("users/{login}/playlists")
    Call<List<PlaylistDTO>> getUserPlaylistsById(@Path("login") String login, @Header("Authorization") String token);
}
