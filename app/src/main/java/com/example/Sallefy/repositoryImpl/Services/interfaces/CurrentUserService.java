package com.example.Sallefy.repositoryImpl.Services.interfaces;

import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.UserDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.UserIsFollowingDTO;

import java.util.Collection;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CurrentUserService {
    @GET("me/followers")
    Call<Collection<UserDTO>> getUserFollowers(@Header("Authorization") String token);

    @GET("me/followings")
    Call<Collection<UserDTO>> getUserFollowing(@Header("Authorization") String token);

    @GET("me/playlists")
    Call<Collection<PlaylistDTO>> getUserOwnedPlaylists(@Header("Authorization") String token);

    @GET("me/playlists/following")
    Call<Collection<PlaylistDTO>> getUserFollowingPlaylists(@Header("Authorization") String token);

    @GET("me/playlists/{id}")
    Call<PlaylistDTO> getUserPlaylistById(
            @Header("Authorization") String token,
            @Path("id") long id
    );

    @GET("me/tracks")
    Call<Collection<TrackDTO>> getUserOwnTracks(@Header("Authorization") String token);

    @GET("me/tracks/liked")
    Call<Collection<TrackDTO>> getUserLikedTracks(@Header("Authorization") String token);

    @GET("me/playlists/{id}")
    Call<TrackDTO> getUserTrackById(
            @Header("Authorization") String token,
            @Path("id") long id
    );

    @GET("users/{login}/follow")
    Call<UserIsFollowingDTO> getUserIsFollowingUserByUsername(
            @Header("Authorization") String token,
            @Path("login") String login
    );

    @PUT("users/{login}/follow")
    Call<UserIsFollowingDTO> followUserByUsername(
            @Header("Authorization") String token,
            @Path("login") String login
    );
}
