package com.example.Sallefy.repositoryImpl.Services.interfaces;

import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.UserIsLikingTrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Outgoing.CoordsDTO;

import java.util.Collection;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TrackService {
    @GET("tracks")
    Call<Collection<TrackDTO>> getAllTracks(
            @Header("Authorization") String token,
            @Query("liked") boolean sortByLiked,
            @Query("played") boolean sortByPlayed,
            @Query("recent") boolean sortByRecent,
            @Query("size") int maxNumTracks);

    @GET("tracks/{id}")
    Call<TrackDTO> getTrackById(@Header("authorization") String token,
                                @Path("id") long id);

    @POST("tracks")
    Call<TrackDTO> createTrack(@Header("Authorization") String token,
                               @Body TrackDTO newTrack);
    @PUT("tracks")
    Call<TrackDTO> updateTrack(@Header("Authorization") String token,
                               @Body TrackDTO existingTrack);

    @DELETE("tracks/{id}")
    Call<ResponseBody> deleteTrack(@Header("authorization") String token,
                                   @Path("id") long id);

    @GET("genres/{id}/tracks")
    Call<Collection<TrackDTO>> getTracksByGenreId(@Header("authorization") String token,
                                                  @Path("id") long genreId);

    @PUT("tracks/{id}/play")
    Call<ResponseBody> registerTrackPlay(@Header("authorization") String token,
                                         @Path("id") long trackId,
                                         @Body CoordsDTO position);

    @PUT("tracks/{id}/like")
    Call<UserIsLikingTrackDTO> likeTrackById(@Header("authorization") String sessionToken, @Path("id") long trackId);

}
