package com.example.Sallefy.repositoryImpl.Services.interfaces;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

import com.example.Sallefy.repositories.DataTransferObjects.Incoming.LoginDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Outgoing.UserLoginDTO;


public interface UserTokenService {
    @POST("authenticate")
    Call<LoginDTO> loginUser(@Body UserLoginDTO login);
}
