package com.example.Sallefy.repositoryImpl.Services.factories;

public abstract class ServiceFactory<T> {
    public abstract T create();
}
