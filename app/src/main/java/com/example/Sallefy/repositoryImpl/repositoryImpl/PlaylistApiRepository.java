package com.example.Sallefy.repositoryImpl.repositoryImpl;

import androidx.lifecycle.MutableLiveData;

import com.example.Sallefy.Utils.RetrofitCallbackAdapter;
import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.PlaylistDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.UserIsFollowingDTO;
import com.example.Sallefy.repositories.Interfaces.PlaylistRepository;
import com.example.Sallefy.repositoryImpl.Services.factories.ServiceFactory;
import com.example.Sallefy.repositoryImpl.Services.interfaces.PlaylistService;

import java.util.Collection;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class PlaylistApiRepository extends SessionObjectRepository implements PlaylistRepository {
    private PlaylistService playlistService = null;
    private final ServiceFactory<PlaylistService> playlistServiceFactory;
    private MutableLiveData<Collection<PlaylistDTO>> allPlaylistCache = new MutableLiveData<>();
    private MutableLiveData<List<PlaylistDTO>> currentUserPlaylistsCache = new MutableLiveData<>();
    private MutableLiveData<PlaylistDTO> createdPlaylistCache = new MutableLiveData<>();
    private MutableLiveData<PlaylistDTO> retrievedPlaylistCache = new MutableLiveData<>();

    public PlaylistApiRepository(ServiceFactory<PlaylistService> serviceFactory) {
        this.playlistServiceFactory = serviceFactory;
    }

    private PlaylistService getPlaylistService() {
        if (null == this.playlistService) {
            this.playlistService = playlistServiceFactory.create();
        }

        return this.playlistService;
    }

    @Override
    public MutableLiveData<Collection<PlaylistDTO>> getAllPlaylists() {
        Call<Collection<PlaylistDTO>> call
                = this.getPlaylistService().getAllPlaylists(this.getSession().getTokenWithBearer());

        call.enqueue(new RetrofitCallbackAdapter<>(
                allPlaylistCache::postValue,
                null,
                Throwable::printStackTrace
        ));

        return allPlaylistCache;
    }

    @Override
    public MutableLiveData<PlaylistDTO> createPlaylist(PlaylistDTO playlist) {
        Call<PlaylistDTO> call
                = this.getPlaylistService().createPlaylist(this.getSession().getTokenWithBearer(), playlist);

        call.enqueue(new RetrofitCallbackAdapter<>(
                (response) -> {
                    response.setStatus(true);
                    response.setError("E_NO_ERROR");
                    createdPlaylistCache.postValue(response);
                },
                (e) -> createdPlaylistCache.postValue(new PlaylistDTO(false, e.getMessage())
                ),
                Throwable::printStackTrace
        ));

        return createdPlaylistCache;
    }

    @Override
    public MutableLiveData<BaseDTO> updatePlaylist(PlaylistDTO playlist) {
        Call<PlaylistDTO> call =
                this.getPlaylistService().updatePlaylist(this.getSession().getTokenWithBearer(), playlist);

        final MutableLiveData<BaseDTO> lastUpdatedTrackCache = new MutableLiveData<>();

        call.enqueue(new RetrofitCallbackAdapter<>(
                        (response) -> {
                            response.setStatus(true);
                            response.setError("E_NO_ERROR");
                            lastUpdatedTrackCache.postValue(response);
                        },
                        (throwable ->
                                lastUpdatedTrackCache.postValue(
                                        new BaseDTO(
                                                false,
                                                throwable.getMessage()
                                        )
                                )
                        ),
                        (throwable ->{
                                throwable.printStackTrace();
                                lastUpdatedTrackCache.postValue(
                                        new BaseDTO(false, throwable.getMessage())
                                );
                            }
                        )
                )
        );

        return lastUpdatedTrackCache;
    }

    @Override
    public MutableLiveData<BaseDTO> deletePlaylist(PlaylistDTO playlist) {
        Call<ResponseBody> call =
                this.getPlaylistService().deletePlaylist(this.getSession().getTokenWithBearer(), playlist.getId());

        final MutableLiveData<BaseDTO> deletedPlaylistCache = new MutableLiveData<>();

        call.enqueue(new RetrofitCallbackAdapter<>(
                responseBody ->
                        deletedPlaylistCache.postValue(
                            new BaseDTO(true, "E_NO_ERROR")
                        )
                ,
                        (throwable ->
                                deletedPlaylistCache.postValue(
                                        new BaseDTO(
                                                false,
                                                throwable.getMessage()
                                        )
                                )
                        ),
                        (throwable -> {
                            throwable.printStackTrace();
                            deletedPlaylistCache.postValue(
                                    new BaseDTO(false, throwable.getMessage())
                            );
                        }
                        )
                )
        );

        return deletedPlaylistCache;
    }

    @Override
    public MutableLiveData<PlaylistDTO> getPlaylistById(long id) {
        Call<PlaylistDTO> call
                = this.getPlaylistService().getPlaylistById(this.getSession().getTokenWithBearer(), id);

        call.enqueue(new RetrofitCallbackAdapter<>(
                (response) -> {
                    response.setStatus(true);
                    response.setError("E_NO_ERROR");
                    retrievedPlaylistCache.postValue(response);
                },
                (e) -> retrievedPlaylistCache.postValue(
                        new PlaylistDTO(
                                false,
                                e.getMessage()
                        )
                ),
                throwable -> {
                    throwable.printStackTrace();
                    retrievedPlaylistCache.postValue(
                            new PlaylistDTO(
                                    false,
                                    throwable.getMessage()
                            )
                    );
                }
                )
        );

        return retrievedPlaylistCache;
    }

    @Override
    public MutableLiveData<UserIsFollowingDTO> isUserFollowingPlaylist(long playlistId) {
        Call<UserIsFollowingDTO> call =
                this.getPlaylistService().isUserFollowing(this.getSession().getTokenWithBearer(), playlistId);

        final MutableLiveData<UserIsFollowingDTO> userIsFollowing = new MutableLiveData<>();

        call.enqueue(new RetrofitCallbackAdapter<>(
                response -> {
                    response.setError("E_NO_ERROR");
                    response.setStatus(true);
                    userIsFollowing.postValue(response);
                },
                (e) -> userIsFollowing.postValue(
                        new UserIsFollowingDTO(
                                false,
                                e.getMessage(),
                                false
                        )
                ),
                throwable -> {
                    throwable.printStackTrace();
                    userIsFollowing.postValue(
                            new UserIsFollowingDTO(
                                    false,
                                    throwable.getMessage(),
                                    false
                            )
                    );
                }
                )
        );

        return userIsFollowing;
    }

    @Override
    public MutableLiveData<UserIsFollowingDTO> followPlayListById(long playlistId) {
        Call<UserIsFollowingDTO> call =
                this.getPlaylistService().followPlaylist(this.getSession().getTokenWithBearer(), playlistId);

        final MutableLiveData<UserIsFollowingDTO> userIsFollowing = new MutableLiveData<>();

        call.enqueue(new RetrofitCallbackAdapter<>(
                        response -> {
                            response.setError("E_NO_ERROR");
                            response.setStatus(true);
                            userIsFollowing.postValue(response);
                        },
                        (e) -> userIsFollowing.postValue(
                                new UserIsFollowingDTO(
                                        false,
                                        e.getMessage(),
                                        false
                                )
                        ),
                        throwable -> {
                            throwable.printStackTrace();
                            userIsFollowing.postValue(
                                    new UserIsFollowingDTO(
                                            false,
                                            throwable.getMessage(),
                                            false
                                    )
                            );
                        }
                )
        );

        return userIsFollowing;
    }

    @Override
    public MutableLiveData<List<PlaylistDTO>> getCurrentUserPlaylists() {
        Call<List<PlaylistDTO>> call =
                this.getPlaylistService().getCurrentUserPlaylists(this.getSession().getToken());

        call.enqueue(new RetrofitCallbackAdapter<>(
                        (response) -> {
                            currentUserPlaylistsCache.postValue(response);
                        },
                        null,
                        Throwable::printStackTrace
                )
        );

        return currentUserPlaylistsCache;
    }
}
