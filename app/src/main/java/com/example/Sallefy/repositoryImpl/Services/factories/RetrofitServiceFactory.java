package com.example.Sallefy.repositoryImpl.Services.factories;

import com.example.Sallefy.Utils.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class RetrofitServiceFactory<T> extends ServiceFactory<T> {
    protected static Retrofit retrofit;

    public RetrofitServiceFactory() {
        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.NETWORK.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public abstract T create();
}
