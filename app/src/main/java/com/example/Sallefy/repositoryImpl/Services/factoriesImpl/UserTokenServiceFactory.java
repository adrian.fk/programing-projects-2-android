package com.example.Sallefy.repositoryImpl.Services.factoriesImpl;


import com.example.Sallefy.repositoryImpl.Services.factories.RetrofitServiceFactory;
import com.example.Sallefy.repositoryImpl.Services.interfaces.UserTokenService;

public class UserTokenServiceFactory extends RetrofitServiceFactory<UserTokenService> {
    public UserTokenService create() {
        return retrofit.create(UserTokenService.class);
    }
}
