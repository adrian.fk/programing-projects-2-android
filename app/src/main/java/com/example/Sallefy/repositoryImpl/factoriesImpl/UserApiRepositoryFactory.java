package com.example.Sallefy.repositoryImpl.factoriesImpl;

import com.example.Sallefy.repositories.Interfaces.BaseUserRepository;
import com.example.Sallefy.repositories.Interfaces.CurrentUserRepository;
import com.example.Sallefy.repositories.Interfaces.UserRepository;
import com.example.Sallefy.repositories.factories.UserRepositoryFactory;
import com.example.Sallefy.repositoryImpl.Services.factoriesImpl.UserTokenServiceFactory;
import com.example.Sallefy.repositoryImpl.repositoryImpl.UserApiRepository;
import com.example.Sallefy.repositoryImpl.Services.factoriesImpl.CurrentUserServiceFactory;
import com.example.Sallefy.repositoryImpl.Services.factoriesImpl.UserServiceFactory;

public class UserApiRepositoryFactory extends UserRepositoryFactory {
    public UserRepository createUserRepository() {
        return new UserApiRepository(
                new CurrentUserServiceFactory(),
                new UserServiceFactory(),
                new UserTokenServiceFactory()
        );
    }

    @Override
    public BaseUserRepository createBaseUserRepository() {
        return this.createUserRepository();
    }

    @Override
    public CurrentUserRepository createCurrentUserRepository() {
        return this.createUserRepository();
    }
}
