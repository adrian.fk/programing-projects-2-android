package com.example.Sallefy.repositoryImpl.factoriesImpl;

import com.example.Sallefy.repositories.Interfaces.ImageRepository;
import com.example.Sallefy.repositories.factories.ImageRepositoryFactory;
import com.example.Sallefy.repositoryImpl.repositoryImpl.ImageApiRepository;

public class ImageApiRepositoryFactory extends ImageRepositoryFactory {
    @Override
    public ImageRepository create() {
        return new ImageApiRepository();
    }
}
