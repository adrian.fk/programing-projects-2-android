package com.example.Sallefy.repositoryImpl.Services.factoriesImpl;


import com.example.Sallefy.repositoryImpl.Services.factories.RetrofitServiceFactory;
import com.example.Sallefy.repositoryImpl.Services.interfaces.PlaylistService;

public class PlaylistServiceFactory extends RetrofitServiceFactory<PlaylistService> {
    @Override
    public PlaylistService create() {
        return retrofit.create(PlaylistService.class);
    }
}
