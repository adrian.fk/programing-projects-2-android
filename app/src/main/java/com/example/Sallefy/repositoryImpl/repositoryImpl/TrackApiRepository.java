package com.example.Sallefy.repositoryImpl.repositoryImpl;

import android.net.Uri;
import android.os.Handler;

import androidx.lifecycle.MutableLiveData;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.example.Sallefy.Utils.RetrofitCallbackAdapter;
import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.example.Sallefy.repositories.DataTransferObjects.BiDirectional.TrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.UserIsLikingTrackDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Outgoing.CoordsDTO;
import com.example.Sallefy.repositories.Interfaces.TrackRepository;
import com.example.Sallefy.repositoryImpl.Services.factories.ServiceFactory;
import com.example.Sallefy.repositoryImpl.Services.interfaces.TrackService;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class TrackApiRepository extends SessionObjectRepository implements TrackRepository {
    private TrackService trackService = null;
    private ServiceFactory<TrackService> trackServiceFactory;
    private MutableLiveData<Collection<TrackDTO>> tracksByGenreCache = new MutableLiveData<>();
    private MutableLiveData<Collection<TrackDTO>> allTracksCache = new MutableLiveData<>();
    private MutableLiveData<TrackDTO> lastCreatedTrackCache = new MutableLiveData<>();
    private MutableLiveData<BaseDTO> lastUpdatedTrackCache = new MutableLiveData<>();
    private MutableLiveData<TrackDTO> lastRetrievedTrack = new MutableLiveData<>();
    private static Map<Long, TrackDTO> lastRetrievedTrackCacheMap = new ConcurrentHashMap<>();
    private final String cloudinaryBaseUrl = "https://res.cloudinary.com/la-salle-sallefy/video/upload";


    public TrackApiRepository(ServiceFactory<TrackService> trackServiceServiceFactory) {
        this.trackServiceFactory = trackServiceServiceFactory;
    }

    private TrackService getTrackService() {
        if (null == this.trackService) {
            this.trackService = trackServiceFactory.create();
        }

        return this.trackService;
    }

    @Override
    public MutableLiveData<Collection<TrackDTO>> getTrackByGenreId(long genreId) {

        Call<Collection<TrackDTO>> call =
                this.getTrackService().getTracksByGenreId(this.getSession().getTokenWithBearer(), genreId);

        call.enqueue(new RetrofitCallbackAdapter<>(
                tracksByGenreCache::postValue,
                        null,
                        null
                )
        );

        return tracksByGenreCache;
    }

    @Override
    public MutableLiveData<Collection<TrackDTO>> getAllTracks(boolean sortByLiked,
                                                             boolean sortByPlayed,
                                                             boolean sortByRecent,
                                                             int maxNumTracks) {
        Call<Collection<TrackDTO>> call =
                this.getTrackService().getAllTracks(
                        this.getSession().getTokenWithBearer(),
                        sortByLiked,
                        sortByPlayed,
                        sortByRecent,
                        maxNumTracks);

        call.enqueue(new RetrofitCallbackAdapter<>(
                        allTracksCache::postValue,
                        null,
                        null
                )
        );

        return allTracksCache;
    }

    @Override
    public MutableLiveData<TrackDTO> createTrack(TrackDTO track, Uri path) {


        final String sessionToken = this.getSession().getToken();

        MediaManager.get().upload(path)
                .unsigned("preset1")
                .callback(new UploadCallback() {
                    @Override
                    public void onStart(String requestId) {
                        lastCreatedTrackCache.postValue(new TrackDTO(false, "E_NO_ERROR: upload start"));
                    }

                    @Override
                    public void onProgress(String requestId, long bytes, long totalBytes) {
                    }

                    @Override
                    public void onSuccess(String requestId, Map resultData) {
                        track.setUrl((String) resultData.get("url"));

                        Call<TrackDTO> call =
                                getTrackService().createTrack(sessionToken, track);

                        call.enqueue(new RetrofitCallbackAdapter<>(
                                        (res) -> {
                                            res.setStatus(true);
                                            res.setError("E_NO_ERROR");
                                            lastCreatedTrackCache.postValue(res);
                                        },
                                        (throwable ->
                                                lastCreatedTrackCache.postValue(
                                                        new TrackDTO(
                                                                false,
                                                                throwable.getMessage()
                                                        )
                                                )
                                        ),
                                        (throwable ->
                                                lastCreatedTrackCache.postValue(
                                                        new TrackDTO(
                                                                false,
                                                                throwable.getMessage()
                                                        )
                                                )
                                        )
                                )
                        );
                    }
                    @Override
                    public void onError(String requestId, ErrorInfo error) {
                        lastCreatedTrackCache.postValue(new TrackDTO(
                                false,
                                error.getDescription()
                        ));
                    }

                    @Override
                    public void onReschedule(String requestId, ErrorInfo error) {

                    }
                })
                .option("resource_type", "video")
                .dispatch();

        return lastCreatedTrackCache;
    }

    @Override
    public MutableLiveData<BaseDTO> updateTrack(TrackDTO track) {
        Call<TrackDTO> call =
                this.getTrackService().updateTrack(this.getSession().getTokenWithBearer(), track);

        call.enqueue(new RetrofitCallbackAdapter<>(
                (response) -> {
                    response.setStatus(true);
                    response.setError("E_NO_ERROR");
                    lastUpdatedTrackCache.postValue(response);
                },
                        (throwable ->
                                lastUpdatedTrackCache.postValue(
                                        new BaseDTO(
                                                false,
                                                throwable.getMessage()
                                        )
                                )
                        ),
                        (throwable ->
                                lastUpdatedTrackCache.postValue(
                                        new BaseDTO(
                                                false,
                                                throwable.getMessage()
                                        )
                                )
                        )
                )
        );

        return lastUpdatedTrackCache;
    }

    private void cleanUpRetrievedTrackCache(Date validTimeIntervalStartpoint) {
        for (Long key : lastRetrievedTrackCacheMap.keySet()) {
            if (!Timestamper.isValidTimeInterval(
                    validTimeIntervalStartpoint,
                    Objects.requireNonNull(lastRetrievedTrackCacheMap.get(key)).getTimestampFetched())) {

                lastRetrievedTrackCacheMap.remove(key);
            }
        }
    }

    @Override
    public MutableLiveData<TrackDTO> getTrackById(long id) {
        Handler handler = new Handler();
        if(lastRetrievedTrackCacheMap.containsKey(id)) {
            TrackDTO cachedTrackDTO = lastRetrievedTrackCacheMap.get(id);

            Date validTimestampStartPoint = Timestamper.getValidTime(Timestamper.MONTH, -1);

            if (cachedTrackDTO != null &&
                    Timestamper.isValidTimeInterval(validTimestampStartPoint, cachedTrackDTO.getTimestampFetched())) {

                handler.postDelayed(() -> this.lastRetrievedTrack.postValue(cachedTrackDTO), 100);
            }
            else {
                if (lastRetrievedTrackCacheMap.keySet().size() > 0) {
                    handler.post(() -> cleanUpRetrievedTrackCache(validTimestampStartPoint));
                }
                fetchUserById(id);
            }
        }
        else {
            fetchUserById(id);
        }

        return this.lastRetrievedTrack;
    }

    private void fetchUserById(long id) {
        Call<TrackDTO> call =
                this.getTrackService().getTrackById(this.getSession().getTokenWithBearer(), id);

        call.enqueue(new RetrofitCallbackAdapter<>(
                        (response) -> {
                            response.setStatus(true);
                            response.setError("E_NO_ERROR");
                            response.stamp();
                            lastRetrievedTrackCacheMap.put(id, response);

                            lastRetrievedTrack.postValue(response);
                        },
                        (throwable ->
                                lastRetrievedTrack.postValue(
                                        new TrackDTO(
                                                false,
                                                throwable.getMessage()
                                        )
                                )
                        ),
                        (throwable ->
                                lastRetrievedTrack.postValue(
                                        new TrackDTO(
                                                false,
                                                throwable.getMessage()
                                        )
                                )
                        )
                )
        );
    }

    @Override
    public MutableLiveData<BaseDTO> deleteTrack(long id) {

        Call<ResponseBody> deleteTrackCall =
                this.getTrackService().deleteTrack(this.getSession().getTokenWithBearer(), id);

        final MutableLiveData<BaseDTO> deleteResponseCache = new MutableLiveData<BaseDTO>();

        deleteTrackCall.enqueue(new RetrofitCallbackAdapter<>(
                        (res) -> deleteResponseCache.postValue(
                                new BaseDTO(
                                        true,
                                        "E_NO_ERROR")
                        ),
                        (throwable ->
                                deleteResponseCache.postValue(
                                        new BaseDTO(
                                                false,
                                                throwable.getMessage()
                                        )
                                )
                        ),
                        (throwable ->
                                deleteResponseCache.postValue(
                                        new BaseDTO(
                                                false,
                                                throwable.getMessage()
                                        )
                                )
                        )
                )
        );

        return deleteResponseCache;
    }

    @Override
    public MutableLiveData<BaseDTO> playTrack(long trackId, CoordsDTO coords) {
        Call<ResponseBody> call =
                this.getTrackService().registerTrackPlay(this.getSession().getTokenWithBearer(), trackId, coords);

        final MutableLiveData<BaseDTO> playResponseCache = new MutableLiveData<>();

        call.enqueue(new RetrofitCallbackAdapter<>(
                        (response) -> playResponseCache.postValue(new BaseDTO(true, "E_NO_ERROR")),
                        (throwable ->
                                playResponseCache.postValue(
                                        new BaseDTO(
                                                false,
                                                throwable.getMessage()
                                        )
                                )
                        ),
                        (throwable ->
                                playResponseCache.postValue(
                                        new BaseDTO(
                                                false,
                                                throwable.getMessage()
                                        )
                                )
                        )
                )
        );

        return playResponseCache;
    }

    @Override
    public MutableLiveData<UserIsLikingTrackDTO> likeTrackById(long trackId) {
        Call<UserIsLikingTrackDTO> call =
                this.getTrackService().likeTrackById(this.getSession().getTokenWithBearer(), trackId);

        final MutableLiveData<UserIsLikingTrackDTO> likeResponseCache = new MutableLiveData<>();

        call.enqueue(new RetrofitCallbackAdapter<>(
                        response -> {
                            response.setError("E_NO_ERROR");
                            response.setStatus(true);
                            likeResponseCache.postValue(response);
                        },
                        throwable ->
                                likeResponseCache.postValue(
                                        new UserIsLikingTrackDTO(
                                                false,
                                                throwable.getMessage()
                                        )
                                ),
                        throwable ->
                                likeResponseCache.postValue(
                                        new UserIsLikingTrackDTO(
                                                false,
                                                throwable.getMessage()
                                        )
                                )
                )
        );

        return likeResponseCache;
    }
}
