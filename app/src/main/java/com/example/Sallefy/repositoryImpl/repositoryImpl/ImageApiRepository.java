package com.example.Sallefy.repositoryImpl.repositoryImpl;

import android.net.Uri;

import androidx.lifecycle.MutableLiveData;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.example.Sallefy.repositories.DataTransferObjects.Incoming.ImageUploadDTO;
import com.example.Sallefy.repositories.Interfaces.ImageRepository;

import java.util.Map;

public class ImageApiRepository implements ImageRepository {
    private final MutableLiveData<ImageUploadDTO> imageUrlCache;

    public ImageApiRepository() {
        this.imageUrlCache = new MutableLiveData<>();
    }

    @Override
    public MutableLiveData<ImageUploadDTO> uploadImage(Uri imageUri) {
        MediaManager.get().upload(imageUri)
                .unsigned("preset1")
                .callback(new UploadCallback() {
                    @Override
                    public void onStart(String requestId) {

                    }

                    @Override
                    public void onProgress(String requestId, long bytes, long totalBytes) {

                    }

                    @Override
                    public void onSuccess(String requestId, Map resultData) {
                        ImageUploadDTO res = new ImageUploadDTO();
                        res.setStatus(true);
                        res.setError("E_NO_ERROR");
                        res.setImageUrl((String) resultData.get("url"));
                        imageUrlCache.postValue(res);
                    }

                    @Override
                    public void onError(String requestId, ErrorInfo error) {
                        imageUrlCache.postValue(
                                new ImageUploadDTO(false, error.getDescription())
                        );
                    }

                    @Override
                    public void onReschedule(String requestId, ErrorInfo error) {

                    }
                })
                .option("resource_type", "image")
                .dispatch();

        return this.imageUrlCache;
    }
}
