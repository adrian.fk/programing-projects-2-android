package com.example.Sallefy.repositoryImpl.repositoryImpl.authentication;

public interface SessionFactory {
    static Session create() {
        return new Session();
    }
}
