package com.example.Sallefy.repositoryImpl.repositoryImpl;

import com.example.Sallefy.repositoryImpl.repositoryImpl.authentication.Session;
import com.example.Sallefy.repositoryImpl.repositoryImpl.authentication.SessionFactory;

public interface SessionBearer {
    Session sessionObj = SessionFactory.create();

    static Session getSession() throws SessionNotOpenException {
        if(!sessionObj.isConfigured()) {
            throw new SessionNotOpenException();
        }
        else {
            return sessionObj;
        }
    }

    static void createSession(String token, String login) {
        sessionObj.setToken(token);
        sessionObj.setUserLogin(login);
    }

    static String getUsernameIfThere() {
        return sessionObj.getUserLogin();
    }
}
