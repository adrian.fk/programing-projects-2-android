package com.example.Sallefy.repositoryImpl.Services.factoriesImpl;

import com.example.Sallefy.repositoryImpl.Services.factories.RetrofitServiceFactory;
import com.example.Sallefy.repositoryImpl.Services.interfaces.UserService;

public class UserServiceFactory extends RetrofitServiceFactory<UserService> {
    @Override
    public UserService create() {
        return retrofit.create(UserService.class);
    }
}
