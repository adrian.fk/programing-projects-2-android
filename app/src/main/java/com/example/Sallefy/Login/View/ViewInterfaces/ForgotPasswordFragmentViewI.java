package com.example.Sallefy.Login.View.ViewInterfaces;

import com.example.Sallefy.Login.Controller.ListenerInterfaces.LoginForgotPassListener;
import com.example.Sallefy.Utils.ViewInterface;

public interface ForgotPasswordFragmentViewI extends ViewInterface {
    void attachBackButtonListener(LoginForgotPassListener listener);
    void attachConfirmButtonListener(LoginForgotPassListener listener);
    String getFormContent();
    void displayNoConnection();
    void displayButtonLock();
    void displayButtonUnlock();
    void pivotView();
    void displayWrongInput();
}
