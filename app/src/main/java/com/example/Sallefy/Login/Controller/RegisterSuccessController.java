package com.example.Sallefy.Login.Controller;

import com.example.Sallefy.Login.Model.DataLogin;
import com.example.Sallefy.Login.View.ViewInterfaces.RegisterSuccessFragViewI;
import com.example.Sallefy.Utils.Constants;

public class RegisterSuccessController implements RegisterSuccessControllerI {
    private RegisterSuccessFragViewI view;
    private DataLogin model;

    public RegisterSuccessController(DataLogin model, RegisterSuccessFragViewI view) {
        this.view = view;
        this.model = model;

        attachListeners();
    }

    private void attachListeners() {
        this.view.attachLoginButtonListener(l -> loginButtonClick());
    }

    private void loginButtonClick() {
        this.model.setState(Constants.LOGIN_STATE.login);
    }
}
