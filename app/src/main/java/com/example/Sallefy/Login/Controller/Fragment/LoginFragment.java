package com.example.Sallefy.Login.Controller.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;

import com.example.Sallefy.Login.Controller.LoginController;
import com.example.Sallefy.Login.Controller.LoginMediator;
import com.example.Sallefy.Login.Model.DataLogin;
import com.example.Sallefy.Login.View.LoginFragmentView;
import com.example.Sallefy.Login.View.ViewInterfaces.LoginFragmentViewI;
import com.example.Sallefy.repositoryImpl.factoriesImpl.UserApiRepositoryFactory;


public class LoginFragment extends AbstractLoginFragment implements LoginMediator {

    private LoginFragmentViewI view;
    private LoginController controller;

    public LoginFragment(DataLogin dataLogin) {
        super(dataLogin);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = new LoginFragmentView(inflater, container);

        UserApiRepositoryFactory userApiRepositoryFactory = new UserApiRepositoryFactory();
        this.controller = new LoginController(this, this.dataLogin, this.view, userApiRepositoryFactory);

        if(this.dataLogin.getUserID() != null && this.dataLogin.getUserID().length() > 0) {
            this.view.setUsernameText(this.dataLogin.getUserID());
        }

        return this.view.getRootView();
    }

    @Override
    public LifecycleOwner provideOwner() {
        return this;
    }
}
