package com.example.Sallefy.Login.Controller.FragmentNavigation;

//Tinkering with own implementation of NavController:

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.HashMap;
import java.util.Map;

public class NavigationController implements NavigationControllerI {
    //RESERVED IDs
    public static final String BACK = "back";




    private FragmentManager fragmentManager;
    private ContainerSpecifier containerSpecifier;
    private Map<String, FragmentFactoryInterface> fragmentFactoryMap;
    private Map<String, Boolean> includeInBackstackMap;
    private Map<String, FragmentTransactionAnimationSpecifier> fragmentTransAnimSpecifierMap;
    private FragmentTransactionAnimationSpecifier defaultFragmentTransactionAnimationSpecifier;

    private FragmentTransaction fmTransaction;
    private Boolean defaultAnimSpecified;


    public NavigationController(FragmentManager fragmentManager,
                                ContainerSpecifier containerSpecifier) {
        this.fragmentManager = fragmentManager;
        this.containerSpecifier = containerSpecifier;
        this.fragmentFactoryMap = new HashMap<>();
        this.includeInBackstackMap = new HashMap<>();
        this.defaultAnimSpecified = false;
        this.fragmentTransAnimSpecifierMap = new HashMap<>();
    }

    public NavigationController(FragmentManager fragmentManager,
                                ContainerSpecifier containerSpecifier,
                                FragmentTransactionAnimationSpecifier fragmentTransactionAnimationSpecifier) {
        this.fragmentManager = fragmentManager;
        this.containerSpecifier = containerSpecifier;
        this.fragmentFactoryMap = new HashMap<>();
        this.includeInBackstackMap = new HashMap<>();
        this.defaultFragmentTransactionAnimationSpecifier = fragmentTransactionAnimationSpecifier;
        this.defaultAnimSpecified = true;
        this.fragmentTransAnimSpecifierMap = new HashMap<>();
    }

    @Override
    public NavigationController add(String id, FragmentFactoryInterface fragmentFactory) {
        this.fragmentFactoryMap.put(id, fragmentFactory);
        this.includeInBackstackMap.put(id, true);
        return this;
    }

    @Override
    public NavigationController add(String id, FragmentFactoryInterface fragmentFactory, Boolean includeInBackstack) {
        this.fragmentFactoryMap.put(id, fragmentFactory);
        this.includeInBackstackMap.put(id, includeInBackstack);
        return this;
    }

    @Override
    public NavigationController add(String id,
                                    FragmentFactoryInterface fragmentFactory,
                                    FragmentTransactionAnimationSpecifier fragmentTransactionAnimationSpecifier,
                                    Boolean includeInBackstack) {
        this.fragmentFactoryMap.put(id, fragmentFactory);
        this.fragmentTransAnimSpecifierMap.put(id, fragmentTransactionAnimationSpecifier);
        this.includeInBackstackMap.put(id, includeInBackstack);
        return this;
    }

    @Override
    public NavigationController add(String id,
                                    FragmentFactoryInterface fragmentFactory,
                                    FragmentTransactionAnimationSpecifier fragmentTransactionAnimationSpecifier) {
        this.fragmentFactoryMap.put(id, fragmentFactory);
        this.fragmentTransAnimSpecifierMap.put(id, fragmentTransactionAnimationSpecifier);
        this.includeInBackstackMap.put(id, true);
        return this;
    }

    @Override
    public void navigate(String id, Bundle args) {
        initTrans();
        attachAnimationSpecification(id);

        if(reservedId(id)) {
            executeReservedFunctionality(id);
        }
        else {
            if(this.fragmentFactoryMap.get(id) != null) {
                trans(
                        id,
                        this.fragmentFactoryMap.get(id).create(args)
                );
            }
        }
    }

    @Override
    public void navigate(String id) {
        this.navigate(id, new Bundle());
    }

    private void attachAnimationSpecification(String id) {
        if(fragmentTransAnimSpecifierMap.get(id) != null) {
            fragmentTransAnimSpecifierMap.get(id)
                    .specifyFragmentTransactionAnimation(this.fmTransaction);
        }
        else if(this.defaultAnimSpecified) {
            this.defaultFragmentTransactionAnimationSpecifier
                    .specifyFragmentTransactionAnimation(this.fmTransaction);
        }
    }

    private void initTrans() {
        this.fmTransaction = this.fragmentManager.beginTransaction();
    }

    private void trans(String id, Fragment dest) {
        this.containerSpecifier.specifyContainerReplacementSentence(this.fmTransaction, dest);
        if(this.includeInBackstackMap.get(id)) {
            this.fmTransaction.addToBackStack(null);
        }
        this.fmTransaction.commit();
    }

    private void executeReservedFunctionality(String id) {
        switch(id) {
            case BACK:
                handleBackTrans();

        }
    }

    private void handleBackTrans() {
        this.fragmentManager.popBackStack();
    }

    private boolean reservedId(String id) {
        switch(id) {
            case BACK:
                return true;

            default:
                return false;
        }
    }

    @Override
    public NavigationController setGeneralTransactionAnim(FragmentTransaction transactionAnim) {
        this.fmTransaction = transactionAnim;
        return this;
    }
}
