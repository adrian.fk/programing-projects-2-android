package com.example.Sallefy.Login.Controller;

import com.example.Sallefy.Login.Model.DataLogin;
import com.example.Sallefy.Login.View.ViewInterfaces.RegisterFragmentViewI;
import com.example.Sallefy.Utils.Constants;
import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.example.Sallefy.repositories.DataTransferObjects.Outgoing.UserRegister;
import com.example.Sallefy.repositories.Interfaces.BaseUserRepository;
import com.example.Sallefy.repositories.Interfaces.UserTokenRepository;
import com.example.Sallefy.repositories.factories.UserRepositoryFactory;

public class RegisterController {
    private RegisterMediator mediator;
    private DataLogin model;
    private RegisterFragmentViewI view;
    private BaseUserRepository baseUserRepository;
    private UserTokenRepository userTokenRepository;

    public RegisterController(RegisterMediator mediator,
                              DataLogin model,
                              RegisterFragmentViewI view,
                              UserRepositoryFactory userRepositoryFactory) {
        this.mediator = mediator;
        this.model = model;
        this.view = view;
        this.baseUserRepository = userRepositoryFactory.createBaseUserRepository();
        this.userTokenRepository = userRepositoryFactory.createUserRepository();
        attachListeners();
    }

    private void attachListeners() {
        this.view.attachBackButtonListener(l -> backButtonClick());
        this.view.attachSignUpButtonListener(l -> signupButtonClick());
    }


    private void backButtonClick() {
        model.setState(Constants.LOGIN_STATE.login);
    }

    private void signupButtonClick() {
        if(validUserCredentials()) {
            this.userTokenRepository
                    .login("temp",
                            "temp"
                    )
                    .observe(this.mediator.provideOwner(), this::processTmpUsrLogin);
        }
    }

    private void processTmpUsrLogin(BaseDTO baseDTO) {
        if (baseDTO.isStatus()) {
            registerUser();
        }
        else {
            onNoConnection(
                    new Throwable(baseDTO.getError())
            );
        }
    }

    private void registerUser() {
        this.baseUserRepository
                .createUser(
                        new UserRegister(
                                this.view.getEmail(),
                                this.view.getFirstNameForm(),
                                this.view.getLastNameForm(),
                                this.view.getUsername(),
                                this.view.getPasswordOne()
                        )
                )
                .observe(this.mediator.provideOwner(), this::processRegisterResponse);
    }

    private void processRegisterResponse(BaseDTO baseDTO) {
        if (baseDTO.isStatus()) {
            onRegisterSuccess();
        }
        else {
            switch (baseDTO.getError()) {
                case "E_REGISTER_FAILURE":
                    onRegisterFailure(
                            new Throwable(baseDTO.getError())
                    );
                    break;

                case "E_NO_CONNECTION":
                    onNoConnection(
                            new Throwable(baseDTO.getError())
                    );
                    break;
            }
        }
    }

    private boolean validUserCredentials() {
        if(passwordCredCheck()) {
            return true;
        }
        this.view.displayPasswordMismatch();
        return false;
    }

    private boolean passwordCredCheck() {
        return this.view.getPasswordOne().length() >= 4
                && this.view.getPasswordOne().equals(this.view.getPasswordTwo());
    }

    private void onRegisterSuccess() {
        this.model.setUserID(this.view.getUsername());
        this.mediator.onRegistrationSuccess();
    }

    private void onRegisterFailure(Throwable throwable) {
        this.view.displayUsernameExists();
    }

    private void onNoConnection(Throwable throwable) {
        this.view.displayNoConnection();
    }
}
