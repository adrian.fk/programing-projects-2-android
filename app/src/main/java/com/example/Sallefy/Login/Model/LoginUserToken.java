package com.example.Sallefy.Login.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginUserToken {
    @SerializedName("id_token")
    @Expose
    private String idToken;

    public LoginUserToken(String idToken) {
        this.idToken = idToken;
    }

    @Override
    public String toString() {
        return "LoginUserToken{" +
                "idToken='" + idToken + '\'' +
                '}';
    }

    public String getIdToken() {
        return idToken;
    }
}
