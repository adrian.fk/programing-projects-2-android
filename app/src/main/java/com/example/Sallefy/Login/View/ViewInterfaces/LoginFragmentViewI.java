package com.example.Sallefy.Login.View.ViewInterfaces;

import com.example.Sallefy.Login.Controller.ListenerInterfaces.LoginListenerI;
import com.example.Sallefy.Utils.ViewInterface;

public interface LoginFragmentViewI extends ViewInterface {
    void attachLoginButtonListener(LoginListenerI listener);
    void attachRegisterButtonListener(LoginListenerI listener);
    void attachForgotPassListener(LoginListenerI listener);
    void displayWrongLogin();
    void displayNoConnection();
    void displayLoginLock();
    void displayLoginUnlock();
    void setUsernameText(String username);
    String getUsernameString();
    String getPasswordString();
}
