package com.example.Sallefy.Login.View;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.Sallefy.Login.Controller.ListenerInterfaces.LoginListenerI;
import com.example.Sallefy.Login.View.ViewInterfaces.LoginFragmentViewI;
import com.example.Sallefy.R;

public class LoginFragmentView implements LoginFragmentViewI, TextWatcher {
    private View rootView;

    private EditText usernameText;
    private EditText passwordText;

    private Button loginButton;
    private ImageButton registerButton;

    private TextView forgotPasswordPrompt;


    public LoginFragmentView(LayoutInflater inflater, ViewGroup container) {

        this.rootView = inflater.inflate(R.layout.login_fragment_login, container, false);
        findViewElements();
        processButtonStatus();
        usernameText.addTextChangedListener(this);
        passwordText.addTextChangedListener(this);
    }

    private void processButtonStatus() {
        if("".equals(usernameText.getText().toString())
                || "".equals(passwordText.getText().toString())) {
            loginButton.setClickable(false);
            loginButton.setAlpha(.2f);
            loginButton.setBackgroundResource(R.drawable.rounded_button_untoggled);
        }
        else {
            loginButton.setAlpha(0.9f);
            loginButton.setBackgroundResource(R.drawable.rounded_button);
            loginButton.setClickable(true);
        }
    }

    private void findViewElements() {
        usernameText = this.rootView.findViewById(R.id.login_form_username);
        passwordText = this.rootView.findViewById(R.id.login_form_password);
        loginButton = this.rootView.findViewById(R.id.buttonLogin);
        registerButton = this.rootView.findViewById(R.id.buttonRegister);
        forgotPasswordPrompt = this.rootView.findViewById(R.id.login_login_forgot_password);
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }

    public String getUsernameString() {
        return usernameText.getText().toString();
    }

    public void setUsernameText(String username) {
        this.usernameText.setText(username);
    }

    public String getPasswordString() {
        return passwordText.getText().toString();
    }


    @Override
    public void attachLoginButtonListener(LoginListenerI listener) {
        this.loginButton.setOnClickListener(listener);
    }

    @Override
    public void attachRegisterButtonListener(LoginListenerI listener) {
        this.registerButton.setOnClickListener(listener);
    }

    @Override
    public void attachForgotPassListener(LoginListenerI listener) {
        this.forgotPasswordPrompt.setOnClickListener(listener);
    }

    @Override
    public void displayWrongLogin() {
        Animation shake = AnimationUtils.loadAnimation(this.rootView.getContext(), R.anim.shake_edittext);
        this.passwordText.startAnimation(shake);
        this.passwordText.setText("");
        this.passwordText.setBackgroundResource(R.drawable.rounded_edittext_red_border);
    }

    @Override
    public void displayNoConnection() {
        Animation shake = AnimationUtils.loadAnimation(this.rootView.getContext(), R.anim.shake_edittext);
        this.passwordText.startAnimation(shake);
        this.passwordText.setText("");
        this.passwordText.setHint(R.string.login_no_connection);
        this.passwordText.setBackgroundResource(R.drawable.rounded_edittext_green_border);
    }

    @Override
    public void displayLoginLock() {
        this.usernameText.setClickable(false);
        this.passwordText.setClickable(false);

        this.loginButton.setAlpha(0.5f);
        this.loginButton.setClickable(false);
    }

    @Override
    public void displayLoginUnlock() {
        this.usernameText.setClickable(true);
        this.passwordText.setClickable(true);
        processButtonStatus();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        processButtonStatus();
        this.passwordText.setBackgroundResource(R.drawable.rounded_edittext);
    }
}
