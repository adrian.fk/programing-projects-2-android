package com.example.Sallefy.Login.Controller.Fragment.Factories;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import com.example.Sallefy.Login.Controller.Fragment.LoginFragment;
import com.example.Sallefy.Login.Controller.FragmentNavigation.FragmentFactoryInterface;
import com.example.Sallefy.Login.Model.DataLogin;

public class LoginFragmentFactory implements FragmentFactoryInterface {
    private DataLogin model;

    public LoginFragmentFactory(DataLogin model) {
        this.model = model;
    }

    @Override
    public Fragment create(Bundle args) {
        return new LoginFragment(model);
    }
}
