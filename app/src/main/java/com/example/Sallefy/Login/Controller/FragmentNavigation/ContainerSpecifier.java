package com.example.Sallefy.Login.Controller.FragmentNavigation;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public interface ContainerSpecifier {
    void specifyContainerReplacementSentence(FragmentTransaction transaction, Fragment dest);
}
