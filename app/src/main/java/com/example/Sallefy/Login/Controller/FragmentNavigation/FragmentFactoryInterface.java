package com.example.Sallefy.Login.Controller.FragmentNavigation;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

public interface FragmentFactoryInterface {
    Fragment create(Bundle args);
}
