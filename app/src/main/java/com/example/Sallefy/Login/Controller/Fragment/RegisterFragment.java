package com.example.Sallefy.Login.Controller.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;

import com.example.Sallefy.Login.Controller.RegisterController;
import com.example.Sallefy.Login.Controller.RegisterMediator;
import com.example.Sallefy.Login.Model.DataLogin;
import com.example.Sallefy.Login.View.ViewInterfaces.RegisterFragmentViewI;
import com.example.Sallefy.Login.View.RegisterFragmentView;
import com.example.Sallefy.Utils.Constants;
import com.example.Sallefy.repositoryImpl.factoriesImpl.UserApiRepositoryFactory;

public class RegisterFragment extends AbstractLoginFragment implements RegisterMediator {

    private RegisterFragmentViewI view;
    private RegisterController controller;

    public RegisterFragment(DataLogin dataLogin) {
        super(dataLogin);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = new RegisterFragmentView(inflater, container);

        UserApiRepositoryFactory userApiRepositoryFactory = new UserApiRepositoryFactory();

        this.controller = new RegisterController(
                this,
                this.dataLogin,
                this.view,
                userApiRepositoryFactory
        );

        return this.view.getRootView();
    }

    @Override
    public LifecycleOwner provideOwner() {
        return this;
    }

    @Override
    public void onRegistrationSuccess() {
        this.dataLogin.setState(Constants.LOGIN_STATE.registerSuccess);
    }
}
