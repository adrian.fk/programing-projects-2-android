package com.example.Sallefy.Login.Controller.FragmentNavigation;

import android.os.Bundle;

import androidx.fragment.app.FragmentTransaction;

public interface NavigationControllerI {


    NavigationController add(String id, FragmentFactoryInterface fragmentFactoryMethod);

    NavigationController add(String id, FragmentFactoryInterface fragmentFactoryMethod, Boolean includeInBackstack);

    NavigationController add(String id,
                                    FragmentFactoryInterface fragmentFactory,
                                    FragmentTransactionAnimationSpecifier fragmentTransactionAnimationSpecifier,
                                    Boolean includeInBackstack);

    NavigationController add(String id,
                             FragmentFactoryInterface fragmentFactory,
                             FragmentTransactionAnimationSpecifier fragmentTransactionAnimationSpecifier);

    void navigate(String id);

    void navigate(String id, Bundle args);

    NavigationController setGeneralTransactionAnim(FragmentTransaction transactionAnim);
}
