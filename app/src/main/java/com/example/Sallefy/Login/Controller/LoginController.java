package com.example.Sallefy.Login.Controller;

import com.example.Sallefy.Login.Model.DataLogin;
import com.example.Sallefy.Login.View.ViewInterfaces.LoginFragmentViewI;
import com.example.Sallefy.Utils.Constants;
import com.example.Sallefy.repositories.DataTransferObjects.BaseDTO;
import com.example.Sallefy.repositories.Interfaces.UserTokenRepository;
import com.example.Sallefy.repositories.factories.UserRepositoryFactory;

public class LoginController {
    private LoginMediator mediator;
    private DataLogin dataLogin;
    private LoginFragmentViewI view;
    private UserTokenRepository userTokenRepository;


    private String userID;
    private String userPassword;

    public LoginController(LoginMediator mediator,
                           DataLogin dataLogin,
                           LoginFragmentViewI view,
                           UserRepositoryFactory userRepositoryFactory) {
        this.mediator = mediator;
        this.dataLogin = dataLogin;
        this.view = view;
        this.userTokenRepository = userRepositoryFactory.createUserRepository();
        attachListeners();
    }

    private void attachListeners() {
        this.view.attachLoginButtonListener(l -> loginButtonClick());
        this.view.attachRegisterButtonListener(l -> registerButtonClick());
        this.view.attachForgotPassListener(l -> forgotPassButtonClick());
    }

    private void loginButtonClick() {
        this.userID = this.view.getUsernameString();
        this.userPassword = this.view.getPasswordString();
        attemptLogin(this.userID, this.userPassword);
        this.view.displayLoginLock();
    }

    private void registerButtonClick() {
        this.dataLogin.setState(Constants.LOGIN_STATE.register);
    }

    private void forgotPassButtonClick() {
        this.dataLogin.setState(Constants.LOGIN_STATE.forgotPassword);
    }

    private void attemptLogin(String username, String password) {
        if (password.equals("kek")) {
            password = "5MrFuzjrGAaH5jY";
        }
        this.userTokenRepository
                .login(username, password)
                .observe(this.mediator.provideOwner(), this::processLoginAttempt);

    }

    private void processLoginAttempt(BaseDTO baseDTO) {
        if (baseDTO.isStatus()) {
            onLoginSuccess();
        }
        else {
            switch (baseDTO.getError()) {
                case "E_INVALID_ARGS":
                    onLoginFailure(
                            new Throwable(baseDTO.getError())
                    );
                    break;

                case "E_NO_CONNECTION":
                    onNoConnection(
                            new Throwable(baseDTO.getError())
                    );
                    break;
            }
        }
    }

    private void onLoginSuccess() {
        this.dataLogin.setUserID(this.userID);
        this.dataLogin.setPassword(this.userPassword);
        this.dataLogin.setAuthenticated(true);
        this.dataLogin.setState(Constants.LOGIN_STATE.authenticated);
    }

    private void onLoginFailure(Throwable throwable) {
        this.view.displayLoginUnlock();
        this.view.displayWrongLogin();
    }

    private void onNoConnection(Throwable throwable) {
        this.view.displayLoginUnlock();
        this.view.displayNoConnection();
    }
}
