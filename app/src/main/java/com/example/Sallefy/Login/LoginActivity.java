package com.example.Sallefy.Login;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.Sallefy.Login.Controller.FragmentNavigation.NavigationController;
import com.example.Sallefy.Login.Controller.FragmentNavigation.NavigationControllerI;
import com.example.Sallefy.Login.Controller.Fragment.Factories.LoginFragmentFactory;
import com.example.Sallefy.Login.Controller.Fragment.Factories.RegisterFragmentFactory;
import com.example.Sallefy.Login.Controller.Fragment.Factories.RegisterSuccessFragmentFactory;
import com.example.Sallefy.Login.Model.DataLogin;
import com.example.Sallefy.R;
import com.example.Sallefy.SallefyUI.SallefyAppActivity;
import com.example.Sallefy.Utils.Constants;

public class LoginActivity extends AppCompatActivity {
    private DataLogin dataLogin;
    private NavigationControllerI fragmentNavigator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_main);
        this.dataLogin = new DataLogin();

        this.fragmentNavigator = new NavigationController(
                getSupportFragmentManager(),
                (transaction, dest) -> transaction.replace(R.id.login_container, dest),
                (transaction) -> transaction.setCustomAnimations(
                        R.anim.enter_from_right,
                        R.anim.exit_to_left,
                        R.anim.enter_from_right,
                        R.anim.exit_to_left
                )
        );
        this.fragmentNavigator
                .add(Constants.LOGIN_STATE.login, new LoginFragmentFactory(this.dataLogin))
                .add(Constants.LOGIN_STATE.register, new RegisterFragmentFactory(this.dataLogin))
                .add(Constants.LOGIN_STATE.registerSuccess, new RegisterSuccessFragmentFactory(this.dataLogin), false);

        attachFragmentStateObserver();

        this.dataLogin.setState(Constants.LOGIN_STATE.login);
    }

    private void attachFragmentStateObserver() {
        this.dataLogin.getState().observe(
                this,
                this::evaluateState
                );
    }

    private void evaluateState(String state) {
        if(state.equals(Constants.LOGIN_STATE.authenticated)) {
            this.dataLogin.setAuthenticated(true);
            Intent intent = new Intent(getApplicationContext(), SallefyAppActivity.class);
            Bundle b = new Bundle();
            b.putString(Constants.LOGIN_CONSTANTS.USER_LOGIN, this.dataLogin.getUserID());
            b.putString(Constants.LOGIN_CONSTANTS.USER_PASSWORD, this.dataLogin.getPassword());
            b.putString(Constants.LOGIN_CONSTANTS.TOKEN, this.dataLogin.getLoginToken());
            intent.putExtras(b);
            startActivity(intent);
            this.finish();
        }
        this.fragmentNavigator.navigate(state);
    }
}
