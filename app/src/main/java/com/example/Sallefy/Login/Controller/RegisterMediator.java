package com.example.Sallefy.Login.Controller;

import com.example.Sallefy.Utils.FeaturedMediatorI;

public interface RegisterMediator extends FeaturedMediatorI {
    void onRegistrationSuccess();
}
