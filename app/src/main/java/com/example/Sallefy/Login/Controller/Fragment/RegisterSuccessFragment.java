package com.example.Sallefy.Login.Controller.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.Sallefy.Login.Controller.RegisterSuccessController;
import com.example.Sallefy.Login.Controller.RegisterSuccessControllerI;
import com.example.Sallefy.Login.Model.DataLogin;
import com.example.Sallefy.Login.View.ViewInterfaces.RegisterSuccessFragViewI;
import com.example.Sallefy.Login.View.RegisterSuccessFragView;

public class RegisterSuccessFragment extends AbstractLoginFragment {
    private RegisterSuccessFragViewI view;
    private RegisterSuccessControllerI controller;

    public RegisterSuccessFragment(DataLogin dataLogin) {
        super(dataLogin);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = new RegisterSuccessFragView(inflater, container);
        this.controller = new RegisterSuccessController(this.dataLogin, this.view);

        return this.view.getRootView();
    }
}
