package com.example.Sallefy.Login.View;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.Sallefy.Login.View.ViewInterfaces.LoginActivityViewI;
import com.example.Sallefy.R;

public class LoginActivityView implements LoginActivityViewI {
    private View rootView;
    private FragmentManager fm;


    public LoginActivityView(LayoutInflater inflater,
                             ViewGroup container,
                             FragmentManager fm) {
        this.rootView = inflater.inflate(R.layout.login_main, container, false);
        this.fm = fm;
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }

    public void transition(Fragment dest) {
        FragmentTransaction trans = this.fm.beginTransaction();
        trans.setCustomAnimations(R.anim.enter_from_right,
                R.anim.exit_to_left,
                R.anim.enter_from_right,
                R.anim.exit_to_left
                );
        trans.replace(R.id.login_container, dest);
        trans.addToBackStack(null);
        trans.commit();
    }

    @Override
    public void transitionBack() {
        this.fm.popBackStack();
    }
}
