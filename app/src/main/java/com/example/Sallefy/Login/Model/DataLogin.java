package com.example.Sallefy.Login.Model;

import androidx.lifecycle.MutableLiveData;

public class DataLogin {

    private MutableLiveData<String> state;

    private boolean authenticated;
    private String userID;
    private String password;
    private String loginToken;

    private String passResetKey;


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassResetKey() {
        return passResetKey;
    }

    public void setPassResetKey(String passResetKey) {
        this.passResetKey = passResetKey;
    }

    public String getLoginToken() {
        return loginToken;
    }

    public void setLoginToken(String loginToken) {
        this.loginToken = loginToken;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public DataLogin() {
        state = new MutableLiveData<>();
    }

    public MutableLiveData<String> getState() {
        return this.state;
    }

    public void setState(String state) {
        this.state.setValue(state);
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }
}
