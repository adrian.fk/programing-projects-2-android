package com.example.Sallefy.Login.Controller.FragmentNavigation;

import androidx.fragment.app.FragmentTransaction;

public interface FragmentTransactionAnimationSpecifier {
    void specifyFragmentTransactionAnimation(FragmentTransaction transaction);
}
