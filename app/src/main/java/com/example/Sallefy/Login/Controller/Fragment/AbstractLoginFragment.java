package com.example.Sallefy.Login.Controller.Fragment;

import androidx.fragment.app.Fragment;

import com.example.Sallefy.Login.Model.DataLogin;

public abstract class AbstractLoginFragment extends Fragment {
    protected DataLogin dataLogin;

    public AbstractLoginFragment(DataLogin dataLogin) {
        this.dataLogin = dataLogin;
    }
}
