package com.example.Sallefy.Login.View.ViewInterfaces;

import com.example.Sallefy.Login.Controller.ListenerInterfaces.RegisterListenerI;
import com.example.Sallefy.Utils.ViewInterface;

public interface RegisterFragmentViewI extends ViewInterface {
    void attachSignUpButtonListener(RegisterListenerI listener);
    void attachBackButtonListener(RegisterListenerI listener);
    void displayPasswordMismatch();
    void displayUsernameExists();
    void displayNoConnection();
    String getUsername();
    String getEmail();
    String getPasswordOne();
    String getPasswordTwo();
    String getFirstNameForm();
    String getLastNameForm();
}
