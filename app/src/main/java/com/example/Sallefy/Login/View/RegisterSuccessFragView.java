package com.example.Sallefy.Login.View;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.Sallefy.Login.Controller.ListenerInterfaces.RegisterSuccessListenerI;
import com.example.Sallefy.Login.View.ViewInterfaces.RegisterSuccessFragViewI;
import com.example.Sallefy.R;

public class RegisterSuccessFragView implements RegisterSuccessFragViewI {
    private View rootView;

    private Button loginButton;

    public RegisterSuccessFragView(LayoutInflater inflater, ViewGroup container) {
        this.rootView = inflater.inflate(R.layout.login_fragment_register_success, container, false);
        findViewElements();
    }

    private void findViewElements() {
        this.loginButton = this.rootView.findViewById(R.id.register_success_buttonLogin);
    }

    @Override
    public void attachLoginButtonListener(RegisterSuccessListenerI listener) {
        this.loginButton.setOnClickListener(listener);
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }
}
