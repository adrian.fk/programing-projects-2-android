package com.example.Sallefy.Login.View;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.Sallefy.Login.Controller.ListenerInterfaces.RegisterListenerI;
import com.example.Sallefy.Login.View.ViewInterfaces.RegisterFragmentViewI;
import com.example.Sallefy.R;

public class RegisterFragmentView implements RegisterFragmentViewI, TextWatcher {
    private View rootView;

    private ImageButton backButton;
    private EditText firstNameForm;
    private EditText lastNameForm;
    private EditText username;
    private EditText emailForm;
    private EditText passFormTwo;
    private EditText passFormOne;
    private Button signUpButton;


    public RegisterFragmentView(LayoutInflater inflater, ViewGroup container) {
        this.rootView = inflater.inflate(R.layout.login_fragment_register,
                container, false);


        findViewElements();

        attachTextChangeListeners();

        processSendButtonStatus();
    }

    private void processSendButtonStatus() {
        if("".equals(firstNameForm.getText().toString())
                || "".equals(lastNameForm.getText().toString())
                || "".equals(username.getText().toString())
                || "".equals(emailForm.getText().toString())
                || "".equals(passFormOne.getText().toString())
                || "".equals(passFormTwo.getText().toString())) {
            signUpButton.setClickable(false);
            signUpButton.setAlpha(.2f);
            signUpButton.setBackgroundResource(R.drawable.rounded_button_untoggled);
        }
        else {
            signUpButton.setAlpha(0.9f);
            signUpButton.setBackgroundResource(R.drawable.rounded_button);
            signUpButton.setClickable(true);
        }
    }

    private void attachTextChangeListeners() {
        this.firstNameForm.addTextChangedListener(this);
        this.lastNameForm.addTextChangedListener(this);
        this.username.addTextChangedListener(this);
        this.emailForm.addTextChangedListener(this);
        this.passFormOne.addTextChangedListener(this);
        this.passFormTwo.addTextChangedListener(this);
    }

    private void findViewElements() {
        this.firstNameForm = this.rootView.findViewById(R.id.login_register_form_first_name);
        this.lastNameForm = this.rootView.findViewById(R.id.login_register_form_last_name);
        this.username = this.rootView.findViewById(R.id.login_register_form_username);
        this.backButton = this.rootView.findViewById(R.id.login_register_button_back);
        this.signUpButton = this.rootView.findViewById(R.id.login_register_button_signup);
        this.emailForm = this.rootView.findViewById(R.id.login_register_email_form);
        this.passFormOne = this.rootView.findViewById(R.id.login_register_form_password);
        this.passFormTwo = this.rootView.findViewById(R.id.login_register_form_password_two);
    }

    @Override
    public void attachSignUpButtonListener(RegisterListenerI listener) {
        this.signUpButton.setOnClickListener(listener);
    }

    @Override
    public void attachBackButtonListener(RegisterListenerI listener) {
        this.backButton.setOnClickListener(listener);
    }

    @Override
    public void displayPasswordMismatch() {
        this.passFormOne.setText("");
        this.passFormTwo.setText("");

        this.passFormOne.setBackgroundResource(R.drawable.rounded_edittext_red_border);
        this.passFormTwo.setBackgroundResource(R.drawable.rounded_edittext_red_border);

        Animation shake = AnimationUtils.loadAnimation(this.rootView.getContext(), R.anim.shake_edittext);
        this.passFormTwo.startAnimation(shake);
        this.passFormOne.startAnimation(shake);

        this.passFormTwo.setHint("Passwords mismatch (Min. 4)");
    }

    @Override
    public void displayUsernameExists() {
        Animation shake = AnimationUtils.loadAnimation(this.rootView.getContext(), R.anim.shake_edittext);
        this.username.startAnimation(shake);
        this.username.setText("");
        this.username.setBackgroundResource(R.drawable.rounded_edittext_red_border);
        this.username.setHint(R.string.login_register_username_exist);
    }

    @Override
    public void displayNoConnection() {
        this.passFormOne.setText("");
        this.passFormTwo.setText("");
        this.username.setText("");
        Animation shake = AnimationUtils.loadAnimation(this.rootView.getContext(), R.anim.shake_edittext);
        this.username.startAnimation(shake);
        this.username.setBackgroundResource(R.drawable.rounded_edittext_green_border);
        this.username.setHint(R.string.login_no_connection);

    }

    @Override
    public String getUsername() {
        return this.username.getText().toString();
    }

    @Override
    public String getEmail() {
        return this.emailForm.getText().toString();
    }

    @Override
    public String  getFirstNameForm() {
        return firstNameForm.getText().toString();
    }

    @Override
    public String getLastNameForm() {
        return lastNameForm.getText().toString();
    }

    @Override
    public String getPasswordOne() {
        return this.passFormOne.getText().toString();
    }

    @Override
    public String getPasswordTwo() {
        return this.passFormTwo.getText().toString();
    }

    @Override
    public View getRootView() {
        return rootView;
    }

    @Override
    public Bundle getViewState() {
        return null;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        resetError();
        processSendButtonStatus();
    }

    private void resetError() {
        this.passFormOne.setBackgroundResource(R.drawable.rounded_edittext);
        this.passFormTwo.setBackgroundResource(R.drawable.rounded_edittext);
        this.username.setBackgroundResource(R.drawable.rounded_edittext);
        this.emailForm.setBackgroundResource(R.drawable.rounded_edittext);
        this.passFormTwo.setHint(R.string.login_password_repeat);
        this.emailForm.setHint(R.string.login_forgot_password_email_form);
        this.username.setHint(R.string.login_username);
    }
}
