package com.example.Sallefy.Login.Controller.Fragment.Factories;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import com.example.Sallefy.Login.Controller.Fragment.RegisterSuccessFragment;
import com.example.Sallefy.Login.Controller.FragmentNavigation.FragmentFactoryInterface;
import com.example.Sallefy.Login.Model.DataLogin;

public class RegisterSuccessFragmentFactory implements FragmentFactoryInterface {
    private DataLogin m;

    public RegisterSuccessFragmentFactory(DataLogin m) {
        this.m = m;
    }

    @Override
    public Fragment create(Bundle args) {
        return new RegisterSuccessFragment(m);
    }
}
