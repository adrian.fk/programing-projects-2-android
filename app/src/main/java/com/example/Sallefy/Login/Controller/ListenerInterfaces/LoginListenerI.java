package com.example.Sallefy.Login.Controller.ListenerInterfaces;

import android.view.View;

public interface LoginListenerI extends View.OnClickListener {
    //Umbrella interface to define which kind of listeners that belongs to this fragment
}
