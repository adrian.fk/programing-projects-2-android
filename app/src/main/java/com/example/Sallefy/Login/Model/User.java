package com.example.Sallefy.Login.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {
    private boolean activated;
    ArrayList<Object> authorities = new ArrayList<>();
    private String createdBy;
    private String createdDate;
    private String email;
    private String firstName;
    private float followers;
    private float following;
    private float id;
    private String imageUrl;
    private String langKey;
    private String lastModifiedBy;
    private String lastModifiedDate;
    private String lastName;
    private String login;
    private float playlists;
    private float tracks;

    public User(
            boolean activated,
            ArrayList<Object> authorities,
            String createdBy,
            String createdDate,
            String email,
            String firstName,
            float followers,
            float following,
            float id,
            String imageUrl,
            String langKey,
            String lastModifiedBy,
            String lastModifiedDate,
            String lastName,
            String login,
            float playlists,
            float tracks) {
        this.activated = activated;
        this.authorities = authorities;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.email = email;
        this.firstName = firstName;
        this.followers = followers;
        this.following = following;
        this.id = id;
        this.imageUrl = imageUrl;
        this.langKey = langKey;
        this.lastModifiedBy = lastModifiedBy;
        this.lastModifiedDate = lastModifiedDate;
        this.lastName = lastName;
        this.login = login;
        this.playlists = playlists;
        this.tracks = tracks;
    }

    public User(String createdBy,
                        String createdDate,
                        String email,
                        String firstName,
                        float followers,
                        float following,
                        float id,
                        String imageUrl,
                        String langKey,
                        String lastName,
                        String login) {
        this.activated = activated;
        this.authorities = authorities;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.email = email;
        this.firstName = firstName;
        this.followers = followers;
        this.following = following;
        this.id = id;
        this.imageUrl = imageUrl;
        this.langKey = langKey;
        this.lastModifiedBy = lastModifiedBy;
        this.lastModifiedDate = lastModifiedDate;
        this.lastName = lastName;
        this.login = login;
        this.playlists = playlists;
        this.tracks = tracks;
    }

    // Getter Methods

    public boolean getActivated() {
        return activated;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public float getFollowers() {
        return followers;
    }

    public float getFollowing() {
        return following;
    }

    public float getId() {
        return id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getLangKey() {
        return langKey;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public String getLastName() {
        return lastName;
    }

    public String getLogin() {
        return login;
    }

    public float getPlaylists() {
        return playlists;
    }

    public float getTracks() {
        return tracks;
    }

    // Setter Methods

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setFollowers(float followers) {
        this.followers = followers;
    }

    public void setFollowing(float following) {
        this.following = following;
    }

    public void setId(float id) {
        this.id = id;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPlaylists(float playlists) {
        this.playlists = playlists;
    }

    public void setTracks(float tracks) {
        this.tracks = tracks;
    }
}
