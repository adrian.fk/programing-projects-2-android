package com.example.Sallefy.Cast;

import android.app.Activity;
import android.net.Uri;
import android.widget.SeekBar;

import com.example.Sallefy.SallefyUI.MusicPlayer.Model.DataMusicPlayerFactory;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaLoadRequestData;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.SessionManager;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.cast.framework.media.uicontroller.UIMediaController;
import com.google.android.gms.common.images.WebImage;

public class CastController implements CastProxy {
    private boolean isCasting;

    private SessionManager sessionManager;

    private CastProxyImpl musicPlayer = DataMusicPlayerFactory.create();

    private UIMediaController uiMediaController;

    private boolean castStreamEnded;




    public CastController() {}



    private final SessionManagerListener<CastSession> sessionManagerListener = new SessionManagerListener<CastSession>() {
        private long sessionProgress;

        @Override
        public void onSessionStarting(CastSession castSession) {

        }

        @Override
        public void onSessionStarted(CastSession castSession, String s) {
            musicPlayer.onApplicationConnected();

            if (musicPlayer.getCurrentlyPlaying() != null) {
                castMedia(
                        musicPlayer.getCurrentlyPlaying().getTitle(),
                        musicPlayer.getCurrentlyPlaying().getSrcUrl(),
                        musicPlayer.getCurrentlyPlaying().getThumbnailUrl(),
                        musicPlayer.getProgress(),
                        musicPlayer.getDuration(),
                        true
                );
            }
            attachRemoteMediaClientListener();
        }

        @Override
        public void onSessionStartFailed(CastSession castSession, int i) {

        }

        @Override
        public void onSessionEnding(CastSession castSession) {
            this.sessionProgress = castSession.getRemoteMediaClient().getApproximateStreamPosition();
        }

        @Override
        public void onSessionEnded(CastSession castSession, int i) {
            musicPlayer.onApplicationDisconnected(this.sessionProgress);
        }

        @Override
        public void onSessionResuming(CastSession castSession, String s) {}

        @Override
        public void onSessionResumed(CastSession castSession, boolean b) {
            this.onSessionStarted(castSession, castSession.getSessionId());
        }

        @Override
        public void onSessionResumeFailed(CastSession castSession, int i) {
        }

        @Override
        public void onSessionSuspended(CastSession castSession, int i) {
        }
    };

    @Override
    public void init(Activity ownerActivity) {
        CastContext castContext = CastContext.getSharedInstance(ownerActivity);
         this.sessionManager = castContext.getSessionManager();
         this.sessionManager.addSessionManagerListener(
                 this.sessionManagerListener, CastSession.class
         );
         this.uiMediaController = new UIMediaController(ownerActivity);
    }

    public void addSeekBar(SeekBar seekBar) {
        this.uiMediaController.bindSeekBar(seekBar);
    }

    @Override
    public void castMedia(String title, String srcUrl, String imgUrl, long position, long duration, boolean autoplay) {
        CastSession castSession = this.sessionManager.getCurrentCastSession();

        MediaMetadata mediaMetadata = new MediaMetadata(MediaMetadata.MEDIA_TYPE_MUSIC_TRACK);
        mediaMetadata.putString(MediaMetadata.KEY_TITLE, title);
        if (imgUrl != null)
        mediaMetadata.addImage(new WebImage(Uri.parse(imgUrl)));

        MediaInfo mediaInfo = new MediaInfo.Builder(srcUrl)
                .setStreamType(MediaInfo.STREAM_TYPE_BUFFERED)
                .setMetadata(mediaMetadata)
                .setStreamDuration(duration * 1000)
                .build();

        RemoteMediaClient remoteMediaClient = castSession.getRemoteMediaClient();

        remoteMediaClient.load(
                new MediaLoadRequestData.Builder()
                        .setMediaInfo(mediaInfo)
                        .setAutoplay(autoplay)
                        .setCurrentTime(position)
                        .build()
        );

        this.castStreamEnded = false;
        this.isCasting = true;
    }

    private void stopCasting() {
        this.isCasting = false;
    }

    public boolean isCasting() {
        return isCasting;
    }

    @Override
    public void togglePlayback(boolean toggle) {
        CastSession castSession = this.sessionManager.getCurrentCastSession();
        RemoteMediaClient remoteMediaClient = castSession.getRemoteMediaClient();

        if (toggle) {
            remoteMediaClient.play();
        }
        else {
            remoteMediaClient.pause();
        }
    }


    private void attachRemoteMediaClientListener() {
        RemoteMediaClient remoteMediaClient = this.sessionManager.getCurrentCastSession().getRemoteMediaClient();
        remoteMediaClient.addProgressListener((progress, duration) -> {
            //Careful adding things here.. this executes every 0,5 second of the current stream
            if(progress >= duration - 500 && !this.castStreamEnded) {
                this.castStreamEnded = true;
                this.musicPlayer.onRemotePlayerStreamEnded();
            }
        }, 500);
    }
}
