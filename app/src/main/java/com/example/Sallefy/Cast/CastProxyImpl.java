package com.example.Sallefy.Cast;

import com.example.Sallefy.SallefyUI.MusicPlayer.Model.Data;
import com.example.Sallefy.SallefyUI.MusicPlayer.Model.PlaybackLocation;

public interface CastProxyImpl {
    Data.Track getCurrentlyPlaying();

    int getDuration();

    int getProgress();

    PlaybackLocation getPlaybackLocation();

    boolean isPlaying();

    void setPlaying(Boolean playing);

    void onApplicationConnected();

    void onRemotePlayerStreamEnded();

    void onApplicationDisconnected(long position);
}
