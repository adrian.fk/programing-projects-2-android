package com.example.Sallefy.Cast;

public interface CastProxyFactory {
    CastProxy castProxy = new CastController();

    static CastProxy create() {
        return castProxy;
    }
}
