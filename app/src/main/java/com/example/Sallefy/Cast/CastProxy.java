package com.example.Sallefy.Cast;

import android.app.Activity;
import android.widget.SeekBar;

public interface CastProxy {
    void castMedia(String title, String srcUrl, String imgUrl, long position, long duration, boolean autoplay);

    /*****************
     * This function is mandatory to run from the scope of the main activity to link the
     * Casting Interface with the activity before using the rest of the interface.
     *
     * @param ownerActivity (mandatory)
     */

    void init(Activity ownerActivity);

    boolean isCasting();

    void togglePlayback(boolean toggle);

    void addSeekBar(SeekBar seekBar);
}
